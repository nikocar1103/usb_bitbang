extern int _sp, _etext, _sdata, _edata, _sbss, _ebss;
extern __attribute__((noreturn)) void main();
//__attribute__((interrupt))
extern void EXTI0_handler();
extern void systick_handler();

void reset()
{
    unsigned int *p_etext = &_etext;
    unsigned int *p_sdata = &_sdata;
    unsigned int *p_edata = &_edata;
    unsigned int *p_sbss = &_sbss;
    unsigned int *p_ebss = &_ebss;

    while(p_sdata < p_edata)
        *p_sdata++ = *p_etext++;

    while(p_sbss < p_ebss)
        *p_sbss++ = 0;

    main();
}

void nek_handler()
{
    __asm__("mov r0, #0");
    __asm__("mov r1, #0");
    __asm__("mov r2, #0");
    __asm__("mov r3, #0");
    __asm__("mov r4, #0");
    __asm__("mov r5, #0");
    __asm__("mov r6, #0");
    __asm__("mov r7, #0");
    __asm__("mov r8, #0");
    __asm__("mov r9, #0");
    __asm__("mov r10, #0");
    __asm__("mov r11, #0");
}

__attribute__((interrupt))
void default_handler()
{
    __asm__("bkpt");
    __asm__("loop: b loop");
}

__attribute__((interrupt))
void hardfault_handler()
{
    __asm__("bkpt");
    __asm__("hfloop: b hfloop");
}

__attribute__((section(".intvect")))
const unsigned vectab[] = 
{
    (unsigned) &_sp,                // &_sp,
    (unsigned) reset,               // reset,
    (unsigned) default_handler,     // NMI,
    (unsigned) hardfault_handler,   // hardFault,
    (unsigned) default_handler,     // memFault,
    (unsigned) default_handler,     // busFault,
    (unsigned) default_handler,     // usageFault,
    (unsigned) 0,                   // 0,
    (unsigned) 0,                   // 0,
    (unsigned) 0,                   // 0,
    (unsigned) 0,                   // 0,
    (unsigned) default_handler,     // SVCall,
    (unsigned) 0,                   // 0,
    (unsigned) 0,                   // 0,
    (unsigned) default_handler,     // PendSV,
    (unsigned) systick_handler,     // SysTick,
    (unsigned) default_handler,     // WWDG,
    (unsigned) default_handler,     // PVD,
    (unsigned) default_handler,     // TAMPER_STAMP,
    (unsigned) default_handler,     // RTC_WKUP,
    (unsigned) default_handler,     // FLASH,
    (unsigned) default_handler,     // RCC,
    (unsigned) EXTI0_handler,       // EXTI0,
    (unsigned) default_handler,     // EXTI1,
    (unsigned) default_handler,     // EXTI2_TS,
    (unsigned) default_handler,     // EXTI3,
    (unsigned) default_handler,     // EXTI4,
    (unsigned) default_handler,     // DMA1_Channel1,
    (unsigned) default_handler,     // DMA1_Channel2,
    (unsigned) default_handler,     // DMA1_Channel3,
    (unsigned) default_handler,     // DMA1_Channel4,
    (unsigned) default_handler,     // DMA1_Channel5,
    (unsigned) default_handler,     // DMA1_Channel6,
    (unsigned) default_handler,     // DMA1_Channel7,
    (unsigned) default_handler,     // ADC1_2,
    (unsigned) default_handler,     // CAN_TX,
    (unsigned) default_handler,     // CAN_RX0,
    (unsigned) default_handler,     // CAN_RX1,
    (unsigned) default_handler,     // CAN_SCE,
    (unsigned) default_handler,     // EXTI9_5,
    (unsigned) default_handler,     // TIM1_BRK,
    (unsigned) default_handler,     // TIM1_UP,
    (unsigned) default_handler,     // TIM1_TRG_COM,
    (unsigned) default_handler,     // TIM1_CC,
    (unsigned) default_handler,     // TIM2,
    (unsigned) default_handler,     // TIM3,
    (unsigned) 0,                   // 0,
    (unsigned) default_handler,     // I2C1_EV,
    (unsigned) default_handler,     // I2C1_ER,
    (unsigned) 0,                   // 0,
    (unsigned) 0,                   // 0,
    (unsigned) default_handler,     // SPI1,
    (unsigned) 0,                   // 0,
    (unsigned) default_handler,     // USART1,
    (unsigned) default_handler,     // USART2,
    (unsigned) default_handler,     // USART3,
    (unsigned) default_handler,     // EXTI15_10,
    (unsigned) default_handler,     // RTC_Alarm,
    (unsigned) 0,                   // 0,
    (unsigned) 0,                   // 0,
    (unsigned) 0,                   // 0,
    (unsigned) 0,                   // 0,
    (unsigned) 0,                   // 0,
    (unsigned) 0,                   // 0,
    (unsigned) 0,                   // 0,
    (unsigned) 0,                   // 0,
    (unsigned) 0,                   // 0,
    (unsigned) 0,                   // 0,
    (unsigned) 0,                   // 0,
    (unsigned) 0,                   // 0,
    (unsigned) default_handler,     // TIM6_DAC1,
    (unsigned) default_handler,     // TIM7_DAC2,
    (unsigned) 0,                   // 0,
    (unsigned) 0,                   // 0,
    (unsigned) 0,                   // 0,
    (unsigned) 0,                   // 0,
    (unsigned) 0,                   // 0,
    (unsigned) 0,                   // 0,
    (unsigned) 0,                   // 0,
    (unsigned) 0,                   // 0,
    (unsigned) default_handler,     // COMP2,
    (unsigned) default_handler,     // COMP4_6
    (unsigned) 0,                   // 0,
    (unsigned) 0,                   // 0,
    (unsigned) 0,                   // 0,
    (unsigned) 0,                   // 0,
    (unsigned) 0,                   // 0,
    (unsigned) 0,                   // 0,
    (unsigned) 0,                   // 0,
    (unsigned) 0,                   // 0,
    (unsigned) 0,                   // 0,
    (unsigned) 0,                   // 0,
    (unsigned) 0,                   // 0,
    (unsigned) 0,                   // 0,
    (unsigned) 0,                   // 0,
    (unsigned) 0,                   // 0,
    (unsigned) 0,                   // 0,
    (unsigned) default_handler      // FPU
};
