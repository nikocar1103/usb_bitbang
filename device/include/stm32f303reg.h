#ifndef __STM32F303__
	#define __STM32F303__

/* DEVICE INFO */

// name : CM4
// revision : r1p0
// endian : little
// mpu_present : False
// fpu_present : False
// nvic_priority_bits : 3
// vendor_systick : False


/* REGISTERS */

// GPIOA
struct GPIOA_MODER_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int MODER0 : 2;
			unsigned int MODER1 : 2;
			unsigned int MODER2 : 2;
			unsigned int MODER3 : 2;
			unsigned int MODER4 : 2;
			unsigned int MODER5 : 2;
			unsigned int MODER6 : 2;
			unsigned int MODER7 : 2;
			unsigned int MODER8 : 2;
			unsigned int MODER9 : 2;
			unsigned int MODER10 : 2;
			unsigned int MODER11 : 2;
			unsigned int MODER12 : 2;
			unsigned int MODER13 : 2;
			unsigned int MODER14 : 2;
			unsigned int MODER15 : 2;
		} __attribute((__packed__));
	};
};
#define GPIOA_MODER (*((volatile struct GPIOA_MODER_s*) 0x48000000))

struct GPIOA_OTYPER_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int OT0 : 1;
			unsigned int OT1 : 1;
			unsigned int OT2 : 1;
			unsigned int OT3 : 1;
			unsigned int OT4 : 1;
			unsigned int OT5 : 1;
			unsigned int OT6 : 1;
			unsigned int OT7 : 1;
			unsigned int OT8 : 1;
			unsigned int OT9 : 1;
			unsigned int OT10 : 1;
			unsigned int OT11 : 1;
			unsigned int OT12 : 1;
			unsigned int OT13 : 1;
			unsigned int OT14 : 1;
			unsigned int OT15 : 1;
		} __attribute((__packed__));
	};
};
#define GPIOA_OTYPER (*((volatile struct GPIOA_OTYPER_s*) 0x48000004))

struct GPIOA_OSPEEDR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int OSPEEDR0 : 2;
			unsigned int OSPEEDR1 : 2;
			unsigned int OSPEEDR2 : 2;
			unsigned int OSPEEDR3 : 2;
			unsigned int OSPEEDR4 : 2;
			unsigned int OSPEEDR5 : 2;
			unsigned int OSPEEDR6 : 2;
			unsigned int OSPEEDR7 : 2;
			unsigned int OSPEEDR8 : 2;
			unsigned int OSPEEDR9 : 2;
			unsigned int OSPEEDR10 : 2;
			unsigned int OSPEEDR11 : 2;
			unsigned int OSPEEDR12 : 2;
			unsigned int OSPEEDR13 : 2;
			unsigned int OSPEEDR14 : 2;
			unsigned int OSPEEDR15 : 2;
		} __attribute((__packed__));
	};
};
#define GPIOA_OSPEEDR (*((volatile struct GPIOA_OSPEEDR_s*) 0x48000008))

struct GPIOA_PUPDR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int PUPDR0 : 2;
			unsigned int PUPDR1 : 2;
			unsigned int PUPDR2 : 2;
			unsigned int PUPDR3 : 2;
			unsigned int PUPDR4 : 2;
			unsigned int PUPDR5 : 2;
			unsigned int PUPDR6 : 2;
			unsigned int PUPDR7 : 2;
			unsigned int PUPDR8 : 2;
			unsigned int PUPDR9 : 2;
			unsigned int PUPDR10 : 2;
			unsigned int PUPDR11 : 2;
			unsigned int PUPDR12 : 2;
			unsigned int PUPDR13 : 2;
			unsigned int PUPDR14 : 2;
			unsigned int PUPDR15 : 2;
		} __attribute((__packed__));
	};
};
#define GPIOA_PUPDR (*((volatile struct GPIOA_PUPDR_s*) 0x4800000c))

struct GPIOA_IDR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int IDR0 : 1;
			unsigned int IDR1 : 1;
			unsigned int IDR2 : 1;
			unsigned int IDR3 : 1;
			unsigned int IDR4 : 1;
			unsigned int IDR5 : 1;
			unsigned int IDR6 : 1;
			unsigned int IDR7 : 1;
			unsigned int IDR8 : 1;
			unsigned int IDR9 : 1;
			unsigned int IDR10 : 1;
			unsigned int IDR11 : 1;
			unsigned int IDR12 : 1;
			unsigned int IDR13 : 1;
			unsigned int IDR14 : 1;
			unsigned int IDR15 : 1;
		} __attribute((__packed__));
	};
};
#define GPIOA_IDR (*((volatile struct GPIOA_IDR_s*) 0x48000010))

struct GPIOA_ODR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int ODR0 : 1;
			unsigned int ODR1 : 1;
			unsigned int ODR2 : 1;
			unsigned int ODR3 : 1;
			unsigned int ODR4 : 1;
			unsigned int ODR5 : 1;
			unsigned int ODR6 : 1;
			unsigned int ODR7 : 1;
			unsigned int ODR8 : 1;
			unsigned int ODR9 : 1;
			unsigned int ODR10 : 1;
			unsigned int ODR11 : 1;
			unsigned int ODR12 : 1;
			unsigned int ODR13 : 1;
			unsigned int ODR14 : 1;
			unsigned int ODR15 : 1;
		} __attribute((__packed__));
	};
};
#define GPIOA_ODR (*((volatile struct GPIOA_ODR_s*) 0x48000014))

struct GPIOA_BSRR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int BS0 : 1;
			unsigned int BS1 : 1;
			unsigned int BS2 : 1;
			unsigned int BS3 : 1;
			unsigned int BS4 : 1;
			unsigned int BS5 : 1;
			unsigned int BS6 : 1;
			unsigned int BS7 : 1;
			unsigned int BS8 : 1;
			unsigned int BS9 : 1;
			unsigned int BS10 : 1;
			unsigned int BS11 : 1;
			unsigned int BS12 : 1;
			unsigned int BS13 : 1;
			unsigned int BS14 : 1;
			unsigned int BS15 : 1;
			unsigned int BR0 : 1;
			unsigned int BR1 : 1;
			unsigned int BR2 : 1;
			unsigned int BR3 : 1;
			unsigned int BR4 : 1;
			unsigned int BR5 : 1;
			unsigned int BR6 : 1;
			unsigned int BR7 : 1;
			unsigned int BR8 : 1;
			unsigned int BR9 : 1;
			unsigned int BR10 : 1;
			unsigned int BR11 : 1;
			unsigned int BR12 : 1;
			unsigned int BR13 : 1;
			unsigned int BR14 : 1;
			unsigned int BR15 : 1;
		} __attribute((__packed__));
	};
};
#define GPIOA_BSRR (*((volatile struct GPIOA_BSRR_s*) 0x48000018))

struct GPIOA_LCKR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int LCK0 : 1;
			unsigned int LCK1 : 1;
			unsigned int LCK2 : 1;
			unsigned int LCK3 : 1;
			unsigned int LCK4 : 1;
			unsigned int LCK5 : 1;
			unsigned int LCK6 : 1;
			unsigned int LCK7 : 1;
			unsigned int LCK8 : 1;
			unsigned int LCK9 : 1;
			unsigned int LCK10 : 1;
			unsigned int LCK11 : 1;
			unsigned int LCK12 : 1;
			unsigned int LCK13 : 1;
			unsigned int LCK14 : 1;
			unsigned int LCK15 : 1;
			unsigned int LCKK : 1;
		} __attribute((__packed__));
	};
};
#define GPIOA_LCKR (*((volatile struct GPIOA_LCKR_s*) 0x4800001c))

struct GPIOA_AFRL_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int AFRL0 : 4;
			unsigned int AFRL1 : 4;
			unsigned int AFRL2 : 4;
			unsigned int AFRL3 : 4;
			unsigned int AFRL4 : 4;
			unsigned int AFRL5 : 4;
			unsigned int AFRL6 : 4;
			unsigned int AFRL7 : 4;
		} __attribute((__packed__));
	};
};
#define GPIOA_AFRL (*((volatile struct GPIOA_AFRL_s*) 0x48000020))

struct GPIOA_AFRH_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int AFRH8 : 4;
			unsigned int AFRH9 : 4;
			unsigned int AFRH10 : 4;
			unsigned int AFRH11 : 4;
			unsigned int AFRH12 : 4;
			unsigned int AFRH13 : 4;
			unsigned int AFRH14 : 4;
			unsigned int AFRH15 : 4;
		} __attribute((__packed__));
	};
};
#define GPIOA_AFRH (*((volatile struct GPIOA_AFRH_s*) 0x48000024))

struct GPIOA_BRR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int BR0 : 1;
			unsigned int BR1 : 1;
			unsigned int BR2 : 1;
			unsigned int BR3 : 1;
			unsigned int BR4 : 1;
			unsigned int BR5 : 1;
			unsigned int BR6 : 1;
			unsigned int BR7 : 1;
			unsigned int BR8 : 1;
			unsigned int BR9 : 1;
			unsigned int BR10 : 1;
			unsigned int BR11 : 1;
			unsigned int BR12 : 1;
			unsigned int BR13 : 1;
			unsigned int BR14 : 1;
			unsigned int BR15 : 1;
		} __attribute((__packed__));
	};
};
#define GPIOA_BRR (*((volatile struct GPIOA_BRR_s*) 0x48000028))

// GPIOB
struct GPIOB_MODER_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int MODER0 : 2;
			unsigned int MODER1 : 2;
			unsigned int MODER2 : 2;
			unsigned int MODER3 : 2;
			unsigned int MODER4 : 2;
			unsigned int MODER5 : 2;
			unsigned int MODER6 : 2;
			unsigned int MODER7 : 2;
			unsigned int MODER8 : 2;
			unsigned int MODER9 : 2;
			unsigned int MODER10 : 2;
			unsigned int MODER11 : 2;
			unsigned int MODER12 : 2;
			unsigned int MODER13 : 2;
			unsigned int MODER14 : 2;
			unsigned int MODER15 : 2;
		} __attribute((__packed__));
	};
};
#define GPIOB_MODER (*((volatile struct GPIOB_MODER_s*) 0x48000400))

struct GPIOB_OTYPER_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int OT0 : 1;
			unsigned int OT1 : 1;
			unsigned int OT2 : 1;
			unsigned int OT3 : 1;
			unsigned int OT4 : 1;
			unsigned int OT5 : 1;
			unsigned int OT6 : 1;
			unsigned int OT7 : 1;
			unsigned int OT8 : 1;
			unsigned int OT9 : 1;
			unsigned int OT10 : 1;
			unsigned int OT11 : 1;
			unsigned int OT12 : 1;
			unsigned int OT13 : 1;
			unsigned int OT14 : 1;
			unsigned int OT15 : 1;
		} __attribute((__packed__));
	};
};
#define GPIOB_OTYPER (*((volatile struct GPIOB_OTYPER_s*) 0x48000404))

struct GPIOB_OSPEEDR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int OSPEEDR0 : 2;
			unsigned int OSPEEDR1 : 2;
			unsigned int OSPEEDR2 : 2;
			unsigned int OSPEEDR3 : 2;
			unsigned int OSPEEDR4 : 2;
			unsigned int OSPEEDR5 : 2;
			unsigned int OSPEEDR6 : 2;
			unsigned int OSPEEDR7 : 2;
			unsigned int OSPEEDR8 : 2;
			unsigned int OSPEEDR9 : 2;
			unsigned int OSPEEDR10 : 2;
			unsigned int OSPEEDR11 : 2;
			unsigned int OSPEEDR12 : 2;
			unsigned int OSPEEDR13 : 2;
			unsigned int OSPEEDR14 : 2;
			unsigned int OSPEEDR15 : 2;
		} __attribute((__packed__));
	};
};
#define GPIOB_OSPEEDR (*((volatile struct GPIOB_OSPEEDR_s*) 0x48000408))

struct GPIOB_PUPDR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int PUPDR0 : 2;
			unsigned int PUPDR1 : 2;
			unsigned int PUPDR2 : 2;
			unsigned int PUPDR3 : 2;
			unsigned int PUPDR4 : 2;
			unsigned int PUPDR5 : 2;
			unsigned int PUPDR6 : 2;
			unsigned int PUPDR7 : 2;
			unsigned int PUPDR8 : 2;
			unsigned int PUPDR9 : 2;
			unsigned int PUPDR10 : 2;
			unsigned int PUPDR11 : 2;
			unsigned int PUPDR12 : 2;
			unsigned int PUPDR13 : 2;
			unsigned int PUPDR14 : 2;
			unsigned int PUPDR15 : 2;
		} __attribute((__packed__));
	};
};
#define GPIOB_PUPDR (*((volatile struct GPIOB_PUPDR_s*) 0x4800040c))

struct GPIOB_IDR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int IDR0 : 1;
			unsigned int IDR1 : 1;
			unsigned int IDR2 : 1;
			unsigned int IDR3 : 1;
			unsigned int IDR4 : 1;
			unsigned int IDR5 : 1;
			unsigned int IDR6 : 1;
			unsigned int IDR7 : 1;
			unsigned int IDR8 : 1;
			unsigned int IDR9 : 1;
			unsigned int IDR10 : 1;
			unsigned int IDR11 : 1;
			unsigned int IDR12 : 1;
			unsigned int IDR13 : 1;
			unsigned int IDR14 : 1;
			unsigned int IDR15 : 1;
		} __attribute((__packed__));
	};
};
#define GPIOB_IDR (*((volatile struct GPIOB_IDR_s*) 0x48000410))

struct GPIOB_ODR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int ODR0 : 1;
			unsigned int ODR1 : 1;
			unsigned int ODR2 : 1;
			unsigned int ODR3 : 1;
			unsigned int ODR4 : 1;
			unsigned int ODR5 : 1;
			unsigned int ODR6 : 1;
			unsigned int ODR7 : 1;
			unsigned int ODR8 : 1;
			unsigned int ODR9 : 1;
			unsigned int ODR10 : 1;
			unsigned int ODR11 : 1;
			unsigned int ODR12 : 1;
			unsigned int ODR13 : 1;
			unsigned int ODR14 : 1;
			unsigned int ODR15 : 1;
		} __attribute((__packed__));
	};
};
#define GPIOB_ODR (*((volatile struct GPIOB_ODR_s*) 0x48000414))

struct GPIOB_BSRR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int BS0 : 1;
			unsigned int BS1 : 1;
			unsigned int BS2 : 1;
			unsigned int BS3 : 1;
			unsigned int BS4 : 1;
			unsigned int BS5 : 1;
			unsigned int BS6 : 1;
			unsigned int BS7 : 1;
			unsigned int BS8 : 1;
			unsigned int BS9 : 1;
			unsigned int BS10 : 1;
			unsigned int BS11 : 1;
			unsigned int BS12 : 1;
			unsigned int BS13 : 1;
			unsigned int BS14 : 1;
			unsigned int BS15 : 1;
			unsigned int BR0 : 1;
			unsigned int BR1 : 1;
			unsigned int BR2 : 1;
			unsigned int BR3 : 1;
			unsigned int BR4 : 1;
			unsigned int BR5 : 1;
			unsigned int BR6 : 1;
			unsigned int BR7 : 1;
			unsigned int BR8 : 1;
			unsigned int BR9 : 1;
			unsigned int BR10 : 1;
			unsigned int BR11 : 1;
			unsigned int BR12 : 1;
			unsigned int BR13 : 1;
			unsigned int BR14 : 1;
			unsigned int BR15 : 1;
		} __attribute((__packed__));
	};
};
#define GPIOB_BSRR (*((volatile struct GPIOB_BSRR_s*) 0x48000418))

struct GPIOB_LCKR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int LCK0 : 1;
			unsigned int LCK1 : 1;
			unsigned int LCK2 : 1;
			unsigned int LCK3 : 1;
			unsigned int LCK4 : 1;
			unsigned int LCK5 : 1;
			unsigned int LCK6 : 1;
			unsigned int LCK7 : 1;
			unsigned int LCK8 : 1;
			unsigned int LCK9 : 1;
			unsigned int LCK10 : 1;
			unsigned int LCK11 : 1;
			unsigned int LCK12 : 1;
			unsigned int LCK13 : 1;
			unsigned int LCK14 : 1;
			unsigned int LCK15 : 1;
			unsigned int LCKK : 1;
		} __attribute((__packed__));
	};
};
#define GPIOB_LCKR (*((volatile struct GPIOB_LCKR_s*) 0x4800041c))

struct GPIOB_AFRL_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int AFRL0 : 4;
			unsigned int AFRL1 : 4;
			unsigned int AFRL2 : 4;
			unsigned int AFRL3 : 4;
			unsigned int AFRL4 : 4;
			unsigned int AFRL5 : 4;
			unsigned int AFRL6 : 4;
			unsigned int AFRL7 : 4;
		} __attribute((__packed__));
	};
};
#define GPIOB_AFRL (*((volatile struct GPIOB_AFRL_s*) 0x48000420))

struct GPIOB_AFRH_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int AFRH8 : 4;
			unsigned int AFRH9 : 4;
			unsigned int AFRH10 : 4;
			unsigned int AFRH11 : 4;
			unsigned int AFRH12 : 4;
			unsigned int AFRH13 : 4;
			unsigned int AFRH14 : 4;
			unsigned int AFRH15 : 4;
		} __attribute((__packed__));
	};
};
#define GPIOB_AFRH (*((volatile struct GPIOB_AFRH_s*) 0x48000424))

struct GPIOB_BRR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int BR0 : 1;
			unsigned int BR1 : 1;
			unsigned int BR2 : 1;
			unsigned int BR3 : 1;
			unsigned int BR4 : 1;
			unsigned int BR5 : 1;
			unsigned int BR6 : 1;
			unsigned int BR7 : 1;
			unsigned int BR8 : 1;
			unsigned int BR9 : 1;
			unsigned int BR10 : 1;
			unsigned int BR11 : 1;
			unsigned int BR12 : 1;
			unsigned int BR13 : 1;
			unsigned int BR14 : 1;
			unsigned int BR15 : 1;
		} __attribute((__packed__));
	};
};
#define GPIOB_BRR (*((volatile struct GPIOB_BRR_s*) 0x48000428))

// GPIOC
struct GPIOC_MODER_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int MODER0 : 2;
			unsigned int MODER1 : 2;
			unsigned int MODER2 : 2;
			unsigned int MODER3 : 2;
			unsigned int MODER4 : 2;
			unsigned int MODER5 : 2;
			unsigned int MODER6 : 2;
			unsigned int MODER7 : 2;
			unsigned int MODER8 : 2;
			unsigned int MODER9 : 2;
			unsigned int MODER10 : 2;
			unsigned int MODER11 : 2;
			unsigned int MODER12 : 2;
			unsigned int MODER13 : 2;
			unsigned int MODER14 : 2;
			unsigned int MODER15 : 2;
		} __attribute((__packed__));
	};
};
#define GPIOC_MODER (*((volatile struct GPIOC_MODER_s*) 0x48000800))

struct GPIOC_OTYPER_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int OT0 : 1;
			unsigned int OT1 : 1;
			unsigned int OT2 : 1;
			unsigned int OT3 : 1;
			unsigned int OT4 : 1;
			unsigned int OT5 : 1;
			unsigned int OT6 : 1;
			unsigned int OT7 : 1;
			unsigned int OT8 : 1;
			unsigned int OT9 : 1;
			unsigned int OT10 : 1;
			unsigned int OT11 : 1;
			unsigned int OT12 : 1;
			unsigned int OT13 : 1;
			unsigned int OT14 : 1;
			unsigned int OT15 : 1;
		} __attribute((__packed__));
	};
};
#define GPIOC_OTYPER (*((volatile struct GPIOC_OTYPER_s*) 0x48000804))

struct GPIOC_OSPEEDR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int OSPEEDR0 : 2;
			unsigned int OSPEEDR1 : 2;
			unsigned int OSPEEDR2 : 2;
			unsigned int OSPEEDR3 : 2;
			unsigned int OSPEEDR4 : 2;
			unsigned int OSPEEDR5 : 2;
			unsigned int OSPEEDR6 : 2;
			unsigned int OSPEEDR7 : 2;
			unsigned int OSPEEDR8 : 2;
			unsigned int OSPEEDR9 : 2;
			unsigned int OSPEEDR10 : 2;
			unsigned int OSPEEDR11 : 2;
			unsigned int OSPEEDR12 : 2;
			unsigned int OSPEEDR13 : 2;
			unsigned int OSPEEDR14 : 2;
			unsigned int OSPEEDR15 : 2;
		} __attribute((__packed__));
	};
};
#define GPIOC_OSPEEDR (*((volatile struct GPIOC_OSPEEDR_s*) 0x48000808))

struct GPIOC_PUPDR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int PUPDR0 : 2;
			unsigned int PUPDR1 : 2;
			unsigned int PUPDR2 : 2;
			unsigned int PUPDR3 : 2;
			unsigned int PUPDR4 : 2;
			unsigned int PUPDR5 : 2;
			unsigned int PUPDR6 : 2;
			unsigned int PUPDR7 : 2;
			unsigned int PUPDR8 : 2;
			unsigned int PUPDR9 : 2;
			unsigned int PUPDR10 : 2;
			unsigned int PUPDR11 : 2;
			unsigned int PUPDR12 : 2;
			unsigned int PUPDR13 : 2;
			unsigned int PUPDR14 : 2;
			unsigned int PUPDR15 : 2;
		} __attribute((__packed__));
	};
};
#define GPIOC_PUPDR (*((volatile struct GPIOC_PUPDR_s*) 0x4800080c))

struct GPIOC_IDR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int IDR0 : 1;
			unsigned int IDR1 : 1;
			unsigned int IDR2 : 1;
			unsigned int IDR3 : 1;
			unsigned int IDR4 : 1;
			unsigned int IDR5 : 1;
			unsigned int IDR6 : 1;
			unsigned int IDR7 : 1;
			unsigned int IDR8 : 1;
			unsigned int IDR9 : 1;
			unsigned int IDR10 : 1;
			unsigned int IDR11 : 1;
			unsigned int IDR12 : 1;
			unsigned int IDR13 : 1;
			unsigned int IDR14 : 1;
			unsigned int IDR15 : 1;
		} __attribute((__packed__));
	};
};
#define GPIOC_IDR (*((volatile struct GPIOC_IDR_s*) 0x48000810))

struct GPIOC_ODR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int ODR0 : 1;
			unsigned int ODR1 : 1;
			unsigned int ODR2 : 1;
			unsigned int ODR3 : 1;
			unsigned int ODR4 : 1;
			unsigned int ODR5 : 1;
			unsigned int ODR6 : 1;
			unsigned int ODR7 : 1;
			unsigned int ODR8 : 1;
			unsigned int ODR9 : 1;
			unsigned int ODR10 : 1;
			unsigned int ODR11 : 1;
			unsigned int ODR12 : 1;
			unsigned int ODR13 : 1;
			unsigned int ODR14 : 1;
			unsigned int ODR15 : 1;
		} __attribute((__packed__));
	};
};
#define GPIOC_ODR (*((volatile struct GPIOC_ODR_s*) 0x48000814))

struct GPIOC_BSRR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int BS0 : 1;
			unsigned int BS1 : 1;
			unsigned int BS2 : 1;
			unsigned int BS3 : 1;
			unsigned int BS4 : 1;
			unsigned int BS5 : 1;
			unsigned int BS6 : 1;
			unsigned int BS7 : 1;
			unsigned int BS8 : 1;
			unsigned int BS9 : 1;
			unsigned int BS10 : 1;
			unsigned int BS11 : 1;
			unsigned int BS12 : 1;
			unsigned int BS13 : 1;
			unsigned int BS14 : 1;
			unsigned int BS15 : 1;
			unsigned int BR0 : 1;
			unsigned int BR1 : 1;
			unsigned int BR2 : 1;
			unsigned int BR3 : 1;
			unsigned int BR4 : 1;
			unsigned int BR5 : 1;
			unsigned int BR6 : 1;
			unsigned int BR7 : 1;
			unsigned int BR8 : 1;
			unsigned int BR9 : 1;
			unsigned int BR10 : 1;
			unsigned int BR11 : 1;
			unsigned int BR12 : 1;
			unsigned int BR13 : 1;
			unsigned int BR14 : 1;
			unsigned int BR15 : 1;
		} __attribute((__packed__));
	};
};
#define GPIOC_BSRR (*((volatile struct GPIOC_BSRR_s*) 0x48000818))

struct GPIOC_LCKR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int LCK0 : 1;
			unsigned int LCK1 : 1;
			unsigned int LCK2 : 1;
			unsigned int LCK3 : 1;
			unsigned int LCK4 : 1;
			unsigned int LCK5 : 1;
			unsigned int LCK6 : 1;
			unsigned int LCK7 : 1;
			unsigned int LCK8 : 1;
			unsigned int LCK9 : 1;
			unsigned int LCK10 : 1;
			unsigned int LCK11 : 1;
			unsigned int LCK12 : 1;
			unsigned int LCK13 : 1;
			unsigned int LCK14 : 1;
			unsigned int LCK15 : 1;
			unsigned int LCKK : 1;
		} __attribute((__packed__));
	};
};
#define GPIOC_LCKR (*((volatile struct GPIOC_LCKR_s*) 0x4800081c))

struct GPIOC_AFRL_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int AFRL0 : 4;
			unsigned int AFRL1 : 4;
			unsigned int AFRL2 : 4;
			unsigned int AFRL3 : 4;
			unsigned int AFRL4 : 4;
			unsigned int AFRL5 : 4;
			unsigned int AFRL6 : 4;
			unsigned int AFRL7 : 4;
		} __attribute((__packed__));
	};
};
#define GPIOC_AFRL (*((volatile struct GPIOC_AFRL_s*) 0x48000820))

struct GPIOC_AFRH_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int AFRH8 : 4;
			unsigned int AFRH9 : 4;
			unsigned int AFRH10 : 4;
			unsigned int AFRH11 : 4;
			unsigned int AFRH12 : 4;
			unsigned int AFRH13 : 4;
			unsigned int AFRH14 : 4;
			unsigned int AFRH15 : 4;
		} __attribute((__packed__));
	};
};
#define GPIOC_AFRH (*((volatile struct GPIOC_AFRH_s*) 0x48000824))

struct GPIOC_BRR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int BR0 : 1;
			unsigned int BR1 : 1;
			unsigned int BR2 : 1;
			unsigned int BR3 : 1;
			unsigned int BR4 : 1;
			unsigned int BR5 : 1;
			unsigned int BR6 : 1;
			unsigned int BR7 : 1;
			unsigned int BR8 : 1;
			unsigned int BR9 : 1;
			unsigned int BR10 : 1;
			unsigned int BR11 : 1;
			unsigned int BR12 : 1;
			unsigned int BR13 : 1;
			unsigned int BR14 : 1;
			unsigned int BR15 : 1;
		} __attribute((__packed__));
	};
};
#define GPIOC_BRR (*((volatile struct GPIOC_BRR_s*) 0x48000828))

// GPIOD
struct GPIOD_MODER_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int MODER0 : 2;
			unsigned int MODER1 : 2;
			unsigned int MODER2 : 2;
			unsigned int MODER3 : 2;
			unsigned int MODER4 : 2;
			unsigned int MODER5 : 2;
			unsigned int MODER6 : 2;
			unsigned int MODER7 : 2;
			unsigned int MODER8 : 2;
			unsigned int MODER9 : 2;
			unsigned int MODER10 : 2;
			unsigned int MODER11 : 2;
			unsigned int MODER12 : 2;
			unsigned int MODER13 : 2;
			unsigned int MODER14 : 2;
			unsigned int MODER15 : 2;
		} __attribute((__packed__));
	};
};
#define GPIOD_MODER (*((volatile struct GPIOD_MODER_s*) 0x48000c00))

struct GPIOD_OTYPER_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int OT0 : 1;
			unsigned int OT1 : 1;
			unsigned int OT2 : 1;
			unsigned int OT3 : 1;
			unsigned int OT4 : 1;
			unsigned int OT5 : 1;
			unsigned int OT6 : 1;
			unsigned int OT7 : 1;
			unsigned int OT8 : 1;
			unsigned int OT9 : 1;
			unsigned int OT10 : 1;
			unsigned int OT11 : 1;
			unsigned int OT12 : 1;
			unsigned int OT13 : 1;
			unsigned int OT14 : 1;
			unsigned int OT15 : 1;
		} __attribute((__packed__));
	};
};
#define GPIOD_OTYPER (*((volatile struct GPIOD_OTYPER_s*) 0x48000c04))

struct GPIOD_OSPEEDR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int OSPEEDR0 : 2;
			unsigned int OSPEEDR1 : 2;
			unsigned int OSPEEDR2 : 2;
			unsigned int OSPEEDR3 : 2;
			unsigned int OSPEEDR4 : 2;
			unsigned int OSPEEDR5 : 2;
			unsigned int OSPEEDR6 : 2;
			unsigned int OSPEEDR7 : 2;
			unsigned int OSPEEDR8 : 2;
			unsigned int OSPEEDR9 : 2;
			unsigned int OSPEEDR10 : 2;
			unsigned int OSPEEDR11 : 2;
			unsigned int OSPEEDR12 : 2;
			unsigned int OSPEEDR13 : 2;
			unsigned int OSPEEDR14 : 2;
			unsigned int OSPEEDR15 : 2;
		} __attribute((__packed__));
	};
};
#define GPIOD_OSPEEDR (*((volatile struct GPIOD_OSPEEDR_s*) 0x48000c08))

struct GPIOD_PUPDR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int PUPDR0 : 2;
			unsigned int PUPDR1 : 2;
			unsigned int PUPDR2 : 2;
			unsigned int PUPDR3 : 2;
			unsigned int PUPDR4 : 2;
			unsigned int PUPDR5 : 2;
			unsigned int PUPDR6 : 2;
			unsigned int PUPDR7 : 2;
			unsigned int PUPDR8 : 2;
			unsigned int PUPDR9 : 2;
			unsigned int PUPDR10 : 2;
			unsigned int PUPDR11 : 2;
			unsigned int PUPDR12 : 2;
			unsigned int PUPDR13 : 2;
			unsigned int PUPDR14 : 2;
			unsigned int PUPDR15 : 2;
		} __attribute((__packed__));
	};
};
#define GPIOD_PUPDR (*((volatile struct GPIOD_PUPDR_s*) 0x48000c0c))

struct GPIOD_IDR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int IDR0 : 1;
			unsigned int IDR1 : 1;
			unsigned int IDR2 : 1;
			unsigned int IDR3 : 1;
			unsigned int IDR4 : 1;
			unsigned int IDR5 : 1;
			unsigned int IDR6 : 1;
			unsigned int IDR7 : 1;
			unsigned int IDR8 : 1;
			unsigned int IDR9 : 1;
			unsigned int IDR10 : 1;
			unsigned int IDR11 : 1;
			unsigned int IDR12 : 1;
			unsigned int IDR13 : 1;
			unsigned int IDR14 : 1;
			unsigned int IDR15 : 1;
		} __attribute((__packed__));
	};
};
#define GPIOD_IDR (*((volatile struct GPIOD_IDR_s*) 0x48000c10))

struct GPIOD_ODR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int ODR0 : 1;
			unsigned int ODR1 : 1;
			unsigned int ODR2 : 1;
			unsigned int ODR3 : 1;
			unsigned int ODR4 : 1;
			unsigned int ODR5 : 1;
			unsigned int ODR6 : 1;
			unsigned int ODR7 : 1;
			unsigned int ODR8 : 1;
			unsigned int ODR9 : 1;
			unsigned int ODR10 : 1;
			unsigned int ODR11 : 1;
			unsigned int ODR12 : 1;
			unsigned int ODR13 : 1;
			unsigned int ODR14 : 1;
			unsigned int ODR15 : 1;
		} __attribute((__packed__));
	};
};
#define GPIOD_ODR (*((volatile struct GPIOD_ODR_s*) 0x48000c14))

struct GPIOD_BSRR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int BS0 : 1;
			unsigned int BS1 : 1;
			unsigned int BS2 : 1;
			unsigned int BS3 : 1;
			unsigned int BS4 : 1;
			unsigned int BS5 : 1;
			unsigned int BS6 : 1;
			unsigned int BS7 : 1;
			unsigned int BS8 : 1;
			unsigned int BS9 : 1;
			unsigned int BS10 : 1;
			unsigned int BS11 : 1;
			unsigned int BS12 : 1;
			unsigned int BS13 : 1;
			unsigned int BS14 : 1;
			unsigned int BS15 : 1;
			unsigned int BR0 : 1;
			unsigned int BR1 : 1;
			unsigned int BR2 : 1;
			unsigned int BR3 : 1;
			unsigned int BR4 : 1;
			unsigned int BR5 : 1;
			unsigned int BR6 : 1;
			unsigned int BR7 : 1;
			unsigned int BR8 : 1;
			unsigned int BR9 : 1;
			unsigned int BR10 : 1;
			unsigned int BR11 : 1;
			unsigned int BR12 : 1;
			unsigned int BR13 : 1;
			unsigned int BR14 : 1;
			unsigned int BR15 : 1;
		} __attribute((__packed__));
	};
};
#define GPIOD_BSRR (*((volatile struct GPIOD_BSRR_s*) 0x48000c18))

struct GPIOD_LCKR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int LCK0 : 1;
			unsigned int LCK1 : 1;
			unsigned int LCK2 : 1;
			unsigned int LCK3 : 1;
			unsigned int LCK4 : 1;
			unsigned int LCK5 : 1;
			unsigned int LCK6 : 1;
			unsigned int LCK7 : 1;
			unsigned int LCK8 : 1;
			unsigned int LCK9 : 1;
			unsigned int LCK10 : 1;
			unsigned int LCK11 : 1;
			unsigned int LCK12 : 1;
			unsigned int LCK13 : 1;
			unsigned int LCK14 : 1;
			unsigned int LCK15 : 1;
			unsigned int LCKK : 1;
		} __attribute((__packed__));
	};
};
#define GPIOD_LCKR (*((volatile struct GPIOD_LCKR_s*) 0x48000c1c))

struct GPIOD_AFRL_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int AFRL0 : 4;
			unsigned int AFRL1 : 4;
			unsigned int AFRL2 : 4;
			unsigned int AFRL3 : 4;
			unsigned int AFRL4 : 4;
			unsigned int AFRL5 : 4;
			unsigned int AFRL6 : 4;
			unsigned int AFRL7 : 4;
		} __attribute((__packed__));
	};
};
#define GPIOD_AFRL (*((volatile struct GPIOD_AFRL_s*) 0x48000c20))

struct GPIOD_AFRH_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int AFRH8 : 4;
			unsigned int AFRH9 : 4;
			unsigned int AFRH10 : 4;
			unsigned int AFRH11 : 4;
			unsigned int AFRH12 : 4;
			unsigned int AFRH13 : 4;
			unsigned int AFRH14 : 4;
			unsigned int AFRH15 : 4;
		} __attribute((__packed__));
	};
};
#define GPIOD_AFRH (*((volatile struct GPIOD_AFRH_s*) 0x48000c24))

struct GPIOD_BRR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int BR0 : 1;
			unsigned int BR1 : 1;
			unsigned int BR2 : 1;
			unsigned int BR3 : 1;
			unsigned int BR4 : 1;
			unsigned int BR5 : 1;
			unsigned int BR6 : 1;
			unsigned int BR7 : 1;
			unsigned int BR8 : 1;
			unsigned int BR9 : 1;
			unsigned int BR10 : 1;
			unsigned int BR11 : 1;
			unsigned int BR12 : 1;
			unsigned int BR13 : 1;
			unsigned int BR14 : 1;
			unsigned int BR15 : 1;
		} __attribute((__packed__));
	};
};
#define GPIOD_BRR (*((volatile struct GPIOD_BRR_s*) 0x48000c28))

// GPIOE
struct GPIOE_MODER_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int MODER0 : 2;
			unsigned int MODER1 : 2;
			unsigned int MODER2 : 2;
			unsigned int MODER3 : 2;
			unsigned int MODER4 : 2;
			unsigned int MODER5 : 2;
			unsigned int MODER6 : 2;
			unsigned int MODER7 : 2;
			unsigned int MODER8 : 2;
			unsigned int MODER9 : 2;
			unsigned int MODER10 : 2;
			unsigned int MODER11 : 2;
			unsigned int MODER12 : 2;
			unsigned int MODER13 : 2;
			unsigned int MODER14 : 2;
			unsigned int MODER15 : 2;
		} __attribute((__packed__));
	};
};
#define GPIOE_MODER (*((volatile struct GPIOE_MODER_s*) 0x48001000))

struct GPIOE_OTYPER_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int OT0 : 1;
			unsigned int OT1 : 1;
			unsigned int OT2 : 1;
			unsigned int OT3 : 1;
			unsigned int OT4 : 1;
			unsigned int OT5 : 1;
			unsigned int OT6 : 1;
			unsigned int OT7 : 1;
			unsigned int OT8 : 1;
			unsigned int OT9 : 1;
			unsigned int OT10 : 1;
			unsigned int OT11 : 1;
			unsigned int OT12 : 1;
			unsigned int OT13 : 1;
			unsigned int OT14 : 1;
			unsigned int OT15 : 1;
		} __attribute((__packed__));
	};
};
#define GPIOE_OTYPER (*((volatile struct GPIOE_OTYPER_s*) 0x48001004))

struct GPIOE_OSPEEDR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int OSPEEDR0 : 2;
			unsigned int OSPEEDR1 : 2;
			unsigned int OSPEEDR2 : 2;
			unsigned int OSPEEDR3 : 2;
			unsigned int OSPEEDR4 : 2;
			unsigned int OSPEEDR5 : 2;
			unsigned int OSPEEDR6 : 2;
			unsigned int OSPEEDR7 : 2;
			unsigned int OSPEEDR8 : 2;
			unsigned int OSPEEDR9 : 2;
			unsigned int OSPEEDR10 : 2;
			unsigned int OSPEEDR11 : 2;
			unsigned int OSPEEDR12 : 2;
			unsigned int OSPEEDR13 : 2;
			unsigned int OSPEEDR14 : 2;
			unsigned int OSPEEDR15 : 2;
		} __attribute((__packed__));
	};
};
#define GPIOE_OSPEEDR (*((volatile struct GPIOE_OSPEEDR_s*) 0x48001008))

struct GPIOE_PUPDR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int PUPDR0 : 2;
			unsigned int PUPDR1 : 2;
			unsigned int PUPDR2 : 2;
			unsigned int PUPDR3 : 2;
			unsigned int PUPDR4 : 2;
			unsigned int PUPDR5 : 2;
			unsigned int PUPDR6 : 2;
			unsigned int PUPDR7 : 2;
			unsigned int PUPDR8 : 2;
			unsigned int PUPDR9 : 2;
			unsigned int PUPDR10 : 2;
			unsigned int PUPDR11 : 2;
			unsigned int PUPDR12 : 2;
			unsigned int PUPDR13 : 2;
			unsigned int PUPDR14 : 2;
			unsigned int PUPDR15 : 2;
		} __attribute((__packed__));
	};
};
#define GPIOE_PUPDR (*((volatile struct GPIOE_PUPDR_s*) 0x4800100c))

struct GPIOE_IDR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int IDR0 : 1;
			unsigned int IDR1 : 1;
			unsigned int IDR2 : 1;
			unsigned int IDR3 : 1;
			unsigned int IDR4 : 1;
			unsigned int IDR5 : 1;
			unsigned int IDR6 : 1;
			unsigned int IDR7 : 1;
			unsigned int IDR8 : 1;
			unsigned int IDR9 : 1;
			unsigned int IDR10 : 1;
			unsigned int IDR11 : 1;
			unsigned int IDR12 : 1;
			unsigned int IDR13 : 1;
			unsigned int IDR14 : 1;
			unsigned int IDR15 : 1;
		} __attribute((__packed__));
	};
};
#define GPIOE_IDR (*((volatile struct GPIOE_IDR_s*) 0x48001010))

struct GPIOE_ODR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int ODR0 : 1;
			unsigned int ODR1 : 1;
			unsigned int ODR2 : 1;
			unsigned int ODR3 : 1;
			unsigned int ODR4 : 1;
			unsigned int ODR5 : 1;
			unsigned int ODR6 : 1;
			unsigned int ODR7 : 1;
			unsigned int ODR8 : 1;
			unsigned int ODR9 : 1;
			unsigned int ODR10 : 1;
			unsigned int ODR11 : 1;
			unsigned int ODR12 : 1;
			unsigned int ODR13 : 1;
			unsigned int ODR14 : 1;
			unsigned int ODR15 : 1;
		} __attribute((__packed__));
	};
};
#define GPIOE_ODR (*((volatile struct GPIOE_ODR_s*) 0x48001014))

struct GPIOE_BSRR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int BS0 : 1;
			unsigned int BS1 : 1;
			unsigned int BS2 : 1;
			unsigned int BS3 : 1;
			unsigned int BS4 : 1;
			unsigned int BS5 : 1;
			unsigned int BS6 : 1;
			unsigned int BS7 : 1;
			unsigned int BS8 : 1;
			unsigned int BS9 : 1;
			unsigned int BS10 : 1;
			unsigned int BS11 : 1;
			unsigned int BS12 : 1;
			unsigned int BS13 : 1;
			unsigned int BS14 : 1;
			unsigned int BS15 : 1;
			unsigned int BR0 : 1;
			unsigned int BR1 : 1;
			unsigned int BR2 : 1;
			unsigned int BR3 : 1;
			unsigned int BR4 : 1;
			unsigned int BR5 : 1;
			unsigned int BR6 : 1;
			unsigned int BR7 : 1;
			unsigned int BR8 : 1;
			unsigned int BR9 : 1;
			unsigned int BR10 : 1;
			unsigned int BR11 : 1;
			unsigned int BR12 : 1;
			unsigned int BR13 : 1;
			unsigned int BR14 : 1;
			unsigned int BR15 : 1;
		} __attribute((__packed__));
	};
};
#define GPIOE_BSRR (*((volatile struct GPIOE_BSRR_s*) 0x48001018))

struct GPIOE_LCKR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int LCK0 : 1;
			unsigned int LCK1 : 1;
			unsigned int LCK2 : 1;
			unsigned int LCK3 : 1;
			unsigned int LCK4 : 1;
			unsigned int LCK5 : 1;
			unsigned int LCK6 : 1;
			unsigned int LCK7 : 1;
			unsigned int LCK8 : 1;
			unsigned int LCK9 : 1;
			unsigned int LCK10 : 1;
			unsigned int LCK11 : 1;
			unsigned int LCK12 : 1;
			unsigned int LCK13 : 1;
			unsigned int LCK14 : 1;
			unsigned int LCK15 : 1;
			unsigned int LCKK : 1;
		} __attribute((__packed__));
	};
};
#define GPIOE_LCKR (*((volatile struct GPIOE_LCKR_s*) 0x4800101c))

struct GPIOE_AFRL_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int AFRL0 : 4;
			unsigned int AFRL1 : 4;
			unsigned int AFRL2 : 4;
			unsigned int AFRL3 : 4;
			unsigned int AFRL4 : 4;
			unsigned int AFRL5 : 4;
			unsigned int AFRL6 : 4;
			unsigned int AFRL7 : 4;
		} __attribute((__packed__));
	};
};
#define GPIOE_AFRL (*((volatile struct GPIOE_AFRL_s*) 0x48001020))

struct GPIOE_AFRH_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int AFRH8 : 4;
			unsigned int AFRH9 : 4;
			unsigned int AFRH10 : 4;
			unsigned int AFRH11 : 4;
			unsigned int AFRH12 : 4;
			unsigned int AFRH13 : 4;
			unsigned int AFRH14 : 4;
			unsigned int AFRH15 : 4;
		} __attribute((__packed__));
	};
};
#define GPIOE_AFRH (*((volatile struct GPIOE_AFRH_s*) 0x48001024))

struct GPIOE_BRR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int BR0 : 1;
			unsigned int BR1 : 1;
			unsigned int BR2 : 1;
			unsigned int BR3 : 1;
			unsigned int BR4 : 1;
			unsigned int BR5 : 1;
			unsigned int BR6 : 1;
			unsigned int BR7 : 1;
			unsigned int BR8 : 1;
			unsigned int BR9 : 1;
			unsigned int BR10 : 1;
			unsigned int BR11 : 1;
			unsigned int BR12 : 1;
			unsigned int BR13 : 1;
			unsigned int BR14 : 1;
			unsigned int BR15 : 1;
		} __attribute((__packed__));
	};
};
#define GPIOE_BRR (*((volatile struct GPIOE_BRR_s*) 0x48001028))

// GPIOF
struct GPIOF_MODER_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int MODER0 : 2;
			unsigned int MODER1 : 2;
			unsigned int MODER2 : 2;
			unsigned int MODER3 : 2;
			unsigned int MODER4 : 2;
			unsigned int MODER5 : 2;
			unsigned int MODER6 : 2;
			unsigned int MODER7 : 2;
			unsigned int MODER8 : 2;
			unsigned int MODER9 : 2;
			unsigned int MODER10 : 2;
			unsigned int MODER11 : 2;
			unsigned int MODER12 : 2;
			unsigned int MODER13 : 2;
			unsigned int MODER14 : 2;
			unsigned int MODER15 : 2;
		} __attribute((__packed__));
	};
};
#define GPIOF_MODER (*((volatile struct GPIOF_MODER_s*) 0x48001400))

struct GPIOF_OTYPER_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int OT0 : 1;
			unsigned int OT1 : 1;
			unsigned int OT2 : 1;
			unsigned int OT3 : 1;
			unsigned int OT4 : 1;
			unsigned int OT5 : 1;
			unsigned int OT6 : 1;
			unsigned int OT7 : 1;
			unsigned int OT8 : 1;
			unsigned int OT9 : 1;
			unsigned int OT10 : 1;
			unsigned int OT11 : 1;
			unsigned int OT12 : 1;
			unsigned int OT13 : 1;
			unsigned int OT14 : 1;
			unsigned int OT15 : 1;
		} __attribute((__packed__));
	};
};
#define GPIOF_OTYPER (*((volatile struct GPIOF_OTYPER_s*) 0x48001404))

struct GPIOF_OSPEEDR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int OSPEEDR0 : 2;
			unsigned int OSPEEDR1 : 2;
			unsigned int OSPEEDR2 : 2;
			unsigned int OSPEEDR3 : 2;
			unsigned int OSPEEDR4 : 2;
			unsigned int OSPEEDR5 : 2;
			unsigned int OSPEEDR6 : 2;
			unsigned int OSPEEDR7 : 2;
			unsigned int OSPEEDR8 : 2;
			unsigned int OSPEEDR9 : 2;
			unsigned int OSPEEDR10 : 2;
			unsigned int OSPEEDR11 : 2;
			unsigned int OSPEEDR12 : 2;
			unsigned int OSPEEDR13 : 2;
			unsigned int OSPEEDR14 : 2;
			unsigned int OSPEEDR15 : 2;
		} __attribute((__packed__));
	};
};
#define GPIOF_OSPEEDR (*((volatile struct GPIOF_OSPEEDR_s*) 0x48001408))

struct GPIOF_PUPDR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int PUPDR0 : 2;
			unsigned int PUPDR1 : 2;
			unsigned int PUPDR2 : 2;
			unsigned int PUPDR3 : 2;
			unsigned int PUPDR4 : 2;
			unsigned int PUPDR5 : 2;
			unsigned int PUPDR6 : 2;
			unsigned int PUPDR7 : 2;
			unsigned int PUPDR8 : 2;
			unsigned int PUPDR9 : 2;
			unsigned int PUPDR10 : 2;
			unsigned int PUPDR11 : 2;
			unsigned int PUPDR12 : 2;
			unsigned int PUPDR13 : 2;
			unsigned int PUPDR14 : 2;
			unsigned int PUPDR15 : 2;
		} __attribute((__packed__));
	};
};
#define GPIOF_PUPDR (*((volatile struct GPIOF_PUPDR_s*) 0x4800140c))

struct GPIOF_IDR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int IDR0 : 1;
			unsigned int IDR1 : 1;
			unsigned int IDR2 : 1;
			unsigned int IDR3 : 1;
			unsigned int IDR4 : 1;
			unsigned int IDR5 : 1;
			unsigned int IDR6 : 1;
			unsigned int IDR7 : 1;
			unsigned int IDR8 : 1;
			unsigned int IDR9 : 1;
			unsigned int IDR10 : 1;
			unsigned int IDR11 : 1;
			unsigned int IDR12 : 1;
			unsigned int IDR13 : 1;
			unsigned int IDR14 : 1;
			unsigned int IDR15 : 1;
		} __attribute((__packed__));
	};
};
#define GPIOF_IDR (*((volatile struct GPIOF_IDR_s*) 0x48001410))

struct GPIOF_ODR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int ODR0 : 1;
			unsigned int ODR1 : 1;
			unsigned int ODR2 : 1;
			unsigned int ODR3 : 1;
			unsigned int ODR4 : 1;
			unsigned int ODR5 : 1;
			unsigned int ODR6 : 1;
			unsigned int ODR7 : 1;
			unsigned int ODR8 : 1;
			unsigned int ODR9 : 1;
			unsigned int ODR10 : 1;
			unsigned int ODR11 : 1;
			unsigned int ODR12 : 1;
			unsigned int ODR13 : 1;
			unsigned int ODR14 : 1;
			unsigned int ODR15 : 1;
		} __attribute((__packed__));
	};
};
#define GPIOF_ODR (*((volatile struct GPIOF_ODR_s*) 0x48001414))

struct GPIOF_BSRR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int BS0 : 1;
			unsigned int BS1 : 1;
			unsigned int BS2 : 1;
			unsigned int BS3 : 1;
			unsigned int BS4 : 1;
			unsigned int BS5 : 1;
			unsigned int BS6 : 1;
			unsigned int BS7 : 1;
			unsigned int BS8 : 1;
			unsigned int BS9 : 1;
			unsigned int BS10 : 1;
			unsigned int BS11 : 1;
			unsigned int BS12 : 1;
			unsigned int BS13 : 1;
			unsigned int BS14 : 1;
			unsigned int BS15 : 1;
			unsigned int BR0 : 1;
			unsigned int BR1 : 1;
			unsigned int BR2 : 1;
			unsigned int BR3 : 1;
			unsigned int BR4 : 1;
			unsigned int BR5 : 1;
			unsigned int BR6 : 1;
			unsigned int BR7 : 1;
			unsigned int BR8 : 1;
			unsigned int BR9 : 1;
			unsigned int BR10 : 1;
			unsigned int BR11 : 1;
			unsigned int BR12 : 1;
			unsigned int BR13 : 1;
			unsigned int BR14 : 1;
			unsigned int BR15 : 1;
		} __attribute((__packed__));
	};
};
#define GPIOF_BSRR (*((volatile struct GPIOF_BSRR_s*) 0x48001418))

struct GPIOF_LCKR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int LCK0 : 1;
			unsigned int LCK1 : 1;
			unsigned int LCK2 : 1;
			unsigned int LCK3 : 1;
			unsigned int LCK4 : 1;
			unsigned int LCK5 : 1;
			unsigned int LCK6 : 1;
			unsigned int LCK7 : 1;
			unsigned int LCK8 : 1;
			unsigned int LCK9 : 1;
			unsigned int LCK10 : 1;
			unsigned int LCK11 : 1;
			unsigned int LCK12 : 1;
			unsigned int LCK13 : 1;
			unsigned int LCK14 : 1;
			unsigned int LCK15 : 1;
			unsigned int LCKK : 1;
		} __attribute((__packed__));
	};
};
#define GPIOF_LCKR (*((volatile struct GPIOF_LCKR_s*) 0x4800141c))

struct GPIOF_AFRL_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int AFRL0 : 4;
			unsigned int AFRL1 : 4;
			unsigned int AFRL2 : 4;
			unsigned int AFRL3 : 4;
			unsigned int AFRL4 : 4;
			unsigned int AFRL5 : 4;
			unsigned int AFRL6 : 4;
			unsigned int AFRL7 : 4;
		} __attribute((__packed__));
	};
};
#define GPIOF_AFRL (*((volatile struct GPIOF_AFRL_s*) 0x48001420))

struct GPIOF_AFRH_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int AFRH8 : 4;
			unsigned int AFRH9 : 4;
			unsigned int AFRH10 : 4;
			unsigned int AFRH11 : 4;
			unsigned int AFRH12 : 4;
			unsigned int AFRH13 : 4;
			unsigned int AFRH14 : 4;
			unsigned int AFRH15 : 4;
		} __attribute((__packed__));
	};
};
#define GPIOF_AFRH (*((volatile struct GPIOF_AFRH_s*) 0x48001424))

struct GPIOF_BRR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int BR0 : 1;
			unsigned int BR1 : 1;
			unsigned int BR2 : 1;
			unsigned int BR3 : 1;
			unsigned int BR4 : 1;
			unsigned int BR5 : 1;
			unsigned int BR6 : 1;
			unsigned int BR7 : 1;
			unsigned int BR8 : 1;
			unsigned int BR9 : 1;
			unsigned int BR10 : 1;
			unsigned int BR11 : 1;
			unsigned int BR12 : 1;
			unsigned int BR13 : 1;
			unsigned int BR14 : 1;
			unsigned int BR15 : 1;
		} __attribute((__packed__));
	};
};
#define GPIOF_BRR (*((volatile struct GPIOF_BRR_s*) 0x48001428))

// GPIOG
struct GPIOG_MODER_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int MODER0 : 2;
			unsigned int MODER1 : 2;
			unsigned int MODER2 : 2;
			unsigned int MODER3 : 2;
			unsigned int MODER4 : 2;
			unsigned int MODER5 : 2;
			unsigned int MODER6 : 2;
			unsigned int MODER7 : 2;
			unsigned int MODER8 : 2;
			unsigned int MODER9 : 2;
			unsigned int MODER10 : 2;
			unsigned int MODER11 : 2;
			unsigned int MODER12 : 2;
			unsigned int MODER13 : 2;
			unsigned int MODER14 : 2;
			unsigned int MODER15 : 2;
		} __attribute((__packed__));
	};
};
#define GPIOG_MODER (*((volatile struct GPIOG_MODER_s*) 0x48001800))

struct GPIOG_OTYPER_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int OT0 : 1;
			unsigned int OT1 : 1;
			unsigned int OT2 : 1;
			unsigned int OT3 : 1;
			unsigned int OT4 : 1;
			unsigned int OT5 : 1;
			unsigned int OT6 : 1;
			unsigned int OT7 : 1;
			unsigned int OT8 : 1;
			unsigned int OT9 : 1;
			unsigned int OT10 : 1;
			unsigned int OT11 : 1;
			unsigned int OT12 : 1;
			unsigned int OT13 : 1;
			unsigned int OT14 : 1;
			unsigned int OT15 : 1;
		} __attribute((__packed__));
	};
};
#define GPIOG_OTYPER (*((volatile struct GPIOG_OTYPER_s*) 0x48001804))

struct GPIOG_OSPEEDR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int OSPEEDR0 : 2;
			unsigned int OSPEEDR1 : 2;
			unsigned int OSPEEDR2 : 2;
			unsigned int OSPEEDR3 : 2;
			unsigned int OSPEEDR4 : 2;
			unsigned int OSPEEDR5 : 2;
			unsigned int OSPEEDR6 : 2;
			unsigned int OSPEEDR7 : 2;
			unsigned int OSPEEDR8 : 2;
			unsigned int OSPEEDR9 : 2;
			unsigned int OSPEEDR10 : 2;
			unsigned int OSPEEDR11 : 2;
			unsigned int OSPEEDR12 : 2;
			unsigned int OSPEEDR13 : 2;
			unsigned int OSPEEDR14 : 2;
			unsigned int OSPEEDR15 : 2;
		} __attribute((__packed__));
	};
};
#define GPIOG_OSPEEDR (*((volatile struct GPIOG_OSPEEDR_s*) 0x48001808))

struct GPIOG_PUPDR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int PUPDR0 : 2;
			unsigned int PUPDR1 : 2;
			unsigned int PUPDR2 : 2;
			unsigned int PUPDR3 : 2;
			unsigned int PUPDR4 : 2;
			unsigned int PUPDR5 : 2;
			unsigned int PUPDR6 : 2;
			unsigned int PUPDR7 : 2;
			unsigned int PUPDR8 : 2;
			unsigned int PUPDR9 : 2;
			unsigned int PUPDR10 : 2;
			unsigned int PUPDR11 : 2;
			unsigned int PUPDR12 : 2;
			unsigned int PUPDR13 : 2;
			unsigned int PUPDR14 : 2;
			unsigned int PUPDR15 : 2;
		} __attribute((__packed__));
	};
};
#define GPIOG_PUPDR (*((volatile struct GPIOG_PUPDR_s*) 0x4800180c))

struct GPIOG_IDR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int IDR0 : 1;
			unsigned int IDR1 : 1;
			unsigned int IDR2 : 1;
			unsigned int IDR3 : 1;
			unsigned int IDR4 : 1;
			unsigned int IDR5 : 1;
			unsigned int IDR6 : 1;
			unsigned int IDR7 : 1;
			unsigned int IDR8 : 1;
			unsigned int IDR9 : 1;
			unsigned int IDR10 : 1;
			unsigned int IDR11 : 1;
			unsigned int IDR12 : 1;
			unsigned int IDR13 : 1;
			unsigned int IDR14 : 1;
			unsigned int IDR15 : 1;
		} __attribute((__packed__));
	};
};
#define GPIOG_IDR (*((volatile struct GPIOG_IDR_s*) 0x48001810))

struct GPIOG_ODR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int ODR0 : 1;
			unsigned int ODR1 : 1;
			unsigned int ODR2 : 1;
			unsigned int ODR3 : 1;
			unsigned int ODR4 : 1;
			unsigned int ODR5 : 1;
			unsigned int ODR6 : 1;
			unsigned int ODR7 : 1;
			unsigned int ODR8 : 1;
			unsigned int ODR9 : 1;
			unsigned int ODR10 : 1;
			unsigned int ODR11 : 1;
			unsigned int ODR12 : 1;
			unsigned int ODR13 : 1;
			unsigned int ODR14 : 1;
			unsigned int ODR15 : 1;
		} __attribute((__packed__));
	};
};
#define GPIOG_ODR (*((volatile struct GPIOG_ODR_s*) 0x48001814))

struct GPIOG_BSRR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int BS0 : 1;
			unsigned int BS1 : 1;
			unsigned int BS2 : 1;
			unsigned int BS3 : 1;
			unsigned int BS4 : 1;
			unsigned int BS5 : 1;
			unsigned int BS6 : 1;
			unsigned int BS7 : 1;
			unsigned int BS8 : 1;
			unsigned int BS9 : 1;
			unsigned int BS10 : 1;
			unsigned int BS11 : 1;
			unsigned int BS12 : 1;
			unsigned int BS13 : 1;
			unsigned int BS14 : 1;
			unsigned int BS15 : 1;
			unsigned int BR0 : 1;
			unsigned int BR1 : 1;
			unsigned int BR2 : 1;
			unsigned int BR3 : 1;
			unsigned int BR4 : 1;
			unsigned int BR5 : 1;
			unsigned int BR6 : 1;
			unsigned int BR7 : 1;
			unsigned int BR8 : 1;
			unsigned int BR9 : 1;
			unsigned int BR10 : 1;
			unsigned int BR11 : 1;
			unsigned int BR12 : 1;
			unsigned int BR13 : 1;
			unsigned int BR14 : 1;
			unsigned int BR15 : 1;
		} __attribute((__packed__));
	};
};
#define GPIOG_BSRR (*((volatile struct GPIOG_BSRR_s*) 0x48001818))

struct GPIOG_LCKR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int LCK0 : 1;
			unsigned int LCK1 : 1;
			unsigned int LCK2 : 1;
			unsigned int LCK3 : 1;
			unsigned int LCK4 : 1;
			unsigned int LCK5 : 1;
			unsigned int LCK6 : 1;
			unsigned int LCK7 : 1;
			unsigned int LCK8 : 1;
			unsigned int LCK9 : 1;
			unsigned int LCK10 : 1;
			unsigned int LCK11 : 1;
			unsigned int LCK12 : 1;
			unsigned int LCK13 : 1;
			unsigned int LCK14 : 1;
			unsigned int LCK15 : 1;
			unsigned int LCKK : 1;
		} __attribute((__packed__));
	};
};
#define GPIOG_LCKR (*((volatile struct GPIOG_LCKR_s*) 0x4800181c))

struct GPIOG_AFRL_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int AFRL0 : 4;
			unsigned int AFRL1 : 4;
			unsigned int AFRL2 : 4;
			unsigned int AFRL3 : 4;
			unsigned int AFRL4 : 4;
			unsigned int AFRL5 : 4;
			unsigned int AFRL6 : 4;
			unsigned int AFRL7 : 4;
		} __attribute((__packed__));
	};
};
#define GPIOG_AFRL (*((volatile struct GPIOG_AFRL_s*) 0x48001820))

struct GPIOG_AFRH_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int AFRH8 : 4;
			unsigned int AFRH9 : 4;
			unsigned int AFRH10 : 4;
			unsigned int AFRH11 : 4;
			unsigned int AFRH12 : 4;
			unsigned int AFRH13 : 4;
			unsigned int AFRH14 : 4;
			unsigned int AFRH15 : 4;
		} __attribute((__packed__));
	};
};
#define GPIOG_AFRH (*((volatile struct GPIOG_AFRH_s*) 0x48001824))

struct GPIOG_BRR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int BR0 : 1;
			unsigned int BR1 : 1;
			unsigned int BR2 : 1;
			unsigned int BR3 : 1;
			unsigned int BR4 : 1;
			unsigned int BR5 : 1;
			unsigned int BR6 : 1;
			unsigned int BR7 : 1;
			unsigned int BR8 : 1;
			unsigned int BR9 : 1;
			unsigned int BR10 : 1;
			unsigned int BR11 : 1;
			unsigned int BR12 : 1;
			unsigned int BR13 : 1;
			unsigned int BR14 : 1;
			unsigned int BR15 : 1;
		} __attribute((__packed__));
	};
};
#define GPIOG_BRR (*((volatile struct GPIOG_BRR_s*) 0x48001828))

// GPIOH
struct GPIOH_MODER_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int MODER0 : 2;
			unsigned int MODER1 : 2;
			unsigned int MODER2 : 2;
			unsigned int MODER3 : 2;
			unsigned int MODER4 : 2;
			unsigned int MODER5 : 2;
			unsigned int MODER6 : 2;
			unsigned int MODER7 : 2;
			unsigned int MODER8 : 2;
			unsigned int MODER9 : 2;
			unsigned int MODER10 : 2;
			unsigned int MODER11 : 2;
			unsigned int MODER12 : 2;
			unsigned int MODER13 : 2;
			unsigned int MODER14 : 2;
			unsigned int MODER15 : 2;
		} __attribute((__packed__));
	};
};
#define GPIOH_MODER (*((volatile struct GPIOH_MODER_s*) 0x48001c00))

struct GPIOH_OTYPER_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int OT0 : 1;
			unsigned int OT1 : 1;
			unsigned int OT2 : 1;
			unsigned int OT3 : 1;
			unsigned int OT4 : 1;
			unsigned int OT5 : 1;
			unsigned int OT6 : 1;
			unsigned int OT7 : 1;
			unsigned int OT8 : 1;
			unsigned int OT9 : 1;
			unsigned int OT10 : 1;
			unsigned int OT11 : 1;
			unsigned int OT12 : 1;
			unsigned int OT13 : 1;
			unsigned int OT14 : 1;
			unsigned int OT15 : 1;
		} __attribute((__packed__));
	};
};
#define GPIOH_OTYPER (*((volatile struct GPIOH_OTYPER_s*) 0x48001c04))

struct GPIOH_OSPEEDR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int OSPEEDR0 : 2;
			unsigned int OSPEEDR1 : 2;
			unsigned int OSPEEDR2 : 2;
			unsigned int OSPEEDR3 : 2;
			unsigned int OSPEEDR4 : 2;
			unsigned int OSPEEDR5 : 2;
			unsigned int OSPEEDR6 : 2;
			unsigned int OSPEEDR7 : 2;
			unsigned int OSPEEDR8 : 2;
			unsigned int OSPEEDR9 : 2;
			unsigned int OSPEEDR10 : 2;
			unsigned int OSPEEDR11 : 2;
			unsigned int OSPEEDR12 : 2;
			unsigned int OSPEEDR13 : 2;
			unsigned int OSPEEDR14 : 2;
			unsigned int OSPEEDR15 : 2;
		} __attribute((__packed__));
	};
};
#define GPIOH_OSPEEDR (*((volatile struct GPIOH_OSPEEDR_s*) 0x48001c08))

struct GPIOH_PUPDR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int PUPDR0 : 2;
			unsigned int PUPDR1 : 2;
			unsigned int PUPDR2 : 2;
			unsigned int PUPDR3 : 2;
			unsigned int PUPDR4 : 2;
			unsigned int PUPDR5 : 2;
			unsigned int PUPDR6 : 2;
			unsigned int PUPDR7 : 2;
			unsigned int PUPDR8 : 2;
			unsigned int PUPDR9 : 2;
			unsigned int PUPDR10 : 2;
			unsigned int PUPDR11 : 2;
			unsigned int PUPDR12 : 2;
			unsigned int PUPDR13 : 2;
			unsigned int PUPDR14 : 2;
			unsigned int PUPDR15 : 2;
		} __attribute((__packed__));
	};
};
#define GPIOH_PUPDR (*((volatile struct GPIOH_PUPDR_s*) 0x48001c0c))

struct GPIOH_IDR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int IDR0 : 1;
			unsigned int IDR1 : 1;
			unsigned int IDR2 : 1;
			unsigned int IDR3 : 1;
			unsigned int IDR4 : 1;
			unsigned int IDR5 : 1;
			unsigned int IDR6 : 1;
			unsigned int IDR7 : 1;
			unsigned int IDR8 : 1;
			unsigned int IDR9 : 1;
			unsigned int IDR10 : 1;
			unsigned int IDR11 : 1;
			unsigned int IDR12 : 1;
			unsigned int IDR13 : 1;
			unsigned int IDR14 : 1;
			unsigned int IDR15 : 1;
		} __attribute((__packed__));
	};
};
#define GPIOH_IDR (*((volatile struct GPIOH_IDR_s*) 0x48001c10))

struct GPIOH_ODR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int ODR0 : 1;
			unsigned int ODR1 : 1;
			unsigned int ODR2 : 1;
			unsigned int ODR3 : 1;
			unsigned int ODR4 : 1;
			unsigned int ODR5 : 1;
			unsigned int ODR6 : 1;
			unsigned int ODR7 : 1;
			unsigned int ODR8 : 1;
			unsigned int ODR9 : 1;
			unsigned int ODR10 : 1;
			unsigned int ODR11 : 1;
			unsigned int ODR12 : 1;
			unsigned int ODR13 : 1;
			unsigned int ODR14 : 1;
			unsigned int ODR15 : 1;
		} __attribute((__packed__));
	};
};
#define GPIOH_ODR (*((volatile struct GPIOH_ODR_s*) 0x48001c14))

struct GPIOH_BSRR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int BS0 : 1;
			unsigned int BS1 : 1;
			unsigned int BS2 : 1;
			unsigned int BS3 : 1;
			unsigned int BS4 : 1;
			unsigned int BS5 : 1;
			unsigned int BS6 : 1;
			unsigned int BS7 : 1;
			unsigned int BS8 : 1;
			unsigned int BS9 : 1;
			unsigned int BS10 : 1;
			unsigned int BS11 : 1;
			unsigned int BS12 : 1;
			unsigned int BS13 : 1;
			unsigned int BS14 : 1;
			unsigned int BS15 : 1;
			unsigned int BR0 : 1;
			unsigned int BR1 : 1;
			unsigned int BR2 : 1;
			unsigned int BR3 : 1;
			unsigned int BR4 : 1;
			unsigned int BR5 : 1;
			unsigned int BR6 : 1;
			unsigned int BR7 : 1;
			unsigned int BR8 : 1;
			unsigned int BR9 : 1;
			unsigned int BR10 : 1;
			unsigned int BR11 : 1;
			unsigned int BR12 : 1;
			unsigned int BR13 : 1;
			unsigned int BR14 : 1;
			unsigned int BR15 : 1;
		} __attribute((__packed__));
	};
};
#define GPIOH_BSRR (*((volatile struct GPIOH_BSRR_s*) 0x48001c18))

struct GPIOH_LCKR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int LCK0 : 1;
			unsigned int LCK1 : 1;
			unsigned int LCK2 : 1;
			unsigned int LCK3 : 1;
			unsigned int LCK4 : 1;
			unsigned int LCK5 : 1;
			unsigned int LCK6 : 1;
			unsigned int LCK7 : 1;
			unsigned int LCK8 : 1;
			unsigned int LCK9 : 1;
			unsigned int LCK10 : 1;
			unsigned int LCK11 : 1;
			unsigned int LCK12 : 1;
			unsigned int LCK13 : 1;
			unsigned int LCK14 : 1;
			unsigned int LCK15 : 1;
			unsigned int LCKK : 1;
		} __attribute((__packed__));
	};
};
#define GPIOH_LCKR (*((volatile struct GPIOH_LCKR_s*) 0x48001c1c))

struct GPIOH_AFRL_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int AFRL0 : 4;
			unsigned int AFRL1 : 4;
			unsigned int AFRL2 : 4;
			unsigned int AFRL3 : 4;
			unsigned int AFRL4 : 4;
			unsigned int AFRL5 : 4;
			unsigned int AFRL6 : 4;
			unsigned int AFRL7 : 4;
		} __attribute((__packed__));
	};
};
#define GPIOH_AFRL (*((volatile struct GPIOH_AFRL_s*) 0x48001c20))

struct GPIOH_AFRH_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int AFRH8 : 4;
			unsigned int AFRH9 : 4;
			unsigned int AFRH10 : 4;
			unsigned int AFRH11 : 4;
			unsigned int AFRH12 : 4;
			unsigned int AFRH13 : 4;
			unsigned int AFRH14 : 4;
			unsigned int AFRH15 : 4;
		} __attribute((__packed__));
	};
};
#define GPIOH_AFRH (*((volatile struct GPIOH_AFRH_s*) 0x48001c24))

struct GPIOH_BRR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int BR0 : 1;
			unsigned int BR1 : 1;
			unsigned int BR2 : 1;
			unsigned int BR3 : 1;
			unsigned int BR4 : 1;
			unsigned int BR5 : 1;
			unsigned int BR6 : 1;
			unsigned int BR7 : 1;
			unsigned int BR8 : 1;
			unsigned int BR9 : 1;
			unsigned int BR10 : 1;
			unsigned int BR11 : 1;
			unsigned int BR12 : 1;
			unsigned int BR13 : 1;
			unsigned int BR14 : 1;
			unsigned int BR15 : 1;
		} __attribute((__packed__));
	};
};
#define GPIOH_BRR (*((volatile struct GPIOH_BRR_s*) 0x48001c28))

// TSC
struct TSC_CR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int TSCE : 1;
			unsigned int START : 1;
			unsigned int AM : 1;
			unsigned int SYNCPOL : 1;
			unsigned int IODEF : 1;
			unsigned int MCV : 3;
			unsigned int : 4;
			unsigned int PGPSC : 3;
			unsigned int SSPSC : 1;
			unsigned int SSE : 1;
			unsigned int SSD : 7;
			unsigned int CTPL : 4;
			unsigned int CTPH : 4;
		} __attribute((__packed__));
	};
};
#define TSC_CR (*((volatile struct TSC_CR_s*) 0x40024000))

struct TSC_IER_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int EOAIE : 1;
			unsigned int MCEIE : 1;
		} __attribute((__packed__));
	};
};
#define TSC_IER (*((volatile struct TSC_IER_s*) 0x40024004))

struct TSC_ICR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int EOAIC : 1;
			unsigned int MCEIC : 1;
		} __attribute((__packed__));
	};
};
#define TSC_ICR (*((volatile struct TSC_ICR_s*) 0x40024008))

struct TSC_ISR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int EOAF : 1;
			unsigned int MCEF : 1;
		} __attribute((__packed__));
	};
};
#define TSC_ISR (*((volatile struct TSC_ISR_s*) 0x4002400c))

struct TSC_IOHCR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int G1_IO1 : 1;
			unsigned int G1_IO2 : 1;
			unsigned int G1_IO3 : 1;
			unsigned int G1_IO4 : 1;
			unsigned int G2_IO1 : 1;
			unsigned int G2_IO2 : 1;
			unsigned int G2_IO3 : 1;
			unsigned int G2_IO4 : 1;
			unsigned int G3_IO1 : 1;
			unsigned int G3_IO2 : 1;
			unsigned int G3_IO3 : 1;
			unsigned int G3_IO4 : 1;
			unsigned int G4_IO1 : 1;
			unsigned int G4_IO2 : 1;
			unsigned int G4_IO3 : 1;
			unsigned int G4_IO4 : 1;
			unsigned int G5_IO1 : 1;
			unsigned int G5_IO2 : 1;
			unsigned int G5_IO3 : 1;
			unsigned int G5_IO4 : 1;
			unsigned int G6_IO1 : 1;
			unsigned int G6_IO2 : 1;
			unsigned int G6_IO3 : 1;
			unsigned int G6_IO4 : 1;
			unsigned int G7_IO1 : 1;
			unsigned int G7_IO2 : 1;
			unsigned int G7_IO3 : 1;
			unsigned int G7_IO4 : 1;
			unsigned int G8_IO1 : 1;
			unsigned int G8_IO2 : 1;
			unsigned int G8_IO3 : 1;
			unsigned int G8_IO4 : 1;
		} __attribute((__packed__));
	};
};
#define TSC_IOHCR (*((volatile struct TSC_IOHCR_s*) 0x40024010))

struct TSC_IOASCR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int G1_IO1 : 1;
			unsigned int G1_IO2 : 1;
			unsigned int G1_IO3 : 1;
			unsigned int G1_IO4 : 1;
			unsigned int G2_IO1 : 1;
			unsigned int G2_IO2 : 1;
			unsigned int G2_IO3 : 1;
			unsigned int G2_IO4 : 1;
			unsigned int G3_IO1 : 1;
			unsigned int G3_IO2 : 1;
			unsigned int G3_IO3 : 1;
			unsigned int G3_IO4 : 1;
			unsigned int G4_IO1 : 1;
			unsigned int G4_IO2 : 1;
			unsigned int G4_IO3 : 1;
			unsigned int G4_IO4 : 1;
			unsigned int G5_IO1 : 1;
			unsigned int G5_IO2 : 1;
			unsigned int G5_IO3 : 1;
			unsigned int G5_IO4 : 1;
			unsigned int G6_IO1 : 1;
			unsigned int G6_IO2 : 1;
			unsigned int G6_IO3 : 1;
			unsigned int G6_IO4 : 1;
			unsigned int G7_IO1 : 1;
			unsigned int G7_IO2 : 1;
			unsigned int G7_IO3 : 1;
			unsigned int G7_IO4 : 1;
			unsigned int G8_IO1 : 1;
			unsigned int G8_IO2 : 1;
			unsigned int G8_IO3 : 1;
			unsigned int G8_IO4 : 1;
		} __attribute((__packed__));
	};
};
#define TSC_IOASCR (*((volatile struct TSC_IOASCR_s*) 0x40024018))

struct TSC_IOSCR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int G1_IO1 : 1;
			unsigned int G1_IO2 : 1;
			unsigned int G1_IO3 : 1;
			unsigned int G1_IO4 : 1;
			unsigned int G2_IO1 : 1;
			unsigned int G2_IO2 : 1;
			unsigned int G2_IO3 : 1;
			unsigned int G2_IO4 : 1;
			unsigned int G3_IO1 : 1;
			unsigned int G3_IO2 : 1;
			unsigned int G3_IO3 : 1;
			unsigned int G3_IO4 : 1;
			unsigned int G4_IO1 : 1;
			unsigned int G4_IO2 : 1;
			unsigned int G4_IO3 : 1;
			unsigned int G4_IO4 : 1;
			unsigned int G5_IO1 : 1;
			unsigned int G5_IO2 : 1;
			unsigned int G5_IO3 : 1;
			unsigned int G5_IO4 : 1;
			unsigned int G6_IO1 : 1;
			unsigned int G6_IO2 : 1;
			unsigned int G6_IO3 : 1;
			unsigned int G6_IO4 : 1;
			unsigned int G7_IO1 : 1;
			unsigned int G7_IO2 : 1;
			unsigned int G7_IO3 : 1;
			unsigned int G7_IO4 : 1;
			unsigned int G8_IO1 : 1;
			unsigned int G8_IO2 : 1;
			unsigned int G8_IO3 : 1;
			unsigned int G8_IO4 : 1;
		} __attribute((__packed__));
	};
};
#define TSC_IOSCR (*((volatile struct TSC_IOSCR_s*) 0x40024020))

struct TSC_IOCCR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int G1_IO1 : 1;
			unsigned int G1_IO2 : 1;
			unsigned int G1_IO3 : 1;
			unsigned int G1_IO4 : 1;
			unsigned int G2_IO1 : 1;
			unsigned int G2_IO2 : 1;
			unsigned int G2_IO3 : 1;
			unsigned int G2_IO4 : 1;
			unsigned int G3_IO1 : 1;
			unsigned int G3_IO2 : 1;
			unsigned int G3_IO3 : 1;
			unsigned int G3_IO4 : 1;
			unsigned int G4_IO1 : 1;
			unsigned int G4_IO2 : 1;
			unsigned int G4_IO3 : 1;
			unsigned int G4_IO4 : 1;
			unsigned int G5_IO1 : 1;
			unsigned int G5_IO2 : 1;
			unsigned int G5_IO3 : 1;
			unsigned int G5_IO4 : 1;
			unsigned int G6_IO1 : 1;
			unsigned int G6_IO2 : 1;
			unsigned int G6_IO3 : 1;
			unsigned int G6_IO4 : 1;
			unsigned int G7_IO1 : 1;
			unsigned int G7_IO2 : 1;
			unsigned int G7_IO3 : 1;
			unsigned int G7_IO4 : 1;
			unsigned int G8_IO1 : 1;
			unsigned int G8_IO2 : 1;
			unsigned int G8_IO3 : 1;
			unsigned int G8_IO4 : 1;
		} __attribute((__packed__));
	};
};
#define TSC_IOCCR (*((volatile struct TSC_IOCCR_s*) 0x40024028))

struct TSC_IOGCSR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int G1E : 1;
			unsigned int G2E : 1;
			unsigned int G3E : 1;
			unsigned int G4E : 1;
			unsigned int G5E : 1;
			unsigned int G6E : 1;
			unsigned int G7E : 1;
			unsigned int G8E : 1;
			unsigned int : 8;
			unsigned int G1S : 1;
			unsigned int G2S : 1;
			unsigned int G3S : 1;
			unsigned int G4S : 1;
			unsigned int G5S : 1;
			unsigned int G6S : 1;
			unsigned int G7S : 1;
			unsigned int G8S : 1;
		} __attribute((__packed__));
	};
};
#define TSC_IOGCSR (*((volatile struct TSC_IOGCSR_s*) 0x40024030))

struct TSC_IOG1CR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int CNT : 14;
		} __attribute((__packed__));
	};
};
#define TSC_IOG1CR (*((volatile struct TSC_IOG1CR_s*) 0x40024034))

struct TSC_IOG2CR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int CNT : 14;
		} __attribute((__packed__));
	};
};
#define TSC_IOG2CR (*((volatile struct TSC_IOG2CR_s*) 0x40024038))

struct TSC_IOG3CR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int CNT : 14;
		} __attribute((__packed__));
	};
};
#define TSC_IOG3CR (*((volatile struct TSC_IOG3CR_s*) 0x4002403c))

struct TSC_IOG4CR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int CNT : 14;
		} __attribute((__packed__));
	};
};
#define TSC_IOG4CR (*((volatile struct TSC_IOG4CR_s*) 0x40024040))

struct TSC_IOG5CR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int CNT : 14;
		} __attribute((__packed__));
	};
};
#define TSC_IOG5CR (*((volatile struct TSC_IOG5CR_s*) 0x40024044))

struct TSC_IOG6CR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int CNT : 14;
		} __attribute((__packed__));
	};
};
#define TSC_IOG6CR (*((volatile struct TSC_IOG6CR_s*) 0x40024048))

struct TSC_IOG7CR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int CNT : 14;
		} __attribute((__packed__));
	};
};
#define TSC_IOG7CR (*((volatile struct TSC_IOG7CR_s*) 0x4002404c))

struct TSC_IOG8CR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int CNT : 14;
		} __attribute((__packed__));
	};
};
#define TSC_IOG8CR (*((volatile struct TSC_IOG8CR_s*) 0x40024050))

// CRC
struct CRC_DR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int DR : 32;
		} __attribute((__packed__));
	};
};
#define CRC_DR (*((volatile struct CRC_DR_s*) 0x40023000))

struct CRC_IDR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int IDR : 8;
		} __attribute((__packed__));
	};
};
#define CRC_IDR (*((volatile struct CRC_IDR_s*) 0x40023004))

struct CRC_CR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int RESET : 1;
			unsigned int : 2;
			unsigned int POLYSIZE : 2;
			unsigned int REV_IN : 2;
			unsigned int REV_OUT : 1;
		} __attribute((__packed__));
	};
};
#define CRC_CR (*((volatile struct CRC_CR_s*) 0x40023008))

struct CRC_INIT_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int INIT : 32;
		} __attribute((__packed__));
	};
};
#define CRC_INIT (*((volatile struct CRC_INIT_s*) 0x40023010))

struct CRC_POL_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int POL : 32;
		} __attribute((__packed__));
	};
};
#define CRC_POL (*((volatile struct CRC_POL_s*) 0x40023014))

// Flash
struct Flash_ACR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			volatile unsigned int LATENCY : 3;
			volatile unsigned int : 1;
			volatile unsigned int PRFTBE : 1;
			volatile unsigned int PRFTBS : 1;
		} __attribute((__packed__));
	};
};
#define Flash_ACR (*((volatile struct Flash_ACR_s*) 0x40022000))

struct Flash_KEYR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int FKEYR : 32;
		} __attribute((__packed__));
	};
};
#define Flash_KEYR (*((volatile struct Flash_KEYR_s*) 0x40022004))

struct Flash_OPTKEYR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int OPTKEYR : 32;
		} __attribute((__packed__));
	};
};
#define Flash_OPTKEYR (*((volatile struct Flash_OPTKEYR_s*) 0x40022008))

struct Flash_SR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int BSY : 1;
			unsigned int : 1;
			unsigned int PGERR : 1;
			unsigned int : 1;
			unsigned int WRPRT : 1;
			unsigned int EOP : 1;
		} __attribute((__packed__));
	};
};
#define Flash_SR (*((volatile struct Flash_SR_s*) 0x4002200c))

struct Flash_CR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int PG : 1;
			unsigned int PER : 1;
			unsigned int MER : 1;
			unsigned int : 1;
			unsigned int OPTPG : 1;
			unsigned int OPTER : 1;
			unsigned int STRT : 1;
			unsigned int LOCK : 1;
			unsigned int : 1;
			unsigned int OPTWRE : 1;
			unsigned int ERRIE : 1;
			unsigned int : 1;
			unsigned int EOPIE : 1;
			unsigned int FORCE_OPTLOAD : 1;
		} __attribute((__packed__));
	};
};
#define Flash_CR (*((volatile struct Flash_CR_s*) 0x40022010))

struct Flash_AR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int FAR : 32;
		} __attribute((__packed__));
	};
};
#define Flash_AR (*((volatile struct Flash_AR_s*) 0x40022014))

struct Flash_OBR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int OPTERR : 1;
			unsigned int LEVEL1_PROT : 1;
			unsigned int LEVEL2_PROT : 1;
			unsigned int : 5;
			unsigned int WDG_SW : 1;
			unsigned int nRST_STOP : 1;
			unsigned int nRST_STDBY : 1;
			unsigned int : 1;
			unsigned int BOOT1 : 1;
			unsigned int VDDA_MONITOR : 1;
			unsigned int SRAM_PARITY_CHECK : 1;
			unsigned int : 1;
			unsigned int Data0 : 8;
			unsigned int Data1 : 8;
		} __attribute((__packed__));
	};
};
#define Flash_OBR (*((volatile struct Flash_OBR_s*) 0x4002201c))

struct Flash_WRPR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int WRP : 32;
		} __attribute((__packed__));
	};
};
#define Flash_WRPR (*((volatile struct Flash_WRPR_s*) 0x40022020))

// RCC
struct RCC_CR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int HSION : 1;
			unsigned int HSIRDY : 1;
			unsigned int : 1;
			unsigned int HSITRIM : 5;
			unsigned int HSICAL : 8;
			unsigned int HSEON : 1;
			unsigned int HSERDY : 1;
			unsigned int HSEBYP : 1;
			unsigned int CSSON : 1;
			unsigned int : 4;
			unsigned int PLLON : 1;
			unsigned int PLLRDY : 1;
		} __attribute((__packed__));
	};
};
#define RCC_CR (*((volatile struct RCC_CR_s*) 0x40021000))

struct RCC_CFGR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int SW : 2;
			unsigned int SWS : 2;
			unsigned int HPRE : 4;
			unsigned int PPRE1 : 3;
			unsigned int PPRE2 : 3;
			unsigned int : 1;
			unsigned int PLLSRC : 2;
			unsigned int PLLXTPRE : 1;
			unsigned int PLLMUL : 4;
			unsigned int USBPRES : 1;
			unsigned int I2SSRC : 1;
			unsigned int MCO : 3;
			unsigned int : 1;
			unsigned int MCOF : 1;
		} __attribute((__packed__));
	};
};
#define RCC_CFGR (*((volatile struct RCC_CFGR_s*) 0x40021004))

struct RCC_CIR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int LSIRDYF : 1;
			unsigned int LSERDYF : 1;
			unsigned int HSIRDYF : 1;
			unsigned int HSERDYF : 1;
			unsigned int PLLRDYF : 1;
			unsigned int : 2;
			unsigned int CSSF : 1;
			unsigned int LSIRDYIE : 1;
			unsigned int LSERDYIE : 1;
			unsigned int HSIRDYIE : 1;
			unsigned int HSERDYIE : 1;
			unsigned int PLLRDYIE : 1;
			unsigned int : 3;
			unsigned int LSIRDYC : 1;
			unsigned int LSERDYC : 1;
			unsigned int HSIRDYC : 1;
			unsigned int HSERDYC : 1;
			unsigned int PLLRDYC : 1;
			unsigned int : 2;
			unsigned int CSSC : 1;
		} __attribute((__packed__));
	};
};
#define RCC_CIR (*((volatile struct RCC_CIR_s*) 0x40021008))

struct RCC_APB2RSTR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int SYSCFGRST : 1;
			unsigned int : 10;
			unsigned int TIM1RST : 1;
			unsigned int SPI1RST : 1;
			unsigned int TIM8RST : 1;
			unsigned int USART1RST : 1;
			unsigned int : 1;
			unsigned int TIM15RST : 1;
			unsigned int TIM16RST : 1;
			unsigned int TIM17RST : 1;
		} __attribute((__packed__));
	};
};
#define RCC_APB2RSTR (*((volatile struct RCC_APB2RSTR_s*) 0x4002100c))

struct RCC_APB1RSTR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int TIM2RST : 1;
			unsigned int TIM3RST : 1;
			unsigned int TIM4RST : 1;
			unsigned int : 1;
			unsigned int TIM6RST : 1;
			unsigned int TIM7RST : 1;
			unsigned int : 5;
			unsigned int WWDGRST : 1;
			unsigned int : 2;
			unsigned int SPI2RST : 1;
			unsigned int SPI3RST : 1;
			unsigned int : 1;
			unsigned int USART2RST : 1;
			unsigned int USART3RST : 1;
			unsigned int UART4RST : 1;
			unsigned int UART5RST : 1;
			unsigned int I2C1RST : 1;
			unsigned int I2C2RST : 1;
			unsigned int USBRST : 1;
			unsigned int : 1;
			unsigned int CANRST : 1;
			unsigned int : 2;
			unsigned int PWRRST : 1;
			unsigned int DACRST : 1;
			unsigned int I2C3RST : 1;
		} __attribute((__packed__));
	};
};
#define RCC_APB1RSTR (*((volatile struct RCC_APB1RSTR_s*) 0x40021010))

struct RCC_AHBENR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int DMAEN : 1;
			unsigned int DMA2EN : 1;
			unsigned int SRAMEN : 1;
			unsigned int : 1;
			unsigned int FLITFEN : 1;
			unsigned int FMCEN : 1;
			unsigned int CRCEN : 1;
			unsigned int : 9;
			unsigned int IOPHEN : 1;
			unsigned int IOPAEN : 1;
			unsigned int IOPBEN : 1;
			unsigned int IOPCEN : 1;
			unsigned int IOPDEN : 1;
			unsigned int IOPEEN : 1;
			unsigned int IOPFEN : 1;
			unsigned int IOPGEN : 1;
			unsigned int TSCEN : 1;
			unsigned int : 3;
			unsigned int ADC12EN : 1;
			unsigned int ADC34EN : 1;
		} __attribute((__packed__));
	};
};
#define RCC_AHBENR (*((volatile struct RCC_AHBENR_s*) 0x40021014))

struct RCC_APB2ENR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int SYSCFGEN : 1;
			unsigned int : 10;
			unsigned int TIM1EN : 1;
			unsigned int SPI1EN : 1;
			unsigned int TIM8EN : 1;
			unsigned int USART1EN : 1;
			unsigned int : 1;
			unsigned int TIM15EN : 1;
			unsigned int TIM16EN : 1;
			unsigned int TIM17EN : 1;
		} __attribute((__packed__));
	};
};
#define RCC_APB2ENR (*((volatile struct RCC_APB2ENR_s*) 0x40021018))

struct RCC_APB1ENR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int TIM2EN : 1;
			unsigned int TIM3EN : 1;
			unsigned int TIM4EN : 1;
			unsigned int : 1;
			unsigned int TIM6EN : 1;
			unsigned int TIM7EN : 1;
			unsigned int : 5;
			unsigned int WWDGEN : 1;
			unsigned int : 2;
			unsigned int SPI2EN : 1;
			unsigned int SPI3EN : 1;
			unsigned int : 1;
			unsigned int USART2EN : 1;
			unsigned int USART3EN : 1;
			unsigned int USART4EN : 1;
			unsigned int USART5EN : 1;
			unsigned int I2C1EN : 1;
			unsigned int I2C2EN : 1;
			unsigned int USBEN : 1;
			unsigned int : 1;
			unsigned int CANEN : 1;
			unsigned int DAC2EN : 1;
			unsigned int : 1;
			unsigned int PWREN : 1;
			unsigned int DACEN : 1;
			unsigned int I2C3EN : 1;
		} __attribute((__packed__));
	};
};
#define RCC_APB1ENR (*((volatile struct RCC_APB1ENR_s*) 0x4002101c))

struct RCC_BDCR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int LSEON : 1;
			unsigned int LSERDY : 1;
			unsigned int LSEBYP : 1;
			unsigned int LSEDRV : 2;
			unsigned int : 3;
			unsigned int RTCSEL : 2;
			unsigned int : 5;
			unsigned int RTCEN : 1;
			unsigned int BDRST : 1;
		} __attribute((__packed__));
	};
};
#define RCC_BDCR (*((volatile struct RCC_BDCR_s*) 0x40021020))

struct RCC_CSR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int LSION : 1;
			unsigned int LSIRDY : 1;
			unsigned int : 22;
			unsigned int RMVF : 1;
			unsigned int OBLRSTF : 1;
			unsigned int PINRSTF : 1;
			unsigned int PORRSTF : 1;
			unsigned int SFTRSTF : 1;
			unsigned int IWDGRSTF : 1;
			unsigned int WWDGRSTF : 1;
			unsigned int LPWRRSTF : 1;
		} __attribute((__packed__));
	};
};
#define RCC_CSR (*((volatile struct RCC_CSR_s*) 0x40021024))

struct RCC_AHBRSTR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int : 5;
			unsigned int FMCRST : 1;
			unsigned int : 10;
			unsigned int IOPHRST : 1;
			unsigned int IOPARST : 1;
			unsigned int IOPBRST : 1;
			unsigned int IOPCRST : 1;
			unsigned int IOPDRST : 1;
			unsigned int IOPERST : 1;
			unsigned int IOPFRST : 1;
			unsigned int IOPGRST : 1;
			unsigned int TSCRST : 1;
			unsigned int : 3;
			unsigned int ADC12RST : 1;
			unsigned int ADC34RST : 1;
		} __attribute((__packed__));
	};
};
#define RCC_AHBRSTR (*((volatile struct RCC_AHBRSTR_s*) 0x40021028))

struct RCC_CFGR2_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int PREDIV : 4;
			unsigned int ADC12PRES : 5;
			unsigned int ADC34PRES : 5;
		} __attribute((__packed__));
	};
};
#define RCC_CFGR2 (*((volatile struct RCC_CFGR2_s*) 0x4002102c))

struct RCC_CFGR3_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int USART1SW : 2;
			unsigned int : 2;
			unsigned int I2C1SW : 1;
			unsigned int I2C2SW : 1;
			unsigned int I2C3SW : 1;
			unsigned int : 1;
			unsigned int TIM1SW : 1;
			unsigned int TIM8SW : 1;
			unsigned int : 6;
			unsigned int USART2SW : 2;
			unsigned int USART3SW : 2;
			unsigned int UART4SW : 2;
			unsigned int UART5SW : 2;
		} __attribute((__packed__));
	};
};
#define RCC_CFGR3 (*((volatile struct RCC_CFGR3_s*) 0x40021030))

// DMA1
struct DMA1_ISR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int GIF1 : 1;
			unsigned int TCIF1 : 1;
			unsigned int HTIF1 : 1;
			unsigned int TEIF1 : 1;
			unsigned int GIF2 : 1;
			unsigned int TCIF2 : 1;
			unsigned int HTIF2 : 1;
			unsigned int TEIF2 : 1;
			unsigned int GIF3 : 1;
			unsigned int TCIF3 : 1;
			unsigned int HTIF3 : 1;
			unsigned int TEIF3 : 1;
			unsigned int GIF4 : 1;
			unsigned int TCIF4 : 1;
			unsigned int HTIF4 : 1;
			unsigned int TEIF4 : 1;
			unsigned int GIF5 : 1;
			unsigned int TCIF5 : 1;
			unsigned int HTIF5 : 1;
			unsigned int TEIF5 : 1;
			unsigned int GIF6 : 1;
			unsigned int TCIF6 : 1;
			unsigned int HTIF6 : 1;
			unsigned int TEIF6 : 1;
			unsigned int GIF7 : 1;
			unsigned int TCIF7 : 1;
			unsigned int HTIF7 : 1;
			unsigned int TEIF7 : 1;
		} __attribute((__packed__));
	};
};
#define DMA1_ISR (*((volatile struct DMA1_ISR_s*) 0x40020000))

struct DMA1_IFCR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int CGIF1 : 1;
			unsigned int CTCIF1 : 1;
			unsigned int CHTIF1 : 1;
			unsigned int CTEIF1 : 1;
			unsigned int CGIF2 : 1;
			unsigned int CTCIF2 : 1;
			unsigned int CHTIF2 : 1;
			unsigned int CTEIF2 : 1;
			unsigned int CGIF3 : 1;
			unsigned int CTCIF3 : 1;
			unsigned int CHTIF3 : 1;
			unsigned int CTEIF3 : 1;
			unsigned int CGIF4 : 1;
			unsigned int CTCIF4 : 1;
			unsigned int CHTIF4 : 1;
			unsigned int CTEIF4 : 1;
			unsigned int CGIF5 : 1;
			unsigned int CTCIF5 : 1;
			unsigned int CHTIF5 : 1;
			unsigned int CTEIF5 : 1;
			unsigned int CGIF6 : 1;
			unsigned int CTCIF6 : 1;
			unsigned int CHTIF6 : 1;
			unsigned int CTEIF6 : 1;
			unsigned int CGIF7 : 1;
			unsigned int CTCIF7 : 1;
			unsigned int CHTIF7 : 1;
			unsigned int CTEIF7 : 1;
		} __attribute((__packed__));
	};
};
#define DMA1_IFCR (*((volatile struct DMA1_IFCR_s*) 0x40020004))

struct DMA1_CCR1_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int EN : 1;
			unsigned int TCIE : 1;
			unsigned int HTIE : 1;
			unsigned int TEIE : 1;
			unsigned int DIR : 1;
			unsigned int CIRC : 1;
			unsigned int PINC : 1;
			unsigned int MINC : 1;
			unsigned int PSIZE : 2;
			unsigned int MSIZE : 2;
			unsigned int PL : 2;
			unsigned int MEM2MEM : 1;
		} __attribute((__packed__));
	};
};
#define DMA1_CCR1 (*((volatile struct DMA1_CCR1_s*) 0x40020008))

struct DMA1_CNDTR1_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int NDT : 16;
		} __attribute((__packed__));
	};
};
#define DMA1_CNDTR1 (*((volatile struct DMA1_CNDTR1_s*) 0x4002000c))

struct DMA1_CPAR1_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int PA : 32;
		} __attribute((__packed__));
	};
};
#define DMA1_CPAR1 (*((volatile struct DMA1_CPAR1_s*) 0x40020010))

struct DMA1_CMAR1_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int MA : 32;
		} __attribute((__packed__));
	};
};
#define DMA1_CMAR1 (*((volatile struct DMA1_CMAR1_s*) 0x40020014))

struct DMA1_CCR2_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int EN : 1;
			unsigned int TCIE : 1;
			unsigned int HTIE : 1;
			unsigned int TEIE : 1;
			unsigned int DIR : 1;
			unsigned int CIRC : 1;
			unsigned int PINC : 1;
			unsigned int MINC : 1;
			unsigned int PSIZE : 2;
			unsigned int MSIZE : 2;
			unsigned int PL : 2;
			unsigned int MEM2MEM : 1;
		} __attribute((__packed__));
	};
};
#define DMA1_CCR2 (*((volatile struct DMA1_CCR2_s*) 0x4002001c))

struct DMA1_CNDTR2_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int NDT : 16;
		} __attribute((__packed__));
	};
};
#define DMA1_CNDTR2 (*((volatile struct DMA1_CNDTR2_s*) 0x40020020))

struct DMA1_CPAR2_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int PA : 32;
		} __attribute((__packed__));
	};
};
#define DMA1_CPAR2 (*((volatile struct DMA1_CPAR2_s*) 0x40020024))

struct DMA1_CMAR2_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int MA : 32;
		} __attribute((__packed__));
	};
};
#define DMA1_CMAR2 (*((volatile struct DMA1_CMAR2_s*) 0x40020028))

struct DMA1_CCR3_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int EN : 1;
			unsigned int TCIE : 1;
			unsigned int HTIE : 1;
			unsigned int TEIE : 1;
			unsigned int DIR : 1;
			unsigned int CIRC : 1;
			unsigned int PINC : 1;
			unsigned int MINC : 1;
			unsigned int PSIZE : 2;
			unsigned int MSIZE : 2;
			unsigned int PL : 2;
			unsigned int MEM2MEM : 1;
		} __attribute((__packed__));
	};
};
#define DMA1_CCR3 (*((volatile struct DMA1_CCR3_s*) 0x40020030))

struct DMA1_CNDTR3_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int NDT : 16;
		} __attribute((__packed__));
	};
};
#define DMA1_CNDTR3 (*((volatile struct DMA1_CNDTR3_s*) 0x40020034))

struct DMA1_CPAR3_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int PA : 32;
		} __attribute((__packed__));
	};
};
#define DMA1_CPAR3 (*((volatile struct DMA1_CPAR3_s*) 0x40020038))

struct DMA1_CMAR3_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int MA : 32;
		} __attribute((__packed__));
	};
};
#define DMA1_CMAR3 (*((volatile struct DMA1_CMAR3_s*) 0x4002003c))

struct DMA1_CCR4_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int EN : 1;
			unsigned int TCIE : 1;
			unsigned int HTIE : 1;
			unsigned int TEIE : 1;
			unsigned int DIR : 1;
			unsigned int CIRC : 1;
			unsigned int PINC : 1;
			unsigned int MINC : 1;
			unsigned int PSIZE : 2;
			unsigned int MSIZE : 2;
			unsigned int PL : 2;
			unsigned int MEM2MEM : 1;
		} __attribute((__packed__));
	};
};
#define DMA1_CCR4 (*((volatile struct DMA1_CCR4_s*) 0x40020044))

struct DMA1_CNDTR4_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int NDT : 16;
		} __attribute((__packed__));
	};
};
#define DMA1_CNDTR4 (*((volatile struct DMA1_CNDTR4_s*) 0x40020048))

struct DMA1_CPAR4_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int PA : 32;
		} __attribute((__packed__));
	};
};
#define DMA1_CPAR4 (*((volatile struct DMA1_CPAR4_s*) 0x4002004c))

struct DMA1_CMAR4_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int MA : 32;
		} __attribute((__packed__));
	};
};
#define DMA1_CMAR4 (*((volatile struct DMA1_CMAR4_s*) 0x40020050))

struct DMA1_CCR5_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int EN : 1;
			unsigned int TCIE : 1;
			unsigned int HTIE : 1;
			unsigned int TEIE : 1;
			unsigned int DIR : 1;
			unsigned int CIRC : 1;
			unsigned int PINC : 1;
			unsigned int MINC : 1;
			unsigned int PSIZE : 2;
			unsigned int MSIZE : 2;
			unsigned int PL : 2;
			unsigned int MEM2MEM : 1;
		} __attribute((__packed__));
	};
};
#define DMA1_CCR5 (*((volatile struct DMA1_CCR5_s*) 0x40020058))

struct DMA1_CNDTR5_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int NDT : 16;
		} __attribute((__packed__));
	};
};
#define DMA1_CNDTR5 (*((volatile struct DMA1_CNDTR5_s*) 0x4002005c))

struct DMA1_CPAR5_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int PA : 32;
		} __attribute((__packed__));
	};
};
#define DMA1_CPAR5 (*((volatile struct DMA1_CPAR5_s*) 0x40020060))

struct DMA1_CMAR5_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int MA : 32;
		} __attribute((__packed__));
	};
};
#define DMA1_CMAR5 (*((volatile struct DMA1_CMAR5_s*) 0x40020064))

struct DMA1_CCR6_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int EN : 1;
			unsigned int TCIE : 1;
			unsigned int HTIE : 1;
			unsigned int TEIE : 1;
			unsigned int DIR : 1;
			unsigned int CIRC : 1;
			unsigned int PINC : 1;
			unsigned int MINC : 1;
			unsigned int PSIZE : 2;
			unsigned int MSIZE : 2;
			unsigned int PL : 2;
			unsigned int MEM2MEM : 1;
		} __attribute((__packed__));
	};
};
#define DMA1_CCR6 (*((volatile struct DMA1_CCR6_s*) 0x4002006c))

struct DMA1_CNDTR6_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int NDT : 16;
		} __attribute((__packed__));
	};
};
#define DMA1_CNDTR6 (*((volatile struct DMA1_CNDTR6_s*) 0x40020070))

struct DMA1_CPAR6_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int PA : 32;
		} __attribute((__packed__));
	};
};
#define DMA1_CPAR6 (*((volatile struct DMA1_CPAR6_s*) 0x40020074))

struct DMA1_CMAR6_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int MA : 32;
		} __attribute((__packed__));
	};
};
#define DMA1_CMAR6 (*((volatile struct DMA1_CMAR6_s*) 0x40020078))

struct DMA1_CCR7_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int EN : 1;
			unsigned int TCIE : 1;
			unsigned int HTIE : 1;
			unsigned int TEIE : 1;
			unsigned int DIR : 1;
			unsigned int CIRC : 1;
			unsigned int PINC : 1;
			unsigned int MINC : 1;
			unsigned int PSIZE : 2;
			unsigned int MSIZE : 2;
			unsigned int PL : 2;
			unsigned int MEM2MEM : 1;
		} __attribute((__packed__));
	};
};
#define DMA1_CCR7 (*((volatile struct DMA1_CCR7_s*) 0x40020080))

struct DMA1_CNDTR7_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int NDT : 16;
		} __attribute((__packed__));
	};
};
#define DMA1_CNDTR7 (*((volatile struct DMA1_CNDTR7_s*) 0x40020084))

struct DMA1_CPAR7_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int PA : 32;
		} __attribute((__packed__));
	};
};
#define DMA1_CPAR7 (*((volatile struct DMA1_CPAR7_s*) 0x40020088))

struct DMA1_CMAR7_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int MA : 32;
		} __attribute((__packed__));
	};
};
#define DMA1_CMAR7 (*((volatile struct DMA1_CMAR7_s*) 0x4002008c))

// DMA2
struct DMA2_ISR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int GIF1 : 1;
			unsigned int TCIF1 : 1;
			unsigned int HTIF1 : 1;
			unsigned int TEIF1 : 1;
			unsigned int GIF2 : 1;
			unsigned int TCIF2 : 1;
			unsigned int HTIF2 : 1;
			unsigned int TEIF2 : 1;
			unsigned int GIF3 : 1;
			unsigned int TCIF3 : 1;
			unsigned int HTIF3 : 1;
			unsigned int TEIF3 : 1;
			unsigned int GIF4 : 1;
			unsigned int TCIF4 : 1;
			unsigned int HTIF4 : 1;
			unsigned int TEIF4 : 1;
			unsigned int GIF5 : 1;
			unsigned int TCIF5 : 1;
			unsigned int HTIF5 : 1;
			unsigned int TEIF5 : 1;
			unsigned int GIF6 : 1;
			unsigned int TCIF6 : 1;
			unsigned int HTIF6 : 1;
			unsigned int TEIF6 : 1;
			unsigned int GIF7 : 1;
			unsigned int TCIF7 : 1;
			unsigned int HTIF7 : 1;
			unsigned int TEIF7 : 1;
		} __attribute((__packed__));
	};
};
#define DMA2_ISR (*((volatile struct DMA2_ISR_s*) 0x40020400))

struct DMA2_IFCR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int CGIF1 : 1;
			unsigned int CTCIF1 : 1;
			unsigned int CHTIF1 : 1;
			unsigned int CTEIF1 : 1;
			unsigned int CGIF2 : 1;
			unsigned int CTCIF2 : 1;
			unsigned int CHTIF2 : 1;
			unsigned int CTEIF2 : 1;
			unsigned int CGIF3 : 1;
			unsigned int CTCIF3 : 1;
			unsigned int CHTIF3 : 1;
			unsigned int CTEIF3 : 1;
			unsigned int CGIF4 : 1;
			unsigned int CTCIF4 : 1;
			unsigned int CHTIF4 : 1;
			unsigned int CTEIF4 : 1;
			unsigned int CGIF5 : 1;
			unsigned int CTCIF5 : 1;
			unsigned int CHTIF5 : 1;
			unsigned int CTEIF5 : 1;
			unsigned int CGIF6 : 1;
			unsigned int CTCIF6 : 1;
			unsigned int CHTIF6 : 1;
			unsigned int CTEIF6 : 1;
			unsigned int CGIF7 : 1;
			unsigned int CTCIF7 : 1;
			unsigned int CHTIF7 : 1;
			unsigned int CTEIF7 : 1;
		} __attribute((__packed__));
	};
};
#define DMA2_IFCR (*((volatile struct DMA2_IFCR_s*) 0x40020404))

struct DMA2_CCR1_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int EN : 1;
			unsigned int TCIE : 1;
			unsigned int HTIE : 1;
			unsigned int TEIE : 1;
			unsigned int DIR : 1;
			unsigned int CIRC : 1;
			unsigned int PINC : 1;
			unsigned int MINC : 1;
			unsigned int PSIZE : 2;
			unsigned int MSIZE : 2;
			unsigned int PL : 2;
			unsigned int MEM2MEM : 1;
		} __attribute((__packed__));
	};
};
#define DMA2_CCR1 (*((volatile struct DMA2_CCR1_s*) 0x40020408))

struct DMA2_CNDTR1_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int NDT : 16;
		} __attribute((__packed__));
	};
};
#define DMA2_CNDTR1 (*((volatile struct DMA2_CNDTR1_s*) 0x4002040c))

struct DMA2_CPAR1_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int PA : 32;
		} __attribute((__packed__));
	};
};
#define DMA2_CPAR1 (*((volatile struct DMA2_CPAR1_s*) 0x40020410))

struct DMA2_CMAR1_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int MA : 32;
		} __attribute((__packed__));
	};
};
#define DMA2_CMAR1 (*((volatile struct DMA2_CMAR1_s*) 0x40020414))

struct DMA2_CCR2_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int EN : 1;
			unsigned int TCIE : 1;
			unsigned int HTIE : 1;
			unsigned int TEIE : 1;
			unsigned int DIR : 1;
			unsigned int CIRC : 1;
			unsigned int PINC : 1;
			unsigned int MINC : 1;
			unsigned int PSIZE : 2;
			unsigned int MSIZE : 2;
			unsigned int PL : 2;
			unsigned int MEM2MEM : 1;
		} __attribute((__packed__));
	};
};
#define DMA2_CCR2 (*((volatile struct DMA2_CCR2_s*) 0x4002041c))

struct DMA2_CNDTR2_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int NDT : 16;
		} __attribute((__packed__));
	};
};
#define DMA2_CNDTR2 (*((volatile struct DMA2_CNDTR2_s*) 0x40020420))

struct DMA2_CPAR2_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int PA : 32;
		} __attribute((__packed__));
	};
};
#define DMA2_CPAR2 (*((volatile struct DMA2_CPAR2_s*) 0x40020424))

struct DMA2_CMAR2_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int MA : 32;
		} __attribute((__packed__));
	};
};
#define DMA2_CMAR2 (*((volatile struct DMA2_CMAR2_s*) 0x40020428))

struct DMA2_CCR3_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int EN : 1;
			unsigned int TCIE : 1;
			unsigned int HTIE : 1;
			unsigned int TEIE : 1;
			unsigned int DIR : 1;
			unsigned int CIRC : 1;
			unsigned int PINC : 1;
			unsigned int MINC : 1;
			unsigned int PSIZE : 2;
			unsigned int MSIZE : 2;
			unsigned int PL : 2;
			unsigned int MEM2MEM : 1;
		} __attribute((__packed__));
	};
};
#define DMA2_CCR3 (*((volatile struct DMA2_CCR3_s*) 0x40020430))

struct DMA2_CNDTR3_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int NDT : 16;
		} __attribute((__packed__));
	};
};
#define DMA2_CNDTR3 (*((volatile struct DMA2_CNDTR3_s*) 0x40020434))

struct DMA2_CPAR3_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int PA : 32;
		} __attribute((__packed__));
	};
};
#define DMA2_CPAR3 (*((volatile struct DMA2_CPAR3_s*) 0x40020438))

struct DMA2_CMAR3_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int MA : 32;
		} __attribute((__packed__));
	};
};
#define DMA2_CMAR3 (*((volatile struct DMA2_CMAR3_s*) 0x4002043c))

struct DMA2_CCR4_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int EN : 1;
			unsigned int TCIE : 1;
			unsigned int HTIE : 1;
			unsigned int TEIE : 1;
			unsigned int DIR : 1;
			unsigned int CIRC : 1;
			unsigned int PINC : 1;
			unsigned int MINC : 1;
			unsigned int PSIZE : 2;
			unsigned int MSIZE : 2;
			unsigned int PL : 2;
			unsigned int MEM2MEM : 1;
		} __attribute((__packed__));
	};
};
#define DMA2_CCR4 (*((volatile struct DMA2_CCR4_s*) 0x40020444))

struct DMA2_CNDTR4_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int NDT : 16;
		} __attribute((__packed__));
	};
};
#define DMA2_CNDTR4 (*((volatile struct DMA2_CNDTR4_s*) 0x40020448))

struct DMA2_CPAR4_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int PA : 32;
		} __attribute((__packed__));
	};
};
#define DMA2_CPAR4 (*((volatile struct DMA2_CPAR4_s*) 0x4002044c))

struct DMA2_CMAR4_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int MA : 32;
		} __attribute((__packed__));
	};
};
#define DMA2_CMAR4 (*((volatile struct DMA2_CMAR4_s*) 0x40020450))

struct DMA2_CCR5_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int EN : 1;
			unsigned int TCIE : 1;
			unsigned int HTIE : 1;
			unsigned int TEIE : 1;
			unsigned int DIR : 1;
			unsigned int CIRC : 1;
			unsigned int PINC : 1;
			unsigned int MINC : 1;
			unsigned int PSIZE : 2;
			unsigned int MSIZE : 2;
			unsigned int PL : 2;
			unsigned int MEM2MEM : 1;
		} __attribute((__packed__));
	};
};
#define DMA2_CCR5 (*((volatile struct DMA2_CCR5_s*) 0x40020458))

struct DMA2_CNDTR5_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int NDT : 16;
		} __attribute((__packed__));
	};
};
#define DMA2_CNDTR5 (*((volatile struct DMA2_CNDTR5_s*) 0x4002045c))

struct DMA2_CPAR5_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int PA : 32;
		} __attribute((__packed__));
	};
};
#define DMA2_CPAR5 (*((volatile struct DMA2_CPAR5_s*) 0x40020460))

struct DMA2_CMAR5_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int MA : 32;
		} __attribute((__packed__));
	};
};
#define DMA2_CMAR5 (*((volatile struct DMA2_CMAR5_s*) 0x40020464))

struct DMA2_CCR6_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int EN : 1;
			unsigned int TCIE : 1;
			unsigned int HTIE : 1;
			unsigned int TEIE : 1;
			unsigned int DIR : 1;
			unsigned int CIRC : 1;
			unsigned int PINC : 1;
			unsigned int MINC : 1;
			unsigned int PSIZE : 2;
			unsigned int MSIZE : 2;
			unsigned int PL : 2;
			unsigned int MEM2MEM : 1;
		} __attribute((__packed__));
	};
};
#define DMA2_CCR6 (*((volatile struct DMA2_CCR6_s*) 0x4002046c))

struct DMA2_CNDTR6_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int NDT : 16;
		} __attribute((__packed__));
	};
};
#define DMA2_CNDTR6 (*((volatile struct DMA2_CNDTR6_s*) 0x40020470))

struct DMA2_CPAR6_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int PA : 32;
		} __attribute((__packed__));
	};
};
#define DMA2_CPAR6 (*((volatile struct DMA2_CPAR6_s*) 0x40020474))

struct DMA2_CMAR6_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int MA : 32;
		} __attribute((__packed__));
	};
};
#define DMA2_CMAR6 (*((volatile struct DMA2_CMAR6_s*) 0x40020478))

struct DMA2_CCR7_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int EN : 1;
			unsigned int TCIE : 1;
			unsigned int HTIE : 1;
			unsigned int TEIE : 1;
			unsigned int DIR : 1;
			unsigned int CIRC : 1;
			unsigned int PINC : 1;
			unsigned int MINC : 1;
			unsigned int PSIZE : 2;
			unsigned int MSIZE : 2;
			unsigned int PL : 2;
			unsigned int MEM2MEM : 1;
		} __attribute((__packed__));
	};
};
#define DMA2_CCR7 (*((volatile struct DMA2_CCR7_s*) 0x40020480))

struct DMA2_CNDTR7_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int NDT : 16;
		} __attribute((__packed__));
	};
};
#define DMA2_CNDTR7 (*((volatile struct DMA2_CNDTR7_s*) 0x40020484))

struct DMA2_CPAR7_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int PA : 32;
		} __attribute((__packed__));
	};
};
#define DMA2_CPAR7 (*((volatile struct DMA2_CPAR7_s*) 0x40020488))

struct DMA2_CMAR7_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int MA : 32;
		} __attribute((__packed__));
	};
};
#define DMA2_CMAR7 (*((volatile struct DMA2_CMAR7_s*) 0x4002048c))

// TIM2
struct TIM2_CR1_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int CEN : 1;
			unsigned int UDIS : 1;
			unsigned int URS : 1;
			unsigned int OPM : 1;
			unsigned int DIR : 1;
			unsigned int CMS : 2;
			unsigned int ARPE : 1;
			unsigned int CKD : 2;
			unsigned int : 1;
			unsigned int UIFREMAP : 1;
		} __attribute((__packed__));
	};
};
#define TIM2_CR1 (*((volatile struct TIM2_CR1_s*) 0x40000000))

struct TIM2_CR2_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int : 3;
			unsigned int CCDS : 1;
			unsigned int MMS : 3;
			unsigned int TI1S : 1;
		} __attribute((__packed__));
	};
};
#define TIM2_CR2 (*((volatile struct TIM2_CR2_s*) 0x40000004))

struct TIM2_SMCR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int SMS : 3;
			unsigned int OCCS : 1;
			unsigned int TS : 3;
			unsigned int MSM : 1;
			unsigned int ETF : 4;
			unsigned int ETPS : 2;
			unsigned int ECE : 1;
			unsigned int ETP : 1;
			unsigned int SMS_3 : 1;
		} __attribute((__packed__));
	};
};
#define TIM2_SMCR (*((volatile struct TIM2_SMCR_s*) 0x40000008))

struct TIM2_DIER_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int UIE : 1;
			unsigned int CC1IE : 1;
			unsigned int CC2IE : 1;
			unsigned int CC3IE : 1;
			unsigned int CC4IE : 1;
			unsigned int : 1;
			unsigned int TIE : 1;
			unsigned int : 1;
			unsigned int UDE : 1;
			unsigned int CC1DE : 1;
			unsigned int CC2DE : 1;
			unsigned int CC3DE : 1;
			unsigned int CC4DE : 1;
			unsigned int : 1;
			unsigned int TDE : 1;
		} __attribute((__packed__));
	};
};
#define TIM2_DIER (*((volatile struct TIM2_DIER_s*) 0x4000000c))

struct TIM2_SR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int UIF : 1;
			unsigned int CC1IF : 1;
			unsigned int CC2IF : 1;
			unsigned int CC3IF : 1;
			unsigned int CC4IF : 1;
			unsigned int : 1;
			unsigned int TIF : 1;
			unsigned int : 2;
			unsigned int CC1OF : 1;
			unsigned int CC2OF : 1;
			unsigned int CC3OF : 1;
			unsigned int CC4OF : 1;
		} __attribute((__packed__));
	};
};
#define TIM2_SR (*((volatile struct TIM2_SR_s*) 0x40000010))

struct TIM2_EGR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int UG : 1;
			unsigned int CC1G : 1;
			unsigned int CC2G : 1;
			unsigned int CC3G : 1;
			unsigned int CC4G : 1;
			unsigned int : 1;
			unsigned int TG : 1;
		} __attribute((__packed__));
	};
};
#define TIM2_EGR (*((volatile struct TIM2_EGR_s*) 0x40000014))

struct TIM2_CCMR1_Output_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int CC1S : 2;
			unsigned int OC1FE : 1;
			unsigned int OC1PE : 1;
			unsigned int OC1M : 3;
			unsigned int OC1CE : 1;
			unsigned int CC2S : 2;
			unsigned int OC2FE : 1;
			unsigned int OC2PE : 1;
			unsigned int OC2M : 3;
			unsigned int OC2CE : 1;
			unsigned int OC1M_3 : 1;
			unsigned int : 7;
			unsigned int OC2M_3 : 1;
		} __attribute((__packed__));
	};
};
#define TIM2_CCMR1_Output (*((volatile struct TIM2_CCMR1_Output_s*) 0x40000018))

struct TIM2_CCMR1_Input_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int CC1S : 2;
			unsigned int IC1PSC : 2;
			unsigned int IC1F : 4;
			unsigned int CC2S : 2;
			unsigned int IC2PSC : 2;
			unsigned int IC2F : 4;
		} __attribute((__packed__));
	};
};
#define TIM2_CCMR1_Input (*((volatile struct TIM2_CCMR1_Input_s*) 0x40000018))

struct TIM2_CCMR2_Output_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int CC3S : 2;
			unsigned int OC3FE : 1;
			unsigned int OC3PE : 1;
			unsigned int OC3M : 3;
			unsigned int OC3CE : 1;
			unsigned int CC4S : 2;
			unsigned int OC4FE : 1;
			unsigned int OC4PE : 1;
			unsigned int OC4M : 3;
			unsigned int O24CE : 1;
			unsigned int OC3M_3 : 1;
			unsigned int : 7;
			unsigned int OC4M_3 : 1;
		} __attribute((__packed__));
	};
};
#define TIM2_CCMR2_Output (*((volatile struct TIM2_CCMR2_Output_s*) 0x4000001c))

struct TIM2_CCMR2_Input_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int CC3S : 2;
			unsigned int IC3PSC : 2;
			unsigned int IC3F : 4;
			unsigned int CC4S : 2;
			unsigned int IC4PSC : 2;
			unsigned int IC4F : 4;
		} __attribute((__packed__));
	};
};
#define TIM2_CCMR2_Input (*((volatile struct TIM2_CCMR2_Input_s*) 0x4000001c))

struct TIM2_CCER_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int CC1E : 1;
			unsigned int CC1P : 1;
			unsigned int : 1;
			unsigned int CC1NP : 1;
			unsigned int CC2E : 1;
			unsigned int CC2P : 1;
			unsigned int : 1;
			unsigned int CC2NP : 1;
			unsigned int CC3E : 1;
			unsigned int CC3P : 1;
			unsigned int : 1;
			unsigned int CC3NP : 1;
			unsigned int CC4E : 1;
			unsigned int CC4P : 1;
			unsigned int : 1;
			unsigned int CC4NP : 1;
		} __attribute((__packed__));
	};
};
#define TIM2_CCER (*((volatile struct TIM2_CCER_s*) 0x40000020))

struct TIM2_CNT_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int CNTL : 16;
			unsigned int CNTH : 15;
			unsigned int CNT_or_UIFCPY : 1;
		} __attribute((__packed__));
	};
};
#define TIM2_CNT (*((volatile struct TIM2_CNT_s*) 0x40000024))

struct TIM2_PSC_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int PSC : 16;
		} __attribute((__packed__));
	};
};
#define TIM2_PSC (*((volatile struct TIM2_PSC_s*) 0x40000028))

struct TIM2_ARR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int ARRL : 16;
			unsigned int ARRH : 16;
		} __attribute((__packed__));
	};
};
#define TIM2_ARR (*((volatile struct TIM2_ARR_s*) 0x4000002c))

struct TIM2_CCR1_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int CCR1L : 16;
			unsigned int CCR1H : 16;
		} __attribute((__packed__));
	};
};
#define TIM2_CCR1 (*((volatile struct TIM2_CCR1_s*) 0x40000034))

struct TIM2_CCR2_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int CCR2L : 16;
			unsigned int CCR2H : 16;
		} __attribute((__packed__));
	};
};
#define TIM2_CCR2 (*((volatile struct TIM2_CCR2_s*) 0x40000038))

struct TIM2_CCR3_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int CCR3L : 16;
			unsigned int CCR3H : 16;
		} __attribute((__packed__));
	};
};
#define TIM2_CCR3 (*((volatile struct TIM2_CCR3_s*) 0x4000003c))

struct TIM2_CCR4_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int CCR4L : 16;
			unsigned int CCR4H : 16;
		} __attribute((__packed__));
	};
};
#define TIM2_CCR4 (*((volatile struct TIM2_CCR4_s*) 0x40000040))

struct TIM2_DCR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int DBA : 5;
			unsigned int : 3;
			unsigned int DBL : 5;
		} __attribute((__packed__));
	};
};
#define TIM2_DCR (*((volatile struct TIM2_DCR_s*) 0x40000048))

struct TIM2_DMAR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int DMAB : 16;
		} __attribute((__packed__));
	};
};
#define TIM2_DMAR (*((volatile struct TIM2_DMAR_s*) 0x4000004c))

// TIM3
struct TIM3_CR1_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int CEN : 1;
			unsigned int UDIS : 1;
			unsigned int URS : 1;
			unsigned int OPM : 1;
			unsigned int DIR : 1;
			unsigned int CMS : 2;
			unsigned int ARPE : 1;
			unsigned int CKD : 2;
			unsigned int : 1;
			unsigned int UIFREMAP : 1;
		} __attribute((__packed__));
	};
};
#define TIM3_CR1 (*((volatile struct TIM3_CR1_s*) 0x40000400))

struct TIM3_CR2_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int : 3;
			unsigned int CCDS : 1;
			unsigned int MMS : 3;
			unsigned int TI1S : 1;
		} __attribute((__packed__));
	};
};
#define TIM3_CR2 (*((volatile struct TIM3_CR2_s*) 0x40000404))

struct TIM3_SMCR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int SMS : 3;
			unsigned int OCCS : 1;
			unsigned int TS : 3;
			unsigned int MSM : 1;
			unsigned int ETF : 4;
			unsigned int ETPS : 2;
			unsigned int ECE : 1;
			unsigned int ETP : 1;
			unsigned int SMS_3 : 1;
		} __attribute((__packed__));
	};
};
#define TIM3_SMCR (*((volatile struct TIM3_SMCR_s*) 0x40000408))

struct TIM3_DIER_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int UIE : 1;
			unsigned int CC1IE : 1;
			unsigned int CC2IE : 1;
			unsigned int CC3IE : 1;
			unsigned int CC4IE : 1;
			unsigned int : 1;
			unsigned int TIE : 1;
			unsigned int : 1;
			unsigned int UDE : 1;
			unsigned int CC1DE : 1;
			unsigned int CC2DE : 1;
			unsigned int CC3DE : 1;
			unsigned int CC4DE : 1;
			unsigned int : 1;
			unsigned int TDE : 1;
		} __attribute((__packed__));
	};
};
#define TIM3_DIER (*((volatile struct TIM3_DIER_s*) 0x4000040c))

struct TIM3_SR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int UIF : 1;
			unsigned int CC1IF : 1;
			unsigned int CC2IF : 1;
			unsigned int CC3IF : 1;
			unsigned int CC4IF : 1;
			unsigned int : 1;
			unsigned int TIF : 1;
			unsigned int : 2;
			unsigned int CC1OF : 1;
			unsigned int CC2OF : 1;
			unsigned int CC3OF : 1;
			unsigned int CC4OF : 1;
		} __attribute((__packed__));
	};
};
#define TIM3_SR (*((volatile struct TIM3_SR_s*) 0x40000410))

struct TIM3_EGR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int UG : 1;
			unsigned int CC1G : 1;
			unsigned int CC2G : 1;
			unsigned int CC3G : 1;
			unsigned int CC4G : 1;
			unsigned int : 1;
			unsigned int TG : 1;
		} __attribute((__packed__));
	};
};
#define TIM3_EGR (*((volatile struct TIM3_EGR_s*) 0x40000414))

struct TIM3_CCMR1_Output_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int CC1S : 2;
			unsigned int OC1FE : 1;
			unsigned int OC1PE : 1;
			unsigned int OC1M : 3;
			unsigned int OC1CE : 1;
			unsigned int CC2S : 2;
			unsigned int OC2FE : 1;
			unsigned int OC2PE : 1;
			unsigned int OC2M : 3;
			unsigned int OC2CE : 1;
			unsigned int OC1M_3 : 1;
			unsigned int : 7;
			unsigned int OC2M_3 : 1;
		} __attribute((__packed__));
	};
};
#define TIM3_CCMR1_Output (*((volatile struct TIM3_CCMR1_Output_s*) 0x40000418))

struct TIM3_CCMR1_Input_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int CC1S : 2;
			unsigned int IC1PSC : 2;
			unsigned int IC1F : 4;
			unsigned int CC2S : 2;
			unsigned int IC2PSC : 2;
			unsigned int IC2F : 4;
		} __attribute((__packed__));
	};
};
#define TIM3_CCMR1_Input (*((volatile struct TIM3_CCMR1_Input_s*) 0x40000418))

struct TIM3_CCMR2_Output_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int CC3S : 2;
			unsigned int OC3FE : 1;
			unsigned int OC3PE : 1;
			unsigned int OC3M : 3;
			unsigned int OC3CE : 1;
			unsigned int CC4S : 2;
			unsigned int OC4FE : 1;
			unsigned int OC4PE : 1;
			unsigned int OC4M : 3;
			unsigned int O24CE : 1;
			unsigned int OC3M_3 : 1;
			unsigned int : 7;
			unsigned int OC4M_3 : 1;
		} __attribute((__packed__));
	};
};
#define TIM3_CCMR2_Output (*((volatile struct TIM3_CCMR2_Output_s*) 0x4000041c))

struct TIM3_CCMR2_Input_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int CC3S : 2;
			unsigned int IC3PSC : 2;
			unsigned int IC3F : 4;
			unsigned int CC4S : 2;
			unsigned int IC4PSC : 2;
			unsigned int IC4F : 4;
		} __attribute((__packed__));
	};
};
#define TIM3_CCMR2_Input (*((volatile struct TIM3_CCMR2_Input_s*) 0x4000041c))

struct TIM3_CCER_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int CC1E : 1;
			unsigned int CC1P : 1;
			unsigned int : 1;
			unsigned int CC1NP : 1;
			unsigned int CC2E : 1;
			unsigned int CC2P : 1;
			unsigned int : 1;
			unsigned int CC2NP : 1;
			unsigned int CC3E : 1;
			unsigned int CC3P : 1;
			unsigned int : 1;
			unsigned int CC3NP : 1;
			unsigned int CC4E : 1;
			unsigned int CC4P : 1;
			unsigned int : 1;
			unsigned int CC4NP : 1;
		} __attribute((__packed__));
	};
};
#define TIM3_CCER (*((volatile struct TIM3_CCER_s*) 0x40000420))

struct TIM3_CNT_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int CNTL : 16;
			unsigned int CNTH : 15;
			unsigned int CNT_or_UIFCPY : 1;
		} __attribute((__packed__));
	};
};
#define TIM3_CNT (*((volatile struct TIM3_CNT_s*) 0x40000424))

struct TIM3_PSC_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int PSC : 16;
		} __attribute((__packed__));
	};
};
#define TIM3_PSC (*((volatile struct TIM3_PSC_s*) 0x40000428))

struct TIM3_ARR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int ARRL : 16;
			unsigned int ARRH : 16;
		} __attribute((__packed__));
	};
};
#define TIM3_ARR (*((volatile struct TIM3_ARR_s*) 0x4000042c))

struct TIM3_CCR1_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int CCR1L : 16;
			unsigned int CCR1H : 16;
		} __attribute((__packed__));
	};
};
#define TIM3_CCR1 (*((volatile struct TIM3_CCR1_s*) 0x40000434))

struct TIM3_CCR2_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int CCR2L : 16;
			unsigned int CCR2H : 16;
		} __attribute((__packed__));
	};
};
#define TIM3_CCR2 (*((volatile struct TIM3_CCR2_s*) 0x40000438))

struct TIM3_CCR3_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int CCR3L : 16;
			unsigned int CCR3H : 16;
		} __attribute((__packed__));
	};
};
#define TIM3_CCR3 (*((volatile struct TIM3_CCR3_s*) 0x4000043c))

struct TIM3_CCR4_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int CCR4L : 16;
			unsigned int CCR4H : 16;
		} __attribute((__packed__));
	};
};
#define TIM3_CCR4 (*((volatile struct TIM3_CCR4_s*) 0x40000440))

struct TIM3_DCR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int DBA : 5;
			unsigned int : 3;
			unsigned int DBL : 5;
		} __attribute((__packed__));
	};
};
#define TIM3_DCR (*((volatile struct TIM3_DCR_s*) 0x40000448))

struct TIM3_DMAR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int DMAB : 16;
		} __attribute((__packed__));
	};
};
#define TIM3_DMAR (*((volatile struct TIM3_DMAR_s*) 0x4000044c))

// TIM4
struct TIM4_CR1_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int CEN : 1;
			unsigned int UDIS : 1;
			unsigned int URS : 1;
			unsigned int OPM : 1;
			unsigned int DIR : 1;
			unsigned int CMS : 2;
			unsigned int ARPE : 1;
			unsigned int CKD : 2;
			unsigned int : 1;
			unsigned int UIFREMAP : 1;
		} __attribute((__packed__));
	};
};
#define TIM4_CR1 (*((volatile struct TIM4_CR1_s*) 0x40000800))

struct TIM4_CR2_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int : 3;
			unsigned int CCDS : 1;
			unsigned int MMS : 3;
			unsigned int TI1S : 1;
		} __attribute((__packed__));
	};
};
#define TIM4_CR2 (*((volatile struct TIM4_CR2_s*) 0x40000804))

struct TIM4_SMCR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int SMS : 3;
			unsigned int OCCS : 1;
			unsigned int TS : 3;
			unsigned int MSM : 1;
			unsigned int ETF : 4;
			unsigned int ETPS : 2;
			unsigned int ECE : 1;
			unsigned int ETP : 1;
			unsigned int SMS_3 : 1;
		} __attribute((__packed__));
	};
};
#define TIM4_SMCR (*((volatile struct TIM4_SMCR_s*) 0x40000808))

struct TIM4_DIER_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int UIE : 1;
			unsigned int CC1IE : 1;
			unsigned int CC2IE : 1;
			unsigned int CC3IE : 1;
			unsigned int CC4IE : 1;
			unsigned int : 1;
			unsigned int TIE : 1;
			unsigned int : 1;
			unsigned int UDE : 1;
			unsigned int CC1DE : 1;
			unsigned int CC2DE : 1;
			unsigned int CC3DE : 1;
			unsigned int CC4DE : 1;
			unsigned int : 1;
			unsigned int TDE : 1;
		} __attribute((__packed__));
	};
};
#define TIM4_DIER (*((volatile struct TIM4_DIER_s*) 0x4000080c))

struct TIM4_SR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int UIF : 1;
			unsigned int CC1IF : 1;
			unsigned int CC2IF : 1;
			unsigned int CC3IF : 1;
			unsigned int CC4IF : 1;
			unsigned int : 1;
			unsigned int TIF : 1;
			unsigned int : 2;
			unsigned int CC1OF : 1;
			unsigned int CC2OF : 1;
			unsigned int CC3OF : 1;
			unsigned int CC4OF : 1;
		} __attribute((__packed__));
	};
};
#define TIM4_SR (*((volatile struct TIM4_SR_s*) 0x40000810))

struct TIM4_EGR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int UG : 1;
			unsigned int CC1G : 1;
			unsigned int CC2G : 1;
			unsigned int CC3G : 1;
			unsigned int CC4G : 1;
			unsigned int : 1;
			unsigned int TG : 1;
		} __attribute((__packed__));
	};
};
#define TIM4_EGR (*((volatile struct TIM4_EGR_s*) 0x40000814))

struct TIM4_CCMR1_Output_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int CC1S : 2;
			unsigned int OC1FE : 1;
			unsigned int OC1PE : 1;
			unsigned int OC1M : 3;
			unsigned int OC1CE : 1;
			unsigned int CC2S : 2;
			unsigned int OC2FE : 1;
			unsigned int OC2PE : 1;
			unsigned int OC2M : 3;
			unsigned int OC2CE : 1;
			unsigned int OC1M_3 : 1;
			unsigned int : 7;
			unsigned int OC2M_3 : 1;
		} __attribute((__packed__));
	};
};
#define TIM4_CCMR1_Output (*((volatile struct TIM4_CCMR1_Output_s*) 0x40000818))

struct TIM4_CCMR1_Input_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int CC1S : 2;
			unsigned int IC1PSC : 2;
			unsigned int IC1F : 4;
			unsigned int CC2S : 2;
			unsigned int IC2PSC : 2;
			unsigned int IC2F : 4;
		} __attribute((__packed__));
	};
};
#define TIM4_CCMR1_Input (*((volatile struct TIM4_CCMR1_Input_s*) 0x40000818))

struct TIM4_CCMR2_Output_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int CC3S : 2;
			unsigned int OC3FE : 1;
			unsigned int OC3PE : 1;
			unsigned int OC3M : 3;
			unsigned int OC3CE : 1;
			unsigned int CC4S : 2;
			unsigned int OC4FE : 1;
			unsigned int OC4PE : 1;
			unsigned int OC4M : 3;
			unsigned int O24CE : 1;
			unsigned int OC3M_3 : 1;
			unsigned int : 7;
			unsigned int OC4M_3 : 1;
		} __attribute((__packed__));
	};
};
#define TIM4_CCMR2_Output (*((volatile struct TIM4_CCMR2_Output_s*) 0x4000081c))

struct TIM4_CCMR2_Input_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int CC3S : 2;
			unsigned int IC3PSC : 2;
			unsigned int IC3F : 4;
			unsigned int CC4S : 2;
			unsigned int IC4PSC : 2;
			unsigned int IC4F : 4;
		} __attribute((__packed__));
	};
};
#define TIM4_CCMR2_Input (*((volatile struct TIM4_CCMR2_Input_s*) 0x4000081c))

struct TIM4_CCER_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int CC1E : 1;
			unsigned int CC1P : 1;
			unsigned int : 1;
			unsigned int CC1NP : 1;
			unsigned int CC2E : 1;
			unsigned int CC2P : 1;
			unsigned int : 1;
			unsigned int CC2NP : 1;
			unsigned int CC3E : 1;
			unsigned int CC3P : 1;
			unsigned int : 1;
			unsigned int CC3NP : 1;
			unsigned int CC4E : 1;
			unsigned int CC4P : 1;
			unsigned int : 1;
			unsigned int CC4NP : 1;
		} __attribute((__packed__));
	};
};
#define TIM4_CCER (*((volatile struct TIM4_CCER_s*) 0x40000820))

struct TIM4_CNT_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int CNTL : 16;
			unsigned int CNTH : 15;
			unsigned int CNT_or_UIFCPY : 1;
		} __attribute((__packed__));
	};
};
#define TIM4_CNT (*((volatile struct TIM4_CNT_s*) 0x40000824))

struct TIM4_PSC_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int PSC : 16;
		} __attribute((__packed__));
	};
};
#define TIM4_PSC (*((volatile struct TIM4_PSC_s*) 0x40000828))

struct TIM4_ARR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int ARRL : 16;
			unsigned int ARRH : 16;
		} __attribute((__packed__));
	};
};
#define TIM4_ARR (*((volatile struct TIM4_ARR_s*) 0x4000082c))

struct TIM4_CCR1_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int CCR1L : 16;
			unsigned int CCR1H : 16;
		} __attribute((__packed__));
	};
};
#define TIM4_CCR1 (*((volatile struct TIM4_CCR1_s*) 0x40000834))

struct TIM4_CCR2_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int CCR2L : 16;
			unsigned int CCR2H : 16;
		} __attribute((__packed__));
	};
};
#define TIM4_CCR2 (*((volatile struct TIM4_CCR2_s*) 0x40000838))

struct TIM4_CCR3_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int CCR3L : 16;
			unsigned int CCR3H : 16;
		} __attribute((__packed__));
	};
};
#define TIM4_CCR3 (*((volatile struct TIM4_CCR3_s*) 0x4000083c))

struct TIM4_CCR4_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int CCR4L : 16;
			unsigned int CCR4H : 16;
		} __attribute((__packed__));
	};
};
#define TIM4_CCR4 (*((volatile struct TIM4_CCR4_s*) 0x40000840))

struct TIM4_DCR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int DBA : 5;
			unsigned int : 3;
			unsigned int DBL : 5;
		} __attribute((__packed__));
	};
};
#define TIM4_DCR (*((volatile struct TIM4_DCR_s*) 0x40000848))

struct TIM4_DMAR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int DMAB : 16;
		} __attribute((__packed__));
	};
};
#define TIM4_DMAR (*((volatile struct TIM4_DMAR_s*) 0x4000084c))

// TIM15
struct TIM15_CR1_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int CEN : 1;
			unsigned int UDIS : 1;
			unsigned int URS : 1;
			unsigned int OPM : 1;
			unsigned int : 3;
			unsigned int ARPE : 1;
			unsigned int CKD : 2;
			unsigned int : 1;
			unsigned int UIFREMAP : 1;
		} __attribute((__packed__));
	};
};
#define TIM15_CR1 (*((volatile struct TIM15_CR1_s*) 0x40014000))

struct TIM15_CR2_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int CCPC : 1;
			unsigned int : 1;
			unsigned int CCUS : 1;
			unsigned int CCDS : 1;
			unsigned int MMS : 3;
			unsigned int TI1S : 1;
			unsigned int OIS1 : 1;
			unsigned int OIS1N : 1;
			unsigned int OIS2 : 1;
		} __attribute((__packed__));
	};
};
#define TIM15_CR2 (*((volatile struct TIM15_CR2_s*) 0x40014004))

struct TIM15_SMCR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int SMS : 3;
			unsigned int : 1;
			unsigned int TS : 3;
			unsigned int MSM : 1;
			unsigned int : 8;
			unsigned int SMS_3 : 1;
		} __attribute((__packed__));
	};
};
#define TIM15_SMCR (*((volatile struct TIM15_SMCR_s*) 0x40014008))

struct TIM15_DIER_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int UIE : 1;
			unsigned int CC1IE : 1;
			unsigned int CC2IE : 1;
			unsigned int : 2;
			unsigned int COMIE : 1;
			unsigned int TIE : 1;
			unsigned int BIE : 1;
			unsigned int UDE : 1;
			unsigned int CC1DE : 1;
			unsigned int CC2DE : 1;
			unsigned int : 2;
			unsigned int COMDE : 1;
			unsigned int TDE : 1;
		} __attribute((__packed__));
	};
};
#define TIM15_DIER (*((volatile struct TIM15_DIER_s*) 0x4001400c))

struct TIM15_SR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int UIF : 1;
			unsigned int CC1IF : 1;
			unsigned int CC2IF : 1;
			unsigned int : 2;
			unsigned int COMIF : 1;
			unsigned int TIF : 1;
			unsigned int BIF : 1;
			unsigned int : 1;
			unsigned int CC1OF : 1;
			unsigned int CC2OF : 1;
		} __attribute((__packed__));
	};
};
#define TIM15_SR (*((volatile struct TIM15_SR_s*) 0x40014010))

struct TIM15_EGR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int UG : 1;
			unsigned int CC1G : 1;
			unsigned int CC2G : 1;
			unsigned int : 2;
			unsigned int COMG : 1;
			unsigned int TG : 1;
			unsigned int BG : 1;
		} __attribute((__packed__));
	};
};
#define TIM15_EGR (*((volatile struct TIM15_EGR_s*) 0x40014014))

struct TIM15_CCMR1_Output_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int CC1S : 2;
			unsigned int OC1FE : 1;
			unsigned int OC1PE : 1;
			unsigned int OC1M : 3;
			unsigned int : 1;
			unsigned int CC2S : 2;
			unsigned int OC2FE : 1;
			unsigned int OC2PE : 1;
			unsigned int OC2M : 3;
			unsigned int : 1;
			unsigned int OC1M_3 : 1;
			unsigned int : 7;
			unsigned int OC2M_3 : 1;
		} __attribute((__packed__));
	};
};
#define TIM15_CCMR1_Output (*((volatile struct TIM15_CCMR1_Output_s*) 0x40014018))

struct TIM15_CCMR1_Input_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int CC1S : 2;
			unsigned int IC1PSC : 2;
			unsigned int IC1F : 4;
			unsigned int CC2S : 2;
			unsigned int IC2PSC : 2;
			unsigned int IC2F : 4;
		} __attribute((__packed__));
	};
};
#define TIM15_CCMR1_Input (*((volatile struct TIM15_CCMR1_Input_s*) 0x40014018))

struct TIM15_CCER_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int CC1E : 1;
			unsigned int CC1P : 1;
			unsigned int CC1NE : 1;
			unsigned int CC1NP : 1;
			unsigned int CC2E : 1;
			unsigned int CC2P : 1;
			unsigned int : 1;
			unsigned int CC2NP : 1;
		} __attribute((__packed__));
	};
};
#define TIM15_CCER (*((volatile struct TIM15_CCER_s*) 0x40014020))

struct TIM15_CNT_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int CNT : 16;
			unsigned int : 15;
			unsigned int UIFCPY : 1;
		} __attribute((__packed__));
	};
};
#define TIM15_CNT (*((volatile struct TIM15_CNT_s*) 0x40014024))

struct TIM15_PSC_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int PSC : 16;
		} __attribute((__packed__));
	};
};
#define TIM15_PSC (*((volatile struct TIM15_PSC_s*) 0x40014028))

struct TIM15_ARR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int ARR : 16;
		} __attribute((__packed__));
	};
};
#define TIM15_ARR (*((volatile struct TIM15_ARR_s*) 0x4001402c))

struct TIM15_RCR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int REP : 8;
		} __attribute((__packed__));
	};
};
#define TIM15_RCR (*((volatile struct TIM15_RCR_s*) 0x40014030))

struct TIM15_CCR1_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int CCR1 : 16;
		} __attribute((__packed__));
	};
};
#define TIM15_CCR1 (*((volatile struct TIM15_CCR1_s*) 0x40014034))

struct TIM15_CCR2_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int CCR2 : 16;
		} __attribute((__packed__));
	};
};
#define TIM15_CCR2 (*((volatile struct TIM15_CCR2_s*) 0x40014038))

struct TIM15_BDTR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int DTG : 8;
			unsigned int LOCK : 2;
			unsigned int OSSI : 1;
			unsigned int OSSR : 1;
			unsigned int BKE : 1;
			unsigned int BKP : 1;
			unsigned int AOE : 1;
			unsigned int MOE : 1;
			unsigned int BKF : 4;
		} __attribute((__packed__));
	};
};
#define TIM15_BDTR (*((volatile struct TIM15_BDTR_s*) 0x40014044))

struct TIM15_DCR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int DBA : 5;
			unsigned int : 3;
			unsigned int DBL : 5;
		} __attribute((__packed__));
	};
};
#define TIM15_DCR (*((volatile struct TIM15_DCR_s*) 0x40014048))

struct TIM15_DMAR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int DMAB : 16;
		} __attribute((__packed__));
	};
};
#define TIM15_DMAR (*((volatile struct TIM15_DMAR_s*) 0x4001404c))

// TIM16
struct TIM16_CR1_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int CEN : 1;
			unsigned int UDIS : 1;
			unsigned int URS : 1;
			unsigned int OPM : 1;
			unsigned int : 3;
			unsigned int ARPE : 1;
			unsigned int CKD : 2;
			unsigned int : 1;
			unsigned int UIFREMAP : 1;
		} __attribute((__packed__));
	};
};
#define TIM16_CR1 (*((volatile struct TIM16_CR1_s*) 0x40014400))

struct TIM16_CR2_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int CCPC : 1;
			unsigned int : 1;
			unsigned int CCUS : 1;
			unsigned int CCDS : 1;
			unsigned int : 4;
			unsigned int OIS1 : 1;
			unsigned int OIS1N : 1;
		} __attribute((__packed__));
	};
};
#define TIM16_CR2 (*((volatile struct TIM16_CR2_s*) 0x40014404))

struct TIM16_DIER_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int UIE : 1;
			unsigned int CC1IE : 1;
			unsigned int : 3;
			unsigned int COMIE : 1;
			unsigned int TIE : 1;
			unsigned int BIE : 1;
			unsigned int UDE : 1;
			unsigned int CC1DE : 1;
			unsigned int : 3;
			unsigned int COMDE : 1;
			unsigned int TDE : 1;
		} __attribute((__packed__));
	};
};
#define TIM16_DIER (*((volatile struct TIM16_DIER_s*) 0x4001440c))

struct TIM16_SR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int UIF : 1;
			unsigned int CC1IF : 1;
			unsigned int : 3;
			unsigned int COMIF : 1;
			unsigned int TIF : 1;
			unsigned int BIF : 1;
			unsigned int : 1;
			unsigned int CC1OF : 1;
		} __attribute((__packed__));
	};
};
#define TIM16_SR (*((volatile struct TIM16_SR_s*) 0x40014410))

struct TIM16_EGR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int UG : 1;
			unsigned int CC1G : 1;
			unsigned int : 3;
			unsigned int COMG : 1;
			unsigned int TG : 1;
			unsigned int BG : 1;
		} __attribute((__packed__));
	};
};
#define TIM16_EGR (*((volatile struct TIM16_EGR_s*) 0x40014414))

struct TIM16_CCMR1_Output_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int CC1S : 2;
			unsigned int OC1FE : 1;
			unsigned int OC1PE : 1;
			unsigned int OC1M : 3;
			unsigned int : 9;
			unsigned int OC1M_3 : 1;
		} __attribute((__packed__));
	};
};
#define TIM16_CCMR1_Output (*((volatile struct TIM16_CCMR1_Output_s*) 0x40014418))

struct TIM16_CCMR1_Input_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int CC1S : 2;
			unsigned int IC1PSC : 2;
			unsigned int IC1F : 4;
		} __attribute((__packed__));
	};
};
#define TIM16_CCMR1_Input (*((volatile struct TIM16_CCMR1_Input_s*) 0x40014418))

struct TIM16_CCER_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int CC1E : 1;
			unsigned int CC1P : 1;
			unsigned int CC1NE : 1;
			unsigned int CC1NP : 1;
		} __attribute((__packed__));
	};
};
#define TIM16_CCER (*((volatile struct TIM16_CCER_s*) 0x40014420))

struct TIM16_CNT_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int CNT : 16;
			unsigned int : 15;
			unsigned int UIFCPY : 1;
		} __attribute((__packed__));
	};
};
#define TIM16_CNT (*((volatile struct TIM16_CNT_s*) 0x40014424))

struct TIM16_PSC_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int PSC : 16;
		} __attribute((__packed__));
	};
};
#define TIM16_PSC (*((volatile struct TIM16_PSC_s*) 0x40014428))

struct TIM16_ARR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int ARR : 16;
		} __attribute((__packed__));
	};
};
#define TIM16_ARR (*((volatile struct TIM16_ARR_s*) 0x4001442c))

struct TIM16_RCR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int REP : 8;
		} __attribute((__packed__));
	};
};
#define TIM16_RCR (*((volatile struct TIM16_RCR_s*) 0x40014430))

struct TIM16_CCR1_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int CCR1 : 16;
		} __attribute((__packed__));
	};
};
#define TIM16_CCR1 (*((volatile struct TIM16_CCR1_s*) 0x40014434))

struct TIM16_BDTR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int DTG : 8;
			unsigned int LOCK : 2;
			unsigned int OSSI : 1;
			unsigned int OSSR : 1;
			unsigned int BKE : 1;
			unsigned int BKP : 1;
			unsigned int AOE : 1;
			unsigned int MOE : 1;
			unsigned int BKF : 4;
		} __attribute((__packed__));
	};
};
#define TIM16_BDTR (*((volatile struct TIM16_BDTR_s*) 0x40014444))

struct TIM16_DCR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int DBA : 5;
			unsigned int : 3;
			unsigned int DBL : 5;
		} __attribute((__packed__));
	};
};
#define TIM16_DCR (*((volatile struct TIM16_DCR_s*) 0x40014448))

struct TIM16_DMAR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int DMAB : 16;
		} __attribute((__packed__));
	};
};
#define TIM16_DMAR (*((volatile struct TIM16_DMAR_s*) 0x4001444c))

struct TIM16_OR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
		} __attribute((__packed__));
	};
};
#define TIM16_OR (*((volatile struct TIM16_OR_s*) 0x40014450))

// TIM17
struct TIM17_CR1_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int CEN : 1;
			unsigned int UDIS : 1;
			unsigned int URS : 1;
			unsigned int OPM : 1;
			unsigned int : 3;
			unsigned int ARPE : 1;
			unsigned int CKD : 2;
			unsigned int : 1;
			unsigned int UIFREMAP : 1;
		} __attribute((__packed__));
	};
};
#define TIM17_CR1 (*((volatile struct TIM17_CR1_s*) 0x40014800))

struct TIM17_CR2_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int CCPC : 1;
			unsigned int : 1;
			unsigned int CCUS : 1;
			unsigned int CCDS : 1;
			unsigned int : 4;
			unsigned int OIS1 : 1;
			unsigned int OIS1N : 1;
		} __attribute((__packed__));
	};
};
#define TIM17_CR2 (*((volatile struct TIM17_CR2_s*) 0x40014804))

struct TIM17_DIER_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int UIE : 1;
			unsigned int CC1IE : 1;
			unsigned int : 3;
			unsigned int COMIE : 1;
			unsigned int TIE : 1;
			unsigned int BIE : 1;
			unsigned int UDE : 1;
			unsigned int CC1DE : 1;
			unsigned int : 3;
			unsigned int COMDE : 1;
			unsigned int TDE : 1;
		} __attribute((__packed__));
	};
};
#define TIM17_DIER (*((volatile struct TIM17_DIER_s*) 0x4001480c))

struct TIM17_SR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int UIF : 1;
			unsigned int CC1IF : 1;
			unsigned int : 3;
			unsigned int COMIF : 1;
			unsigned int TIF : 1;
			unsigned int BIF : 1;
			unsigned int : 1;
			unsigned int CC1OF : 1;
		} __attribute((__packed__));
	};
};
#define TIM17_SR (*((volatile struct TIM17_SR_s*) 0x40014810))

struct TIM17_EGR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int UG : 1;
			unsigned int CC1G : 1;
			unsigned int : 3;
			unsigned int COMG : 1;
			unsigned int TG : 1;
			unsigned int BG : 1;
		} __attribute((__packed__));
	};
};
#define TIM17_EGR (*((volatile struct TIM17_EGR_s*) 0x40014814))

struct TIM17_CCMR1_Output_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int CC1S : 2;
			unsigned int OC1FE : 1;
			unsigned int OC1PE : 1;
			unsigned int OC1M : 3;
			unsigned int : 9;
			unsigned int OC1M_3 : 1;
		} __attribute((__packed__));
	};
};
#define TIM17_CCMR1_Output (*((volatile struct TIM17_CCMR1_Output_s*) 0x40014818))

struct TIM17_CCMR1_Input_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int CC1S : 2;
			unsigned int IC1PSC : 2;
			unsigned int IC1F : 4;
		} __attribute((__packed__));
	};
};
#define TIM17_CCMR1_Input (*((volatile struct TIM17_CCMR1_Input_s*) 0x40014818))

struct TIM17_CCER_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int CC1E : 1;
			unsigned int CC1P : 1;
			unsigned int CC1NE : 1;
			unsigned int CC1NP : 1;
		} __attribute((__packed__));
	};
};
#define TIM17_CCER (*((volatile struct TIM17_CCER_s*) 0x40014820))

struct TIM17_CNT_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int CNT : 16;
			unsigned int : 15;
			unsigned int UIFCPY : 1;
		} __attribute((__packed__));
	};
};
#define TIM17_CNT (*((volatile struct TIM17_CNT_s*) 0x40014824))

struct TIM17_PSC_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int PSC : 16;
		} __attribute((__packed__));
	};
};
#define TIM17_PSC (*((volatile struct TIM17_PSC_s*) 0x40014828))

struct TIM17_ARR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int ARR : 16;
		} __attribute((__packed__));
	};
};
#define TIM17_ARR (*((volatile struct TIM17_ARR_s*) 0x4001482c))

struct TIM17_RCR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int REP : 8;
		} __attribute((__packed__));
	};
};
#define TIM17_RCR (*((volatile struct TIM17_RCR_s*) 0x40014830))

struct TIM17_CCR1_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int CCR1 : 16;
		} __attribute((__packed__));
	};
};
#define TIM17_CCR1 (*((volatile struct TIM17_CCR1_s*) 0x40014834))

struct TIM17_BDTR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int DTG : 8;
			unsigned int LOCK : 2;
			unsigned int OSSI : 1;
			unsigned int OSSR : 1;
			unsigned int BKE : 1;
			unsigned int BKP : 1;
			unsigned int AOE : 1;
			unsigned int MOE : 1;
			unsigned int BKF : 4;
		} __attribute((__packed__));
	};
};
#define TIM17_BDTR (*((volatile struct TIM17_BDTR_s*) 0x40014844))

struct TIM17_DCR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int DBA : 5;
			unsigned int : 3;
			unsigned int DBL : 5;
		} __attribute((__packed__));
	};
};
#define TIM17_DCR (*((volatile struct TIM17_DCR_s*) 0x40014848))

struct TIM17_DMAR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int DMAB : 16;
		} __attribute((__packed__));
	};
};
#define TIM17_DMAR (*((volatile struct TIM17_DMAR_s*) 0x4001484c))

// USART1
struct USART1_CR1_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int UE : 1;
			unsigned int UESM : 1;
			unsigned int RE : 1;
			unsigned int TE : 1;
			unsigned int IDLEIE : 1;
			unsigned int RXNEIE : 1;
			unsigned int TCIE : 1;
			unsigned int TXEIE : 1;
			unsigned int PEIE : 1;
			unsigned int PS : 1;
			unsigned int PCE : 1;
			unsigned int WAKE : 1;
			unsigned int M : 1;
			unsigned int MME : 1;
			unsigned int CMIE : 1;
			unsigned int OVER8 : 1;
			unsigned int DEDT : 5;
			unsigned int DEAT : 5;
			unsigned int RTOIE : 1;
			unsigned int EOBIE : 1;
		} __attribute((__packed__));
	};
};
#define USART1_CR1 (*((volatile struct USART1_CR1_s*) 0x40013800))

struct USART1_CR2_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int : 4;
			unsigned int ADDM7 : 1;
			unsigned int LBDL : 1;
			unsigned int LBDIE : 1;
			unsigned int : 1;
			unsigned int LBCL : 1;
			unsigned int CPHA : 1;
			unsigned int CPOL : 1;
			unsigned int CLKEN : 1;
			unsigned int STOP : 2;
			unsigned int LINEN : 1;
			unsigned int SWAP : 1;
			unsigned int RXINV : 1;
			unsigned int TXINV : 1;
			unsigned int DATAINV : 1;
			unsigned int MSBFIRST : 1;
			unsigned int ABREN : 1;
			unsigned int ABRMOD : 2;
			unsigned int RTOEN : 1;
			unsigned int ADD0 : 4;
			unsigned int ADD4 : 4;
		} __attribute((__packed__));
	};
};
#define USART1_CR2 (*((volatile struct USART1_CR2_s*) 0x40013804))

struct USART1_CR3_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int EIE : 1;
			unsigned int IREN : 1;
			unsigned int IRLP : 1;
			unsigned int HDSEL : 1;
			unsigned int NACK : 1;
			unsigned int SCEN : 1;
			unsigned int DMAR : 1;
			unsigned int DMAT : 1;
			unsigned int RTSE : 1;
			unsigned int CTSE : 1;
			unsigned int CTSIE : 1;
			unsigned int ONEBIT : 1;
			unsigned int OVRDIS : 1;
			unsigned int DDRE : 1;
			unsigned int DEM : 1;
			unsigned int DEP : 1;
			unsigned int : 1;
			unsigned int SCARCNT : 3;
			unsigned int WUS : 2;
			unsigned int WUFIE : 1;
		} __attribute((__packed__));
	};
};
#define USART1_CR3 (*((volatile struct USART1_CR3_s*) 0x40013808))

struct USART1_BRR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int DIV_Fraction : 4;
			unsigned int DIV_Mantissa : 12;
		} __attribute((__packed__));
	};
};
#define USART1_BRR (*((volatile struct USART1_BRR_s*) 0x4001380c))

struct USART1_GTPR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int PSC : 8;
			unsigned int GT : 8;
		} __attribute((__packed__));
	};
};
#define USART1_GTPR (*((volatile struct USART1_GTPR_s*) 0x40013810))

struct USART1_RTOR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int RTO : 24;
			unsigned int BLEN : 8;
		} __attribute((__packed__));
	};
};
#define USART1_RTOR (*((volatile struct USART1_RTOR_s*) 0x40013814))

struct USART1_RQR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int ABRRQ : 1;
			unsigned int SBKRQ : 1;
			unsigned int MMRQ : 1;
			unsigned int RXFRQ : 1;
			unsigned int TXFRQ : 1;
		} __attribute((__packed__));
	};
};
#define USART1_RQR (*((volatile struct USART1_RQR_s*) 0x40013818))

struct USART1_ISR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int PE : 1;
			unsigned int FE : 1;
			unsigned int NF : 1;
			unsigned int ORE : 1;
			unsigned int IDLE : 1;
			unsigned int RXNE : 1;
			unsigned int TC : 1;
			unsigned int TXE : 1;
			unsigned int LBDF : 1;
			unsigned int CTSIF : 1;
			unsigned int CTS : 1;
			unsigned int RTOF : 1;
			unsigned int EOBF : 1;
			unsigned int : 1;
			unsigned int ABRE : 1;
			unsigned int ABRF : 1;
			unsigned int BUSY : 1;
			unsigned int CMF : 1;
			unsigned int SBKF : 1;
			unsigned int RWU : 1;
			unsigned int WUF : 1;
			unsigned int TEACK : 1;
			unsigned int REACK : 1;
		} __attribute((__packed__));
	};
};
#define USART1_ISR (*((volatile struct USART1_ISR_s*) 0x4001381c))

struct USART1_ICR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int PECF : 1;
			unsigned int FECF : 1;
			unsigned int NCF : 1;
			unsigned int ORECF : 1;
			unsigned int IDLECF : 1;
			unsigned int : 1;
			unsigned int TCCF : 1;
			unsigned int : 1;
			unsigned int LBDCF : 1;
			unsigned int CTSCF : 1;
			unsigned int : 1;
			unsigned int RTOCF : 1;
			unsigned int EOBCF : 1;
			unsigned int : 4;
			unsigned int CMCF : 1;
			unsigned int : 2;
			unsigned int WUCF : 1;
		} __attribute((__packed__));
	};
};
#define USART1_ICR (*((volatile struct USART1_ICR_s*) 0x40013820))

struct USART1_RDR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int RDR : 9;
		} __attribute((__packed__));
	};
};
#define USART1_RDR (*((volatile struct USART1_RDR_s*) 0x40013824))

struct USART1_TDR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int TDR : 9;
		} __attribute((__packed__));
	};
};
#define USART1_TDR (*((volatile struct USART1_TDR_s*) 0x40013828))

// USART2
struct USART2_CR1_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int UE : 1;
			unsigned int UESM : 1;
			unsigned int RE : 1;
			unsigned int TE : 1;
			unsigned int IDLEIE : 1;
			unsigned int RXNEIE : 1;
			unsigned int TCIE : 1;
			unsigned int TXEIE : 1;
			unsigned int PEIE : 1;
			unsigned int PS : 1;
			unsigned int PCE : 1;
			unsigned int WAKE : 1;
			unsigned int M : 1;
			unsigned int MME : 1;
			unsigned int CMIE : 1;
			unsigned int OVER8 : 1;
			unsigned int DEDT : 5;
			unsigned int DEAT : 5;
			unsigned int RTOIE : 1;
			unsigned int EOBIE : 1;
		} __attribute((__packed__));
	};
};
#define USART2_CR1 (*((volatile struct USART2_CR1_s*) 0x40004400))

struct USART2_CR2_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int : 4;
			unsigned int ADDM7 : 1;
			unsigned int LBDL : 1;
			unsigned int LBDIE : 1;
			unsigned int : 1;
			unsigned int LBCL : 1;
			unsigned int CPHA : 1;
			unsigned int CPOL : 1;
			unsigned int CLKEN : 1;
			unsigned int STOP : 2;
			unsigned int LINEN : 1;
			unsigned int SWAP : 1;
			unsigned int RXINV : 1;
			unsigned int TXINV : 1;
			unsigned int DATAINV : 1;
			unsigned int MSBFIRST : 1;
			unsigned int ABREN : 1;
			unsigned int ABRMOD : 2;
			unsigned int RTOEN : 1;
			unsigned int ADD0 : 4;
			unsigned int ADD4 : 4;
		} __attribute((__packed__));
	};
};
#define USART2_CR2 (*((volatile struct USART2_CR2_s*) 0x40004404))

struct USART2_CR3_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int EIE : 1;
			unsigned int IREN : 1;
			unsigned int IRLP : 1;
			unsigned int HDSEL : 1;
			unsigned int NACK : 1;
			unsigned int SCEN : 1;
			unsigned int DMAR : 1;
			unsigned int DMAT : 1;
			unsigned int RTSE : 1;
			unsigned int CTSE : 1;
			unsigned int CTSIE : 1;
			unsigned int ONEBIT : 1;
			unsigned int OVRDIS : 1;
			unsigned int DDRE : 1;
			unsigned int DEM : 1;
			unsigned int DEP : 1;
			unsigned int : 1;
			unsigned int SCARCNT : 3;
			unsigned int WUS : 2;
			unsigned int WUFIE : 1;
		} __attribute((__packed__));
	};
};
#define USART2_CR3 (*((volatile struct USART2_CR3_s*) 0x40004408))

struct USART2_BRR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int DIV_Fraction : 4;
			unsigned int DIV_Mantissa : 12;
		} __attribute((__packed__));
	};
};
#define USART2_BRR (*((volatile struct USART2_BRR_s*) 0x4000440c))

struct USART2_GTPR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int PSC : 8;
			unsigned int GT : 8;
		} __attribute((__packed__));
	};
};
#define USART2_GTPR (*((volatile struct USART2_GTPR_s*) 0x40004410))

struct USART2_RTOR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int RTO : 24;
			unsigned int BLEN : 8;
		} __attribute((__packed__));
	};
};
#define USART2_RTOR (*((volatile struct USART2_RTOR_s*) 0x40004414))

struct USART2_RQR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int ABRRQ : 1;
			unsigned int SBKRQ : 1;
			unsigned int MMRQ : 1;
			unsigned int RXFRQ : 1;
			unsigned int TXFRQ : 1;
		} __attribute((__packed__));
	};
};
#define USART2_RQR (*((volatile struct USART2_RQR_s*) 0x40004418))

struct USART2_ISR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int PE : 1;
			unsigned int FE : 1;
			unsigned int NF : 1;
			unsigned int ORE : 1;
			unsigned int IDLE : 1;
			unsigned int RXNE : 1;
			unsigned int TC : 1;
			unsigned int TXE : 1;
			unsigned int LBDF : 1;
			unsigned int CTSIF : 1;
			unsigned int CTS : 1;
			unsigned int RTOF : 1;
			unsigned int EOBF : 1;
			unsigned int : 1;
			unsigned int ABRE : 1;
			unsigned int ABRF : 1;
			unsigned int BUSY : 1;
			unsigned int CMF : 1;
			unsigned int SBKF : 1;
			unsigned int RWU : 1;
			unsigned int WUF : 1;
			unsigned int TEACK : 1;
			unsigned int REACK : 1;
		} __attribute((__packed__));
	};
};
#define USART2_ISR (*((volatile struct USART2_ISR_s*) 0x4000441c))

struct USART2_ICR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int PECF : 1;
			unsigned int FECF : 1;
			unsigned int NCF : 1;
			unsigned int ORECF : 1;
			unsigned int IDLECF : 1;
			unsigned int : 1;
			unsigned int TCCF : 1;
			unsigned int : 1;
			unsigned int LBDCF : 1;
			unsigned int CTSCF : 1;
			unsigned int : 1;
			unsigned int RTOCF : 1;
			unsigned int EOBCF : 1;
			unsigned int : 4;
			unsigned int CMCF : 1;
			unsigned int : 2;
			unsigned int WUCF : 1;
		} __attribute((__packed__));
	};
};
#define USART2_ICR (*((volatile struct USART2_ICR_s*) 0x40004420))

struct USART2_RDR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int RDR : 9;
		} __attribute((__packed__));
	};
};
#define USART2_RDR (*((volatile struct USART2_RDR_s*) 0x40004424))

struct USART2_TDR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int TDR : 9;
		} __attribute((__packed__));
	};
};
#define USART2_TDR (*((volatile struct USART2_TDR_s*) 0x40004428))

// USART3
struct USART3_CR1_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int UE : 1;
			unsigned int UESM : 1;
			unsigned int RE : 1;
			unsigned int TE : 1;
			unsigned int IDLEIE : 1;
			unsigned int RXNEIE : 1;
			unsigned int TCIE : 1;
			unsigned int TXEIE : 1;
			unsigned int PEIE : 1;
			unsigned int PS : 1;
			unsigned int PCE : 1;
			unsigned int WAKE : 1;
			unsigned int M : 1;
			unsigned int MME : 1;
			unsigned int CMIE : 1;
			unsigned int OVER8 : 1;
			unsigned int DEDT : 5;
			unsigned int DEAT : 5;
			unsigned int RTOIE : 1;
			unsigned int EOBIE : 1;
		} __attribute((__packed__));
	};
};
#define USART3_CR1 (*((volatile struct USART3_CR1_s*) 0x40004800))

struct USART3_CR2_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int : 4;
			unsigned int ADDM7 : 1;
			unsigned int LBDL : 1;
			unsigned int LBDIE : 1;
			unsigned int : 1;
			unsigned int LBCL : 1;
			unsigned int CPHA : 1;
			unsigned int CPOL : 1;
			unsigned int CLKEN : 1;
			unsigned int STOP : 2;
			unsigned int LINEN : 1;
			unsigned int SWAP : 1;
			unsigned int RXINV : 1;
			unsigned int TXINV : 1;
			unsigned int DATAINV : 1;
			unsigned int MSBFIRST : 1;
			unsigned int ABREN : 1;
			unsigned int ABRMOD : 2;
			unsigned int RTOEN : 1;
			unsigned int ADD0 : 4;
			unsigned int ADD4 : 4;
		} __attribute((__packed__));
	};
};
#define USART3_CR2 (*((volatile struct USART3_CR2_s*) 0x40004804))

struct USART3_CR3_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int EIE : 1;
			unsigned int IREN : 1;
			unsigned int IRLP : 1;
			unsigned int HDSEL : 1;
			unsigned int NACK : 1;
			unsigned int SCEN : 1;
			unsigned int DMAR : 1;
			unsigned int DMAT : 1;
			unsigned int RTSE : 1;
			unsigned int CTSE : 1;
			unsigned int CTSIE : 1;
			unsigned int ONEBIT : 1;
			unsigned int OVRDIS : 1;
			unsigned int DDRE : 1;
			unsigned int DEM : 1;
			unsigned int DEP : 1;
			unsigned int : 1;
			unsigned int SCARCNT : 3;
			unsigned int WUS : 2;
			unsigned int WUFIE : 1;
		} __attribute((__packed__));
	};
};
#define USART3_CR3 (*((volatile struct USART3_CR3_s*) 0x40004808))

struct USART3_BRR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int DIV_Fraction : 4;
			unsigned int DIV_Mantissa : 12;
		} __attribute((__packed__));
	};
};
#define USART3_BRR (*((volatile struct USART3_BRR_s*) 0x4000480c))

struct USART3_GTPR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int PSC : 8;
			unsigned int GT : 8;
		} __attribute((__packed__));
	};
};
#define USART3_GTPR (*((volatile struct USART3_GTPR_s*) 0x40004810))

struct USART3_RTOR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int RTO : 24;
			unsigned int BLEN : 8;
		} __attribute((__packed__));
	};
};
#define USART3_RTOR (*((volatile struct USART3_RTOR_s*) 0x40004814))

struct USART3_RQR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int ABRRQ : 1;
			unsigned int SBKRQ : 1;
			unsigned int MMRQ : 1;
			unsigned int RXFRQ : 1;
			unsigned int TXFRQ : 1;
		} __attribute((__packed__));
	};
};
#define USART3_RQR (*((volatile struct USART3_RQR_s*) 0x40004818))

struct USART3_ISR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int PE : 1;
			unsigned int FE : 1;
			unsigned int NF : 1;
			unsigned int ORE : 1;
			unsigned int IDLE : 1;
			unsigned int RXNE : 1;
			unsigned int TC : 1;
			unsigned int TXE : 1;
			unsigned int LBDF : 1;
			unsigned int CTSIF : 1;
			unsigned int CTS : 1;
			unsigned int RTOF : 1;
			unsigned int EOBF : 1;
			unsigned int : 1;
			unsigned int ABRE : 1;
			unsigned int ABRF : 1;
			unsigned int BUSY : 1;
			unsigned int CMF : 1;
			unsigned int SBKF : 1;
			unsigned int RWU : 1;
			unsigned int WUF : 1;
			unsigned int TEACK : 1;
			unsigned int REACK : 1;
		} __attribute((__packed__));
	};
};
#define USART3_ISR (*((volatile struct USART3_ISR_s*) 0x4000481c))

struct USART3_ICR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int PECF : 1;
			unsigned int FECF : 1;
			unsigned int NCF : 1;
			unsigned int ORECF : 1;
			unsigned int IDLECF : 1;
			unsigned int : 1;
			unsigned int TCCF : 1;
			unsigned int : 1;
			unsigned int LBDCF : 1;
			unsigned int CTSCF : 1;
			unsigned int : 1;
			unsigned int RTOCF : 1;
			unsigned int EOBCF : 1;
			unsigned int : 4;
			unsigned int CMCF : 1;
			unsigned int : 2;
			unsigned int WUCF : 1;
		} __attribute((__packed__));
	};
};
#define USART3_ICR (*((volatile struct USART3_ICR_s*) 0x40004820))

struct USART3_RDR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int RDR : 9;
		} __attribute((__packed__));
	};
};
#define USART3_RDR (*((volatile struct USART3_RDR_s*) 0x40004824))

struct USART3_TDR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int TDR : 9;
		} __attribute((__packed__));
	};
};
#define USART3_TDR (*((volatile struct USART3_TDR_s*) 0x40004828))

// UART4
struct UART4_CR1_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int UE : 1;
			unsigned int UESM : 1;
			unsigned int RE : 1;
			unsigned int TE : 1;
			unsigned int IDLEIE : 1;
			unsigned int RXNEIE : 1;
			unsigned int TCIE : 1;
			unsigned int TXEIE : 1;
			unsigned int PEIE : 1;
			unsigned int PS : 1;
			unsigned int PCE : 1;
			unsigned int WAKE : 1;
			unsigned int M : 1;
			unsigned int MME : 1;
			unsigned int CMIE : 1;
			unsigned int OVER8 : 1;
			unsigned int DEDT : 5;
			unsigned int DEAT : 5;
			unsigned int RTOIE : 1;
			unsigned int EOBIE : 1;
		} __attribute((__packed__));
	};
};
#define UART4_CR1 (*((volatile struct UART4_CR1_s*) 0x40004c00))

struct UART4_CR2_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int : 4;
			unsigned int ADDM7 : 1;
			unsigned int LBDL : 1;
			unsigned int LBDIE : 1;
			unsigned int : 1;
			unsigned int LBCL : 1;
			unsigned int CPHA : 1;
			unsigned int CPOL : 1;
			unsigned int CLKEN : 1;
			unsigned int STOP : 2;
			unsigned int LINEN : 1;
			unsigned int SWAP : 1;
			unsigned int RXINV : 1;
			unsigned int TXINV : 1;
			unsigned int DATAINV : 1;
			unsigned int MSBFIRST : 1;
			unsigned int ABREN : 1;
			unsigned int ABRMOD : 2;
			unsigned int RTOEN : 1;
			unsigned int ADD0 : 4;
			unsigned int ADD4 : 4;
		} __attribute((__packed__));
	};
};
#define UART4_CR2 (*((volatile struct UART4_CR2_s*) 0x40004c04))

struct UART4_CR3_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int EIE : 1;
			unsigned int IREN : 1;
			unsigned int IRLP : 1;
			unsigned int HDSEL : 1;
			unsigned int NACK : 1;
			unsigned int SCEN : 1;
			unsigned int DMAR : 1;
			unsigned int DMAT : 1;
			unsigned int RTSE : 1;
			unsigned int CTSE : 1;
			unsigned int CTSIE : 1;
			unsigned int ONEBIT : 1;
			unsigned int OVRDIS : 1;
			unsigned int DDRE : 1;
			unsigned int DEM : 1;
			unsigned int DEP : 1;
			unsigned int : 1;
			unsigned int SCARCNT : 3;
			unsigned int WUS : 2;
			unsigned int WUFIE : 1;
		} __attribute((__packed__));
	};
};
#define UART4_CR3 (*((volatile struct UART4_CR3_s*) 0x40004c08))

struct UART4_BRR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int DIV_Fraction : 4;
			unsigned int DIV_Mantissa : 12;
		} __attribute((__packed__));
	};
};
#define UART4_BRR (*((volatile struct UART4_BRR_s*) 0x40004c0c))

struct UART4_GTPR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int PSC : 8;
			unsigned int GT : 8;
		} __attribute((__packed__));
	};
};
#define UART4_GTPR (*((volatile struct UART4_GTPR_s*) 0x40004c10))

struct UART4_RTOR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int RTO : 24;
			unsigned int BLEN : 8;
		} __attribute((__packed__));
	};
};
#define UART4_RTOR (*((volatile struct UART4_RTOR_s*) 0x40004c14))

struct UART4_RQR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int ABRRQ : 1;
			unsigned int SBKRQ : 1;
			unsigned int MMRQ : 1;
			unsigned int RXFRQ : 1;
			unsigned int TXFRQ : 1;
		} __attribute((__packed__));
	};
};
#define UART4_RQR (*((volatile struct UART4_RQR_s*) 0x40004c18))

struct UART4_ISR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int PE : 1;
			unsigned int FE : 1;
			unsigned int NF : 1;
			unsigned int ORE : 1;
			unsigned int IDLE : 1;
			unsigned int RXNE : 1;
			unsigned int TC : 1;
			unsigned int TXE : 1;
			unsigned int LBDF : 1;
			unsigned int CTSIF : 1;
			unsigned int CTS : 1;
			unsigned int RTOF : 1;
			unsigned int EOBF : 1;
			unsigned int : 1;
			unsigned int ABRE : 1;
			unsigned int ABRF : 1;
			unsigned int BUSY : 1;
			unsigned int CMF : 1;
			unsigned int SBKF : 1;
			unsigned int RWU : 1;
			unsigned int WUF : 1;
			unsigned int TEACK : 1;
			unsigned int REACK : 1;
		} __attribute((__packed__));
	};
};
#define UART4_ISR (*((volatile struct UART4_ISR_s*) 0x40004c1c))

struct UART4_ICR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int PECF : 1;
			unsigned int FECF : 1;
			unsigned int NCF : 1;
			unsigned int ORECF : 1;
			unsigned int IDLECF : 1;
			unsigned int : 1;
			unsigned int TCCF : 1;
			unsigned int : 1;
			unsigned int LBDCF : 1;
			unsigned int CTSCF : 1;
			unsigned int : 1;
			unsigned int RTOCF : 1;
			unsigned int EOBCF : 1;
			unsigned int : 4;
			unsigned int CMCF : 1;
			unsigned int : 2;
			unsigned int WUCF : 1;
		} __attribute((__packed__));
	};
};
#define UART4_ICR (*((volatile struct UART4_ICR_s*) 0x40004c20))

struct UART4_RDR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int RDR : 9;
		} __attribute((__packed__));
	};
};
#define UART4_RDR (*((volatile struct UART4_RDR_s*) 0x40004c24))

struct UART4_TDR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int TDR : 9;
		} __attribute((__packed__));
	};
};
#define UART4_TDR (*((volatile struct UART4_TDR_s*) 0x40004c28))

// UART5
struct UART5_CR1_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int UE : 1;
			unsigned int UESM : 1;
			unsigned int RE : 1;
			unsigned int TE : 1;
			unsigned int IDLEIE : 1;
			unsigned int RXNEIE : 1;
			unsigned int TCIE : 1;
			unsigned int TXEIE : 1;
			unsigned int PEIE : 1;
			unsigned int PS : 1;
			unsigned int PCE : 1;
			unsigned int WAKE : 1;
			unsigned int M : 1;
			unsigned int MME : 1;
			unsigned int CMIE : 1;
			unsigned int OVER8 : 1;
			unsigned int DEDT : 5;
			unsigned int DEAT : 5;
			unsigned int RTOIE : 1;
			unsigned int EOBIE : 1;
		} __attribute((__packed__));
	};
};
#define UART5_CR1 (*((volatile struct UART5_CR1_s*) 0x40005000))

struct UART5_CR2_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int : 4;
			unsigned int ADDM7 : 1;
			unsigned int LBDL : 1;
			unsigned int LBDIE : 1;
			unsigned int : 1;
			unsigned int LBCL : 1;
			unsigned int CPHA : 1;
			unsigned int CPOL : 1;
			unsigned int CLKEN : 1;
			unsigned int STOP : 2;
			unsigned int LINEN : 1;
			unsigned int SWAP : 1;
			unsigned int RXINV : 1;
			unsigned int TXINV : 1;
			unsigned int DATAINV : 1;
			unsigned int MSBFIRST : 1;
			unsigned int ABREN : 1;
			unsigned int ABRMOD : 2;
			unsigned int RTOEN : 1;
			unsigned int ADD0 : 4;
			unsigned int ADD4 : 4;
		} __attribute((__packed__));
	};
};
#define UART5_CR2 (*((volatile struct UART5_CR2_s*) 0x40005004))

struct UART5_CR3_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int EIE : 1;
			unsigned int IREN : 1;
			unsigned int IRLP : 1;
			unsigned int HDSEL : 1;
			unsigned int NACK : 1;
			unsigned int SCEN : 1;
			unsigned int DMAR : 1;
			unsigned int DMAT : 1;
			unsigned int RTSE : 1;
			unsigned int CTSE : 1;
			unsigned int CTSIE : 1;
			unsigned int ONEBIT : 1;
			unsigned int OVRDIS : 1;
			unsigned int DDRE : 1;
			unsigned int DEM : 1;
			unsigned int DEP : 1;
			unsigned int : 1;
			unsigned int SCARCNT : 3;
			unsigned int WUS : 2;
			unsigned int WUFIE : 1;
		} __attribute((__packed__));
	};
};
#define UART5_CR3 (*((volatile struct UART5_CR3_s*) 0x40005008))

struct UART5_BRR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int DIV_Fraction : 4;
			unsigned int DIV_Mantissa : 12;
		} __attribute((__packed__));
	};
};
#define UART5_BRR (*((volatile struct UART5_BRR_s*) 0x4000500c))

struct UART5_GTPR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int PSC : 8;
			unsigned int GT : 8;
		} __attribute((__packed__));
	};
};
#define UART5_GTPR (*((volatile struct UART5_GTPR_s*) 0x40005010))

struct UART5_RTOR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int RTO : 24;
			unsigned int BLEN : 8;
		} __attribute((__packed__));
	};
};
#define UART5_RTOR (*((volatile struct UART5_RTOR_s*) 0x40005014))

struct UART5_RQR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int ABRRQ : 1;
			unsigned int SBKRQ : 1;
			unsigned int MMRQ : 1;
			unsigned int RXFRQ : 1;
			unsigned int TXFRQ : 1;
		} __attribute((__packed__));
	};
};
#define UART5_RQR (*((volatile struct UART5_RQR_s*) 0x40005018))

struct UART5_ISR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int PE : 1;
			unsigned int FE : 1;
			unsigned int NF : 1;
			unsigned int ORE : 1;
			unsigned int IDLE : 1;
			unsigned int RXNE : 1;
			unsigned int TC : 1;
			unsigned int TXE : 1;
			unsigned int LBDF : 1;
			unsigned int CTSIF : 1;
			unsigned int CTS : 1;
			unsigned int RTOF : 1;
			unsigned int EOBF : 1;
			unsigned int : 1;
			unsigned int ABRE : 1;
			unsigned int ABRF : 1;
			unsigned int BUSY : 1;
			unsigned int CMF : 1;
			unsigned int SBKF : 1;
			unsigned int RWU : 1;
			unsigned int WUF : 1;
			unsigned int TEACK : 1;
			unsigned int REACK : 1;
		} __attribute((__packed__));
	};
};
#define UART5_ISR (*((volatile struct UART5_ISR_s*) 0x4000501c))

struct UART5_ICR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int PECF : 1;
			unsigned int FECF : 1;
			unsigned int NCF : 1;
			unsigned int ORECF : 1;
			unsigned int IDLECF : 1;
			unsigned int : 1;
			unsigned int TCCF : 1;
			unsigned int : 1;
			unsigned int LBDCF : 1;
			unsigned int CTSCF : 1;
			unsigned int : 1;
			unsigned int RTOCF : 1;
			unsigned int EOBCF : 1;
			unsigned int : 4;
			unsigned int CMCF : 1;
			unsigned int : 2;
			unsigned int WUCF : 1;
		} __attribute((__packed__));
	};
};
#define UART5_ICR (*((volatile struct UART5_ICR_s*) 0x40005020))

struct UART5_RDR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int RDR : 9;
		} __attribute((__packed__));
	};
};
#define UART5_RDR (*((volatile struct UART5_RDR_s*) 0x40005024))

struct UART5_TDR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int TDR : 9;
		} __attribute((__packed__));
	};
};
#define UART5_TDR (*((volatile struct UART5_TDR_s*) 0x40005028))

// SPI1
struct SPI1_CR1_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int CPHA : 1;
			unsigned int CPOL : 1;
			unsigned int MSTR : 1;
			unsigned int BR : 3;
			unsigned int SPE : 1;
			unsigned int LSBFIRST : 1;
			unsigned int SSI : 1;
			unsigned int SSM : 1;
			unsigned int RXONLY : 1;
			unsigned int CRCL : 1;
			unsigned int CRCNEXT : 1;
			unsigned int CRCEN : 1;
			unsigned int BIDIOE : 1;
			unsigned int BIDIMODE : 1;
		} __attribute((__packed__));
	};
};
#define SPI1_CR1 (*((volatile struct SPI1_CR1_s*) 0x40013000))

struct SPI1_CR2_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int RXDMAEN : 1;
			unsigned int TXDMAEN : 1;
			unsigned int SSOE : 1;
			unsigned int NSSP : 1;
			unsigned int FRF : 1;
			unsigned int ERRIE : 1;
			unsigned int RXNEIE : 1;
			unsigned int TXEIE : 1;
			unsigned int DS : 4;
			unsigned int FRXTH : 1;
			unsigned int LDMA_RX : 1;
			unsigned int LDMA_TX : 1;
		} __attribute((__packed__));
	};
};
#define SPI1_CR2 (*((volatile struct SPI1_CR2_s*) 0x40013004))

struct SPI1_SR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int RXNE : 1;
			unsigned int TXE : 1;
			unsigned int CHSIDE : 1;
			unsigned int UDR : 1;
			unsigned int CRCERR : 1;
			unsigned int MODF : 1;
			unsigned int OVR : 1;
			unsigned int BSY : 1;
			unsigned int TIFRFE : 1;
			unsigned int FRLVL : 2;
			unsigned int FTLVL : 2;
		} __attribute((__packed__));
	};
};
#define SPI1_SR (*((volatile struct SPI1_SR_s*) 0x40013008))

struct SPI1_DR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int DR : 16;
		} __attribute((__packed__));
	};
};
#define SPI1_DR (*((volatile struct SPI1_DR_s*) 0x4001300c))

struct SPI1_CRCPR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int CRCPOLY : 16;
		} __attribute((__packed__));
	};
};
#define SPI1_CRCPR (*((volatile struct SPI1_CRCPR_s*) 0x40013010))

struct SPI1_RXCRCR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int RxCRC : 16;
		} __attribute((__packed__));
	};
};
#define SPI1_RXCRCR (*((volatile struct SPI1_RXCRCR_s*) 0x40013014))

struct SPI1_TXCRCR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int TxCRC : 16;
		} __attribute((__packed__));
	};
};
#define SPI1_TXCRCR (*((volatile struct SPI1_TXCRCR_s*) 0x40013018))

struct SPI1_I2SCFGR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int CHLEN : 1;
			unsigned int DATLEN : 2;
			unsigned int CKPOL : 1;
			unsigned int I2SSTD : 2;
			unsigned int : 1;
			unsigned int PCMSYNC : 1;
			unsigned int I2SCFG : 2;
			unsigned int I2SE : 1;
			unsigned int I2SMOD : 1;
		} __attribute((__packed__));
	};
};
#define SPI1_I2SCFGR (*((volatile struct SPI1_I2SCFGR_s*) 0x4001301c))

struct SPI1_I2SPR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int I2SDIV : 8;
			unsigned int ODD : 1;
			unsigned int MCKOE : 1;
		} __attribute((__packed__));
	};
};
#define SPI1_I2SPR (*((volatile struct SPI1_I2SPR_s*) 0x40013020))

// SPI2
struct SPI2_CR1_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int CPHA : 1;
			unsigned int CPOL : 1;
			unsigned int MSTR : 1;
			unsigned int BR : 3;
			unsigned int SPE : 1;
			unsigned int LSBFIRST : 1;
			unsigned int SSI : 1;
			unsigned int SSM : 1;
			unsigned int RXONLY : 1;
			unsigned int CRCL : 1;
			unsigned int CRCNEXT : 1;
			unsigned int CRCEN : 1;
			unsigned int BIDIOE : 1;
			unsigned int BIDIMODE : 1;
		} __attribute((__packed__));
	};
};
#define SPI2_CR1 (*((volatile struct SPI2_CR1_s*) 0x40003800))

struct SPI2_CR2_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int RXDMAEN : 1;
			unsigned int TXDMAEN : 1;
			unsigned int SSOE : 1;
			unsigned int NSSP : 1;
			unsigned int FRF : 1;
			unsigned int ERRIE : 1;
			unsigned int RXNEIE : 1;
			unsigned int TXEIE : 1;
			unsigned int DS : 4;
			unsigned int FRXTH : 1;
			unsigned int LDMA_RX : 1;
			unsigned int LDMA_TX : 1;
		} __attribute((__packed__));
	};
};
#define SPI2_CR2 (*((volatile struct SPI2_CR2_s*) 0x40003804))

struct SPI2_SR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int RXNE : 1;
			unsigned int TXE : 1;
			unsigned int CHSIDE : 1;
			unsigned int UDR : 1;
			unsigned int CRCERR : 1;
			unsigned int MODF : 1;
			unsigned int OVR : 1;
			unsigned int BSY : 1;
			unsigned int TIFRFE : 1;
			unsigned int FRLVL : 2;
			unsigned int FTLVL : 2;
		} __attribute((__packed__));
	};
};
#define SPI2_SR (*((volatile struct SPI2_SR_s*) 0x40003808))

struct SPI2_DR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int DR : 16;
		} __attribute((__packed__));
	};
};
#define SPI2_DR (*((volatile struct SPI2_DR_s*) 0x4000380c))

struct SPI2_CRCPR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int CRCPOLY : 16;
		} __attribute((__packed__));
	};
};
#define SPI2_CRCPR (*((volatile struct SPI2_CRCPR_s*) 0x40003810))

struct SPI2_RXCRCR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int RxCRC : 16;
		} __attribute((__packed__));
	};
};
#define SPI2_RXCRCR (*((volatile struct SPI2_RXCRCR_s*) 0x40003814))

struct SPI2_TXCRCR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int TxCRC : 16;
		} __attribute((__packed__));
	};
};
#define SPI2_TXCRCR (*((volatile struct SPI2_TXCRCR_s*) 0x40003818))

struct SPI2_I2SCFGR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int CHLEN : 1;
			unsigned int DATLEN : 2;
			unsigned int CKPOL : 1;
			unsigned int I2SSTD : 2;
			unsigned int : 1;
			unsigned int PCMSYNC : 1;
			unsigned int I2SCFG : 2;
			unsigned int I2SE : 1;
			unsigned int I2SMOD : 1;
		} __attribute((__packed__));
	};
};
#define SPI2_I2SCFGR (*((volatile struct SPI2_I2SCFGR_s*) 0x4000381c))

struct SPI2_I2SPR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int I2SDIV : 8;
			unsigned int ODD : 1;
			unsigned int MCKOE : 1;
		} __attribute((__packed__));
	};
};
#define SPI2_I2SPR (*((volatile struct SPI2_I2SPR_s*) 0x40003820))

// SPI3
struct SPI3_CR1_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int CPHA : 1;
			unsigned int CPOL : 1;
			unsigned int MSTR : 1;
			unsigned int BR : 3;
			unsigned int SPE : 1;
			unsigned int LSBFIRST : 1;
			unsigned int SSI : 1;
			unsigned int SSM : 1;
			unsigned int RXONLY : 1;
			unsigned int CRCL : 1;
			unsigned int CRCNEXT : 1;
			unsigned int CRCEN : 1;
			unsigned int BIDIOE : 1;
			unsigned int BIDIMODE : 1;
		} __attribute((__packed__));
	};
};
#define SPI3_CR1 (*((volatile struct SPI3_CR1_s*) 0x40003c00))

struct SPI3_CR2_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int RXDMAEN : 1;
			unsigned int TXDMAEN : 1;
			unsigned int SSOE : 1;
			unsigned int NSSP : 1;
			unsigned int FRF : 1;
			unsigned int ERRIE : 1;
			unsigned int RXNEIE : 1;
			unsigned int TXEIE : 1;
			unsigned int DS : 4;
			unsigned int FRXTH : 1;
			unsigned int LDMA_RX : 1;
			unsigned int LDMA_TX : 1;
		} __attribute((__packed__));
	};
};
#define SPI3_CR2 (*((volatile struct SPI3_CR2_s*) 0x40003c04))

struct SPI3_SR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int RXNE : 1;
			unsigned int TXE : 1;
			unsigned int CHSIDE : 1;
			unsigned int UDR : 1;
			unsigned int CRCERR : 1;
			unsigned int MODF : 1;
			unsigned int OVR : 1;
			unsigned int BSY : 1;
			unsigned int TIFRFE : 1;
			unsigned int FRLVL : 2;
			unsigned int FTLVL : 2;
		} __attribute((__packed__));
	};
};
#define SPI3_SR (*((volatile struct SPI3_SR_s*) 0x40003c08))

struct SPI3_DR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int DR : 16;
		} __attribute((__packed__));
	};
};
#define SPI3_DR (*((volatile struct SPI3_DR_s*) 0x40003c0c))

struct SPI3_CRCPR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int CRCPOLY : 16;
		} __attribute((__packed__));
	};
};
#define SPI3_CRCPR (*((volatile struct SPI3_CRCPR_s*) 0x40003c10))

struct SPI3_RXCRCR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int RxCRC : 16;
		} __attribute((__packed__));
	};
};
#define SPI3_RXCRCR (*((volatile struct SPI3_RXCRCR_s*) 0x40003c14))

struct SPI3_TXCRCR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int TxCRC : 16;
		} __attribute((__packed__));
	};
};
#define SPI3_TXCRCR (*((volatile struct SPI3_TXCRCR_s*) 0x40003c18))

struct SPI3_I2SCFGR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int CHLEN : 1;
			unsigned int DATLEN : 2;
			unsigned int CKPOL : 1;
			unsigned int I2SSTD : 2;
			unsigned int : 1;
			unsigned int PCMSYNC : 1;
			unsigned int I2SCFG : 2;
			unsigned int I2SE : 1;
			unsigned int I2SMOD : 1;
		} __attribute((__packed__));
	};
};
#define SPI3_I2SCFGR (*((volatile struct SPI3_I2SCFGR_s*) 0x40003c1c))

struct SPI3_I2SPR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int I2SDIV : 8;
			unsigned int ODD : 1;
			unsigned int MCKOE : 1;
		} __attribute((__packed__));
	};
};
#define SPI3_I2SPR (*((volatile struct SPI3_I2SPR_s*) 0x40003c20))

// I2S2ext
struct I2S2ext_CR1_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int CPHA : 1;
			unsigned int CPOL : 1;
			unsigned int MSTR : 1;
			unsigned int BR : 3;
			unsigned int SPE : 1;
			unsigned int LSBFIRST : 1;
			unsigned int SSI : 1;
			unsigned int SSM : 1;
			unsigned int RXONLY : 1;
			unsigned int CRCL : 1;
			unsigned int CRCNEXT : 1;
			unsigned int CRCEN : 1;
			unsigned int BIDIOE : 1;
			unsigned int BIDIMODE : 1;
		} __attribute((__packed__));
	};
};
#define I2S2ext_CR1 (*((volatile struct I2S2ext_CR1_s*) 0x40003400))

struct I2S2ext_CR2_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int RXDMAEN : 1;
			unsigned int TXDMAEN : 1;
			unsigned int SSOE : 1;
			unsigned int NSSP : 1;
			unsigned int FRF : 1;
			unsigned int ERRIE : 1;
			unsigned int RXNEIE : 1;
			unsigned int TXEIE : 1;
			unsigned int DS : 4;
			unsigned int FRXTH : 1;
			unsigned int LDMA_RX : 1;
			unsigned int LDMA_TX : 1;
		} __attribute((__packed__));
	};
};
#define I2S2ext_CR2 (*((volatile struct I2S2ext_CR2_s*) 0x40003404))

struct I2S2ext_SR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int RXNE : 1;
			unsigned int TXE : 1;
			unsigned int CHSIDE : 1;
			unsigned int UDR : 1;
			unsigned int CRCERR : 1;
			unsigned int MODF : 1;
			unsigned int OVR : 1;
			unsigned int BSY : 1;
			unsigned int TIFRFE : 1;
			unsigned int FRLVL : 2;
			unsigned int FTLVL : 2;
		} __attribute((__packed__));
	};
};
#define I2S2ext_SR (*((volatile struct I2S2ext_SR_s*) 0x40003408))

struct I2S2ext_DR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int DR : 16;
		} __attribute((__packed__));
	};
};
#define I2S2ext_DR (*((volatile struct I2S2ext_DR_s*) 0x4000340c))

struct I2S2ext_CRCPR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int CRCPOLY : 16;
		} __attribute((__packed__));
	};
};
#define I2S2ext_CRCPR (*((volatile struct I2S2ext_CRCPR_s*) 0x40003410))

struct I2S2ext_RXCRCR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int RxCRC : 16;
		} __attribute((__packed__));
	};
};
#define I2S2ext_RXCRCR (*((volatile struct I2S2ext_RXCRCR_s*) 0x40003414))

struct I2S2ext_TXCRCR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int TxCRC : 16;
		} __attribute((__packed__));
	};
};
#define I2S2ext_TXCRCR (*((volatile struct I2S2ext_TXCRCR_s*) 0x40003418))

struct I2S2ext_I2SCFGR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int CHLEN : 1;
			unsigned int DATLEN : 2;
			unsigned int CKPOL : 1;
			unsigned int I2SSTD : 2;
			unsigned int : 1;
			unsigned int PCMSYNC : 1;
			unsigned int I2SCFG : 2;
			unsigned int I2SE : 1;
			unsigned int I2SMOD : 1;
		} __attribute((__packed__));
	};
};
#define I2S2ext_I2SCFGR (*((volatile struct I2S2ext_I2SCFGR_s*) 0x4000341c))

struct I2S2ext_I2SPR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int I2SDIV : 8;
			unsigned int ODD : 1;
			unsigned int MCKOE : 1;
		} __attribute((__packed__));
	};
};
#define I2S2ext_I2SPR (*((volatile struct I2S2ext_I2SPR_s*) 0x40003420))

// I2S3ext
struct I2S3ext_CR1_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int CPHA : 1;
			unsigned int CPOL : 1;
			unsigned int MSTR : 1;
			unsigned int BR : 3;
			unsigned int SPE : 1;
			unsigned int LSBFIRST : 1;
			unsigned int SSI : 1;
			unsigned int SSM : 1;
			unsigned int RXONLY : 1;
			unsigned int CRCL : 1;
			unsigned int CRCNEXT : 1;
			unsigned int CRCEN : 1;
			unsigned int BIDIOE : 1;
			unsigned int BIDIMODE : 1;
		} __attribute((__packed__));
	};
};
#define I2S3ext_CR1 (*((volatile struct I2S3ext_CR1_s*) 0x40004000))

struct I2S3ext_CR2_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int RXDMAEN : 1;
			unsigned int TXDMAEN : 1;
			unsigned int SSOE : 1;
			unsigned int NSSP : 1;
			unsigned int FRF : 1;
			unsigned int ERRIE : 1;
			unsigned int RXNEIE : 1;
			unsigned int TXEIE : 1;
			unsigned int DS : 4;
			unsigned int FRXTH : 1;
			unsigned int LDMA_RX : 1;
			unsigned int LDMA_TX : 1;
		} __attribute((__packed__));
	};
};
#define I2S3ext_CR2 (*((volatile struct I2S3ext_CR2_s*) 0x40004004))

struct I2S3ext_SR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int RXNE : 1;
			unsigned int TXE : 1;
			unsigned int CHSIDE : 1;
			unsigned int UDR : 1;
			unsigned int CRCERR : 1;
			unsigned int MODF : 1;
			unsigned int OVR : 1;
			unsigned int BSY : 1;
			unsigned int TIFRFE : 1;
			unsigned int FRLVL : 2;
			unsigned int FTLVL : 2;
		} __attribute((__packed__));
	};
};
#define I2S3ext_SR (*((volatile struct I2S3ext_SR_s*) 0x40004008))

struct I2S3ext_DR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int DR : 16;
		} __attribute((__packed__));
	};
};
#define I2S3ext_DR (*((volatile struct I2S3ext_DR_s*) 0x4000400c))

struct I2S3ext_CRCPR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int CRCPOLY : 16;
		} __attribute((__packed__));
	};
};
#define I2S3ext_CRCPR (*((volatile struct I2S3ext_CRCPR_s*) 0x40004010))

struct I2S3ext_RXCRCR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int RxCRC : 16;
		} __attribute((__packed__));
	};
};
#define I2S3ext_RXCRCR (*((volatile struct I2S3ext_RXCRCR_s*) 0x40004014))

struct I2S3ext_TXCRCR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int TxCRC : 16;
		} __attribute((__packed__));
	};
};
#define I2S3ext_TXCRCR (*((volatile struct I2S3ext_TXCRCR_s*) 0x40004018))

struct I2S3ext_I2SCFGR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int CHLEN : 1;
			unsigned int DATLEN : 2;
			unsigned int CKPOL : 1;
			unsigned int I2SSTD : 2;
			unsigned int : 1;
			unsigned int PCMSYNC : 1;
			unsigned int I2SCFG : 2;
			unsigned int I2SE : 1;
			unsigned int I2SMOD : 1;
		} __attribute((__packed__));
	};
};
#define I2S3ext_I2SCFGR (*((volatile struct I2S3ext_I2SCFGR_s*) 0x4000401c))

struct I2S3ext_I2SPR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int I2SDIV : 8;
			unsigned int ODD : 1;
			unsigned int MCKOE : 1;
		} __attribute((__packed__));
	};
};
#define I2S3ext_I2SPR (*((volatile struct I2S3ext_I2SPR_s*) 0x40004020))

// SPI4
struct SPI4_CR1_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int CPHA : 1;
			unsigned int CPOL : 1;
			unsigned int MSTR : 1;
			unsigned int BR : 3;
			unsigned int SPE : 1;
			unsigned int LSBFIRST : 1;
			unsigned int SSI : 1;
			unsigned int SSM : 1;
			unsigned int RXONLY : 1;
			unsigned int CRCL : 1;
			unsigned int CRCNEXT : 1;
			unsigned int CRCEN : 1;
			unsigned int BIDIOE : 1;
			unsigned int BIDIMODE : 1;
		} __attribute((__packed__));
	};
};
#define SPI4_CR1 (*((volatile struct SPI4_CR1_s*) 0x40013c00))

struct SPI4_CR2_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int RXDMAEN : 1;
			unsigned int TXDMAEN : 1;
			unsigned int SSOE : 1;
			unsigned int NSSP : 1;
			unsigned int FRF : 1;
			unsigned int ERRIE : 1;
			unsigned int RXNEIE : 1;
			unsigned int TXEIE : 1;
			unsigned int DS : 4;
			unsigned int FRXTH : 1;
			unsigned int LDMA_RX : 1;
			unsigned int LDMA_TX : 1;
		} __attribute((__packed__));
	};
};
#define SPI4_CR2 (*((volatile struct SPI4_CR2_s*) 0x40013c04))

struct SPI4_SR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int RXNE : 1;
			unsigned int TXE : 1;
			unsigned int CHSIDE : 1;
			unsigned int UDR : 1;
			unsigned int CRCERR : 1;
			unsigned int MODF : 1;
			unsigned int OVR : 1;
			unsigned int BSY : 1;
			unsigned int TIFRFE : 1;
			unsigned int FRLVL : 2;
			unsigned int FTLVL : 2;
		} __attribute((__packed__));
	};
};
#define SPI4_SR (*((volatile struct SPI4_SR_s*) 0x40013c08))

struct SPI4_DR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int DR : 16;
		} __attribute((__packed__));
	};
};
#define SPI4_DR (*((volatile struct SPI4_DR_s*) 0x40013c0c))

struct SPI4_CRCPR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int CRCPOLY : 16;
		} __attribute((__packed__));
	};
};
#define SPI4_CRCPR (*((volatile struct SPI4_CRCPR_s*) 0x40013c10))

struct SPI4_RXCRCR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int RxCRC : 16;
		} __attribute((__packed__));
	};
};
#define SPI4_RXCRCR (*((volatile struct SPI4_RXCRCR_s*) 0x40013c14))

struct SPI4_TXCRCR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int TxCRC : 16;
		} __attribute((__packed__));
	};
};
#define SPI4_TXCRCR (*((volatile struct SPI4_TXCRCR_s*) 0x40013c18))

struct SPI4_I2SCFGR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int CHLEN : 1;
			unsigned int DATLEN : 2;
			unsigned int CKPOL : 1;
			unsigned int I2SSTD : 2;
			unsigned int : 1;
			unsigned int PCMSYNC : 1;
			unsigned int I2SCFG : 2;
			unsigned int I2SE : 1;
			unsigned int I2SMOD : 1;
		} __attribute((__packed__));
	};
};
#define SPI4_I2SCFGR (*((volatile struct SPI4_I2SCFGR_s*) 0x40013c1c))

struct SPI4_I2SPR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int I2SDIV : 8;
			unsigned int ODD : 1;
			unsigned int MCKOE : 1;
		} __attribute((__packed__));
	};
};
#define SPI4_I2SPR (*((volatile struct SPI4_I2SPR_s*) 0x40013c20))

// EXTI
struct EXTI_IMR1_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int MR0 : 1;
			unsigned int MR1 : 1;
			unsigned int MR2 : 1;
			unsigned int MR3 : 1;
			unsigned int MR4 : 1;
			unsigned int MR5 : 1;
			unsigned int MR6 : 1;
			unsigned int MR7 : 1;
			unsigned int MR8 : 1;
			unsigned int MR9 : 1;
			unsigned int MR10 : 1;
			unsigned int MR11 : 1;
			unsigned int MR12 : 1;
			unsigned int MR13 : 1;
			unsigned int MR14 : 1;
			unsigned int MR15 : 1;
			unsigned int MR16 : 1;
			unsigned int MR17 : 1;
			unsigned int MR18 : 1;
			unsigned int MR19 : 1;
			unsigned int MR20 : 1;
			unsigned int MR21 : 1;
			unsigned int MR22 : 1;
			unsigned int MR23 : 1;
			unsigned int MR24 : 1;
			unsigned int MR25 : 1;
			unsigned int MR26 : 1;
			unsigned int MR27 : 1;
			unsigned int MR28 : 1;
			unsigned int MR29 : 1;
			unsigned int MR30 : 1;
			unsigned int MR31 : 1;
		} __attribute((__packed__));
	};
};
#define EXTI_IMR1 (*((volatile struct EXTI_IMR1_s*) 0x40010400))

struct EXTI_EMR1_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int MR0 : 1;
			unsigned int MR1 : 1;
			unsigned int MR2 : 1;
			unsigned int MR3 : 1;
			unsigned int MR4 : 1;
			unsigned int MR5 : 1;
			unsigned int MR6 : 1;
			unsigned int MR7 : 1;
			unsigned int MR8 : 1;
			unsigned int MR9 : 1;
			unsigned int MR10 : 1;
			unsigned int MR11 : 1;
			unsigned int MR12 : 1;
			unsigned int MR13 : 1;
			unsigned int MR14 : 1;
			unsigned int MR15 : 1;
			unsigned int MR16 : 1;
			unsigned int MR17 : 1;
			unsigned int MR18 : 1;
			unsigned int MR19 : 1;
			unsigned int MR20 : 1;
			unsigned int MR21 : 1;
			unsigned int MR22 : 1;
			unsigned int MR23 : 1;
			unsigned int MR24 : 1;
			unsigned int MR25 : 1;
			unsigned int MR26 : 1;
			unsigned int MR27 : 1;
			unsigned int MR28 : 1;
			unsigned int MR29 : 1;
			unsigned int MR30 : 1;
			unsigned int MR31 : 1;
		} __attribute((__packed__));
	};
};
#define EXTI_EMR1 (*((volatile struct EXTI_EMR1_s*) 0x40010404))

struct EXTI_RTSR1_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int TR0 : 1;
			unsigned int TR1 : 1;
			unsigned int TR2 : 1;
			unsigned int TR3 : 1;
			unsigned int TR4 : 1;
			unsigned int TR5 : 1;
			unsigned int TR6 : 1;
			unsigned int TR7 : 1;
			unsigned int TR8 : 1;
			unsigned int TR9 : 1;
			unsigned int TR10 : 1;
			unsigned int TR11 : 1;
			unsigned int TR12 : 1;
			unsigned int TR13 : 1;
			unsigned int TR14 : 1;
			unsigned int TR15 : 1;
			unsigned int TR16 : 1;
			unsigned int TR17 : 1;
			unsigned int TR18 : 1;
			unsigned int TR19 : 1;
			unsigned int TR20 : 1;
			unsigned int TR21 : 1;
			unsigned int TR22 : 1;
			unsigned int : 6;
			unsigned int TR29 : 1;
			unsigned int TR30 : 1;
			unsigned int TR31 : 1;
		} __attribute((__packed__));
	};
};
#define EXTI_RTSR1 (*((volatile struct EXTI_RTSR1_s*) 0x40010408))

struct EXTI_FTSR1_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int TR0 : 1;
			unsigned int TR1 : 1;
			unsigned int TR2 : 1;
			unsigned int TR3 : 1;
			unsigned int TR4 : 1;
			unsigned int TR5 : 1;
			unsigned int TR6 : 1;
			unsigned int TR7 : 1;
			unsigned int TR8 : 1;
			unsigned int TR9 : 1;
			unsigned int TR10 : 1;
			unsigned int TR11 : 1;
			unsigned int TR12 : 1;
			unsigned int TR13 : 1;
			unsigned int TR14 : 1;
			unsigned int TR15 : 1;
			unsigned int TR16 : 1;
			unsigned int TR17 : 1;
			unsigned int TR18 : 1;
			unsigned int TR19 : 1;
			unsigned int TR20 : 1;
			unsigned int TR21 : 1;
			unsigned int TR22 : 1;
			unsigned int : 6;
			unsigned int TR29 : 1;
			unsigned int TR30 : 1;
			unsigned int TR31 : 1;
		} __attribute((__packed__));
	};
};
#define EXTI_FTSR1 (*((volatile struct EXTI_FTSR1_s*) 0x4001040c))

struct EXTI_SWIER1_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int SWIER0 : 1;
			unsigned int SWIER1 : 1;
			unsigned int SWIER2 : 1;
			unsigned int SWIER3 : 1;
			unsigned int SWIER4 : 1;
			unsigned int SWIER5 : 1;
			unsigned int SWIER6 : 1;
			unsigned int SWIER7 : 1;
			unsigned int SWIER8 : 1;
			unsigned int SWIER9 : 1;
			unsigned int SWIER10 : 1;
			unsigned int SWIER11 : 1;
			unsigned int SWIER12 : 1;
			unsigned int SWIER13 : 1;
			unsigned int SWIER14 : 1;
			unsigned int SWIER15 : 1;
			unsigned int SWIER16 : 1;
			unsigned int SWIER17 : 1;
			unsigned int SWIER18 : 1;
			unsigned int SWIER19 : 1;
			unsigned int SWIER20 : 1;
			unsigned int SWIER21 : 1;
			unsigned int SWIER22 : 1;
			unsigned int : 6;
			unsigned int SWIER29 : 1;
			unsigned int SWIER30 : 1;
			unsigned int SWIER31 : 1;
		} __attribute((__packed__));
	};
};
#define EXTI_SWIER1 (*((volatile struct EXTI_SWIER1_s*) 0x40010410))

struct EXTI_PR1_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int PR0 : 1;
			unsigned int PR1 : 1;
			unsigned int PR2 : 1;
			unsigned int PR3 : 1;
			unsigned int PR4 : 1;
			unsigned int PR5 : 1;
			unsigned int PR6 : 1;
			unsigned int PR7 : 1;
			unsigned int PR8 : 1;
			unsigned int PR9 : 1;
			unsigned int PR10 : 1;
			unsigned int PR11 : 1;
			unsigned int PR12 : 1;
			unsigned int PR13 : 1;
			unsigned int PR14 : 1;
			unsigned int PR15 : 1;
			unsigned int PR16 : 1;
			unsigned int PR17 : 1;
			unsigned int PR18 : 1;
			unsigned int PR19 : 1;
			unsigned int PR20 : 1;
			unsigned int PR21 : 1;
			unsigned int PR22 : 1;
			unsigned int : 6;
			unsigned int PR29 : 1;
			unsigned int PR30 : 1;
			unsigned int PR31 : 1;
		} __attribute((__packed__));
	};
};
#define EXTI_PR1 (*((volatile struct EXTI_PR1_s*) 0x40010414))

struct EXTI_IMR2_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int MR32 : 1;
			unsigned int MR33 : 1;
			unsigned int MR34 : 1;
			unsigned int MR35 : 1;
		} __attribute((__packed__));
	};
};
#define EXTI_IMR2 (*((volatile struct EXTI_IMR2_s*) 0x40010418))

struct EXTI_EMR2_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int MR32 : 1;
			unsigned int MR33 : 1;
			unsigned int MR34 : 1;
			unsigned int MR35 : 1;
		} __attribute((__packed__));
	};
};
#define EXTI_EMR2 (*((volatile struct EXTI_EMR2_s*) 0x4001041c))

struct EXTI_RTSR2_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int TR32 : 1;
			unsigned int TR33 : 1;
		} __attribute((__packed__));
	};
};
#define EXTI_RTSR2 (*((volatile struct EXTI_RTSR2_s*) 0x40010420))

struct EXTI_FTSR2_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int TR32 : 1;
			unsigned int TR33 : 1;
		} __attribute((__packed__));
	};
};
#define EXTI_FTSR2 (*((volatile struct EXTI_FTSR2_s*) 0x40010424))

struct EXTI_SWIER2_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int SWIER32 : 1;
			unsigned int SWIER33 : 1;
		} __attribute((__packed__));
	};
};
#define EXTI_SWIER2 (*((volatile struct EXTI_SWIER2_s*) 0x40010428))

struct EXTI_PR2_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int PR32 : 1;
			unsigned int PR33 : 1;
		} __attribute((__packed__));
	};
};
#define EXTI_PR2 (*((volatile struct EXTI_PR2_s*) 0x4001042c))

// PWR
struct PWR_CR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int LPDS : 1;
			unsigned int PDDS : 1;
			unsigned int CWUF : 1;
			unsigned int CSBF : 1;
			unsigned int PVDE : 1;
			unsigned int PLS : 3;
			unsigned int DBP : 1;
		} __attribute((__packed__));
	};
};
#define PWR_CR (*((volatile struct PWR_CR_s*) 0x40007000))

struct PWR_CSR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int WUF : 1;
			unsigned int SBF : 1;
			unsigned int PVDO : 1;
			unsigned int : 5;
			unsigned int EWUP1 : 1;
			unsigned int EWUP2 : 1;
		} __attribute((__packed__));
	};
};
#define PWR_CSR (*((volatile struct PWR_CSR_s*) 0x40007004))

// CAN
struct CAN_MCR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int INRQ : 1;
			unsigned int SLEEP : 1;
			unsigned int TXFP : 1;
			unsigned int RFLM : 1;
			unsigned int NART : 1;
			unsigned int AWUM : 1;
			unsigned int ABOM : 1;
			unsigned int TTCM : 1;
			unsigned int : 7;
			unsigned int RESET : 1;
			unsigned int DBF : 1;
		} __attribute((__packed__));
	};
};
#define CAN_MCR (*((volatile struct CAN_MCR_s*) 0x40006400))

struct CAN_MSR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int INAK : 1;
			unsigned int SLAK : 1;
			unsigned int ERRI : 1;
			unsigned int WKUI : 1;
			unsigned int SLAKI : 1;
			unsigned int : 3;
			unsigned int TXM : 1;
			unsigned int RXM : 1;
			unsigned int SAMP : 1;
			unsigned int RX : 1;
		} __attribute((__packed__));
	};
};
#define CAN_MSR (*((volatile struct CAN_MSR_s*) 0x40006404))

struct CAN_TSR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int RQCP0 : 1;
			unsigned int TXOK0 : 1;
			unsigned int ALST0 : 1;
			unsigned int TERR0 : 1;
			unsigned int : 3;
			unsigned int ABRQ0 : 1;
			unsigned int RQCP1 : 1;
			unsigned int TXOK1 : 1;
			unsigned int ALST1 : 1;
			unsigned int TERR1 : 1;
			unsigned int : 3;
			unsigned int ABRQ1 : 1;
			unsigned int RQCP2 : 1;
			unsigned int TXOK2 : 1;
			unsigned int ALST2 : 1;
			unsigned int TERR2 : 1;
			unsigned int : 3;
			unsigned int ABRQ2 : 1;
			unsigned int CODE : 2;
			unsigned int TME0 : 1;
			unsigned int TME1 : 1;
			unsigned int TME2 : 1;
			unsigned int LOW0 : 1;
			unsigned int LOW1 : 1;
			unsigned int LOW2 : 1;
		} __attribute((__packed__));
	};
};
#define CAN_TSR (*((volatile struct CAN_TSR_s*) 0x40006408))

struct CAN_RF0R_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int FMP0 : 2;
			unsigned int : 1;
			unsigned int FULL0 : 1;
			unsigned int FOVR0 : 1;
			unsigned int RFOM0 : 1;
		} __attribute((__packed__));
	};
};
#define CAN_RF0R (*((volatile struct CAN_RF0R_s*) 0x4000640c))

struct CAN_RF1R_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int FMP1 : 2;
			unsigned int : 1;
			unsigned int FULL1 : 1;
			unsigned int FOVR1 : 1;
			unsigned int RFOM1 : 1;
		} __attribute((__packed__));
	};
};
#define CAN_RF1R (*((volatile struct CAN_RF1R_s*) 0x40006410))

struct CAN_IER_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int TMEIE : 1;
			unsigned int FMPIE0 : 1;
			unsigned int FFIE0 : 1;
			unsigned int FOVIE0 : 1;
			unsigned int FMPIE1 : 1;
			unsigned int FFIE1 : 1;
			unsigned int FOVIE1 : 1;
			unsigned int : 1;
			unsigned int EWGIE : 1;
			unsigned int EPVIE : 1;
			unsigned int BOFIE : 1;
			unsigned int LECIE : 1;
			unsigned int : 3;
			unsigned int ERRIE : 1;
			unsigned int WKUIE : 1;
			unsigned int SLKIE : 1;
		} __attribute((__packed__));
	};
};
#define CAN_IER (*((volatile struct CAN_IER_s*) 0x40006414))

struct CAN_ESR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int EWGF : 1;
			unsigned int EPVF : 1;
			unsigned int BOFF : 1;
			unsigned int : 1;
			unsigned int LEC : 3;
			unsigned int : 9;
			unsigned int TEC : 8;
			unsigned int REC : 8;
		} __attribute((__packed__));
	};
};
#define CAN_ESR (*((volatile struct CAN_ESR_s*) 0x40006418))

struct CAN_BTR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int BRP : 10;
			unsigned int : 6;
			unsigned int TS1 : 4;
			unsigned int TS2 : 3;
			unsigned int : 1;
			unsigned int SJW : 2;
			unsigned int : 4;
			unsigned int LBKM : 1;
			unsigned int SILM : 1;
		} __attribute((__packed__));
	};
};
#define CAN_BTR (*((volatile struct CAN_BTR_s*) 0x4000641c))

struct CAN_TI0R_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int TXRQ : 1;
			unsigned int RTR : 1;
			unsigned int IDE : 1;
			unsigned int EXID : 18;
			unsigned int STID : 11;
		} __attribute((__packed__));
	};
};
#define CAN_TI0R (*((volatile struct CAN_TI0R_s*) 0x40006580))

struct CAN_TDT0R_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int DLC : 4;
			unsigned int : 4;
			unsigned int TGT : 1;
			unsigned int : 7;
			unsigned int TIME : 16;
		} __attribute((__packed__));
	};
};
#define CAN_TDT0R (*((volatile struct CAN_TDT0R_s*) 0x40006584))

struct CAN_TDL0R_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int DATA0 : 8;
			unsigned int DATA1 : 8;
			unsigned int DATA2 : 8;
			unsigned int DATA3 : 8;
		} __attribute((__packed__));
	};
};
#define CAN_TDL0R (*((volatile struct CAN_TDL0R_s*) 0x40006588))

struct CAN_TDH0R_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int DATA4 : 8;
			unsigned int DATA5 : 8;
			unsigned int DATA6 : 8;
			unsigned int DATA7 : 8;
		} __attribute((__packed__));
	};
};
#define CAN_TDH0R (*((volatile struct CAN_TDH0R_s*) 0x4000658c))

struct CAN_TI1R_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int TXRQ : 1;
			unsigned int RTR : 1;
			unsigned int IDE : 1;
			unsigned int EXID : 18;
			unsigned int STID : 11;
		} __attribute((__packed__));
	};
};
#define CAN_TI1R (*((volatile struct CAN_TI1R_s*) 0x40006590))

struct CAN_TDT1R_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int DLC : 4;
			unsigned int : 4;
			unsigned int TGT : 1;
			unsigned int : 7;
			unsigned int TIME : 16;
		} __attribute((__packed__));
	};
};
#define CAN_TDT1R (*((volatile struct CAN_TDT1R_s*) 0x40006594))

struct CAN_TDL1R_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int DATA0 : 8;
			unsigned int DATA1 : 8;
			unsigned int DATA2 : 8;
			unsigned int DATA3 : 8;
		} __attribute((__packed__));
	};
};
#define CAN_TDL1R (*((volatile struct CAN_TDL1R_s*) 0x40006598))

struct CAN_TDH1R_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int DATA4 : 8;
			unsigned int DATA5 : 8;
			unsigned int DATA6 : 8;
			unsigned int DATA7 : 8;
		} __attribute((__packed__));
	};
};
#define CAN_TDH1R (*((volatile struct CAN_TDH1R_s*) 0x4000659c))

struct CAN_TI2R_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int TXRQ : 1;
			unsigned int RTR : 1;
			unsigned int IDE : 1;
			unsigned int EXID : 18;
			unsigned int STID : 11;
		} __attribute((__packed__));
	};
};
#define CAN_TI2R (*((volatile struct CAN_TI2R_s*) 0x400065a0))

struct CAN_TDT2R_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int DLC : 4;
			unsigned int : 4;
			unsigned int TGT : 1;
			unsigned int : 7;
			unsigned int TIME : 16;
		} __attribute((__packed__));
	};
};
#define CAN_TDT2R (*((volatile struct CAN_TDT2R_s*) 0x400065a4))

struct CAN_TDL2R_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int DATA0 : 8;
			unsigned int DATA1 : 8;
			unsigned int DATA2 : 8;
			unsigned int DATA3 : 8;
		} __attribute((__packed__));
	};
};
#define CAN_TDL2R (*((volatile struct CAN_TDL2R_s*) 0x400065a8))

struct CAN_TDH2R_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int DATA4 : 8;
			unsigned int DATA5 : 8;
			unsigned int DATA6 : 8;
			unsigned int DATA7 : 8;
		} __attribute((__packed__));
	};
};
#define CAN_TDH2R (*((volatile struct CAN_TDH2R_s*) 0x400065ac))

struct CAN_RI0R_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int : 1;
			unsigned int RTR : 1;
			unsigned int IDE : 1;
			unsigned int EXID : 18;
			unsigned int STID : 11;
		} __attribute((__packed__));
	};
};
#define CAN_RI0R (*((volatile struct CAN_RI0R_s*) 0x400065b0))

struct CAN_RDT0R_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int DLC : 4;
			unsigned int : 4;
			unsigned int FMI : 8;
			unsigned int TIME : 16;
		} __attribute((__packed__));
	};
};
#define CAN_RDT0R (*((volatile struct CAN_RDT0R_s*) 0x400065b4))

struct CAN_RDL0R_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int DATA0 : 8;
			unsigned int DATA1 : 8;
			unsigned int DATA2 : 8;
			unsigned int DATA3 : 8;
		} __attribute((__packed__));
	};
};
#define CAN_RDL0R (*((volatile struct CAN_RDL0R_s*) 0x400065b8))

struct CAN_RDH0R_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int DATA4 : 8;
			unsigned int DATA5 : 8;
			unsigned int DATA6 : 8;
			unsigned int DATA7 : 8;
		} __attribute((__packed__));
	};
};
#define CAN_RDH0R (*((volatile struct CAN_RDH0R_s*) 0x400065bc))

struct CAN_RI1R_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int : 1;
			unsigned int RTR : 1;
			unsigned int IDE : 1;
			unsigned int EXID : 18;
			unsigned int STID : 11;
		} __attribute((__packed__));
	};
};
#define CAN_RI1R (*((volatile struct CAN_RI1R_s*) 0x400065c0))

struct CAN_RDT1R_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int DLC : 4;
			unsigned int : 4;
			unsigned int FMI : 8;
			unsigned int TIME : 16;
		} __attribute((__packed__));
	};
};
#define CAN_RDT1R (*((volatile struct CAN_RDT1R_s*) 0x400065c4))

struct CAN_RDL1R_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int DATA0 : 8;
			unsigned int DATA1 : 8;
			unsigned int DATA2 : 8;
			unsigned int DATA3 : 8;
		} __attribute((__packed__));
	};
};
#define CAN_RDL1R (*((volatile struct CAN_RDL1R_s*) 0x400065c8))

struct CAN_RDH1R_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int DATA4 : 8;
			unsigned int DATA5 : 8;
			unsigned int DATA6 : 8;
			unsigned int DATA7 : 8;
		} __attribute((__packed__));
	};
};
#define CAN_RDH1R (*((volatile struct CAN_RDH1R_s*) 0x400065cc))

struct CAN_FMR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int FINIT : 1;
			unsigned int : 7;
			unsigned int CAN2SB : 6;
		} __attribute((__packed__));
	};
};
#define CAN_FMR (*((volatile struct CAN_FMR_s*) 0x40006600))

struct CAN_FM1R_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int FBM0 : 1;
			unsigned int FBM1 : 1;
			unsigned int FBM2 : 1;
			unsigned int FBM3 : 1;
			unsigned int FBM4 : 1;
			unsigned int FBM5 : 1;
			unsigned int FBM6 : 1;
			unsigned int FBM7 : 1;
			unsigned int FBM8 : 1;
			unsigned int FBM9 : 1;
			unsigned int FBM10 : 1;
			unsigned int FBM11 : 1;
			unsigned int FBM12 : 1;
			unsigned int FBM13 : 1;
			unsigned int FBM14 : 1;
			unsigned int FBM15 : 1;
			unsigned int FBM16 : 1;
			unsigned int FBM17 : 1;
			unsigned int FBM18 : 1;
			unsigned int FBM19 : 1;
			unsigned int FBM20 : 1;
			unsigned int FBM21 : 1;
			unsigned int FBM22 : 1;
			unsigned int FBM23 : 1;
			unsigned int FBM24 : 1;
			unsigned int FBM25 : 1;
			unsigned int FBM26 : 1;
			unsigned int FBM27 : 1;
		} __attribute((__packed__));
	};
};
#define CAN_FM1R (*((volatile struct CAN_FM1R_s*) 0x40006604))

struct CAN_FS1R_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int FSC0 : 1;
			unsigned int FSC1 : 1;
			unsigned int FSC2 : 1;
			unsigned int FSC3 : 1;
			unsigned int FSC4 : 1;
			unsigned int FSC5 : 1;
			unsigned int FSC6 : 1;
			unsigned int FSC7 : 1;
			unsigned int FSC8 : 1;
			unsigned int FSC9 : 1;
			unsigned int FSC10 : 1;
			unsigned int FSC11 : 1;
			unsigned int FSC12 : 1;
			unsigned int FSC13 : 1;
			unsigned int FSC14 : 1;
			unsigned int FSC15 : 1;
			unsigned int FSC16 : 1;
			unsigned int FSC17 : 1;
			unsigned int FSC18 : 1;
			unsigned int FSC19 : 1;
			unsigned int FSC20 : 1;
			unsigned int FSC21 : 1;
			unsigned int FSC22 : 1;
			unsigned int FSC23 : 1;
			unsigned int FSC24 : 1;
			unsigned int FSC25 : 1;
			unsigned int FSC26 : 1;
			unsigned int FSC27 : 1;
		} __attribute((__packed__));
	};
};
#define CAN_FS1R (*((volatile struct CAN_FS1R_s*) 0x4000660c))

struct CAN_FFA1R_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int FFA0 : 1;
			unsigned int FFA1 : 1;
			unsigned int FFA2 : 1;
			unsigned int FFA3 : 1;
			unsigned int FFA4 : 1;
			unsigned int FFA5 : 1;
			unsigned int FFA6 : 1;
			unsigned int FFA7 : 1;
			unsigned int FFA8 : 1;
			unsigned int FFA9 : 1;
			unsigned int FFA10 : 1;
			unsigned int FFA11 : 1;
			unsigned int FFA12 : 1;
			unsigned int FFA13 : 1;
			unsigned int FFA14 : 1;
			unsigned int FFA15 : 1;
			unsigned int FFA16 : 1;
			unsigned int FFA17 : 1;
			unsigned int FFA18 : 1;
			unsigned int FFA19 : 1;
			unsigned int FFA20 : 1;
			unsigned int FFA21 : 1;
			unsigned int FFA22 : 1;
			unsigned int FFA23 : 1;
			unsigned int FFA24 : 1;
			unsigned int FFA25 : 1;
			unsigned int FFA26 : 1;
			unsigned int FFA27 : 1;
		} __attribute((__packed__));
	};
};
#define CAN_FFA1R (*((volatile struct CAN_FFA1R_s*) 0x40006614))

struct CAN_FA1R_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int FACT0 : 1;
			unsigned int FACT1 : 1;
			unsigned int FACT2 : 1;
			unsigned int FACT3 : 1;
			unsigned int FACT4 : 1;
			unsigned int FACT5 : 1;
			unsigned int FACT6 : 1;
			unsigned int FACT7 : 1;
			unsigned int FACT8 : 1;
			unsigned int FACT9 : 1;
			unsigned int FACT10 : 1;
			unsigned int FACT11 : 1;
			unsigned int FACT12 : 1;
			unsigned int FACT13 : 1;
			unsigned int FACT14 : 1;
			unsigned int FACT15 : 1;
			unsigned int FACT16 : 1;
			unsigned int FACT17 : 1;
			unsigned int FACT18 : 1;
			unsigned int FACT19 : 1;
			unsigned int FACT20 : 1;
			unsigned int FACT21 : 1;
			unsigned int FACT22 : 1;
			unsigned int FACT23 : 1;
			unsigned int FACT24 : 1;
			unsigned int FACT25 : 1;
			unsigned int FACT26 : 1;
			unsigned int FACT27 : 1;
		} __attribute((__packed__));
	};
};
#define CAN_FA1R (*((volatile struct CAN_FA1R_s*) 0x4000661c))

struct CAN_F0R1_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int FB0 : 1;
			unsigned int FB1 : 1;
			unsigned int FB2 : 1;
			unsigned int FB3 : 1;
			unsigned int FB4 : 1;
			unsigned int FB5 : 1;
			unsigned int FB6 : 1;
			unsigned int FB7 : 1;
			unsigned int FB8 : 1;
			unsigned int FB9 : 1;
			unsigned int FB10 : 1;
			unsigned int FB11 : 1;
			unsigned int FB12 : 1;
			unsigned int FB13 : 1;
			unsigned int FB14 : 1;
			unsigned int FB15 : 1;
			unsigned int FB16 : 1;
			unsigned int FB17 : 1;
			unsigned int FB18 : 1;
			unsigned int FB19 : 1;
			unsigned int FB20 : 1;
			unsigned int FB21 : 1;
			unsigned int FB22 : 1;
			unsigned int FB23 : 1;
			unsigned int FB24 : 1;
			unsigned int FB25 : 1;
			unsigned int FB26 : 1;
			unsigned int FB27 : 1;
			unsigned int FB28 : 1;
			unsigned int FB29 : 1;
			unsigned int FB30 : 1;
			unsigned int FB31 : 1;
		} __attribute((__packed__));
	};
};
#define CAN_F0R1 (*((volatile struct CAN_F0R1_s*) 0x40006640))

struct CAN_F0R2_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int FB0 : 1;
			unsigned int FB1 : 1;
			unsigned int FB2 : 1;
			unsigned int FB3 : 1;
			unsigned int FB4 : 1;
			unsigned int FB5 : 1;
			unsigned int FB6 : 1;
			unsigned int FB7 : 1;
			unsigned int FB8 : 1;
			unsigned int FB9 : 1;
			unsigned int FB10 : 1;
			unsigned int FB11 : 1;
			unsigned int FB12 : 1;
			unsigned int FB13 : 1;
			unsigned int FB14 : 1;
			unsigned int FB15 : 1;
			unsigned int FB16 : 1;
			unsigned int FB17 : 1;
			unsigned int FB18 : 1;
			unsigned int FB19 : 1;
			unsigned int FB20 : 1;
			unsigned int FB21 : 1;
			unsigned int FB22 : 1;
			unsigned int FB23 : 1;
			unsigned int FB24 : 1;
			unsigned int FB25 : 1;
			unsigned int FB26 : 1;
			unsigned int FB27 : 1;
			unsigned int FB28 : 1;
			unsigned int FB29 : 1;
			unsigned int FB30 : 1;
			unsigned int FB31 : 1;
		} __attribute((__packed__));
	};
};
#define CAN_F0R2 (*((volatile struct CAN_F0R2_s*) 0x40006644))

struct CAN_F1R1_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int FB0 : 1;
			unsigned int FB1 : 1;
			unsigned int FB2 : 1;
			unsigned int FB3 : 1;
			unsigned int FB4 : 1;
			unsigned int FB5 : 1;
			unsigned int FB6 : 1;
			unsigned int FB7 : 1;
			unsigned int FB8 : 1;
			unsigned int FB9 : 1;
			unsigned int FB10 : 1;
			unsigned int FB11 : 1;
			unsigned int FB12 : 1;
			unsigned int FB13 : 1;
			unsigned int FB14 : 1;
			unsigned int FB15 : 1;
			unsigned int FB16 : 1;
			unsigned int FB17 : 1;
			unsigned int FB18 : 1;
			unsigned int FB19 : 1;
			unsigned int FB20 : 1;
			unsigned int FB21 : 1;
			unsigned int FB22 : 1;
			unsigned int FB23 : 1;
			unsigned int FB24 : 1;
			unsigned int FB25 : 1;
			unsigned int FB26 : 1;
			unsigned int FB27 : 1;
			unsigned int FB28 : 1;
			unsigned int FB29 : 1;
			unsigned int FB30 : 1;
			unsigned int FB31 : 1;
		} __attribute((__packed__));
	};
};
#define CAN_F1R1 (*((volatile struct CAN_F1R1_s*) 0x40006648))

struct CAN_F1R2_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int FB0 : 1;
			unsigned int FB1 : 1;
			unsigned int FB2 : 1;
			unsigned int FB3 : 1;
			unsigned int FB4 : 1;
			unsigned int FB5 : 1;
			unsigned int FB6 : 1;
			unsigned int FB7 : 1;
			unsigned int FB8 : 1;
			unsigned int FB9 : 1;
			unsigned int FB10 : 1;
			unsigned int FB11 : 1;
			unsigned int FB12 : 1;
			unsigned int FB13 : 1;
			unsigned int FB14 : 1;
			unsigned int FB15 : 1;
			unsigned int FB16 : 1;
			unsigned int FB17 : 1;
			unsigned int FB18 : 1;
			unsigned int FB19 : 1;
			unsigned int FB20 : 1;
			unsigned int FB21 : 1;
			unsigned int FB22 : 1;
			unsigned int FB23 : 1;
			unsigned int FB24 : 1;
			unsigned int FB25 : 1;
			unsigned int FB26 : 1;
			unsigned int FB27 : 1;
			unsigned int FB28 : 1;
			unsigned int FB29 : 1;
			unsigned int FB30 : 1;
			unsigned int FB31 : 1;
		} __attribute((__packed__));
	};
};
#define CAN_F1R2 (*((volatile struct CAN_F1R2_s*) 0x4000664c))

struct CAN_F2R1_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int FB0 : 1;
			unsigned int FB1 : 1;
			unsigned int FB2 : 1;
			unsigned int FB3 : 1;
			unsigned int FB4 : 1;
			unsigned int FB5 : 1;
			unsigned int FB6 : 1;
			unsigned int FB7 : 1;
			unsigned int FB8 : 1;
			unsigned int FB9 : 1;
			unsigned int FB10 : 1;
			unsigned int FB11 : 1;
			unsigned int FB12 : 1;
			unsigned int FB13 : 1;
			unsigned int FB14 : 1;
			unsigned int FB15 : 1;
			unsigned int FB16 : 1;
			unsigned int FB17 : 1;
			unsigned int FB18 : 1;
			unsigned int FB19 : 1;
			unsigned int FB20 : 1;
			unsigned int FB21 : 1;
			unsigned int FB22 : 1;
			unsigned int FB23 : 1;
			unsigned int FB24 : 1;
			unsigned int FB25 : 1;
			unsigned int FB26 : 1;
			unsigned int FB27 : 1;
			unsigned int FB28 : 1;
			unsigned int FB29 : 1;
			unsigned int FB30 : 1;
			unsigned int FB31 : 1;
		} __attribute((__packed__));
	};
};
#define CAN_F2R1 (*((volatile struct CAN_F2R1_s*) 0x40006650))

struct CAN_F2R2_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int FB0 : 1;
			unsigned int FB1 : 1;
			unsigned int FB2 : 1;
			unsigned int FB3 : 1;
			unsigned int FB4 : 1;
			unsigned int FB5 : 1;
			unsigned int FB6 : 1;
			unsigned int FB7 : 1;
			unsigned int FB8 : 1;
			unsigned int FB9 : 1;
			unsigned int FB10 : 1;
			unsigned int FB11 : 1;
			unsigned int FB12 : 1;
			unsigned int FB13 : 1;
			unsigned int FB14 : 1;
			unsigned int FB15 : 1;
			unsigned int FB16 : 1;
			unsigned int FB17 : 1;
			unsigned int FB18 : 1;
			unsigned int FB19 : 1;
			unsigned int FB20 : 1;
			unsigned int FB21 : 1;
			unsigned int FB22 : 1;
			unsigned int FB23 : 1;
			unsigned int FB24 : 1;
			unsigned int FB25 : 1;
			unsigned int FB26 : 1;
			unsigned int FB27 : 1;
			unsigned int FB28 : 1;
			unsigned int FB29 : 1;
			unsigned int FB30 : 1;
			unsigned int FB31 : 1;
		} __attribute((__packed__));
	};
};
#define CAN_F2R2 (*((volatile struct CAN_F2R2_s*) 0x40006654))

struct CAN_F3R1_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int FB0 : 1;
			unsigned int FB1 : 1;
			unsigned int FB2 : 1;
			unsigned int FB3 : 1;
			unsigned int FB4 : 1;
			unsigned int FB5 : 1;
			unsigned int FB6 : 1;
			unsigned int FB7 : 1;
			unsigned int FB8 : 1;
			unsigned int FB9 : 1;
			unsigned int FB10 : 1;
			unsigned int FB11 : 1;
			unsigned int FB12 : 1;
			unsigned int FB13 : 1;
			unsigned int FB14 : 1;
			unsigned int FB15 : 1;
			unsigned int FB16 : 1;
			unsigned int FB17 : 1;
			unsigned int FB18 : 1;
			unsigned int FB19 : 1;
			unsigned int FB20 : 1;
			unsigned int FB21 : 1;
			unsigned int FB22 : 1;
			unsigned int FB23 : 1;
			unsigned int FB24 : 1;
			unsigned int FB25 : 1;
			unsigned int FB26 : 1;
			unsigned int FB27 : 1;
			unsigned int FB28 : 1;
			unsigned int FB29 : 1;
			unsigned int FB30 : 1;
			unsigned int FB31 : 1;
		} __attribute((__packed__));
	};
};
#define CAN_F3R1 (*((volatile struct CAN_F3R1_s*) 0x40006658))

struct CAN_F3R2_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int FB0 : 1;
			unsigned int FB1 : 1;
			unsigned int FB2 : 1;
			unsigned int FB3 : 1;
			unsigned int FB4 : 1;
			unsigned int FB5 : 1;
			unsigned int FB6 : 1;
			unsigned int FB7 : 1;
			unsigned int FB8 : 1;
			unsigned int FB9 : 1;
			unsigned int FB10 : 1;
			unsigned int FB11 : 1;
			unsigned int FB12 : 1;
			unsigned int FB13 : 1;
			unsigned int FB14 : 1;
			unsigned int FB15 : 1;
			unsigned int FB16 : 1;
			unsigned int FB17 : 1;
			unsigned int FB18 : 1;
			unsigned int FB19 : 1;
			unsigned int FB20 : 1;
			unsigned int FB21 : 1;
			unsigned int FB22 : 1;
			unsigned int FB23 : 1;
			unsigned int FB24 : 1;
			unsigned int FB25 : 1;
			unsigned int FB26 : 1;
			unsigned int FB27 : 1;
			unsigned int FB28 : 1;
			unsigned int FB29 : 1;
			unsigned int FB30 : 1;
			unsigned int FB31 : 1;
		} __attribute((__packed__));
	};
};
#define CAN_F3R2 (*((volatile struct CAN_F3R2_s*) 0x4000665c))

struct CAN_F4R1_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int FB0 : 1;
			unsigned int FB1 : 1;
			unsigned int FB2 : 1;
			unsigned int FB3 : 1;
			unsigned int FB4 : 1;
			unsigned int FB5 : 1;
			unsigned int FB6 : 1;
			unsigned int FB7 : 1;
			unsigned int FB8 : 1;
			unsigned int FB9 : 1;
			unsigned int FB10 : 1;
			unsigned int FB11 : 1;
			unsigned int FB12 : 1;
			unsigned int FB13 : 1;
			unsigned int FB14 : 1;
			unsigned int FB15 : 1;
			unsigned int FB16 : 1;
			unsigned int FB17 : 1;
			unsigned int FB18 : 1;
			unsigned int FB19 : 1;
			unsigned int FB20 : 1;
			unsigned int FB21 : 1;
			unsigned int FB22 : 1;
			unsigned int FB23 : 1;
			unsigned int FB24 : 1;
			unsigned int FB25 : 1;
			unsigned int FB26 : 1;
			unsigned int FB27 : 1;
			unsigned int FB28 : 1;
			unsigned int FB29 : 1;
			unsigned int FB30 : 1;
			unsigned int FB31 : 1;
		} __attribute((__packed__));
	};
};
#define CAN_F4R1 (*((volatile struct CAN_F4R1_s*) 0x40006660))

struct CAN_F4R2_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int FB0 : 1;
			unsigned int FB1 : 1;
			unsigned int FB2 : 1;
			unsigned int FB3 : 1;
			unsigned int FB4 : 1;
			unsigned int FB5 : 1;
			unsigned int FB6 : 1;
			unsigned int FB7 : 1;
			unsigned int FB8 : 1;
			unsigned int FB9 : 1;
			unsigned int FB10 : 1;
			unsigned int FB11 : 1;
			unsigned int FB12 : 1;
			unsigned int FB13 : 1;
			unsigned int FB14 : 1;
			unsigned int FB15 : 1;
			unsigned int FB16 : 1;
			unsigned int FB17 : 1;
			unsigned int FB18 : 1;
			unsigned int FB19 : 1;
			unsigned int FB20 : 1;
			unsigned int FB21 : 1;
			unsigned int FB22 : 1;
			unsigned int FB23 : 1;
			unsigned int FB24 : 1;
			unsigned int FB25 : 1;
			unsigned int FB26 : 1;
			unsigned int FB27 : 1;
			unsigned int FB28 : 1;
			unsigned int FB29 : 1;
			unsigned int FB30 : 1;
			unsigned int FB31 : 1;
		} __attribute((__packed__));
	};
};
#define CAN_F4R2 (*((volatile struct CAN_F4R2_s*) 0x40006664))

struct CAN_F5R1_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int FB0 : 1;
			unsigned int FB1 : 1;
			unsigned int FB2 : 1;
			unsigned int FB3 : 1;
			unsigned int FB4 : 1;
			unsigned int FB5 : 1;
			unsigned int FB6 : 1;
			unsigned int FB7 : 1;
			unsigned int FB8 : 1;
			unsigned int FB9 : 1;
			unsigned int FB10 : 1;
			unsigned int FB11 : 1;
			unsigned int FB12 : 1;
			unsigned int FB13 : 1;
			unsigned int FB14 : 1;
			unsigned int FB15 : 1;
			unsigned int FB16 : 1;
			unsigned int FB17 : 1;
			unsigned int FB18 : 1;
			unsigned int FB19 : 1;
			unsigned int FB20 : 1;
			unsigned int FB21 : 1;
			unsigned int FB22 : 1;
			unsigned int FB23 : 1;
			unsigned int FB24 : 1;
			unsigned int FB25 : 1;
			unsigned int FB26 : 1;
			unsigned int FB27 : 1;
			unsigned int FB28 : 1;
			unsigned int FB29 : 1;
			unsigned int FB30 : 1;
			unsigned int FB31 : 1;
		} __attribute((__packed__));
	};
};
#define CAN_F5R1 (*((volatile struct CAN_F5R1_s*) 0x40006668))

struct CAN_F5R2_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int FB0 : 1;
			unsigned int FB1 : 1;
			unsigned int FB2 : 1;
			unsigned int FB3 : 1;
			unsigned int FB4 : 1;
			unsigned int FB5 : 1;
			unsigned int FB6 : 1;
			unsigned int FB7 : 1;
			unsigned int FB8 : 1;
			unsigned int FB9 : 1;
			unsigned int FB10 : 1;
			unsigned int FB11 : 1;
			unsigned int FB12 : 1;
			unsigned int FB13 : 1;
			unsigned int FB14 : 1;
			unsigned int FB15 : 1;
			unsigned int FB16 : 1;
			unsigned int FB17 : 1;
			unsigned int FB18 : 1;
			unsigned int FB19 : 1;
			unsigned int FB20 : 1;
			unsigned int FB21 : 1;
			unsigned int FB22 : 1;
			unsigned int FB23 : 1;
			unsigned int FB24 : 1;
			unsigned int FB25 : 1;
			unsigned int FB26 : 1;
			unsigned int FB27 : 1;
			unsigned int FB28 : 1;
			unsigned int FB29 : 1;
			unsigned int FB30 : 1;
			unsigned int FB31 : 1;
		} __attribute((__packed__));
	};
};
#define CAN_F5R2 (*((volatile struct CAN_F5R2_s*) 0x4000666c))

struct CAN_F6R1_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int FB0 : 1;
			unsigned int FB1 : 1;
			unsigned int FB2 : 1;
			unsigned int FB3 : 1;
			unsigned int FB4 : 1;
			unsigned int FB5 : 1;
			unsigned int FB6 : 1;
			unsigned int FB7 : 1;
			unsigned int FB8 : 1;
			unsigned int FB9 : 1;
			unsigned int FB10 : 1;
			unsigned int FB11 : 1;
			unsigned int FB12 : 1;
			unsigned int FB13 : 1;
			unsigned int FB14 : 1;
			unsigned int FB15 : 1;
			unsigned int FB16 : 1;
			unsigned int FB17 : 1;
			unsigned int FB18 : 1;
			unsigned int FB19 : 1;
			unsigned int FB20 : 1;
			unsigned int FB21 : 1;
			unsigned int FB22 : 1;
			unsigned int FB23 : 1;
			unsigned int FB24 : 1;
			unsigned int FB25 : 1;
			unsigned int FB26 : 1;
			unsigned int FB27 : 1;
			unsigned int FB28 : 1;
			unsigned int FB29 : 1;
			unsigned int FB30 : 1;
			unsigned int FB31 : 1;
		} __attribute((__packed__));
	};
};
#define CAN_F6R1 (*((volatile struct CAN_F6R1_s*) 0x40006670))

struct CAN_F6R2_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int FB0 : 1;
			unsigned int FB1 : 1;
			unsigned int FB2 : 1;
			unsigned int FB3 : 1;
			unsigned int FB4 : 1;
			unsigned int FB5 : 1;
			unsigned int FB6 : 1;
			unsigned int FB7 : 1;
			unsigned int FB8 : 1;
			unsigned int FB9 : 1;
			unsigned int FB10 : 1;
			unsigned int FB11 : 1;
			unsigned int FB12 : 1;
			unsigned int FB13 : 1;
			unsigned int FB14 : 1;
			unsigned int FB15 : 1;
			unsigned int FB16 : 1;
			unsigned int FB17 : 1;
			unsigned int FB18 : 1;
			unsigned int FB19 : 1;
			unsigned int FB20 : 1;
			unsigned int FB21 : 1;
			unsigned int FB22 : 1;
			unsigned int FB23 : 1;
			unsigned int FB24 : 1;
			unsigned int FB25 : 1;
			unsigned int FB26 : 1;
			unsigned int FB27 : 1;
			unsigned int FB28 : 1;
			unsigned int FB29 : 1;
			unsigned int FB30 : 1;
			unsigned int FB31 : 1;
		} __attribute((__packed__));
	};
};
#define CAN_F6R2 (*((volatile struct CAN_F6R2_s*) 0x40006674))

struct CAN_F7R1_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int FB0 : 1;
			unsigned int FB1 : 1;
			unsigned int FB2 : 1;
			unsigned int FB3 : 1;
			unsigned int FB4 : 1;
			unsigned int FB5 : 1;
			unsigned int FB6 : 1;
			unsigned int FB7 : 1;
			unsigned int FB8 : 1;
			unsigned int FB9 : 1;
			unsigned int FB10 : 1;
			unsigned int FB11 : 1;
			unsigned int FB12 : 1;
			unsigned int FB13 : 1;
			unsigned int FB14 : 1;
			unsigned int FB15 : 1;
			unsigned int FB16 : 1;
			unsigned int FB17 : 1;
			unsigned int FB18 : 1;
			unsigned int FB19 : 1;
			unsigned int FB20 : 1;
			unsigned int FB21 : 1;
			unsigned int FB22 : 1;
			unsigned int FB23 : 1;
			unsigned int FB24 : 1;
			unsigned int FB25 : 1;
			unsigned int FB26 : 1;
			unsigned int FB27 : 1;
			unsigned int FB28 : 1;
			unsigned int FB29 : 1;
			unsigned int FB30 : 1;
			unsigned int FB31 : 1;
		} __attribute((__packed__));
	};
};
#define CAN_F7R1 (*((volatile struct CAN_F7R1_s*) 0x40006678))

struct CAN_F7R2_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int FB0 : 1;
			unsigned int FB1 : 1;
			unsigned int FB2 : 1;
			unsigned int FB3 : 1;
			unsigned int FB4 : 1;
			unsigned int FB5 : 1;
			unsigned int FB6 : 1;
			unsigned int FB7 : 1;
			unsigned int FB8 : 1;
			unsigned int FB9 : 1;
			unsigned int FB10 : 1;
			unsigned int FB11 : 1;
			unsigned int FB12 : 1;
			unsigned int FB13 : 1;
			unsigned int FB14 : 1;
			unsigned int FB15 : 1;
			unsigned int FB16 : 1;
			unsigned int FB17 : 1;
			unsigned int FB18 : 1;
			unsigned int FB19 : 1;
			unsigned int FB20 : 1;
			unsigned int FB21 : 1;
			unsigned int FB22 : 1;
			unsigned int FB23 : 1;
			unsigned int FB24 : 1;
			unsigned int FB25 : 1;
			unsigned int FB26 : 1;
			unsigned int FB27 : 1;
			unsigned int FB28 : 1;
			unsigned int FB29 : 1;
			unsigned int FB30 : 1;
			unsigned int FB31 : 1;
		} __attribute((__packed__));
	};
};
#define CAN_F7R2 (*((volatile struct CAN_F7R2_s*) 0x4000667c))

struct CAN_F8R1_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int FB0 : 1;
			unsigned int FB1 : 1;
			unsigned int FB2 : 1;
			unsigned int FB3 : 1;
			unsigned int FB4 : 1;
			unsigned int FB5 : 1;
			unsigned int FB6 : 1;
			unsigned int FB7 : 1;
			unsigned int FB8 : 1;
			unsigned int FB9 : 1;
			unsigned int FB10 : 1;
			unsigned int FB11 : 1;
			unsigned int FB12 : 1;
			unsigned int FB13 : 1;
			unsigned int FB14 : 1;
			unsigned int FB15 : 1;
			unsigned int FB16 : 1;
			unsigned int FB17 : 1;
			unsigned int FB18 : 1;
			unsigned int FB19 : 1;
			unsigned int FB20 : 1;
			unsigned int FB21 : 1;
			unsigned int FB22 : 1;
			unsigned int FB23 : 1;
			unsigned int FB24 : 1;
			unsigned int FB25 : 1;
			unsigned int FB26 : 1;
			unsigned int FB27 : 1;
			unsigned int FB28 : 1;
			unsigned int FB29 : 1;
			unsigned int FB30 : 1;
			unsigned int FB31 : 1;
		} __attribute((__packed__));
	};
};
#define CAN_F8R1 (*((volatile struct CAN_F8R1_s*) 0x40006680))

struct CAN_F8R2_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int FB0 : 1;
			unsigned int FB1 : 1;
			unsigned int FB2 : 1;
			unsigned int FB3 : 1;
			unsigned int FB4 : 1;
			unsigned int FB5 : 1;
			unsigned int FB6 : 1;
			unsigned int FB7 : 1;
			unsigned int FB8 : 1;
			unsigned int FB9 : 1;
			unsigned int FB10 : 1;
			unsigned int FB11 : 1;
			unsigned int FB12 : 1;
			unsigned int FB13 : 1;
			unsigned int FB14 : 1;
			unsigned int FB15 : 1;
			unsigned int FB16 : 1;
			unsigned int FB17 : 1;
			unsigned int FB18 : 1;
			unsigned int FB19 : 1;
			unsigned int FB20 : 1;
			unsigned int FB21 : 1;
			unsigned int FB22 : 1;
			unsigned int FB23 : 1;
			unsigned int FB24 : 1;
			unsigned int FB25 : 1;
			unsigned int FB26 : 1;
			unsigned int FB27 : 1;
			unsigned int FB28 : 1;
			unsigned int FB29 : 1;
			unsigned int FB30 : 1;
			unsigned int FB31 : 1;
		} __attribute((__packed__));
	};
};
#define CAN_F8R2 (*((volatile struct CAN_F8R2_s*) 0x40006684))

struct CAN_F9R1_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int FB0 : 1;
			unsigned int FB1 : 1;
			unsigned int FB2 : 1;
			unsigned int FB3 : 1;
			unsigned int FB4 : 1;
			unsigned int FB5 : 1;
			unsigned int FB6 : 1;
			unsigned int FB7 : 1;
			unsigned int FB8 : 1;
			unsigned int FB9 : 1;
			unsigned int FB10 : 1;
			unsigned int FB11 : 1;
			unsigned int FB12 : 1;
			unsigned int FB13 : 1;
			unsigned int FB14 : 1;
			unsigned int FB15 : 1;
			unsigned int FB16 : 1;
			unsigned int FB17 : 1;
			unsigned int FB18 : 1;
			unsigned int FB19 : 1;
			unsigned int FB20 : 1;
			unsigned int FB21 : 1;
			unsigned int FB22 : 1;
			unsigned int FB23 : 1;
			unsigned int FB24 : 1;
			unsigned int FB25 : 1;
			unsigned int FB26 : 1;
			unsigned int FB27 : 1;
			unsigned int FB28 : 1;
			unsigned int FB29 : 1;
			unsigned int FB30 : 1;
			unsigned int FB31 : 1;
		} __attribute((__packed__));
	};
};
#define CAN_F9R1 (*((volatile struct CAN_F9R1_s*) 0x40006688))

struct CAN_F9R2_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int FB0 : 1;
			unsigned int FB1 : 1;
			unsigned int FB2 : 1;
			unsigned int FB3 : 1;
			unsigned int FB4 : 1;
			unsigned int FB5 : 1;
			unsigned int FB6 : 1;
			unsigned int FB7 : 1;
			unsigned int FB8 : 1;
			unsigned int FB9 : 1;
			unsigned int FB10 : 1;
			unsigned int FB11 : 1;
			unsigned int FB12 : 1;
			unsigned int FB13 : 1;
			unsigned int FB14 : 1;
			unsigned int FB15 : 1;
			unsigned int FB16 : 1;
			unsigned int FB17 : 1;
			unsigned int FB18 : 1;
			unsigned int FB19 : 1;
			unsigned int FB20 : 1;
			unsigned int FB21 : 1;
			unsigned int FB22 : 1;
			unsigned int FB23 : 1;
			unsigned int FB24 : 1;
			unsigned int FB25 : 1;
			unsigned int FB26 : 1;
			unsigned int FB27 : 1;
			unsigned int FB28 : 1;
			unsigned int FB29 : 1;
			unsigned int FB30 : 1;
			unsigned int FB31 : 1;
		} __attribute((__packed__));
	};
};
#define CAN_F9R2 (*((volatile struct CAN_F9R2_s*) 0x4000668c))

struct CAN_F10R1_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int FB0 : 1;
			unsigned int FB1 : 1;
			unsigned int FB2 : 1;
			unsigned int FB3 : 1;
			unsigned int FB4 : 1;
			unsigned int FB5 : 1;
			unsigned int FB6 : 1;
			unsigned int FB7 : 1;
			unsigned int FB8 : 1;
			unsigned int FB9 : 1;
			unsigned int FB10 : 1;
			unsigned int FB11 : 1;
			unsigned int FB12 : 1;
			unsigned int FB13 : 1;
			unsigned int FB14 : 1;
			unsigned int FB15 : 1;
			unsigned int FB16 : 1;
			unsigned int FB17 : 1;
			unsigned int FB18 : 1;
			unsigned int FB19 : 1;
			unsigned int FB20 : 1;
			unsigned int FB21 : 1;
			unsigned int FB22 : 1;
			unsigned int FB23 : 1;
			unsigned int FB24 : 1;
			unsigned int FB25 : 1;
			unsigned int FB26 : 1;
			unsigned int FB27 : 1;
			unsigned int FB28 : 1;
			unsigned int FB29 : 1;
			unsigned int FB30 : 1;
			unsigned int FB31 : 1;
		} __attribute((__packed__));
	};
};
#define CAN_F10R1 (*((volatile struct CAN_F10R1_s*) 0x40006690))

struct CAN_F10R2_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int FB0 : 1;
			unsigned int FB1 : 1;
			unsigned int FB2 : 1;
			unsigned int FB3 : 1;
			unsigned int FB4 : 1;
			unsigned int FB5 : 1;
			unsigned int FB6 : 1;
			unsigned int FB7 : 1;
			unsigned int FB8 : 1;
			unsigned int FB9 : 1;
			unsigned int FB10 : 1;
			unsigned int FB11 : 1;
			unsigned int FB12 : 1;
			unsigned int FB13 : 1;
			unsigned int FB14 : 1;
			unsigned int FB15 : 1;
			unsigned int FB16 : 1;
			unsigned int FB17 : 1;
			unsigned int FB18 : 1;
			unsigned int FB19 : 1;
			unsigned int FB20 : 1;
			unsigned int FB21 : 1;
			unsigned int FB22 : 1;
			unsigned int FB23 : 1;
			unsigned int FB24 : 1;
			unsigned int FB25 : 1;
			unsigned int FB26 : 1;
			unsigned int FB27 : 1;
			unsigned int FB28 : 1;
			unsigned int FB29 : 1;
			unsigned int FB30 : 1;
			unsigned int FB31 : 1;
		} __attribute((__packed__));
	};
};
#define CAN_F10R2 (*((volatile struct CAN_F10R2_s*) 0x40006694))

struct CAN_F11R1_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int FB0 : 1;
			unsigned int FB1 : 1;
			unsigned int FB2 : 1;
			unsigned int FB3 : 1;
			unsigned int FB4 : 1;
			unsigned int FB5 : 1;
			unsigned int FB6 : 1;
			unsigned int FB7 : 1;
			unsigned int FB8 : 1;
			unsigned int FB9 : 1;
			unsigned int FB10 : 1;
			unsigned int FB11 : 1;
			unsigned int FB12 : 1;
			unsigned int FB13 : 1;
			unsigned int FB14 : 1;
			unsigned int FB15 : 1;
			unsigned int FB16 : 1;
			unsigned int FB17 : 1;
			unsigned int FB18 : 1;
			unsigned int FB19 : 1;
			unsigned int FB20 : 1;
			unsigned int FB21 : 1;
			unsigned int FB22 : 1;
			unsigned int FB23 : 1;
			unsigned int FB24 : 1;
			unsigned int FB25 : 1;
			unsigned int FB26 : 1;
			unsigned int FB27 : 1;
			unsigned int FB28 : 1;
			unsigned int FB29 : 1;
			unsigned int FB30 : 1;
			unsigned int FB31 : 1;
		} __attribute((__packed__));
	};
};
#define CAN_F11R1 (*((volatile struct CAN_F11R1_s*) 0x40006698))

struct CAN_F11R2_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int FB0 : 1;
			unsigned int FB1 : 1;
			unsigned int FB2 : 1;
			unsigned int FB3 : 1;
			unsigned int FB4 : 1;
			unsigned int FB5 : 1;
			unsigned int FB6 : 1;
			unsigned int FB7 : 1;
			unsigned int FB8 : 1;
			unsigned int FB9 : 1;
			unsigned int FB10 : 1;
			unsigned int FB11 : 1;
			unsigned int FB12 : 1;
			unsigned int FB13 : 1;
			unsigned int FB14 : 1;
			unsigned int FB15 : 1;
			unsigned int FB16 : 1;
			unsigned int FB17 : 1;
			unsigned int FB18 : 1;
			unsigned int FB19 : 1;
			unsigned int FB20 : 1;
			unsigned int FB21 : 1;
			unsigned int FB22 : 1;
			unsigned int FB23 : 1;
			unsigned int FB24 : 1;
			unsigned int FB25 : 1;
			unsigned int FB26 : 1;
			unsigned int FB27 : 1;
			unsigned int FB28 : 1;
			unsigned int FB29 : 1;
			unsigned int FB30 : 1;
			unsigned int FB31 : 1;
		} __attribute((__packed__));
	};
};
#define CAN_F11R2 (*((volatile struct CAN_F11R2_s*) 0x4000669c))

struct CAN_F12R1_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int FB0 : 1;
			unsigned int FB1 : 1;
			unsigned int FB2 : 1;
			unsigned int FB3 : 1;
			unsigned int FB4 : 1;
			unsigned int FB5 : 1;
			unsigned int FB6 : 1;
			unsigned int FB7 : 1;
			unsigned int FB8 : 1;
			unsigned int FB9 : 1;
			unsigned int FB10 : 1;
			unsigned int FB11 : 1;
			unsigned int FB12 : 1;
			unsigned int FB13 : 1;
			unsigned int FB14 : 1;
			unsigned int FB15 : 1;
			unsigned int FB16 : 1;
			unsigned int FB17 : 1;
			unsigned int FB18 : 1;
			unsigned int FB19 : 1;
			unsigned int FB20 : 1;
			unsigned int FB21 : 1;
			unsigned int FB22 : 1;
			unsigned int FB23 : 1;
			unsigned int FB24 : 1;
			unsigned int FB25 : 1;
			unsigned int FB26 : 1;
			unsigned int FB27 : 1;
			unsigned int FB28 : 1;
			unsigned int FB29 : 1;
			unsigned int FB30 : 1;
			unsigned int FB31 : 1;
		} __attribute((__packed__));
	};
};
#define CAN_F12R1 (*((volatile struct CAN_F12R1_s*) 0x400066a0))

struct CAN_F12R2_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int FB0 : 1;
			unsigned int FB1 : 1;
			unsigned int FB2 : 1;
			unsigned int FB3 : 1;
			unsigned int FB4 : 1;
			unsigned int FB5 : 1;
			unsigned int FB6 : 1;
			unsigned int FB7 : 1;
			unsigned int FB8 : 1;
			unsigned int FB9 : 1;
			unsigned int FB10 : 1;
			unsigned int FB11 : 1;
			unsigned int FB12 : 1;
			unsigned int FB13 : 1;
			unsigned int FB14 : 1;
			unsigned int FB15 : 1;
			unsigned int FB16 : 1;
			unsigned int FB17 : 1;
			unsigned int FB18 : 1;
			unsigned int FB19 : 1;
			unsigned int FB20 : 1;
			unsigned int FB21 : 1;
			unsigned int FB22 : 1;
			unsigned int FB23 : 1;
			unsigned int FB24 : 1;
			unsigned int FB25 : 1;
			unsigned int FB26 : 1;
			unsigned int FB27 : 1;
			unsigned int FB28 : 1;
			unsigned int FB29 : 1;
			unsigned int FB30 : 1;
			unsigned int FB31 : 1;
		} __attribute((__packed__));
	};
};
#define CAN_F12R2 (*((volatile struct CAN_F12R2_s*) 0x400066a4))

struct CAN_F13R1_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int FB0 : 1;
			unsigned int FB1 : 1;
			unsigned int FB2 : 1;
			unsigned int FB3 : 1;
			unsigned int FB4 : 1;
			unsigned int FB5 : 1;
			unsigned int FB6 : 1;
			unsigned int FB7 : 1;
			unsigned int FB8 : 1;
			unsigned int FB9 : 1;
			unsigned int FB10 : 1;
			unsigned int FB11 : 1;
			unsigned int FB12 : 1;
			unsigned int FB13 : 1;
			unsigned int FB14 : 1;
			unsigned int FB15 : 1;
			unsigned int FB16 : 1;
			unsigned int FB17 : 1;
			unsigned int FB18 : 1;
			unsigned int FB19 : 1;
			unsigned int FB20 : 1;
			unsigned int FB21 : 1;
			unsigned int FB22 : 1;
			unsigned int FB23 : 1;
			unsigned int FB24 : 1;
			unsigned int FB25 : 1;
			unsigned int FB26 : 1;
			unsigned int FB27 : 1;
			unsigned int FB28 : 1;
			unsigned int FB29 : 1;
			unsigned int FB30 : 1;
			unsigned int FB31 : 1;
		} __attribute((__packed__));
	};
};
#define CAN_F13R1 (*((volatile struct CAN_F13R1_s*) 0x400066a8))

struct CAN_F13R2_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int FB0 : 1;
			unsigned int FB1 : 1;
			unsigned int FB2 : 1;
			unsigned int FB3 : 1;
			unsigned int FB4 : 1;
			unsigned int FB5 : 1;
			unsigned int FB6 : 1;
			unsigned int FB7 : 1;
			unsigned int FB8 : 1;
			unsigned int FB9 : 1;
			unsigned int FB10 : 1;
			unsigned int FB11 : 1;
			unsigned int FB12 : 1;
			unsigned int FB13 : 1;
			unsigned int FB14 : 1;
			unsigned int FB15 : 1;
			unsigned int FB16 : 1;
			unsigned int FB17 : 1;
			unsigned int FB18 : 1;
			unsigned int FB19 : 1;
			unsigned int FB20 : 1;
			unsigned int FB21 : 1;
			unsigned int FB22 : 1;
			unsigned int FB23 : 1;
			unsigned int FB24 : 1;
			unsigned int FB25 : 1;
			unsigned int FB26 : 1;
			unsigned int FB27 : 1;
			unsigned int FB28 : 1;
			unsigned int FB29 : 1;
			unsigned int FB30 : 1;
			unsigned int FB31 : 1;
		} __attribute((__packed__));
	};
};
#define CAN_F13R2 (*((volatile struct CAN_F13R2_s*) 0x400066ac))

struct CAN_F14R1_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int FB0 : 1;
			unsigned int FB1 : 1;
			unsigned int FB2 : 1;
			unsigned int FB3 : 1;
			unsigned int FB4 : 1;
			unsigned int FB5 : 1;
			unsigned int FB6 : 1;
			unsigned int FB7 : 1;
			unsigned int FB8 : 1;
			unsigned int FB9 : 1;
			unsigned int FB10 : 1;
			unsigned int FB11 : 1;
			unsigned int FB12 : 1;
			unsigned int FB13 : 1;
			unsigned int FB14 : 1;
			unsigned int FB15 : 1;
			unsigned int FB16 : 1;
			unsigned int FB17 : 1;
			unsigned int FB18 : 1;
			unsigned int FB19 : 1;
			unsigned int FB20 : 1;
			unsigned int FB21 : 1;
			unsigned int FB22 : 1;
			unsigned int FB23 : 1;
			unsigned int FB24 : 1;
			unsigned int FB25 : 1;
			unsigned int FB26 : 1;
			unsigned int FB27 : 1;
			unsigned int FB28 : 1;
			unsigned int FB29 : 1;
			unsigned int FB30 : 1;
			unsigned int FB31 : 1;
		} __attribute((__packed__));
	};
};
#define CAN_F14R1 (*((volatile struct CAN_F14R1_s*) 0x400066b0))

struct CAN_F14R2_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int FB0 : 1;
			unsigned int FB1 : 1;
			unsigned int FB2 : 1;
			unsigned int FB3 : 1;
			unsigned int FB4 : 1;
			unsigned int FB5 : 1;
			unsigned int FB6 : 1;
			unsigned int FB7 : 1;
			unsigned int FB8 : 1;
			unsigned int FB9 : 1;
			unsigned int FB10 : 1;
			unsigned int FB11 : 1;
			unsigned int FB12 : 1;
			unsigned int FB13 : 1;
			unsigned int FB14 : 1;
			unsigned int FB15 : 1;
			unsigned int FB16 : 1;
			unsigned int FB17 : 1;
			unsigned int FB18 : 1;
			unsigned int FB19 : 1;
			unsigned int FB20 : 1;
			unsigned int FB21 : 1;
			unsigned int FB22 : 1;
			unsigned int FB23 : 1;
			unsigned int FB24 : 1;
			unsigned int FB25 : 1;
			unsigned int FB26 : 1;
			unsigned int FB27 : 1;
			unsigned int FB28 : 1;
			unsigned int FB29 : 1;
			unsigned int FB30 : 1;
			unsigned int FB31 : 1;
		} __attribute((__packed__));
	};
};
#define CAN_F14R2 (*((volatile struct CAN_F14R2_s*) 0x400066b4))

struct CAN_F15R1_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int FB0 : 1;
			unsigned int FB1 : 1;
			unsigned int FB2 : 1;
			unsigned int FB3 : 1;
			unsigned int FB4 : 1;
			unsigned int FB5 : 1;
			unsigned int FB6 : 1;
			unsigned int FB7 : 1;
			unsigned int FB8 : 1;
			unsigned int FB9 : 1;
			unsigned int FB10 : 1;
			unsigned int FB11 : 1;
			unsigned int FB12 : 1;
			unsigned int FB13 : 1;
			unsigned int FB14 : 1;
			unsigned int FB15 : 1;
			unsigned int FB16 : 1;
			unsigned int FB17 : 1;
			unsigned int FB18 : 1;
			unsigned int FB19 : 1;
			unsigned int FB20 : 1;
			unsigned int FB21 : 1;
			unsigned int FB22 : 1;
			unsigned int FB23 : 1;
			unsigned int FB24 : 1;
			unsigned int FB25 : 1;
			unsigned int FB26 : 1;
			unsigned int FB27 : 1;
			unsigned int FB28 : 1;
			unsigned int FB29 : 1;
			unsigned int FB30 : 1;
			unsigned int FB31 : 1;
		} __attribute((__packed__));
	};
};
#define CAN_F15R1 (*((volatile struct CAN_F15R1_s*) 0x400066b8))

struct CAN_F15R2_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int FB0 : 1;
			unsigned int FB1 : 1;
			unsigned int FB2 : 1;
			unsigned int FB3 : 1;
			unsigned int FB4 : 1;
			unsigned int FB5 : 1;
			unsigned int FB6 : 1;
			unsigned int FB7 : 1;
			unsigned int FB8 : 1;
			unsigned int FB9 : 1;
			unsigned int FB10 : 1;
			unsigned int FB11 : 1;
			unsigned int FB12 : 1;
			unsigned int FB13 : 1;
			unsigned int FB14 : 1;
			unsigned int FB15 : 1;
			unsigned int FB16 : 1;
			unsigned int FB17 : 1;
			unsigned int FB18 : 1;
			unsigned int FB19 : 1;
			unsigned int FB20 : 1;
			unsigned int FB21 : 1;
			unsigned int FB22 : 1;
			unsigned int FB23 : 1;
			unsigned int FB24 : 1;
			unsigned int FB25 : 1;
			unsigned int FB26 : 1;
			unsigned int FB27 : 1;
			unsigned int FB28 : 1;
			unsigned int FB29 : 1;
			unsigned int FB30 : 1;
			unsigned int FB31 : 1;
		} __attribute((__packed__));
	};
};
#define CAN_F15R2 (*((volatile struct CAN_F15R2_s*) 0x400066bc))

struct CAN_F16R1_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int FB0 : 1;
			unsigned int FB1 : 1;
			unsigned int FB2 : 1;
			unsigned int FB3 : 1;
			unsigned int FB4 : 1;
			unsigned int FB5 : 1;
			unsigned int FB6 : 1;
			unsigned int FB7 : 1;
			unsigned int FB8 : 1;
			unsigned int FB9 : 1;
			unsigned int FB10 : 1;
			unsigned int FB11 : 1;
			unsigned int FB12 : 1;
			unsigned int FB13 : 1;
			unsigned int FB14 : 1;
			unsigned int FB15 : 1;
			unsigned int FB16 : 1;
			unsigned int FB17 : 1;
			unsigned int FB18 : 1;
			unsigned int FB19 : 1;
			unsigned int FB20 : 1;
			unsigned int FB21 : 1;
			unsigned int FB22 : 1;
			unsigned int FB23 : 1;
			unsigned int FB24 : 1;
			unsigned int FB25 : 1;
			unsigned int FB26 : 1;
			unsigned int FB27 : 1;
			unsigned int FB28 : 1;
			unsigned int FB29 : 1;
			unsigned int FB30 : 1;
			unsigned int FB31 : 1;
		} __attribute((__packed__));
	};
};
#define CAN_F16R1 (*((volatile struct CAN_F16R1_s*) 0x400066c0))

struct CAN_F16R2_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int FB0 : 1;
			unsigned int FB1 : 1;
			unsigned int FB2 : 1;
			unsigned int FB3 : 1;
			unsigned int FB4 : 1;
			unsigned int FB5 : 1;
			unsigned int FB6 : 1;
			unsigned int FB7 : 1;
			unsigned int FB8 : 1;
			unsigned int FB9 : 1;
			unsigned int FB10 : 1;
			unsigned int FB11 : 1;
			unsigned int FB12 : 1;
			unsigned int FB13 : 1;
			unsigned int FB14 : 1;
			unsigned int FB15 : 1;
			unsigned int FB16 : 1;
			unsigned int FB17 : 1;
			unsigned int FB18 : 1;
			unsigned int FB19 : 1;
			unsigned int FB20 : 1;
			unsigned int FB21 : 1;
			unsigned int FB22 : 1;
			unsigned int FB23 : 1;
			unsigned int FB24 : 1;
			unsigned int FB25 : 1;
			unsigned int FB26 : 1;
			unsigned int FB27 : 1;
			unsigned int FB28 : 1;
			unsigned int FB29 : 1;
			unsigned int FB30 : 1;
			unsigned int FB31 : 1;
		} __attribute((__packed__));
	};
};
#define CAN_F16R2 (*((volatile struct CAN_F16R2_s*) 0x400066c4))

struct CAN_F17R1_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int FB0 : 1;
			unsigned int FB1 : 1;
			unsigned int FB2 : 1;
			unsigned int FB3 : 1;
			unsigned int FB4 : 1;
			unsigned int FB5 : 1;
			unsigned int FB6 : 1;
			unsigned int FB7 : 1;
			unsigned int FB8 : 1;
			unsigned int FB9 : 1;
			unsigned int FB10 : 1;
			unsigned int FB11 : 1;
			unsigned int FB12 : 1;
			unsigned int FB13 : 1;
			unsigned int FB14 : 1;
			unsigned int FB15 : 1;
			unsigned int FB16 : 1;
			unsigned int FB17 : 1;
			unsigned int FB18 : 1;
			unsigned int FB19 : 1;
			unsigned int FB20 : 1;
			unsigned int FB21 : 1;
			unsigned int FB22 : 1;
			unsigned int FB23 : 1;
			unsigned int FB24 : 1;
			unsigned int FB25 : 1;
			unsigned int FB26 : 1;
			unsigned int FB27 : 1;
			unsigned int FB28 : 1;
			unsigned int FB29 : 1;
			unsigned int FB30 : 1;
			unsigned int FB31 : 1;
		} __attribute((__packed__));
	};
};
#define CAN_F17R1 (*((volatile struct CAN_F17R1_s*) 0x400066c8))

struct CAN_F17R2_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int FB0 : 1;
			unsigned int FB1 : 1;
			unsigned int FB2 : 1;
			unsigned int FB3 : 1;
			unsigned int FB4 : 1;
			unsigned int FB5 : 1;
			unsigned int FB6 : 1;
			unsigned int FB7 : 1;
			unsigned int FB8 : 1;
			unsigned int FB9 : 1;
			unsigned int FB10 : 1;
			unsigned int FB11 : 1;
			unsigned int FB12 : 1;
			unsigned int FB13 : 1;
			unsigned int FB14 : 1;
			unsigned int FB15 : 1;
			unsigned int FB16 : 1;
			unsigned int FB17 : 1;
			unsigned int FB18 : 1;
			unsigned int FB19 : 1;
			unsigned int FB20 : 1;
			unsigned int FB21 : 1;
			unsigned int FB22 : 1;
			unsigned int FB23 : 1;
			unsigned int FB24 : 1;
			unsigned int FB25 : 1;
			unsigned int FB26 : 1;
			unsigned int FB27 : 1;
			unsigned int FB28 : 1;
			unsigned int FB29 : 1;
			unsigned int FB30 : 1;
			unsigned int FB31 : 1;
		} __attribute((__packed__));
	};
};
#define CAN_F17R2 (*((volatile struct CAN_F17R2_s*) 0x400066cc))

struct CAN_F18R1_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int FB0 : 1;
			unsigned int FB1 : 1;
			unsigned int FB2 : 1;
			unsigned int FB3 : 1;
			unsigned int FB4 : 1;
			unsigned int FB5 : 1;
			unsigned int FB6 : 1;
			unsigned int FB7 : 1;
			unsigned int FB8 : 1;
			unsigned int FB9 : 1;
			unsigned int FB10 : 1;
			unsigned int FB11 : 1;
			unsigned int FB12 : 1;
			unsigned int FB13 : 1;
			unsigned int FB14 : 1;
			unsigned int FB15 : 1;
			unsigned int FB16 : 1;
			unsigned int FB17 : 1;
			unsigned int FB18 : 1;
			unsigned int FB19 : 1;
			unsigned int FB20 : 1;
			unsigned int FB21 : 1;
			unsigned int FB22 : 1;
			unsigned int FB23 : 1;
			unsigned int FB24 : 1;
			unsigned int FB25 : 1;
			unsigned int FB26 : 1;
			unsigned int FB27 : 1;
			unsigned int FB28 : 1;
			unsigned int FB29 : 1;
			unsigned int FB30 : 1;
			unsigned int FB31 : 1;
		} __attribute((__packed__));
	};
};
#define CAN_F18R1 (*((volatile struct CAN_F18R1_s*) 0x400066d0))

struct CAN_F18R2_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int FB0 : 1;
			unsigned int FB1 : 1;
			unsigned int FB2 : 1;
			unsigned int FB3 : 1;
			unsigned int FB4 : 1;
			unsigned int FB5 : 1;
			unsigned int FB6 : 1;
			unsigned int FB7 : 1;
			unsigned int FB8 : 1;
			unsigned int FB9 : 1;
			unsigned int FB10 : 1;
			unsigned int FB11 : 1;
			unsigned int FB12 : 1;
			unsigned int FB13 : 1;
			unsigned int FB14 : 1;
			unsigned int FB15 : 1;
			unsigned int FB16 : 1;
			unsigned int FB17 : 1;
			unsigned int FB18 : 1;
			unsigned int FB19 : 1;
			unsigned int FB20 : 1;
			unsigned int FB21 : 1;
			unsigned int FB22 : 1;
			unsigned int FB23 : 1;
			unsigned int FB24 : 1;
			unsigned int FB25 : 1;
			unsigned int FB26 : 1;
			unsigned int FB27 : 1;
			unsigned int FB28 : 1;
			unsigned int FB29 : 1;
			unsigned int FB30 : 1;
			unsigned int FB31 : 1;
		} __attribute((__packed__));
	};
};
#define CAN_F18R2 (*((volatile struct CAN_F18R2_s*) 0x400066d4))

struct CAN_F19R1_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int FB0 : 1;
			unsigned int FB1 : 1;
			unsigned int FB2 : 1;
			unsigned int FB3 : 1;
			unsigned int FB4 : 1;
			unsigned int FB5 : 1;
			unsigned int FB6 : 1;
			unsigned int FB7 : 1;
			unsigned int FB8 : 1;
			unsigned int FB9 : 1;
			unsigned int FB10 : 1;
			unsigned int FB11 : 1;
			unsigned int FB12 : 1;
			unsigned int FB13 : 1;
			unsigned int FB14 : 1;
			unsigned int FB15 : 1;
			unsigned int FB16 : 1;
			unsigned int FB17 : 1;
			unsigned int FB18 : 1;
			unsigned int FB19 : 1;
			unsigned int FB20 : 1;
			unsigned int FB21 : 1;
			unsigned int FB22 : 1;
			unsigned int FB23 : 1;
			unsigned int FB24 : 1;
			unsigned int FB25 : 1;
			unsigned int FB26 : 1;
			unsigned int FB27 : 1;
			unsigned int FB28 : 1;
			unsigned int FB29 : 1;
			unsigned int FB30 : 1;
			unsigned int FB31 : 1;
		} __attribute((__packed__));
	};
};
#define CAN_F19R1 (*((volatile struct CAN_F19R1_s*) 0x400066d8))

struct CAN_F19R2_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int FB0 : 1;
			unsigned int FB1 : 1;
			unsigned int FB2 : 1;
			unsigned int FB3 : 1;
			unsigned int FB4 : 1;
			unsigned int FB5 : 1;
			unsigned int FB6 : 1;
			unsigned int FB7 : 1;
			unsigned int FB8 : 1;
			unsigned int FB9 : 1;
			unsigned int FB10 : 1;
			unsigned int FB11 : 1;
			unsigned int FB12 : 1;
			unsigned int FB13 : 1;
			unsigned int FB14 : 1;
			unsigned int FB15 : 1;
			unsigned int FB16 : 1;
			unsigned int FB17 : 1;
			unsigned int FB18 : 1;
			unsigned int FB19 : 1;
			unsigned int FB20 : 1;
			unsigned int FB21 : 1;
			unsigned int FB22 : 1;
			unsigned int FB23 : 1;
			unsigned int FB24 : 1;
			unsigned int FB25 : 1;
			unsigned int FB26 : 1;
			unsigned int FB27 : 1;
			unsigned int FB28 : 1;
			unsigned int FB29 : 1;
			unsigned int FB30 : 1;
			unsigned int FB31 : 1;
		} __attribute((__packed__));
	};
};
#define CAN_F19R2 (*((volatile struct CAN_F19R2_s*) 0x400066dc))

struct CAN_F20R1_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int FB0 : 1;
			unsigned int FB1 : 1;
			unsigned int FB2 : 1;
			unsigned int FB3 : 1;
			unsigned int FB4 : 1;
			unsigned int FB5 : 1;
			unsigned int FB6 : 1;
			unsigned int FB7 : 1;
			unsigned int FB8 : 1;
			unsigned int FB9 : 1;
			unsigned int FB10 : 1;
			unsigned int FB11 : 1;
			unsigned int FB12 : 1;
			unsigned int FB13 : 1;
			unsigned int FB14 : 1;
			unsigned int FB15 : 1;
			unsigned int FB16 : 1;
			unsigned int FB17 : 1;
			unsigned int FB18 : 1;
			unsigned int FB19 : 1;
			unsigned int FB20 : 1;
			unsigned int FB21 : 1;
			unsigned int FB22 : 1;
			unsigned int FB23 : 1;
			unsigned int FB24 : 1;
			unsigned int FB25 : 1;
			unsigned int FB26 : 1;
			unsigned int FB27 : 1;
			unsigned int FB28 : 1;
			unsigned int FB29 : 1;
			unsigned int FB30 : 1;
			unsigned int FB31 : 1;
		} __attribute((__packed__));
	};
};
#define CAN_F20R1 (*((volatile struct CAN_F20R1_s*) 0x400066e0))

struct CAN_F20R2_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int FB0 : 1;
			unsigned int FB1 : 1;
			unsigned int FB2 : 1;
			unsigned int FB3 : 1;
			unsigned int FB4 : 1;
			unsigned int FB5 : 1;
			unsigned int FB6 : 1;
			unsigned int FB7 : 1;
			unsigned int FB8 : 1;
			unsigned int FB9 : 1;
			unsigned int FB10 : 1;
			unsigned int FB11 : 1;
			unsigned int FB12 : 1;
			unsigned int FB13 : 1;
			unsigned int FB14 : 1;
			unsigned int FB15 : 1;
			unsigned int FB16 : 1;
			unsigned int FB17 : 1;
			unsigned int FB18 : 1;
			unsigned int FB19 : 1;
			unsigned int FB20 : 1;
			unsigned int FB21 : 1;
			unsigned int FB22 : 1;
			unsigned int FB23 : 1;
			unsigned int FB24 : 1;
			unsigned int FB25 : 1;
			unsigned int FB26 : 1;
			unsigned int FB27 : 1;
			unsigned int FB28 : 1;
			unsigned int FB29 : 1;
			unsigned int FB30 : 1;
			unsigned int FB31 : 1;
		} __attribute((__packed__));
	};
};
#define CAN_F20R2 (*((volatile struct CAN_F20R2_s*) 0x400066e4))

struct CAN_F21R1_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int FB0 : 1;
			unsigned int FB1 : 1;
			unsigned int FB2 : 1;
			unsigned int FB3 : 1;
			unsigned int FB4 : 1;
			unsigned int FB5 : 1;
			unsigned int FB6 : 1;
			unsigned int FB7 : 1;
			unsigned int FB8 : 1;
			unsigned int FB9 : 1;
			unsigned int FB10 : 1;
			unsigned int FB11 : 1;
			unsigned int FB12 : 1;
			unsigned int FB13 : 1;
			unsigned int FB14 : 1;
			unsigned int FB15 : 1;
			unsigned int FB16 : 1;
			unsigned int FB17 : 1;
			unsigned int FB18 : 1;
			unsigned int FB19 : 1;
			unsigned int FB20 : 1;
			unsigned int FB21 : 1;
			unsigned int FB22 : 1;
			unsigned int FB23 : 1;
			unsigned int FB24 : 1;
			unsigned int FB25 : 1;
			unsigned int FB26 : 1;
			unsigned int FB27 : 1;
			unsigned int FB28 : 1;
			unsigned int FB29 : 1;
			unsigned int FB30 : 1;
			unsigned int FB31 : 1;
		} __attribute((__packed__));
	};
};
#define CAN_F21R1 (*((volatile struct CAN_F21R1_s*) 0x400066e8))

struct CAN_F21R2_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int FB0 : 1;
			unsigned int FB1 : 1;
			unsigned int FB2 : 1;
			unsigned int FB3 : 1;
			unsigned int FB4 : 1;
			unsigned int FB5 : 1;
			unsigned int FB6 : 1;
			unsigned int FB7 : 1;
			unsigned int FB8 : 1;
			unsigned int FB9 : 1;
			unsigned int FB10 : 1;
			unsigned int FB11 : 1;
			unsigned int FB12 : 1;
			unsigned int FB13 : 1;
			unsigned int FB14 : 1;
			unsigned int FB15 : 1;
			unsigned int FB16 : 1;
			unsigned int FB17 : 1;
			unsigned int FB18 : 1;
			unsigned int FB19 : 1;
			unsigned int FB20 : 1;
			unsigned int FB21 : 1;
			unsigned int FB22 : 1;
			unsigned int FB23 : 1;
			unsigned int FB24 : 1;
			unsigned int FB25 : 1;
			unsigned int FB26 : 1;
			unsigned int FB27 : 1;
			unsigned int FB28 : 1;
			unsigned int FB29 : 1;
			unsigned int FB30 : 1;
			unsigned int FB31 : 1;
		} __attribute((__packed__));
	};
};
#define CAN_F21R2 (*((volatile struct CAN_F21R2_s*) 0x400066ec))

struct CAN_F22R1_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int FB0 : 1;
			unsigned int FB1 : 1;
			unsigned int FB2 : 1;
			unsigned int FB3 : 1;
			unsigned int FB4 : 1;
			unsigned int FB5 : 1;
			unsigned int FB6 : 1;
			unsigned int FB7 : 1;
			unsigned int FB8 : 1;
			unsigned int FB9 : 1;
			unsigned int FB10 : 1;
			unsigned int FB11 : 1;
			unsigned int FB12 : 1;
			unsigned int FB13 : 1;
			unsigned int FB14 : 1;
			unsigned int FB15 : 1;
			unsigned int FB16 : 1;
			unsigned int FB17 : 1;
			unsigned int FB18 : 1;
			unsigned int FB19 : 1;
			unsigned int FB20 : 1;
			unsigned int FB21 : 1;
			unsigned int FB22 : 1;
			unsigned int FB23 : 1;
			unsigned int FB24 : 1;
			unsigned int FB25 : 1;
			unsigned int FB26 : 1;
			unsigned int FB27 : 1;
			unsigned int FB28 : 1;
			unsigned int FB29 : 1;
			unsigned int FB30 : 1;
			unsigned int FB31 : 1;
		} __attribute((__packed__));
	};
};
#define CAN_F22R1 (*((volatile struct CAN_F22R1_s*) 0x400066f0))

struct CAN_F22R2_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int FB0 : 1;
			unsigned int FB1 : 1;
			unsigned int FB2 : 1;
			unsigned int FB3 : 1;
			unsigned int FB4 : 1;
			unsigned int FB5 : 1;
			unsigned int FB6 : 1;
			unsigned int FB7 : 1;
			unsigned int FB8 : 1;
			unsigned int FB9 : 1;
			unsigned int FB10 : 1;
			unsigned int FB11 : 1;
			unsigned int FB12 : 1;
			unsigned int FB13 : 1;
			unsigned int FB14 : 1;
			unsigned int FB15 : 1;
			unsigned int FB16 : 1;
			unsigned int FB17 : 1;
			unsigned int FB18 : 1;
			unsigned int FB19 : 1;
			unsigned int FB20 : 1;
			unsigned int FB21 : 1;
			unsigned int FB22 : 1;
			unsigned int FB23 : 1;
			unsigned int FB24 : 1;
			unsigned int FB25 : 1;
			unsigned int FB26 : 1;
			unsigned int FB27 : 1;
			unsigned int FB28 : 1;
			unsigned int FB29 : 1;
			unsigned int FB30 : 1;
			unsigned int FB31 : 1;
		} __attribute((__packed__));
	};
};
#define CAN_F22R2 (*((volatile struct CAN_F22R2_s*) 0x400066f4))

struct CAN_F23R1_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int FB0 : 1;
			unsigned int FB1 : 1;
			unsigned int FB2 : 1;
			unsigned int FB3 : 1;
			unsigned int FB4 : 1;
			unsigned int FB5 : 1;
			unsigned int FB6 : 1;
			unsigned int FB7 : 1;
			unsigned int FB8 : 1;
			unsigned int FB9 : 1;
			unsigned int FB10 : 1;
			unsigned int FB11 : 1;
			unsigned int FB12 : 1;
			unsigned int FB13 : 1;
			unsigned int FB14 : 1;
			unsigned int FB15 : 1;
			unsigned int FB16 : 1;
			unsigned int FB17 : 1;
			unsigned int FB18 : 1;
			unsigned int FB19 : 1;
			unsigned int FB20 : 1;
			unsigned int FB21 : 1;
			unsigned int FB22 : 1;
			unsigned int FB23 : 1;
			unsigned int FB24 : 1;
			unsigned int FB25 : 1;
			unsigned int FB26 : 1;
			unsigned int FB27 : 1;
			unsigned int FB28 : 1;
			unsigned int FB29 : 1;
			unsigned int FB30 : 1;
			unsigned int FB31 : 1;
		} __attribute((__packed__));
	};
};
#define CAN_F23R1 (*((volatile struct CAN_F23R1_s*) 0x400066f8))

struct CAN_F23R2_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int FB0 : 1;
			unsigned int FB1 : 1;
			unsigned int FB2 : 1;
			unsigned int FB3 : 1;
			unsigned int FB4 : 1;
			unsigned int FB5 : 1;
			unsigned int FB6 : 1;
			unsigned int FB7 : 1;
			unsigned int FB8 : 1;
			unsigned int FB9 : 1;
			unsigned int FB10 : 1;
			unsigned int FB11 : 1;
			unsigned int FB12 : 1;
			unsigned int FB13 : 1;
			unsigned int FB14 : 1;
			unsigned int FB15 : 1;
			unsigned int FB16 : 1;
			unsigned int FB17 : 1;
			unsigned int FB18 : 1;
			unsigned int FB19 : 1;
			unsigned int FB20 : 1;
			unsigned int FB21 : 1;
			unsigned int FB22 : 1;
			unsigned int FB23 : 1;
			unsigned int FB24 : 1;
			unsigned int FB25 : 1;
			unsigned int FB26 : 1;
			unsigned int FB27 : 1;
			unsigned int FB28 : 1;
			unsigned int FB29 : 1;
			unsigned int FB30 : 1;
			unsigned int FB31 : 1;
		} __attribute((__packed__));
	};
};
#define CAN_F23R2 (*((volatile struct CAN_F23R2_s*) 0x400066fc))

struct CAN_F24R1_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int FB0 : 1;
			unsigned int FB1 : 1;
			unsigned int FB2 : 1;
			unsigned int FB3 : 1;
			unsigned int FB4 : 1;
			unsigned int FB5 : 1;
			unsigned int FB6 : 1;
			unsigned int FB7 : 1;
			unsigned int FB8 : 1;
			unsigned int FB9 : 1;
			unsigned int FB10 : 1;
			unsigned int FB11 : 1;
			unsigned int FB12 : 1;
			unsigned int FB13 : 1;
			unsigned int FB14 : 1;
			unsigned int FB15 : 1;
			unsigned int FB16 : 1;
			unsigned int FB17 : 1;
			unsigned int FB18 : 1;
			unsigned int FB19 : 1;
			unsigned int FB20 : 1;
			unsigned int FB21 : 1;
			unsigned int FB22 : 1;
			unsigned int FB23 : 1;
			unsigned int FB24 : 1;
			unsigned int FB25 : 1;
			unsigned int FB26 : 1;
			unsigned int FB27 : 1;
			unsigned int FB28 : 1;
			unsigned int FB29 : 1;
			unsigned int FB30 : 1;
			unsigned int FB31 : 1;
		} __attribute((__packed__));
	};
};
#define CAN_F24R1 (*((volatile struct CAN_F24R1_s*) 0x40006700))

struct CAN_F24R2_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int FB0 : 1;
			unsigned int FB1 : 1;
			unsigned int FB2 : 1;
			unsigned int FB3 : 1;
			unsigned int FB4 : 1;
			unsigned int FB5 : 1;
			unsigned int FB6 : 1;
			unsigned int FB7 : 1;
			unsigned int FB8 : 1;
			unsigned int FB9 : 1;
			unsigned int FB10 : 1;
			unsigned int FB11 : 1;
			unsigned int FB12 : 1;
			unsigned int FB13 : 1;
			unsigned int FB14 : 1;
			unsigned int FB15 : 1;
			unsigned int FB16 : 1;
			unsigned int FB17 : 1;
			unsigned int FB18 : 1;
			unsigned int FB19 : 1;
			unsigned int FB20 : 1;
			unsigned int FB21 : 1;
			unsigned int FB22 : 1;
			unsigned int FB23 : 1;
			unsigned int FB24 : 1;
			unsigned int FB25 : 1;
			unsigned int FB26 : 1;
			unsigned int FB27 : 1;
			unsigned int FB28 : 1;
			unsigned int FB29 : 1;
			unsigned int FB30 : 1;
			unsigned int FB31 : 1;
		} __attribute((__packed__));
	};
};
#define CAN_F24R2 (*((volatile struct CAN_F24R2_s*) 0x40006704))

struct CAN_F25R1_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int FB0 : 1;
			unsigned int FB1 : 1;
			unsigned int FB2 : 1;
			unsigned int FB3 : 1;
			unsigned int FB4 : 1;
			unsigned int FB5 : 1;
			unsigned int FB6 : 1;
			unsigned int FB7 : 1;
			unsigned int FB8 : 1;
			unsigned int FB9 : 1;
			unsigned int FB10 : 1;
			unsigned int FB11 : 1;
			unsigned int FB12 : 1;
			unsigned int FB13 : 1;
			unsigned int FB14 : 1;
			unsigned int FB15 : 1;
			unsigned int FB16 : 1;
			unsigned int FB17 : 1;
			unsigned int FB18 : 1;
			unsigned int FB19 : 1;
			unsigned int FB20 : 1;
			unsigned int FB21 : 1;
			unsigned int FB22 : 1;
			unsigned int FB23 : 1;
			unsigned int FB24 : 1;
			unsigned int FB25 : 1;
			unsigned int FB26 : 1;
			unsigned int FB27 : 1;
			unsigned int FB28 : 1;
			unsigned int FB29 : 1;
			unsigned int FB30 : 1;
			unsigned int FB31 : 1;
		} __attribute((__packed__));
	};
};
#define CAN_F25R1 (*((volatile struct CAN_F25R1_s*) 0x40006708))

struct CAN_F25R2_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int FB0 : 1;
			unsigned int FB1 : 1;
			unsigned int FB2 : 1;
			unsigned int FB3 : 1;
			unsigned int FB4 : 1;
			unsigned int FB5 : 1;
			unsigned int FB6 : 1;
			unsigned int FB7 : 1;
			unsigned int FB8 : 1;
			unsigned int FB9 : 1;
			unsigned int FB10 : 1;
			unsigned int FB11 : 1;
			unsigned int FB12 : 1;
			unsigned int FB13 : 1;
			unsigned int FB14 : 1;
			unsigned int FB15 : 1;
			unsigned int FB16 : 1;
			unsigned int FB17 : 1;
			unsigned int FB18 : 1;
			unsigned int FB19 : 1;
			unsigned int FB20 : 1;
			unsigned int FB21 : 1;
			unsigned int FB22 : 1;
			unsigned int FB23 : 1;
			unsigned int FB24 : 1;
			unsigned int FB25 : 1;
			unsigned int FB26 : 1;
			unsigned int FB27 : 1;
			unsigned int FB28 : 1;
			unsigned int FB29 : 1;
			unsigned int FB30 : 1;
			unsigned int FB31 : 1;
		} __attribute((__packed__));
	};
};
#define CAN_F25R2 (*((volatile struct CAN_F25R2_s*) 0x4000670c))

struct CAN_F26R1_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int FB0 : 1;
			unsigned int FB1 : 1;
			unsigned int FB2 : 1;
			unsigned int FB3 : 1;
			unsigned int FB4 : 1;
			unsigned int FB5 : 1;
			unsigned int FB6 : 1;
			unsigned int FB7 : 1;
			unsigned int FB8 : 1;
			unsigned int FB9 : 1;
			unsigned int FB10 : 1;
			unsigned int FB11 : 1;
			unsigned int FB12 : 1;
			unsigned int FB13 : 1;
			unsigned int FB14 : 1;
			unsigned int FB15 : 1;
			unsigned int FB16 : 1;
			unsigned int FB17 : 1;
			unsigned int FB18 : 1;
			unsigned int FB19 : 1;
			unsigned int FB20 : 1;
			unsigned int FB21 : 1;
			unsigned int FB22 : 1;
			unsigned int FB23 : 1;
			unsigned int FB24 : 1;
			unsigned int FB25 : 1;
			unsigned int FB26 : 1;
			unsigned int FB27 : 1;
			unsigned int FB28 : 1;
			unsigned int FB29 : 1;
			unsigned int FB30 : 1;
			unsigned int FB31 : 1;
		} __attribute((__packed__));
	};
};
#define CAN_F26R1 (*((volatile struct CAN_F26R1_s*) 0x40006710))

struct CAN_F26R2_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int FB0 : 1;
			unsigned int FB1 : 1;
			unsigned int FB2 : 1;
			unsigned int FB3 : 1;
			unsigned int FB4 : 1;
			unsigned int FB5 : 1;
			unsigned int FB6 : 1;
			unsigned int FB7 : 1;
			unsigned int FB8 : 1;
			unsigned int FB9 : 1;
			unsigned int FB10 : 1;
			unsigned int FB11 : 1;
			unsigned int FB12 : 1;
			unsigned int FB13 : 1;
			unsigned int FB14 : 1;
			unsigned int FB15 : 1;
			unsigned int FB16 : 1;
			unsigned int FB17 : 1;
			unsigned int FB18 : 1;
			unsigned int FB19 : 1;
			unsigned int FB20 : 1;
			unsigned int FB21 : 1;
			unsigned int FB22 : 1;
			unsigned int FB23 : 1;
			unsigned int FB24 : 1;
			unsigned int FB25 : 1;
			unsigned int FB26 : 1;
			unsigned int FB27 : 1;
			unsigned int FB28 : 1;
			unsigned int FB29 : 1;
			unsigned int FB30 : 1;
			unsigned int FB31 : 1;
		} __attribute((__packed__));
	};
};
#define CAN_F26R2 (*((volatile struct CAN_F26R2_s*) 0x40006714))

struct CAN_F27R1_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int FB0 : 1;
			unsigned int FB1 : 1;
			unsigned int FB2 : 1;
			unsigned int FB3 : 1;
			unsigned int FB4 : 1;
			unsigned int FB5 : 1;
			unsigned int FB6 : 1;
			unsigned int FB7 : 1;
			unsigned int FB8 : 1;
			unsigned int FB9 : 1;
			unsigned int FB10 : 1;
			unsigned int FB11 : 1;
			unsigned int FB12 : 1;
			unsigned int FB13 : 1;
			unsigned int FB14 : 1;
			unsigned int FB15 : 1;
			unsigned int FB16 : 1;
			unsigned int FB17 : 1;
			unsigned int FB18 : 1;
			unsigned int FB19 : 1;
			unsigned int FB20 : 1;
			unsigned int FB21 : 1;
			unsigned int FB22 : 1;
			unsigned int FB23 : 1;
			unsigned int FB24 : 1;
			unsigned int FB25 : 1;
			unsigned int FB26 : 1;
			unsigned int FB27 : 1;
			unsigned int FB28 : 1;
			unsigned int FB29 : 1;
			unsigned int FB30 : 1;
			unsigned int FB31 : 1;
		} __attribute((__packed__));
	};
};
#define CAN_F27R1 (*((volatile struct CAN_F27R1_s*) 0x40006718))

struct CAN_F27R2_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int FB0 : 1;
			unsigned int FB1 : 1;
			unsigned int FB2 : 1;
			unsigned int FB3 : 1;
			unsigned int FB4 : 1;
			unsigned int FB5 : 1;
			unsigned int FB6 : 1;
			unsigned int FB7 : 1;
			unsigned int FB8 : 1;
			unsigned int FB9 : 1;
			unsigned int FB10 : 1;
			unsigned int FB11 : 1;
			unsigned int FB12 : 1;
			unsigned int FB13 : 1;
			unsigned int FB14 : 1;
			unsigned int FB15 : 1;
			unsigned int FB16 : 1;
			unsigned int FB17 : 1;
			unsigned int FB18 : 1;
			unsigned int FB19 : 1;
			unsigned int FB20 : 1;
			unsigned int FB21 : 1;
			unsigned int FB22 : 1;
			unsigned int FB23 : 1;
			unsigned int FB24 : 1;
			unsigned int FB25 : 1;
			unsigned int FB26 : 1;
			unsigned int FB27 : 1;
			unsigned int FB28 : 1;
			unsigned int FB29 : 1;
			unsigned int FB30 : 1;
			unsigned int FB31 : 1;
		} __attribute((__packed__));
	};
};
#define CAN_F27R2 (*((volatile struct CAN_F27R2_s*) 0x4000671c))

// USB_FS
struct USB_FS_USB_EP0R_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int EA : 4;
			unsigned int STAT_TX : 2;
			unsigned int DTOG_TX : 1;
			unsigned int CTR_TX : 1;
			unsigned int EP_KIND : 1;
			unsigned int EP_TYPE : 2;
			unsigned int SETUP : 1;
			unsigned int STAT_RX : 2;
			unsigned int DTOG_RX : 1;
			unsigned int CTR_RX : 1;
		} __attribute((__packed__));
	};
};
#define USB_FS_USB_EP0R (*((volatile struct USB_FS_USB_EP0R_s*) 0x40005c00))

struct USB_FS_USB_EP1R_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int EA : 4;
			unsigned int STAT_TX : 2;
			unsigned int DTOG_TX : 1;
			unsigned int CTR_TX : 1;
			unsigned int EP_KIND : 1;
			unsigned int EP_TYPE : 2;
			unsigned int SETUP : 1;
			unsigned int STAT_RX : 2;
			unsigned int DTOG_RX : 1;
			unsigned int CTR_RX : 1;
		} __attribute((__packed__));
	};
};
#define USB_FS_USB_EP1R (*((volatile struct USB_FS_USB_EP1R_s*) 0x40005c04))

struct USB_FS_USB_EP2R_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int EA : 4;
			unsigned int STAT_TX : 2;
			unsigned int DTOG_TX : 1;
			unsigned int CTR_TX : 1;
			unsigned int EP_KIND : 1;
			unsigned int EP_TYPE : 2;
			unsigned int SETUP : 1;
			unsigned int STAT_RX : 2;
			unsigned int DTOG_RX : 1;
			unsigned int CTR_RX : 1;
		} __attribute((__packed__));
	};
};
#define USB_FS_USB_EP2R (*((volatile struct USB_FS_USB_EP2R_s*) 0x40005c08))

struct USB_FS_USB_EP3R_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int EA : 4;
			unsigned int STAT_TX : 2;
			unsigned int DTOG_TX : 1;
			unsigned int CTR_TX : 1;
			unsigned int EP_KIND : 1;
			unsigned int EP_TYPE : 2;
			unsigned int SETUP : 1;
			unsigned int STAT_RX : 2;
			unsigned int DTOG_RX : 1;
			unsigned int CTR_RX : 1;
		} __attribute((__packed__));
	};
};
#define USB_FS_USB_EP3R (*((volatile struct USB_FS_USB_EP3R_s*) 0x40005c0c))

struct USB_FS_USB_EP4R_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int EA : 4;
			unsigned int STAT_TX : 2;
			unsigned int DTOG_TX : 1;
			unsigned int CTR_TX : 1;
			unsigned int EP_KIND : 1;
			unsigned int EP_TYPE : 2;
			unsigned int SETUP : 1;
			unsigned int STAT_RX : 2;
			unsigned int DTOG_RX : 1;
			unsigned int CTR_RX : 1;
		} __attribute((__packed__));
	};
};
#define USB_FS_USB_EP4R (*((volatile struct USB_FS_USB_EP4R_s*) 0x40005c10))

struct USB_FS_USB_EP5R_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int EA : 4;
			unsigned int STAT_TX : 2;
			unsigned int DTOG_TX : 1;
			unsigned int CTR_TX : 1;
			unsigned int EP_KIND : 1;
			unsigned int EP_TYPE : 2;
			unsigned int SETUP : 1;
			unsigned int STAT_RX : 2;
			unsigned int DTOG_RX : 1;
			unsigned int CTR_RX : 1;
		} __attribute((__packed__));
	};
};
#define USB_FS_USB_EP5R (*((volatile struct USB_FS_USB_EP5R_s*) 0x40005c14))

struct USB_FS_USB_EP6R_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int EA : 4;
			unsigned int STAT_TX : 2;
			unsigned int DTOG_TX : 1;
			unsigned int CTR_TX : 1;
			unsigned int EP_KIND : 1;
			unsigned int EP_TYPE : 2;
			unsigned int SETUP : 1;
			unsigned int STAT_RX : 2;
			unsigned int DTOG_RX : 1;
			unsigned int CTR_RX : 1;
		} __attribute((__packed__));
	};
};
#define USB_FS_USB_EP6R (*((volatile struct USB_FS_USB_EP6R_s*) 0x40005c18))

struct USB_FS_USB_EP7R_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int EA : 4;
			unsigned int STAT_TX : 2;
			unsigned int DTOG_TX : 1;
			unsigned int CTR_TX : 1;
			unsigned int EP_KIND : 1;
			unsigned int EP_TYPE : 2;
			unsigned int SETUP : 1;
			unsigned int STAT_RX : 2;
			unsigned int DTOG_RX : 1;
			unsigned int CTR_RX : 1;
		} __attribute((__packed__));
	};
};
#define USB_FS_USB_EP7R (*((volatile struct USB_FS_USB_EP7R_s*) 0x40005c1c))

struct USB_FS_USB_CNTR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int FRES : 1;
			unsigned int PDWN : 1;
			unsigned int LPMODE : 1;
			unsigned int FSUSP : 1;
			unsigned int RESUME : 1;
			unsigned int : 3;
			unsigned int ESOFM : 1;
			unsigned int SOFM : 1;
			unsigned int RESETM : 1;
			unsigned int SUSPM : 1;
			unsigned int WKUPM : 1;
			unsigned int ERRM : 1;
			unsigned int PMAOVRM : 1;
			unsigned int CTRM : 1;
		} __attribute((__packed__));
	};
};
#define USB_FS_USB_CNTR (*((volatile struct USB_FS_USB_CNTR_s*) 0x40005c40))

struct USB_FS_ISTR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int EP_ID : 4;
			unsigned int DIR : 1;
			unsigned int : 3;
			unsigned int ESOF : 1;
			unsigned int SOF : 1;
			unsigned int RESET : 1;
			unsigned int SUSP : 1;
			unsigned int WKUP : 1;
			unsigned int ERR : 1;
			unsigned int PMAOVR : 1;
			unsigned int CTR : 1;
		} __attribute((__packed__));
	};
};
#define USB_FS_ISTR (*((volatile struct USB_FS_ISTR_s*) 0x40005c44))

struct USB_FS_FNR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int FN : 11;
			unsigned int LSOF : 2;
			unsigned int LCK : 1;
			unsigned int RXDM : 1;
			unsigned int RXDP : 1;
		} __attribute((__packed__));
	};
};
#define USB_FS_FNR (*((volatile struct USB_FS_FNR_s*) 0x40005c48))

struct USB_FS_DADDR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int ADD : 1;
			unsigned int ADD1 : 1;
			unsigned int ADD2 : 1;
			unsigned int ADD3 : 1;
			unsigned int ADD4 : 1;
			unsigned int ADD5 : 1;
			unsigned int ADD6 : 1;
			unsigned int EF : 1;
		} __attribute((__packed__));
	};
};
#define USB_FS_DADDR (*((volatile struct USB_FS_DADDR_s*) 0x40005c4c))

struct USB_FS_BTABLE_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int : 3;
			unsigned int BTABLE : 13;
		} __attribute((__packed__));
	};
};
#define USB_FS_BTABLE (*((volatile struct USB_FS_BTABLE_s*) 0x40005c50))

// I2C1
struct I2C1_CR1_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int PE : 1;
			unsigned int TXIE : 1;
			unsigned int RXIE : 1;
			unsigned int ADDRIE : 1;
			unsigned int NACKIE : 1;
			unsigned int STOPIE : 1;
			unsigned int TCIE : 1;
			unsigned int ERRIE : 1;
			unsigned int DNF : 4;
			unsigned int ANFOFF : 1;
			unsigned int SWRST : 1;
			unsigned int TXDMAEN : 1;
			unsigned int RXDMAEN : 1;
			unsigned int SBC : 1;
			unsigned int NOSTRETCH : 1;
			unsigned int WUPEN : 1;
			unsigned int GCEN : 1;
			unsigned int SMBHEN : 1;
			unsigned int SMBDEN : 1;
			unsigned int ALERTEN : 1;
			unsigned int PECEN : 1;
		} __attribute((__packed__));
	};
};
#define I2C1_CR1 (*((volatile struct I2C1_CR1_s*) 0x40005400))

struct I2C1_CR2_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int SADD0 : 1;
			unsigned int SADD1 : 7;
			unsigned int SADD8 : 2;
			unsigned int RD_WRN : 1;
			unsigned int ADD10 : 1;
			unsigned int HEAD10R : 1;
			unsigned int START : 1;
			unsigned int STOP : 1;
			unsigned int NACK : 1;
			unsigned int NBYTES : 8;
			unsigned int RELOAD : 1;
			unsigned int AUTOEND : 1;
			unsigned int PECBYTE : 1;
		} __attribute((__packed__));
	};
};
#define I2C1_CR2 (*((volatile struct I2C1_CR2_s*) 0x40005404))

struct I2C1_OAR1_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int OA1_0 : 1;
			unsigned int OA1_1 : 7;
			unsigned int OA1_8 : 2;
			unsigned int OA1MODE : 1;
			unsigned int : 4;
			unsigned int OA1EN : 1;
		} __attribute((__packed__));
	};
};
#define I2C1_OAR1 (*((volatile struct I2C1_OAR1_s*) 0x40005408))

struct I2C1_OAR2_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int : 1;
			unsigned int OA2 : 7;
			unsigned int OA2MSK : 3;
			unsigned int : 4;
			unsigned int OA2EN : 1;
		} __attribute((__packed__));
	};
};
#define I2C1_OAR2 (*((volatile struct I2C1_OAR2_s*) 0x4000540c))

struct I2C1_TIMINGR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int SCLL : 8;
			unsigned int SCLH : 8;
			unsigned int SDADEL : 4;
			unsigned int SCLDEL : 4;
			unsigned int : 4;
			unsigned int PRESC : 4;
		} __attribute((__packed__));
	};
};
#define I2C1_TIMINGR (*((volatile struct I2C1_TIMINGR_s*) 0x40005410))

struct I2C1_TIMEOUTR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int TIMEOUTA : 12;
			unsigned int TIDLE : 1;
			unsigned int : 2;
			unsigned int TIMOUTEN : 1;
			unsigned int TIMEOUTB : 12;
			unsigned int : 3;
			unsigned int TEXTEN : 1;
		} __attribute((__packed__));
	};
};
#define I2C1_TIMEOUTR (*((volatile struct I2C1_TIMEOUTR_s*) 0x40005414))

struct I2C1_ISR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int TXE : 1;
			unsigned int TXIS : 1;
			unsigned int RXNE : 1;
			unsigned int ADDR : 1;
			unsigned int NACKF : 1;
			unsigned int STOPF : 1;
			unsigned int TC : 1;
			unsigned int TCR : 1;
			unsigned int BERR : 1;
			unsigned int ARLO : 1;
			unsigned int OVR : 1;
			unsigned int PECERR : 1;
			unsigned int TIMEOUT : 1;
			unsigned int ALERT : 1;
			unsigned int : 1;
			unsigned int BUSY : 1;
			unsigned int DIR : 1;
			unsigned int ADDCODE : 7;
		} __attribute((__packed__));
	};
};
#define I2C1_ISR (*((volatile struct I2C1_ISR_s*) 0x40005418))

struct I2C1_ICR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int : 3;
			unsigned int ADDRCF : 1;
			unsigned int NACKCF : 1;
			unsigned int STOPCF : 1;
			unsigned int : 2;
			unsigned int BERRCF : 1;
			unsigned int ARLOCF : 1;
			unsigned int OVRCF : 1;
			unsigned int PECCF : 1;
			unsigned int TIMOUTCF : 1;
			unsigned int ALERTCF : 1;
		} __attribute((__packed__));
	};
};
#define I2C1_ICR (*((volatile struct I2C1_ICR_s*) 0x4000541c))

struct I2C1_PECR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int PEC : 8;
		} __attribute((__packed__));
	};
};
#define I2C1_PECR (*((volatile struct I2C1_PECR_s*) 0x40005420))

struct I2C1_RXDR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int RXDATA : 8;
		} __attribute((__packed__));
	};
};
#define I2C1_RXDR (*((volatile struct I2C1_RXDR_s*) 0x40005424))

struct I2C1_TXDR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int TXDATA : 8;
		} __attribute((__packed__));
	};
};
#define I2C1_TXDR (*((volatile struct I2C1_TXDR_s*) 0x40005428))

// I2C2
struct I2C2_CR1_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int PE : 1;
			unsigned int TXIE : 1;
			unsigned int RXIE : 1;
			unsigned int ADDRIE : 1;
			unsigned int NACKIE : 1;
			unsigned int STOPIE : 1;
			unsigned int TCIE : 1;
			unsigned int ERRIE : 1;
			unsigned int DNF : 4;
			unsigned int ANFOFF : 1;
			unsigned int SWRST : 1;
			unsigned int TXDMAEN : 1;
			unsigned int RXDMAEN : 1;
			unsigned int SBC : 1;
			unsigned int NOSTRETCH : 1;
			unsigned int WUPEN : 1;
			unsigned int GCEN : 1;
			unsigned int SMBHEN : 1;
			unsigned int SMBDEN : 1;
			unsigned int ALERTEN : 1;
			unsigned int PECEN : 1;
		} __attribute((__packed__));
	};
};
#define I2C2_CR1 (*((volatile struct I2C2_CR1_s*) 0x40005800))

struct I2C2_CR2_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int SADD0 : 1;
			unsigned int SADD1 : 7;
			unsigned int SADD8 : 2;
			unsigned int RD_WRN : 1;
			unsigned int ADD10 : 1;
			unsigned int HEAD10R : 1;
			unsigned int START : 1;
			unsigned int STOP : 1;
			unsigned int NACK : 1;
			unsigned int NBYTES : 8;
			unsigned int RELOAD : 1;
			unsigned int AUTOEND : 1;
			unsigned int PECBYTE : 1;
		} __attribute((__packed__));
	};
};
#define I2C2_CR2 (*((volatile struct I2C2_CR2_s*) 0x40005804))

struct I2C2_OAR1_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int OA1_0 : 1;
			unsigned int OA1_1 : 7;
			unsigned int OA1_8 : 2;
			unsigned int OA1MODE : 1;
			unsigned int : 4;
			unsigned int OA1EN : 1;
		} __attribute((__packed__));
	};
};
#define I2C2_OAR1 (*((volatile struct I2C2_OAR1_s*) 0x40005808))

struct I2C2_OAR2_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int : 1;
			unsigned int OA2 : 7;
			unsigned int OA2MSK : 3;
			unsigned int : 4;
			unsigned int OA2EN : 1;
		} __attribute((__packed__));
	};
};
#define I2C2_OAR2 (*((volatile struct I2C2_OAR2_s*) 0x4000580c))

struct I2C2_TIMINGR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int SCLL : 8;
			unsigned int SCLH : 8;
			unsigned int SDADEL : 4;
			unsigned int SCLDEL : 4;
			unsigned int : 4;
			unsigned int PRESC : 4;
		} __attribute((__packed__));
	};
};
#define I2C2_TIMINGR (*((volatile struct I2C2_TIMINGR_s*) 0x40005810))

struct I2C2_TIMEOUTR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int TIMEOUTA : 12;
			unsigned int TIDLE : 1;
			unsigned int : 2;
			unsigned int TIMOUTEN : 1;
			unsigned int TIMEOUTB : 12;
			unsigned int : 3;
			unsigned int TEXTEN : 1;
		} __attribute((__packed__));
	};
};
#define I2C2_TIMEOUTR (*((volatile struct I2C2_TIMEOUTR_s*) 0x40005814))

struct I2C2_ISR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int TXE : 1;
			unsigned int TXIS : 1;
			unsigned int RXNE : 1;
			unsigned int ADDR : 1;
			unsigned int NACKF : 1;
			unsigned int STOPF : 1;
			unsigned int TC : 1;
			unsigned int TCR : 1;
			unsigned int BERR : 1;
			unsigned int ARLO : 1;
			unsigned int OVR : 1;
			unsigned int PECERR : 1;
			unsigned int TIMEOUT : 1;
			unsigned int ALERT : 1;
			unsigned int : 1;
			unsigned int BUSY : 1;
			unsigned int DIR : 1;
			unsigned int ADDCODE : 7;
		} __attribute((__packed__));
	};
};
#define I2C2_ISR (*((volatile struct I2C2_ISR_s*) 0x40005818))

struct I2C2_ICR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int : 3;
			unsigned int ADDRCF : 1;
			unsigned int NACKCF : 1;
			unsigned int STOPCF : 1;
			unsigned int : 2;
			unsigned int BERRCF : 1;
			unsigned int ARLOCF : 1;
			unsigned int OVRCF : 1;
			unsigned int PECCF : 1;
			unsigned int TIMOUTCF : 1;
			unsigned int ALERTCF : 1;
		} __attribute((__packed__));
	};
};
#define I2C2_ICR (*((volatile struct I2C2_ICR_s*) 0x4000581c))

struct I2C2_PECR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int PEC : 8;
		} __attribute((__packed__));
	};
};
#define I2C2_PECR (*((volatile struct I2C2_PECR_s*) 0x40005820))

struct I2C2_RXDR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int RXDATA : 8;
		} __attribute((__packed__));
	};
};
#define I2C2_RXDR (*((volatile struct I2C2_RXDR_s*) 0x40005824))

struct I2C2_TXDR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int TXDATA : 8;
		} __attribute((__packed__));
	};
};
#define I2C2_TXDR (*((volatile struct I2C2_TXDR_s*) 0x40005828))

// I2C3
struct I2C3_CR1_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int PE : 1;
			unsigned int TXIE : 1;
			unsigned int RXIE : 1;
			unsigned int ADDRIE : 1;
			unsigned int NACKIE : 1;
			unsigned int STOPIE : 1;
			unsigned int TCIE : 1;
			unsigned int ERRIE : 1;
			unsigned int DNF : 4;
			unsigned int ANFOFF : 1;
			unsigned int SWRST : 1;
			unsigned int TXDMAEN : 1;
			unsigned int RXDMAEN : 1;
			unsigned int SBC : 1;
			unsigned int NOSTRETCH : 1;
			unsigned int WUPEN : 1;
			unsigned int GCEN : 1;
			unsigned int SMBHEN : 1;
			unsigned int SMBDEN : 1;
			unsigned int ALERTEN : 1;
			unsigned int PECEN : 1;
		} __attribute((__packed__));
	};
};
#define I2C3_CR1 (*((volatile struct I2C3_CR1_s*) 0x40007800))

struct I2C3_CR2_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int SADD0 : 1;
			unsigned int SADD1 : 7;
			unsigned int SADD8 : 2;
			unsigned int RD_WRN : 1;
			unsigned int ADD10 : 1;
			unsigned int HEAD10R : 1;
			unsigned int START : 1;
			unsigned int STOP : 1;
			unsigned int NACK : 1;
			unsigned int NBYTES : 8;
			unsigned int RELOAD : 1;
			unsigned int AUTOEND : 1;
			unsigned int PECBYTE : 1;
		} __attribute((__packed__));
	};
};
#define I2C3_CR2 (*((volatile struct I2C3_CR2_s*) 0x40007804))

struct I2C3_OAR1_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int OA1_0 : 1;
			unsigned int OA1_1 : 7;
			unsigned int OA1_8 : 2;
			unsigned int OA1MODE : 1;
			unsigned int : 4;
			unsigned int OA1EN : 1;
		} __attribute((__packed__));
	};
};
#define I2C3_OAR1 (*((volatile struct I2C3_OAR1_s*) 0x40007808))

struct I2C3_OAR2_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int : 1;
			unsigned int OA2 : 7;
			unsigned int OA2MSK : 3;
			unsigned int : 4;
			unsigned int OA2EN : 1;
		} __attribute((__packed__));
	};
};
#define I2C3_OAR2 (*((volatile struct I2C3_OAR2_s*) 0x4000780c))

struct I2C3_TIMINGR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int SCLL : 8;
			unsigned int SCLH : 8;
			unsigned int SDADEL : 4;
			unsigned int SCLDEL : 4;
			unsigned int : 4;
			unsigned int PRESC : 4;
		} __attribute((__packed__));
	};
};
#define I2C3_TIMINGR (*((volatile struct I2C3_TIMINGR_s*) 0x40007810))

struct I2C3_TIMEOUTR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int TIMEOUTA : 12;
			unsigned int TIDLE : 1;
			unsigned int : 2;
			unsigned int TIMOUTEN : 1;
			unsigned int TIMEOUTB : 12;
			unsigned int : 3;
			unsigned int TEXTEN : 1;
		} __attribute((__packed__));
	};
};
#define I2C3_TIMEOUTR (*((volatile struct I2C3_TIMEOUTR_s*) 0x40007814))

struct I2C3_ISR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int TXE : 1;
			unsigned int TXIS : 1;
			unsigned int RXNE : 1;
			unsigned int ADDR : 1;
			unsigned int NACKF : 1;
			unsigned int STOPF : 1;
			unsigned int TC : 1;
			unsigned int TCR : 1;
			unsigned int BERR : 1;
			unsigned int ARLO : 1;
			unsigned int OVR : 1;
			unsigned int PECERR : 1;
			unsigned int TIMEOUT : 1;
			unsigned int ALERT : 1;
			unsigned int : 1;
			unsigned int BUSY : 1;
			unsigned int DIR : 1;
			unsigned int ADDCODE : 7;
		} __attribute((__packed__));
	};
};
#define I2C3_ISR (*((volatile struct I2C3_ISR_s*) 0x40007818))

struct I2C3_ICR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int : 3;
			unsigned int ADDRCF : 1;
			unsigned int NACKCF : 1;
			unsigned int STOPCF : 1;
			unsigned int : 2;
			unsigned int BERRCF : 1;
			unsigned int ARLOCF : 1;
			unsigned int OVRCF : 1;
			unsigned int PECCF : 1;
			unsigned int TIMOUTCF : 1;
			unsigned int ALERTCF : 1;
		} __attribute((__packed__));
	};
};
#define I2C3_ICR (*((volatile struct I2C3_ICR_s*) 0x4000781c))

struct I2C3_PECR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int PEC : 8;
		} __attribute((__packed__));
	};
};
#define I2C3_PECR (*((volatile struct I2C3_PECR_s*) 0x40007820))

struct I2C3_RXDR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int RXDATA : 8;
		} __attribute((__packed__));
	};
};
#define I2C3_RXDR (*((volatile struct I2C3_RXDR_s*) 0x40007824))

struct I2C3_TXDR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int TXDATA : 8;
		} __attribute((__packed__));
	};
};
#define I2C3_TXDR (*((volatile struct I2C3_TXDR_s*) 0x40007828))

// IWDG
struct IWDG_KR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int KEY : 16;
		} __attribute((__packed__));
	};
};
#define IWDG_KR (*((volatile struct IWDG_KR_s*) 0x40003000))

struct IWDG_PR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int PR : 3;
		} __attribute((__packed__));
	};
};
#define IWDG_PR (*((volatile struct IWDG_PR_s*) 0x40003004))

struct IWDG_RLR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int RL : 12;
		} __attribute((__packed__));
	};
};
#define IWDG_RLR (*((volatile struct IWDG_RLR_s*) 0x40003008))

struct IWDG_SR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int PVU : 1;
			unsigned int RVU : 1;
			unsigned int WVU : 1;
		} __attribute((__packed__));
	};
};
#define IWDG_SR (*((volatile struct IWDG_SR_s*) 0x4000300c))

struct IWDG_WINR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int WIN : 12;
		} __attribute((__packed__));
	};
};
#define IWDG_WINR (*((volatile struct IWDG_WINR_s*) 0x40003010))

// WWDG
struct WWDG_CR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int T : 7;
			unsigned int WDGA : 1;
		} __attribute((__packed__));
	};
};
#define WWDG_CR (*((volatile struct WWDG_CR_s*) 0x40002c00))

struct WWDG_CFR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int W : 7;
			unsigned int WDGTB : 2;
			unsigned int EWI : 1;
		} __attribute((__packed__));
	};
};
#define WWDG_CFR (*((volatile struct WWDG_CFR_s*) 0x40002c04))

struct WWDG_SR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int EWIF : 1;
		} __attribute((__packed__));
	};
};
#define WWDG_SR (*((volatile struct WWDG_SR_s*) 0x40002c08))

// RTC
struct RTC_TR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int SU : 4;
			unsigned int ST : 3;
			unsigned int : 1;
			unsigned int MNU : 4;
			unsigned int MNT : 3;
			unsigned int : 1;
			unsigned int HU : 4;
			unsigned int HT : 2;
			unsigned int PM : 1;
		} __attribute((__packed__));
	};
};
#define RTC_TR (*((volatile struct RTC_TR_s*) 0x40002800))

struct RTC_DR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int DU : 4;
			unsigned int DT : 2;
			unsigned int : 2;
			unsigned int MU : 4;
			unsigned int MT : 1;
			unsigned int WDU : 3;
			unsigned int YU : 4;
			unsigned int YT : 4;
		} __attribute((__packed__));
	};
};
#define RTC_DR (*((volatile struct RTC_DR_s*) 0x40002804))

struct RTC_CR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int WCKSEL : 3;
			unsigned int TSEDGE : 1;
			unsigned int REFCKON : 1;
			unsigned int BYPSHAD : 1;
			unsigned int FMT : 1;
			unsigned int : 1;
			unsigned int ALRAE : 1;
			unsigned int ALRBE : 1;
			unsigned int WUTE : 1;
			unsigned int TSE : 1;
			unsigned int ALRAIE : 1;
			unsigned int ALRBIE : 1;
			unsigned int WUTIE : 1;
			unsigned int TSIE : 1;
			unsigned int ADD1H : 1;
			unsigned int SUB1H : 1;
			unsigned int BKP : 1;
			unsigned int COSEL : 1;
			unsigned int POL : 1;
			unsigned int OSEL : 2;
			unsigned int COE : 1;
		} __attribute((__packed__));
	};
};
#define RTC_CR (*((volatile struct RTC_CR_s*) 0x40002808))

struct RTC_ISR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int ALRAWF : 1;
			unsigned int ALRBWF : 1;
			unsigned int WUTWF : 1;
			unsigned int SHPF : 1;
			unsigned int INITS : 1;
			unsigned int RSF : 1;
			unsigned int INITF : 1;
			unsigned int INIT : 1;
			unsigned int ALRAF : 1;
			unsigned int ALRBF : 1;
			unsigned int WUTF : 1;
			unsigned int TSF : 1;
			unsigned int TSOVF : 1;
			unsigned int TAMP1F : 1;
			unsigned int TAMP2F : 1;
			unsigned int TAMP3F : 1;
			unsigned int RECALPF : 1;
		} __attribute((__packed__));
	};
};
#define RTC_ISR (*((volatile struct RTC_ISR_s*) 0x4000280c))

struct RTC_PRER_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int PREDIV_S : 15;
			unsigned int : 1;
			unsigned int PREDIV_A : 7;
		} __attribute((__packed__));
	};
};
#define RTC_PRER (*((volatile struct RTC_PRER_s*) 0x40002810))

struct RTC_WUTR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int WUT : 16;
		} __attribute((__packed__));
	};
};
#define RTC_WUTR (*((volatile struct RTC_WUTR_s*) 0x40002814))

struct RTC_ALRMAR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int SU : 4;
			unsigned int ST : 3;
			unsigned int MSK1 : 1;
			unsigned int MNU : 4;
			unsigned int MNT : 3;
			unsigned int MSK2 : 1;
			unsigned int HU : 4;
			unsigned int HT : 2;
			unsigned int PM : 1;
			unsigned int MSK3 : 1;
			unsigned int DU : 4;
			unsigned int DT : 2;
			unsigned int WDSEL : 1;
			unsigned int MSK4 : 1;
		} __attribute((__packed__));
	};
};
#define RTC_ALRMAR (*((volatile struct RTC_ALRMAR_s*) 0x4000281c))

struct RTC_ALRMBR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int SU : 4;
			unsigned int ST : 3;
			unsigned int MSK1 : 1;
			unsigned int MNU : 4;
			unsigned int MNT : 3;
			unsigned int MSK2 : 1;
			unsigned int HU : 4;
			unsigned int HT : 2;
			unsigned int PM : 1;
			unsigned int MSK3 : 1;
			unsigned int DU : 4;
			unsigned int DT : 2;
			unsigned int WDSEL : 1;
			unsigned int MSK4 : 1;
		} __attribute((__packed__));
	};
};
#define RTC_ALRMBR (*((volatile struct RTC_ALRMBR_s*) 0x40002820))

struct RTC_WPR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int KEY : 8;
		} __attribute((__packed__));
	};
};
#define RTC_WPR (*((volatile struct RTC_WPR_s*) 0x40002824))

struct RTC_SSR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int SS : 16;
		} __attribute((__packed__));
	};
};
#define RTC_SSR (*((volatile struct RTC_SSR_s*) 0x40002828))

struct RTC_SHIFTR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int SUBFS : 15;
			unsigned int : 16;
			unsigned int ADD1S : 1;
		} __attribute((__packed__));
	};
};
#define RTC_SHIFTR (*((volatile struct RTC_SHIFTR_s*) 0x4000282c))

struct RTC_TSTR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int SU : 4;
			unsigned int ST : 3;
			unsigned int : 1;
			unsigned int MNU : 4;
			unsigned int MNT : 3;
			unsigned int : 1;
			unsigned int HU : 4;
			unsigned int HT : 2;
			unsigned int PM : 1;
		} __attribute((__packed__));
	};
};
#define RTC_TSTR (*((volatile struct RTC_TSTR_s*) 0x40002830))

struct RTC_TSDR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int DU : 4;
			unsigned int DT : 2;
			unsigned int : 2;
			unsigned int MU : 4;
			unsigned int MT : 1;
			unsigned int WDU : 3;
		} __attribute((__packed__));
	};
};
#define RTC_TSDR (*((volatile struct RTC_TSDR_s*) 0x40002834))

struct RTC_TSSSR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int SS : 16;
		} __attribute((__packed__));
	};
};
#define RTC_TSSSR (*((volatile struct RTC_TSSSR_s*) 0x40002838))

struct RTC_CALR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int CALM : 9;
			unsigned int : 4;
			unsigned int CALW16 : 1;
			unsigned int CALW8 : 1;
			unsigned int CALP : 1;
		} __attribute((__packed__));
	};
};
#define RTC_CALR (*((volatile struct RTC_CALR_s*) 0x4000283c))

struct RTC_TAFCR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int TAMP1E : 1;
			unsigned int TAMP1TRG : 1;
			unsigned int TAMPIE : 1;
			unsigned int TAMP2E : 1;
			unsigned int TAMP2TRG : 1;
			unsigned int TAMP3E : 1;
			unsigned int TAMP3TRG : 1;
			unsigned int TAMPTS : 1;
			unsigned int TAMPFREQ : 3;
			unsigned int TAMPFLT : 2;
			unsigned int TAMPPRCH : 2;
			unsigned int TAMPPUDIS : 1;
			unsigned int : 2;
			unsigned int PC13VALUE : 1;
			unsigned int PC13MODE : 1;
			unsigned int PC14VALUE : 1;
			unsigned int PC14MODE : 1;
			unsigned int PC15VALUE : 1;
			unsigned int PC15MODE : 1;
		} __attribute((__packed__));
	};
};
#define RTC_TAFCR (*((volatile struct RTC_TAFCR_s*) 0x40002840))

struct RTC_ALRMASSR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int SS : 15;
			unsigned int : 9;
			unsigned int MASKSS : 4;
		} __attribute((__packed__));
	};
};
#define RTC_ALRMASSR (*((volatile struct RTC_ALRMASSR_s*) 0x40002844))

struct RTC_ALRMBSSR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int SS : 15;
			unsigned int : 9;
			unsigned int MASKSS : 4;
		} __attribute((__packed__));
	};
};
#define RTC_ALRMBSSR (*((volatile struct RTC_ALRMBSSR_s*) 0x40002848))

struct RTC_BKP0R_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int BKP : 32;
		} __attribute((__packed__));
	};
};
#define RTC_BKP0R (*((volatile struct RTC_BKP0R_s*) 0x40002850))

struct RTC_BKP1R_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int BKP : 32;
		} __attribute((__packed__));
	};
};
#define RTC_BKP1R (*((volatile struct RTC_BKP1R_s*) 0x40002854))

struct RTC_BKP2R_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int BKP : 32;
		} __attribute((__packed__));
	};
};
#define RTC_BKP2R (*((volatile struct RTC_BKP2R_s*) 0x40002858))

struct RTC_BKP3R_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int BKP : 32;
		} __attribute((__packed__));
	};
};
#define RTC_BKP3R (*((volatile struct RTC_BKP3R_s*) 0x4000285c))

struct RTC_BKP4R_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int BKP : 32;
		} __attribute((__packed__));
	};
};
#define RTC_BKP4R (*((volatile struct RTC_BKP4R_s*) 0x40002860))

struct RTC_BKP5R_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int BKP : 32;
		} __attribute((__packed__));
	};
};
#define RTC_BKP5R (*((volatile struct RTC_BKP5R_s*) 0x40002864))

struct RTC_BKP6R_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int BKP : 32;
		} __attribute((__packed__));
	};
};
#define RTC_BKP6R (*((volatile struct RTC_BKP6R_s*) 0x40002868))

struct RTC_BKP7R_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int BKP : 32;
		} __attribute((__packed__));
	};
};
#define RTC_BKP7R (*((volatile struct RTC_BKP7R_s*) 0x4000286c))

struct RTC_BKP8R_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int BKP : 32;
		} __attribute((__packed__));
	};
};
#define RTC_BKP8R (*((volatile struct RTC_BKP8R_s*) 0x40002870))

struct RTC_BKP9R_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int BKP : 32;
		} __attribute((__packed__));
	};
};
#define RTC_BKP9R (*((volatile struct RTC_BKP9R_s*) 0x40002874))

struct RTC_BKP10R_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int BKP : 32;
		} __attribute((__packed__));
	};
};
#define RTC_BKP10R (*((volatile struct RTC_BKP10R_s*) 0x40002878))

struct RTC_BKP11R_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int BKP : 32;
		} __attribute((__packed__));
	};
};
#define RTC_BKP11R (*((volatile struct RTC_BKP11R_s*) 0x4000287c))

struct RTC_BKP12R_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int BKP : 32;
		} __attribute((__packed__));
	};
};
#define RTC_BKP12R (*((volatile struct RTC_BKP12R_s*) 0x40002880))

struct RTC_BKP13R_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int BKP : 32;
		} __attribute((__packed__));
	};
};
#define RTC_BKP13R (*((volatile struct RTC_BKP13R_s*) 0x40002884))

struct RTC_BKP14R_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int BKP : 32;
		} __attribute((__packed__));
	};
};
#define RTC_BKP14R (*((volatile struct RTC_BKP14R_s*) 0x40002888))

struct RTC_BKP15R_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int BKP : 32;
		} __attribute((__packed__));
	};
};
#define RTC_BKP15R (*((volatile struct RTC_BKP15R_s*) 0x4000288c))

struct RTC_BKP16R_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int BKP : 32;
		} __attribute((__packed__));
	};
};
#define RTC_BKP16R (*((volatile struct RTC_BKP16R_s*) 0x40002890))

struct RTC_BKP17R_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int BKP : 32;
		} __attribute((__packed__));
	};
};
#define RTC_BKP17R (*((volatile struct RTC_BKP17R_s*) 0x40002894))

struct RTC_BKP18R_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int BKP : 32;
		} __attribute((__packed__));
	};
};
#define RTC_BKP18R (*((volatile struct RTC_BKP18R_s*) 0x40002898))

struct RTC_BKP19R_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int BKP : 32;
		} __attribute((__packed__));
	};
};
#define RTC_BKP19R (*((volatile struct RTC_BKP19R_s*) 0x4000289c))

struct RTC_BKP20R_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int BKP : 32;
		} __attribute((__packed__));
	};
};
#define RTC_BKP20R (*((volatile struct RTC_BKP20R_s*) 0x400028a0))

struct RTC_BKP21R_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int BKP : 32;
		} __attribute((__packed__));
	};
};
#define RTC_BKP21R (*((volatile struct RTC_BKP21R_s*) 0x400028a4))

struct RTC_BKP22R_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int BKP : 32;
		} __attribute((__packed__));
	};
};
#define RTC_BKP22R (*((volatile struct RTC_BKP22R_s*) 0x400028a8))

struct RTC_BKP23R_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int BKP : 32;
		} __attribute((__packed__));
	};
};
#define RTC_BKP23R (*((volatile struct RTC_BKP23R_s*) 0x400028ac))

struct RTC_BKP24R_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int BKP : 32;
		} __attribute((__packed__));
	};
};
#define RTC_BKP24R (*((volatile struct RTC_BKP24R_s*) 0x400028b0))

struct RTC_BKP25R_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int BKP : 32;
		} __attribute((__packed__));
	};
};
#define RTC_BKP25R (*((volatile struct RTC_BKP25R_s*) 0x400028b4))

struct RTC_BKP26R_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int BKP : 32;
		} __attribute((__packed__));
	};
};
#define RTC_BKP26R (*((volatile struct RTC_BKP26R_s*) 0x400028b8))

struct RTC_BKP27R_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int BKP : 32;
		} __attribute((__packed__));
	};
};
#define RTC_BKP27R (*((volatile struct RTC_BKP27R_s*) 0x400028bc))

struct RTC_BKP28R_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int BKP : 32;
		} __attribute((__packed__));
	};
};
#define RTC_BKP28R (*((volatile struct RTC_BKP28R_s*) 0x400028c0))

struct RTC_BKP29R_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int BKP : 32;
		} __attribute((__packed__));
	};
};
#define RTC_BKP29R (*((volatile struct RTC_BKP29R_s*) 0x400028c4))

struct RTC_BKP30R_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int BKP : 32;
		} __attribute((__packed__));
	};
};
#define RTC_BKP30R (*((volatile struct RTC_BKP30R_s*) 0x400028c8))

struct RTC_BKP31R_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int BKP : 32;
		} __attribute((__packed__));
	};
};
#define RTC_BKP31R (*((volatile struct RTC_BKP31R_s*) 0x400028cc))

// TIM6
struct TIM6_CR1_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int CEN : 1;
			unsigned int UDIS : 1;
			unsigned int URS : 1;
			unsigned int OPM : 1;
			unsigned int : 3;
			unsigned int ARPE : 1;
			unsigned int : 3;
			unsigned int UIFREMAP : 1;
		} __attribute((__packed__));
	};
};
#define TIM6_CR1 (*((volatile struct TIM6_CR1_s*) 0x40001000))

struct TIM6_CR2_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int : 4;
			unsigned int MMS : 3;
		} __attribute((__packed__));
	};
};
#define TIM6_CR2 (*((volatile struct TIM6_CR2_s*) 0x40001004))

struct TIM6_DIER_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int UIE : 1;
			unsigned int : 7;
			unsigned int UDE : 1;
		} __attribute((__packed__));
	};
};
#define TIM6_DIER (*((volatile struct TIM6_DIER_s*) 0x4000100c))

struct TIM6_SR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int UIF : 1;
		} __attribute((__packed__));
	};
};
#define TIM6_SR (*((volatile struct TIM6_SR_s*) 0x40001010))

struct TIM6_EGR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int UG : 1;
		} __attribute((__packed__));
	};
};
#define TIM6_EGR (*((volatile struct TIM6_EGR_s*) 0x40001014))

struct TIM6_CNT_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int CNT : 16;
			unsigned int : 15;
			unsigned int UIFCPY : 1;
		} __attribute((__packed__));
	};
};
#define TIM6_CNT (*((volatile struct TIM6_CNT_s*) 0x40001024))

struct TIM6_PSC_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int PSC : 16;
		} __attribute((__packed__));
	};
};
#define TIM6_PSC (*((volatile struct TIM6_PSC_s*) 0x40001028))

struct TIM6_ARR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int ARR : 16;
		} __attribute((__packed__));
	};
};
#define TIM6_ARR (*((volatile struct TIM6_ARR_s*) 0x4000102c))

// TIM7
struct TIM7_CR1_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int CEN : 1;
			unsigned int UDIS : 1;
			unsigned int URS : 1;
			unsigned int OPM : 1;
			unsigned int : 3;
			unsigned int ARPE : 1;
			unsigned int : 3;
			unsigned int UIFREMAP : 1;
		} __attribute((__packed__));
	};
};
#define TIM7_CR1 (*((volatile struct TIM7_CR1_s*) 0x40001400))

struct TIM7_CR2_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int : 4;
			unsigned int MMS : 3;
		} __attribute((__packed__));
	};
};
#define TIM7_CR2 (*((volatile struct TIM7_CR2_s*) 0x40001404))

struct TIM7_DIER_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int UIE : 1;
			unsigned int : 7;
			unsigned int UDE : 1;
		} __attribute((__packed__));
	};
};
#define TIM7_DIER (*((volatile struct TIM7_DIER_s*) 0x4000140c))

struct TIM7_SR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int UIF : 1;
		} __attribute((__packed__));
	};
};
#define TIM7_SR (*((volatile struct TIM7_SR_s*) 0x40001410))

struct TIM7_EGR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int UG : 1;
		} __attribute((__packed__));
	};
};
#define TIM7_EGR (*((volatile struct TIM7_EGR_s*) 0x40001414))

struct TIM7_CNT_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int CNT : 16;
			unsigned int : 15;
			unsigned int UIFCPY : 1;
		} __attribute((__packed__));
	};
};
#define TIM7_CNT (*((volatile struct TIM7_CNT_s*) 0x40001424))

struct TIM7_PSC_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int PSC : 16;
		} __attribute((__packed__));
	};
};
#define TIM7_PSC (*((volatile struct TIM7_PSC_s*) 0x40001428))

struct TIM7_ARR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int ARR : 16;
		} __attribute((__packed__));
	};
};
#define TIM7_ARR (*((volatile struct TIM7_ARR_s*) 0x4000142c))

// DAC
struct DAC_CR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int EN1 : 1;
			unsigned int BOFF1 : 1;
			unsigned int TEN1 : 1;
			unsigned int TSEL1 : 3;
			unsigned int WAVE1 : 2;
			unsigned int MAMP1 : 4;
			unsigned int DMAEN1 : 1;
			unsigned int DMAUDRIE1 : 1;
			unsigned int : 2;
			unsigned int EN2 : 1;
			unsigned int BOFF2 : 1;
			unsigned int TEN2 : 1;
			unsigned int TSEL2 : 3;
			unsigned int WAVE2 : 2;
			unsigned int MAMP2 : 4;
			unsigned int DMAEN2 : 1;
			unsigned int DMAUDRIE2 : 1;
		} __attribute((__packed__));
	};
};
#define DAC_CR (*((volatile struct DAC_CR_s*) 0x40007400))

struct DAC_SWTRIGR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int SWTRIG1 : 1;
			unsigned int SWTRIG2 : 1;
		} __attribute((__packed__));
	};
};
#define DAC_SWTRIGR (*((volatile struct DAC_SWTRIGR_s*) 0x40007404))

struct DAC_DHR12R1_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int DACC1DHR : 12;
		} __attribute((__packed__));
	};
};
#define DAC_DHR12R1 (*((volatile struct DAC_DHR12R1_s*) 0x40007408))

struct DAC_DHR12L1_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int : 4;
			unsigned int DACC1DHR : 12;
		} __attribute((__packed__));
	};
};
#define DAC_DHR12L1 (*((volatile struct DAC_DHR12L1_s*) 0x4000740c))

struct DAC_DHR8R1_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int DACC1DHR : 8;
		} __attribute((__packed__));
	};
};
#define DAC_DHR8R1 (*((volatile struct DAC_DHR8R1_s*) 0x40007410))

struct DAC_DHR12R2_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int DACC2DHR : 12;
		} __attribute((__packed__));
	};
};
#define DAC_DHR12R2 (*((volatile struct DAC_DHR12R2_s*) 0x40007414))

struct DAC_DHR12L2_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int : 4;
			unsigned int DACC2DHR : 12;
		} __attribute((__packed__));
	};
};
#define DAC_DHR12L2 (*((volatile struct DAC_DHR12L2_s*) 0x40007418))

struct DAC_DHR8R2_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int DACC2DHR : 8;
		} __attribute((__packed__));
	};
};
#define DAC_DHR8R2 (*((volatile struct DAC_DHR8R2_s*) 0x4000741c))

struct DAC_DHR12RD_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int DACC1DHR : 12;
			unsigned int : 4;
			unsigned int DACC2DHR : 12;
		} __attribute((__packed__));
	};
};
#define DAC_DHR12RD (*((volatile struct DAC_DHR12RD_s*) 0x40007420))

struct DAC_DHR12LD_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int : 4;
			unsigned int DACC1DHR : 12;
			unsigned int : 4;
			unsigned int DACC2DHR : 12;
		} __attribute((__packed__));
	};
};
#define DAC_DHR12LD (*((volatile struct DAC_DHR12LD_s*) 0x40007424))

struct DAC_DHR8RD_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int DACC1DHR : 8;
			unsigned int DACC2DHR : 8;
		} __attribute((__packed__));
	};
};
#define DAC_DHR8RD (*((volatile struct DAC_DHR8RD_s*) 0x40007428))

struct DAC_DOR1_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int DACC1DOR : 12;
		} __attribute((__packed__));
	};
};
#define DAC_DOR1 (*((volatile struct DAC_DOR1_s*) 0x4000742c))

struct DAC_DOR2_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int DACC2DOR : 12;
		} __attribute((__packed__));
	};
};
#define DAC_DOR2 (*((volatile struct DAC_DOR2_s*) 0x40007430))

struct DAC_SR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int : 13;
			unsigned int DMAUDR1 : 1;
			unsigned int : 15;
			unsigned int DMAUDR2 : 1;
		} __attribute((__packed__));
	};
};
#define DAC_SR (*((volatile struct DAC_SR_s*) 0x40007434))

// DBGMCU
struct DBGMCU_IDCODE_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int DEV_ID : 12;
			unsigned int : 4;
			unsigned int REV_ID : 16;
		} __attribute((__packed__));
	};
};
#define DBGMCU_IDCODE (*((volatile struct DBGMCU_IDCODE_s*) 0xe0042000))

struct DBGMCU_CR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int DBG_SLEEP : 1;
			unsigned int DBG_STOP : 1;
			unsigned int DBG_STANDBY : 1;
			unsigned int : 2;
			unsigned int TRACE_IOEN : 1;
			unsigned int TRACE_MODE : 2;
		} __attribute((__packed__));
	};
};
#define DBGMCU_CR (*((volatile struct DBGMCU_CR_s*) 0xe0042004))

struct DBGMCU_APB1FZ_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int DBG_TIM2_STOP : 1;
			unsigned int DBG_TIM3_STOP : 1;
			unsigned int DBG_TIM4_STOP : 1;
			unsigned int DBG_TIM5_STOP : 1;
			unsigned int DBG_TIM6_STOP : 1;
			unsigned int DBG_TIM7_STOP : 1;
			unsigned int DBG_TIM12_STOP : 1;
			unsigned int DBG_TIM13_STOP : 1;
			unsigned int DBG_TIMER14_STOP : 1;
			unsigned int DBG_TIM18_STOP : 1;
			unsigned int DBG_RTC_STOP : 1;
			unsigned int DBG_WWDG_STOP : 1;
			unsigned int DBG_IWDG_STOP : 1;
			unsigned int : 8;
			unsigned int I2C1_SMBUS_TIMEOUT : 1;
			unsigned int I2C2_SMBUS_TIMEOUT : 1;
			unsigned int : 2;
			unsigned int DBG_CAN_STOP : 1;
		} __attribute((__packed__));
	};
};
#define DBGMCU_APB1FZ (*((volatile struct DBGMCU_APB1FZ_s*) 0xe0042008))

struct DBGMCU_APB2FZ_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int : 2;
			unsigned int DBG_TIM15_STOP : 1;
			unsigned int DBG_TIM16_STOP : 1;
			unsigned int DBG_TIM17_STO : 1;
			unsigned int DBG_TIM19_STOP : 1;
		} __attribute((__packed__));
	};
};
#define DBGMCU_APB2FZ (*((volatile struct DBGMCU_APB2FZ_s*) 0xe004200c))

// TIM1
struct TIM1_CR1_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int CEN : 1;
			unsigned int UDIS : 1;
			unsigned int URS : 1;
			unsigned int OPM : 1;
			unsigned int DIR : 1;
			unsigned int CMS : 2;
			unsigned int ARPE : 1;
			unsigned int CKD : 2;
			unsigned int : 1;
			unsigned int UIFREMAP : 1;
		} __attribute((__packed__));
	};
};
#define TIM1_CR1 (*((volatile struct TIM1_CR1_s*) 0x40012c00))

struct TIM1_CR2_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int CCPC : 1;
			unsigned int : 1;
			unsigned int CCUS : 1;
			unsigned int CCDS : 1;
			unsigned int MMS : 3;
			unsigned int TI1S : 1;
			unsigned int OIS1 : 1;
			unsigned int OIS1N : 1;
			unsigned int OIS2 : 1;
			unsigned int OIS2N : 1;
			unsigned int OIS3 : 1;
			unsigned int OIS3N : 1;
			unsigned int OIS4 : 1;
			unsigned int : 1;
			unsigned int OIS5 : 1;
			unsigned int : 1;
			unsigned int OIS6 : 1;
			unsigned int : 1;
			unsigned int MMS2 : 4;
		} __attribute((__packed__));
	};
};
#define TIM1_CR2 (*((volatile struct TIM1_CR2_s*) 0x40012c04))

struct TIM1_SMCR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int SMS : 3;
			unsigned int OCCS : 1;
			unsigned int TS : 3;
			unsigned int MSM : 1;
			unsigned int ETF : 4;
			unsigned int ETPS : 2;
			unsigned int ECE : 1;
			unsigned int ETP : 1;
			unsigned int SMS3 : 1;
		} __attribute((__packed__));
	};
};
#define TIM1_SMCR (*((volatile struct TIM1_SMCR_s*) 0x40012c08))

struct TIM1_DIER_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int UIE : 1;
			unsigned int CC1IE : 1;
			unsigned int CC2IE : 1;
			unsigned int CC3IE : 1;
			unsigned int CC4IE : 1;
			unsigned int COMIE : 1;
			unsigned int TIE : 1;
			unsigned int BIE : 1;
			unsigned int UDE : 1;
			unsigned int CC1DE : 1;
			unsigned int CC2DE : 1;
			unsigned int CC3DE : 1;
			unsigned int CC4DE : 1;
			unsigned int COMDE : 1;
			unsigned int TDE : 1;
		} __attribute((__packed__));
	};
};
#define TIM1_DIER (*((volatile struct TIM1_DIER_s*) 0x40012c0c))

struct TIM1_SR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int UIF : 1;
			unsigned int CC1IF : 1;
			unsigned int CC2IF : 1;
			unsigned int CC3IF : 1;
			unsigned int CC4IF : 1;
			unsigned int COMIF : 1;
			unsigned int TIF : 1;
			unsigned int BIF : 1;
			unsigned int B2IF : 1;
			unsigned int CC1OF : 1;
			unsigned int CC2OF : 1;
			unsigned int CC3OF : 1;
			unsigned int CC4OF : 1;
			unsigned int : 3;
			unsigned int C5IF : 1;
			unsigned int C6IF : 1;
		} __attribute((__packed__));
	};
};
#define TIM1_SR (*((volatile struct TIM1_SR_s*) 0x40012c10))

struct TIM1_EGR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int UG : 1;
			unsigned int CC1G : 1;
			unsigned int CC2G : 1;
			unsigned int CC3G : 1;
			unsigned int CC4G : 1;
			unsigned int COMG : 1;
			unsigned int TG : 1;
			unsigned int BG : 1;
			unsigned int B2G : 1;
		} __attribute((__packed__));
	};
};
#define TIM1_EGR (*((volatile struct TIM1_EGR_s*) 0x40012c14))

struct TIM1_CCMR1_Output_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int CC1S : 2;
			unsigned int OC1FE : 1;
			unsigned int OC1PE : 1;
			unsigned int OC1M : 3;
			unsigned int OC1CE : 1;
			unsigned int CC2S : 2;
			unsigned int OC2FE : 1;
			unsigned int OC2PE : 1;
			unsigned int OC2M : 3;
			unsigned int OC2CE : 1;
			unsigned int OC1M_3 : 1;
			unsigned int : 7;
			unsigned int OC2M_3 : 1;
		} __attribute((__packed__));
	};
};
#define TIM1_CCMR1_Output (*((volatile struct TIM1_CCMR1_Output_s*) 0x40012c18))

struct TIM1_CCMR1_Input_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int CC1S : 2;
			unsigned int IC1PCS : 2;
			unsigned int IC1F : 4;
			unsigned int CC2S : 2;
			unsigned int IC2PCS : 2;
			unsigned int IC2F : 4;
		} __attribute((__packed__));
	};
};
#define TIM1_CCMR1_Input (*((volatile struct TIM1_CCMR1_Input_s*) 0x40012c18))

struct TIM1_CCMR2_Output_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int CC3S : 2;
			unsigned int OC3FE : 1;
			unsigned int OC3PE : 1;
			unsigned int OC3M : 3;
			unsigned int OC3CE : 1;
			unsigned int CC4S : 2;
			unsigned int OC4FE : 1;
			unsigned int OC4PE : 1;
			unsigned int OC4M : 3;
			unsigned int OC4CE : 1;
			unsigned int OC3M_3 : 1;
			unsigned int : 7;
			unsigned int OC4M_3 : 1;
		} __attribute((__packed__));
	};
};
#define TIM1_CCMR2_Output (*((volatile struct TIM1_CCMR2_Output_s*) 0x40012c1c))

struct TIM1_CCMR2_Input_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int CC3S : 2;
			unsigned int IC3PSC : 2;
			unsigned int IC3F : 4;
			unsigned int CC4S : 2;
			unsigned int IC4PSC : 2;
			unsigned int IC4F : 4;
		} __attribute((__packed__));
	};
};
#define TIM1_CCMR2_Input (*((volatile struct TIM1_CCMR2_Input_s*) 0x40012c1c))

struct TIM1_CCER_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int CC1E : 1;
			unsigned int CC1P : 1;
			unsigned int CC1NE : 1;
			unsigned int CC1NP : 1;
			unsigned int CC2E : 1;
			unsigned int CC2P : 1;
			unsigned int CC2NE : 1;
			unsigned int CC2NP : 1;
			unsigned int CC3E : 1;
			unsigned int CC3P : 1;
			unsigned int CC3NE : 1;
			unsigned int CC3NP : 1;
			unsigned int CC4E : 1;
			unsigned int CC4P : 1;
			unsigned int : 1;
			unsigned int CC4NP : 1;
			unsigned int CC5E : 1;
			unsigned int CC5P : 1;
			unsigned int : 2;
			unsigned int CC6E : 1;
			unsigned int CC6P : 1;
		} __attribute((__packed__));
	};
};
#define TIM1_CCER (*((volatile struct TIM1_CCER_s*) 0x40012c20))

struct TIM1_CNT_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int CNT : 16;
			unsigned int : 15;
			unsigned int UIFCPY : 1;
		} __attribute((__packed__));
	};
};
#define TIM1_CNT (*((volatile struct TIM1_CNT_s*) 0x40012c24))

struct TIM1_PSC_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int PSC : 16;
		} __attribute((__packed__));
	};
};
#define TIM1_PSC (*((volatile struct TIM1_PSC_s*) 0x40012c28))

struct TIM1_ARR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int ARR : 16;
		} __attribute((__packed__));
	};
};
#define TIM1_ARR (*((volatile struct TIM1_ARR_s*) 0x40012c2c))

struct TIM1_RCR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int REP : 16;
		} __attribute((__packed__));
	};
};
#define TIM1_RCR (*((volatile struct TIM1_RCR_s*) 0x40012c30))

struct TIM1_CCR1_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int CCR1 : 16;
		} __attribute((__packed__));
	};
};
#define TIM1_CCR1 (*((volatile struct TIM1_CCR1_s*) 0x40012c34))

struct TIM1_CCR2_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int CCR2 : 16;
		} __attribute((__packed__));
	};
};
#define TIM1_CCR2 (*((volatile struct TIM1_CCR2_s*) 0x40012c38))

struct TIM1_CCR3_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int CCR3 : 16;
		} __attribute((__packed__));
	};
};
#define TIM1_CCR3 (*((volatile struct TIM1_CCR3_s*) 0x40012c3c))

struct TIM1_CCR4_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int CCR4 : 16;
		} __attribute((__packed__));
	};
};
#define TIM1_CCR4 (*((volatile struct TIM1_CCR4_s*) 0x40012c40))

struct TIM1_BDTR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int DTG : 8;
			unsigned int LOCK : 2;
			unsigned int OSSI : 1;
			unsigned int OSSR : 1;
			unsigned int BKE : 1;
			unsigned int BKP : 1;
			unsigned int AOE : 1;
			unsigned int MOE : 1;
			unsigned int BKF : 4;
			unsigned int BK2F : 4;
			unsigned int BK2E : 1;
			unsigned int BK2P : 1;
		} __attribute((__packed__));
	};
};
#define TIM1_BDTR (*((volatile struct TIM1_BDTR_s*) 0x40012c44))

struct TIM1_DCR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int DBA : 5;
			unsigned int : 3;
			unsigned int DBL : 5;
		} __attribute((__packed__));
	};
};
#define TIM1_DCR (*((volatile struct TIM1_DCR_s*) 0x40012c48))

struct TIM1_DMAR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int DMAB : 16;
		} __attribute((__packed__));
	};
};
#define TIM1_DMAR (*((volatile struct TIM1_DMAR_s*) 0x40012c4c))

struct TIM1_CCMR3_Output_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int : 2;
			unsigned int OC5FE : 1;
			unsigned int OC5PE : 1;
			unsigned int OC5M : 3;
			unsigned int OC5CE : 1;
			unsigned int : 2;
			unsigned int OC6FE : 1;
			unsigned int OC6PE : 1;
			unsigned int OC6M : 3;
			unsigned int OC6CE : 1;
			unsigned int OC5M_3 : 1;
			unsigned int : 7;
			unsigned int OC6M_3 : 1;
		} __attribute((__packed__));
	};
};
#define TIM1_CCMR3_Output (*((volatile struct TIM1_CCMR3_Output_s*) 0x40012c54))

struct TIM1_CCR5_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int CCR5 : 16;
			unsigned int : 13;
			unsigned int GC5C1 : 1;
			unsigned int GC5C2 : 1;
			unsigned int GC5C3 : 1;
		} __attribute((__packed__));
	};
};
#define TIM1_CCR5 (*((volatile struct TIM1_CCR5_s*) 0x40012c58))

struct TIM1_CCR6_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int CCR6 : 16;
		} __attribute((__packed__));
	};
};
#define TIM1_CCR6 (*((volatile struct TIM1_CCR6_s*) 0x40012c5c))

struct TIM1_OR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int TIM1_ETR_ADC1_RMP : 2;
			unsigned int TIM1_ETR_ADC4_RMP : 2;
		} __attribute((__packed__));
	};
};
#define TIM1_OR (*((volatile struct TIM1_OR_s*) 0x40012c60))

// TIM20
struct TIM20_CR1_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int CEN : 1;
			unsigned int UDIS : 1;
			unsigned int URS : 1;
			unsigned int OPM : 1;
			unsigned int DIR : 1;
			unsigned int CMS : 2;
			unsigned int ARPE : 1;
			unsigned int CKD : 2;
			unsigned int : 1;
			unsigned int UIFREMAP : 1;
		} __attribute((__packed__));
	};
};
#define TIM20_CR1 (*((volatile struct TIM20_CR1_s*) 0x40015000))

struct TIM20_CR2_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int CCPC : 1;
			unsigned int : 1;
			unsigned int CCUS : 1;
			unsigned int CCDS : 1;
			unsigned int MMS : 3;
			unsigned int TI1S : 1;
			unsigned int OIS1 : 1;
			unsigned int OIS1N : 1;
			unsigned int OIS2 : 1;
			unsigned int OIS2N : 1;
			unsigned int OIS3 : 1;
			unsigned int OIS3N : 1;
			unsigned int OIS4 : 1;
			unsigned int : 1;
			unsigned int OIS5 : 1;
			unsigned int : 1;
			unsigned int OIS6 : 1;
			unsigned int : 1;
			unsigned int MMS2 : 4;
		} __attribute((__packed__));
	};
};
#define TIM20_CR2 (*((volatile struct TIM20_CR2_s*) 0x40015004))

struct TIM20_SMCR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int SMS : 3;
			unsigned int OCCS : 1;
			unsigned int TS : 3;
			unsigned int MSM : 1;
			unsigned int ETF : 4;
			unsigned int ETPS : 2;
			unsigned int ECE : 1;
			unsigned int ETP : 1;
			unsigned int SMS3 : 1;
		} __attribute((__packed__));
	};
};
#define TIM20_SMCR (*((volatile struct TIM20_SMCR_s*) 0x40015008))

struct TIM20_DIER_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int UIE : 1;
			unsigned int CC1IE : 1;
			unsigned int CC2IE : 1;
			unsigned int CC3IE : 1;
			unsigned int CC4IE : 1;
			unsigned int COMIE : 1;
			unsigned int TIE : 1;
			unsigned int BIE : 1;
			unsigned int UDE : 1;
			unsigned int CC1DE : 1;
			unsigned int CC2DE : 1;
			unsigned int CC3DE : 1;
			unsigned int CC4DE : 1;
			unsigned int COMDE : 1;
			unsigned int TDE : 1;
		} __attribute((__packed__));
	};
};
#define TIM20_DIER (*((volatile struct TIM20_DIER_s*) 0x4001500c))

struct TIM20_SR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int UIF : 1;
			unsigned int CC1IF : 1;
			unsigned int CC2IF : 1;
			unsigned int CC3IF : 1;
			unsigned int CC4IF : 1;
			unsigned int COMIF : 1;
			unsigned int TIF : 1;
			unsigned int BIF : 1;
			unsigned int B2IF : 1;
			unsigned int CC1OF : 1;
			unsigned int CC2OF : 1;
			unsigned int CC3OF : 1;
			unsigned int CC4OF : 1;
			unsigned int : 3;
			unsigned int C5IF : 1;
			unsigned int C6IF : 1;
		} __attribute((__packed__));
	};
};
#define TIM20_SR (*((volatile struct TIM20_SR_s*) 0x40015010))

struct TIM20_EGR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int UG : 1;
			unsigned int CC1G : 1;
			unsigned int CC2G : 1;
			unsigned int CC3G : 1;
			unsigned int CC4G : 1;
			unsigned int COMG : 1;
			unsigned int TG : 1;
			unsigned int BG : 1;
			unsigned int B2G : 1;
		} __attribute((__packed__));
	};
};
#define TIM20_EGR (*((volatile struct TIM20_EGR_s*) 0x40015014))

struct TIM20_CCMR1_Output_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int CC1S : 2;
			unsigned int OC1FE : 1;
			unsigned int OC1PE : 1;
			unsigned int OC1M : 3;
			unsigned int OC1CE : 1;
			unsigned int CC2S : 2;
			unsigned int OC2FE : 1;
			unsigned int OC2PE : 1;
			unsigned int OC2M : 3;
			unsigned int OC2CE : 1;
			unsigned int OC1M_3 : 1;
			unsigned int : 7;
			unsigned int OC2M_3 : 1;
		} __attribute((__packed__));
	};
};
#define TIM20_CCMR1_Output (*((volatile struct TIM20_CCMR1_Output_s*) 0x40015018))

struct TIM20_CCMR1_Input_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int CC1S : 2;
			unsigned int IC1PCS : 2;
			unsigned int IC1F : 4;
			unsigned int CC2S : 2;
			unsigned int IC2PCS : 2;
			unsigned int IC2F : 4;
		} __attribute((__packed__));
	};
};
#define TIM20_CCMR1_Input (*((volatile struct TIM20_CCMR1_Input_s*) 0x40015018))

struct TIM20_CCMR2_Output_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int CC3S : 2;
			unsigned int OC3FE : 1;
			unsigned int OC3PE : 1;
			unsigned int OC3M : 3;
			unsigned int OC3CE : 1;
			unsigned int CC4S : 2;
			unsigned int OC4FE : 1;
			unsigned int OC4PE : 1;
			unsigned int OC4M : 3;
			unsigned int OC4CE : 1;
			unsigned int OC3M_3 : 1;
			unsigned int : 7;
			unsigned int OC4M_3 : 1;
		} __attribute((__packed__));
	};
};
#define TIM20_CCMR2_Output (*((volatile struct TIM20_CCMR2_Output_s*) 0x4001501c))

struct TIM20_CCMR2_Input_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int CC3S : 2;
			unsigned int IC3PSC : 2;
			unsigned int IC3F : 4;
			unsigned int CC4S : 2;
			unsigned int IC4PSC : 2;
			unsigned int IC4F : 4;
		} __attribute((__packed__));
	};
};
#define TIM20_CCMR2_Input (*((volatile struct TIM20_CCMR2_Input_s*) 0x4001501c))

struct TIM20_CCER_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int CC1E : 1;
			unsigned int CC1P : 1;
			unsigned int CC1NE : 1;
			unsigned int CC1NP : 1;
			unsigned int CC2E : 1;
			unsigned int CC2P : 1;
			unsigned int CC2NE : 1;
			unsigned int CC2NP : 1;
			unsigned int CC3E : 1;
			unsigned int CC3P : 1;
			unsigned int CC3NE : 1;
			unsigned int CC3NP : 1;
			unsigned int CC4E : 1;
			unsigned int CC4P : 1;
			unsigned int : 1;
			unsigned int CC4NP : 1;
			unsigned int CC5E : 1;
			unsigned int CC5P : 1;
			unsigned int : 2;
			unsigned int CC6E : 1;
			unsigned int CC6P : 1;
		} __attribute((__packed__));
	};
};
#define TIM20_CCER (*((volatile struct TIM20_CCER_s*) 0x40015020))

struct TIM20_CNT_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int CNT : 16;
			unsigned int : 15;
			unsigned int UIFCPY : 1;
		} __attribute((__packed__));
	};
};
#define TIM20_CNT (*((volatile struct TIM20_CNT_s*) 0x40015024))

struct TIM20_PSC_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int PSC : 16;
		} __attribute((__packed__));
	};
};
#define TIM20_PSC (*((volatile struct TIM20_PSC_s*) 0x40015028))

struct TIM20_ARR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int ARR : 16;
		} __attribute((__packed__));
	};
};
#define TIM20_ARR (*((volatile struct TIM20_ARR_s*) 0x4001502c))

struct TIM20_RCR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int REP : 16;
		} __attribute((__packed__));
	};
};
#define TIM20_RCR (*((volatile struct TIM20_RCR_s*) 0x40015030))

struct TIM20_CCR1_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int CCR1 : 16;
		} __attribute((__packed__));
	};
};
#define TIM20_CCR1 (*((volatile struct TIM20_CCR1_s*) 0x40015034))

struct TIM20_CCR2_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int CCR2 : 16;
		} __attribute((__packed__));
	};
};
#define TIM20_CCR2 (*((volatile struct TIM20_CCR2_s*) 0x40015038))

struct TIM20_CCR3_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int CCR3 : 16;
		} __attribute((__packed__));
	};
};
#define TIM20_CCR3 (*((volatile struct TIM20_CCR3_s*) 0x4001503c))

struct TIM20_CCR4_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int CCR4 : 16;
		} __attribute((__packed__));
	};
};
#define TIM20_CCR4 (*((volatile struct TIM20_CCR4_s*) 0x40015040))

struct TIM20_BDTR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int DTG : 8;
			unsigned int LOCK : 2;
			unsigned int OSSI : 1;
			unsigned int OSSR : 1;
			unsigned int BKE : 1;
			unsigned int BKP : 1;
			unsigned int AOE : 1;
			unsigned int MOE : 1;
			unsigned int BKF : 4;
			unsigned int BK2F : 4;
			unsigned int BK2E : 1;
			unsigned int BK2P : 1;
		} __attribute((__packed__));
	};
};
#define TIM20_BDTR (*((volatile struct TIM20_BDTR_s*) 0x40015044))

struct TIM20_DCR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int DBA : 5;
			unsigned int : 3;
			unsigned int DBL : 5;
		} __attribute((__packed__));
	};
};
#define TIM20_DCR (*((volatile struct TIM20_DCR_s*) 0x40015048))

struct TIM20_DMAR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int DMAB : 16;
		} __attribute((__packed__));
	};
};
#define TIM20_DMAR (*((volatile struct TIM20_DMAR_s*) 0x4001504c))

struct TIM20_CCMR3_Output_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int : 2;
			unsigned int OC5FE : 1;
			unsigned int OC5PE : 1;
			unsigned int OC5M : 3;
			unsigned int OC5CE : 1;
			unsigned int : 2;
			unsigned int OC6FE : 1;
			unsigned int OC6PE : 1;
			unsigned int OC6M : 3;
			unsigned int OC6CE : 1;
			unsigned int OC5M_3 : 1;
			unsigned int : 7;
			unsigned int OC6M_3 : 1;
		} __attribute((__packed__));
	};
};
#define TIM20_CCMR3_Output (*((volatile struct TIM20_CCMR3_Output_s*) 0x40015054))

struct TIM20_CCR5_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int CCR5 : 16;
			unsigned int : 13;
			unsigned int GC5C1 : 1;
			unsigned int GC5C2 : 1;
			unsigned int GC5C3 : 1;
		} __attribute((__packed__));
	};
};
#define TIM20_CCR5 (*((volatile struct TIM20_CCR5_s*) 0x40015058))

struct TIM20_CCR6_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int CCR6 : 16;
		} __attribute((__packed__));
	};
};
#define TIM20_CCR6 (*((volatile struct TIM20_CCR6_s*) 0x4001505c))

struct TIM20_OR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int TIM1_ETR_ADC1_RMP : 2;
			unsigned int TIM1_ETR_ADC4_RMP : 2;
		} __attribute((__packed__));
	};
};
#define TIM20_OR (*((volatile struct TIM20_OR_s*) 0x40015060))

// TIM8
struct TIM8_CR1_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int CEN : 1;
			unsigned int UDIS : 1;
			unsigned int URS : 1;
			unsigned int OPM : 1;
			unsigned int DIR : 1;
			unsigned int CMS : 2;
			unsigned int ARPE : 1;
			unsigned int CKD : 2;
			unsigned int : 1;
			unsigned int UIFREMAP : 1;
		} __attribute((__packed__));
	};
};
#define TIM8_CR1 (*((volatile struct TIM8_CR1_s*) 0x40013400))

struct TIM8_CR2_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int CCPC : 1;
			unsigned int : 1;
			unsigned int CCUS : 1;
			unsigned int CCDS : 1;
			unsigned int MMS : 3;
			unsigned int TI1S : 1;
			unsigned int OIS1 : 1;
			unsigned int OIS1N : 1;
			unsigned int OIS2 : 1;
			unsigned int OIS2N : 1;
			unsigned int OIS3 : 1;
			unsigned int OIS3N : 1;
			unsigned int OIS4 : 1;
			unsigned int : 1;
			unsigned int OIS5 : 1;
			unsigned int : 1;
			unsigned int OIS6 : 1;
			unsigned int : 1;
			unsigned int MMS2 : 4;
		} __attribute((__packed__));
	};
};
#define TIM8_CR2 (*((volatile struct TIM8_CR2_s*) 0x40013404))

struct TIM8_SMCR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int SMS : 3;
			unsigned int OCCS : 1;
			unsigned int TS : 3;
			unsigned int MSM : 1;
			unsigned int ETF : 4;
			unsigned int ETPS : 2;
			unsigned int ECE : 1;
			unsigned int ETP : 1;
			unsigned int SMS3 : 1;
		} __attribute((__packed__));
	};
};
#define TIM8_SMCR (*((volatile struct TIM8_SMCR_s*) 0x40013408))

struct TIM8_DIER_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int UIE : 1;
			unsigned int CC1IE : 1;
			unsigned int CC2IE : 1;
			unsigned int CC3IE : 1;
			unsigned int CC4IE : 1;
			unsigned int COMIE : 1;
			unsigned int TIE : 1;
			unsigned int BIE : 1;
			unsigned int UDE : 1;
			unsigned int CC1DE : 1;
			unsigned int CC2DE : 1;
			unsigned int CC3DE : 1;
			unsigned int CC4DE : 1;
			unsigned int COMDE : 1;
			unsigned int TDE : 1;
		} __attribute((__packed__));
	};
};
#define TIM8_DIER (*((volatile struct TIM8_DIER_s*) 0x4001340c))

struct TIM8_SR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int UIF : 1;
			unsigned int CC1IF : 1;
			unsigned int CC2IF : 1;
			unsigned int CC3IF : 1;
			unsigned int CC4IF : 1;
			unsigned int COMIF : 1;
			unsigned int TIF : 1;
			unsigned int BIF : 1;
			unsigned int B2IF : 1;
			unsigned int CC1OF : 1;
			unsigned int CC2OF : 1;
			unsigned int CC3OF : 1;
			unsigned int CC4OF : 1;
			unsigned int : 3;
			unsigned int C5IF : 1;
			unsigned int C6IF : 1;
		} __attribute((__packed__));
	};
};
#define TIM8_SR (*((volatile struct TIM8_SR_s*) 0x40013410))

struct TIM8_EGR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int UG : 1;
			unsigned int CC1G : 1;
			unsigned int CC2G : 1;
			unsigned int CC3G : 1;
			unsigned int CC4G : 1;
			unsigned int COMG : 1;
			unsigned int TG : 1;
			unsigned int BG : 1;
			unsigned int B2G : 1;
		} __attribute((__packed__));
	};
};
#define TIM8_EGR (*((volatile struct TIM8_EGR_s*) 0x40013414))

struct TIM8_CCMR1_Output_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int CC1S : 2;
			unsigned int OC1FE : 1;
			unsigned int OC1PE : 1;
			unsigned int OC1M : 3;
			unsigned int OC1CE : 1;
			unsigned int CC2S : 2;
			unsigned int OC2FE : 1;
			unsigned int OC2PE : 1;
			unsigned int OC2M : 3;
			unsigned int OC2CE : 1;
			unsigned int OC1M_3 : 1;
			unsigned int : 7;
			unsigned int OC2M_3 : 1;
		} __attribute((__packed__));
	};
};
#define TIM8_CCMR1_Output (*((volatile struct TIM8_CCMR1_Output_s*) 0x40013418))

struct TIM8_CCMR1_Input_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int CC1S : 2;
			unsigned int IC1PCS : 2;
			unsigned int IC1F : 4;
			unsigned int CC2S : 2;
			unsigned int IC2PCS : 2;
			unsigned int IC2F : 4;
		} __attribute((__packed__));
	};
};
#define TIM8_CCMR1_Input (*((volatile struct TIM8_CCMR1_Input_s*) 0x40013418))

struct TIM8_CCMR2_Output_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int CC3S : 2;
			unsigned int OC3FE : 1;
			unsigned int OC3PE : 1;
			unsigned int OC3M : 3;
			unsigned int OC3CE : 1;
			unsigned int CC4S : 2;
			unsigned int OC4FE : 1;
			unsigned int OC4PE : 1;
			unsigned int OC4M : 3;
			unsigned int OC4CE : 1;
			unsigned int OC3M_3 : 1;
			unsigned int : 7;
			unsigned int OC4M_3 : 1;
		} __attribute((__packed__));
	};
};
#define TIM8_CCMR2_Output (*((volatile struct TIM8_CCMR2_Output_s*) 0x4001341c))

struct TIM8_CCMR2_Input_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int CC3S : 2;
			unsigned int IC3PSC : 2;
			unsigned int IC3F : 4;
			unsigned int CC4S : 2;
			unsigned int IC4PSC : 2;
			unsigned int IC4F : 4;
		} __attribute((__packed__));
	};
};
#define TIM8_CCMR2_Input (*((volatile struct TIM8_CCMR2_Input_s*) 0x4001341c))

struct TIM8_CCER_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int CC1E : 1;
			unsigned int CC1P : 1;
			unsigned int CC1NE : 1;
			unsigned int CC1NP : 1;
			unsigned int CC2E : 1;
			unsigned int CC2P : 1;
			unsigned int CC2NE : 1;
			unsigned int CC2NP : 1;
			unsigned int CC3E : 1;
			unsigned int CC3P : 1;
			unsigned int CC3NE : 1;
			unsigned int CC3NP : 1;
			unsigned int CC4E : 1;
			unsigned int CC4P : 1;
			unsigned int : 1;
			unsigned int CC4NP : 1;
			unsigned int CC5E : 1;
			unsigned int CC5P : 1;
			unsigned int : 2;
			unsigned int CC6E : 1;
			unsigned int CC6P : 1;
		} __attribute((__packed__));
	};
};
#define TIM8_CCER (*((volatile struct TIM8_CCER_s*) 0x40013420))

struct TIM8_CNT_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int CNT : 16;
			unsigned int : 15;
			unsigned int UIFCPY : 1;
		} __attribute((__packed__));
	};
};
#define TIM8_CNT (*((volatile struct TIM8_CNT_s*) 0x40013424))

struct TIM8_PSC_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int PSC : 16;
		} __attribute((__packed__));
	};
};
#define TIM8_PSC (*((volatile struct TIM8_PSC_s*) 0x40013428))

struct TIM8_ARR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int ARR : 16;
		} __attribute((__packed__));
	};
};
#define TIM8_ARR (*((volatile struct TIM8_ARR_s*) 0x4001342c))

struct TIM8_RCR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int REP : 16;
		} __attribute((__packed__));
	};
};
#define TIM8_RCR (*((volatile struct TIM8_RCR_s*) 0x40013430))

struct TIM8_CCR1_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int CCR1 : 16;
		} __attribute((__packed__));
	};
};
#define TIM8_CCR1 (*((volatile struct TIM8_CCR1_s*) 0x40013434))

struct TIM8_CCR2_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int CCR2 : 16;
		} __attribute((__packed__));
	};
};
#define TIM8_CCR2 (*((volatile struct TIM8_CCR2_s*) 0x40013438))

struct TIM8_CCR3_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int CCR3 : 16;
		} __attribute((__packed__));
	};
};
#define TIM8_CCR3 (*((volatile struct TIM8_CCR3_s*) 0x4001343c))

struct TIM8_CCR4_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int CCR4 : 16;
		} __attribute((__packed__));
	};
};
#define TIM8_CCR4 (*((volatile struct TIM8_CCR4_s*) 0x40013440))

struct TIM8_BDTR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int DTG : 8;
			unsigned int LOCK : 2;
			unsigned int OSSI : 1;
			unsigned int OSSR : 1;
			unsigned int BKE : 1;
			unsigned int BKP : 1;
			unsigned int AOE : 1;
			unsigned int MOE : 1;
			unsigned int BKF : 4;
			unsigned int BK2F : 4;
			unsigned int BK2E : 1;
			unsigned int BK2P : 1;
		} __attribute((__packed__));
	};
};
#define TIM8_BDTR (*((volatile struct TIM8_BDTR_s*) 0x40013444))

struct TIM8_DCR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int DBA : 5;
			unsigned int : 3;
			unsigned int DBL : 5;
		} __attribute((__packed__));
	};
};
#define TIM8_DCR (*((volatile struct TIM8_DCR_s*) 0x40013448))

struct TIM8_DMAR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int DMAB : 16;
		} __attribute((__packed__));
	};
};
#define TIM8_DMAR (*((volatile struct TIM8_DMAR_s*) 0x4001344c))

struct TIM8_CCMR3_Output_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int : 2;
			unsigned int OC5FE : 1;
			unsigned int OC5PE : 1;
			unsigned int OC5M : 3;
			unsigned int OC5CE : 1;
			unsigned int : 2;
			unsigned int OC6FE : 1;
			unsigned int OC6PE : 1;
			unsigned int OC6M : 3;
			unsigned int OC6CE : 1;
			unsigned int OC5M_3 : 1;
			unsigned int : 7;
			unsigned int OC6M_3 : 1;
		} __attribute((__packed__));
	};
};
#define TIM8_CCMR3_Output (*((volatile struct TIM8_CCMR3_Output_s*) 0x40013454))

struct TIM8_CCR5_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int CCR5 : 16;
			unsigned int : 13;
			unsigned int GC5C1 : 1;
			unsigned int GC5C2 : 1;
			unsigned int GC5C3 : 1;
		} __attribute((__packed__));
	};
};
#define TIM8_CCR5 (*((volatile struct TIM8_CCR5_s*) 0x40013458))

struct TIM8_CCR6_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int CCR6 : 16;
		} __attribute((__packed__));
	};
};
#define TIM8_CCR6 (*((volatile struct TIM8_CCR6_s*) 0x4001345c))

struct TIM8_OR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int TIM8_ETR_ADC2_RMP : 2;
			unsigned int TIM8_ETR_ADC3_RMP : 2;
		} __attribute((__packed__));
	};
};
#define TIM8_OR (*((volatile struct TIM8_OR_s*) 0x40013460))

// ADC1
struct ADC1_ISR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int ADRDY : 1;
			unsigned int EOSMP : 1;
			unsigned int EOC : 1;
			unsigned int EOS : 1;
			unsigned int OVR : 1;
			unsigned int JEOC : 1;
			unsigned int JEOS : 1;
			unsigned int AWD1 : 1;
			unsigned int AWD2 : 1;
			unsigned int AWD3 : 1;
			unsigned int JQOVF : 1;
		} __attribute((__packed__));
	};
};
#define ADC1_ISR (*((volatile struct ADC1_ISR_s*) 0x50000000))

struct ADC1_IER_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int ADRDYIE : 1;
			unsigned int EOSMPIE : 1;
			unsigned int EOCIE : 1;
			unsigned int EOSIE : 1;
			unsigned int OVRIE : 1;
			unsigned int JEOCIE : 1;
			unsigned int JEOSIE : 1;
			unsigned int AWD1IE : 1;
			unsigned int AWD2IE : 1;
			unsigned int AWD3IE : 1;
			unsigned int JQOVFIE : 1;
		} __attribute((__packed__));
	};
};
#define ADC1_IER (*((volatile struct ADC1_IER_s*) 0x50000004))

struct ADC1_CR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int ADEN : 1;
			unsigned int ADDIS : 1;
			unsigned int ADSTART : 1;
			unsigned int JADSTART : 1;
			unsigned int ADSTP : 1;
			unsigned int JADSTP : 1;
			unsigned int : 22;
			unsigned int ADVREGEN : 1;
			unsigned int DEEPPWD : 1;
			unsigned int ADCALDIF : 1;
			unsigned int ADCAL : 1;
		} __attribute((__packed__));
	};
};
#define ADC1_CR (*((volatile struct ADC1_CR_s*) 0x50000008))

struct ADC1_CFGR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int DMAEN : 1;
			unsigned int DMACFG : 1;
			unsigned int : 1;
			unsigned int RES : 2;
			unsigned int ALIGN : 1;
			unsigned int EXTSEL : 4;
			unsigned int EXTEN : 2;
			unsigned int OVRMOD : 1;
			unsigned int CONT : 1;
			unsigned int AUTDLY : 1;
			unsigned int AUTOFF : 1;
			unsigned int DISCEN : 1;
			unsigned int DISCNUM : 3;
			unsigned int JDISCEN : 1;
			unsigned int JQM : 1;
			unsigned int AWD1SGL : 1;
			unsigned int AWD1EN : 1;
			unsigned int JAWD1EN : 1;
			unsigned int JAUTO : 1;
			unsigned int AWDCH1CH : 5;
		} __attribute((__packed__));
	};
};
#define ADC1_CFGR (*((volatile struct ADC1_CFGR_s*) 0x5000000c))

struct ADC1_SMPR1_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int : 3;
			unsigned int SMP1 : 3;
			unsigned int SMP2 : 3;
			unsigned int SMP3 : 3;
			unsigned int SMP4 : 3;
			unsigned int SMP5 : 3;
			unsigned int SMP6 : 3;
			unsigned int SMP7 : 3;
			unsigned int SMP8 : 3;
			unsigned int SMP9 : 3;
		} __attribute((__packed__));
	};
};
#define ADC1_SMPR1 (*((volatile struct ADC1_SMPR1_s*) 0x50000014))

struct ADC1_SMPR2_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int SMP10 : 3;
			unsigned int SMP11 : 3;
			unsigned int SMP12 : 3;
			unsigned int SMP13 : 3;
			unsigned int SMP14 : 3;
			unsigned int SMP15 : 3;
			unsigned int SMP16 : 3;
			unsigned int SMP17 : 3;
			unsigned int SMP18 : 3;
		} __attribute((__packed__));
	};
};
#define ADC1_SMPR2 (*((volatile struct ADC1_SMPR2_s*) 0x50000018))

struct ADC1_TR1_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int LT1 : 12;
			unsigned int : 4;
			unsigned int HT1 : 12;
		} __attribute((__packed__));
	};
};
#define ADC1_TR1 (*((volatile struct ADC1_TR1_s*) 0x50000020))

struct ADC1_TR2_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int LT2 : 8;
			unsigned int : 8;
			unsigned int HT2 : 8;
		} __attribute((__packed__));
	};
};
#define ADC1_TR2 (*((volatile struct ADC1_TR2_s*) 0x50000024))

struct ADC1_TR3_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int LT3 : 8;
			unsigned int : 8;
			unsigned int HT3 : 8;
		} __attribute((__packed__));
	};
};
#define ADC1_TR3 (*((volatile struct ADC1_TR3_s*) 0x50000028))

struct ADC1_SQR1_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int L3 : 4;
			unsigned int : 2;
			unsigned int SQ1 : 5;
			unsigned int : 1;
			unsigned int SQ2 : 5;
			unsigned int : 1;
			unsigned int SQ3 : 5;
			unsigned int : 1;
			unsigned int SQ4 : 5;
		} __attribute((__packed__));
	};
};
#define ADC1_SQR1 (*((volatile struct ADC1_SQR1_s*) 0x50000030))

struct ADC1_SQR2_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int SQ5 : 5;
			unsigned int : 1;
			unsigned int SQ6 : 5;
			unsigned int : 1;
			unsigned int SQ7 : 5;
			unsigned int : 1;
			unsigned int SQ8 : 5;
			unsigned int : 1;
			unsigned int SQ9 : 5;
		} __attribute((__packed__));
	};
};
#define ADC1_SQR2 (*((volatile struct ADC1_SQR2_s*) 0x50000034))

struct ADC1_SQR3_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int SQ10 : 5;
			unsigned int : 1;
			unsigned int SQ11 : 5;
			unsigned int : 1;
			unsigned int SQ12 : 5;
			unsigned int : 1;
			unsigned int SQ13 : 5;
			unsigned int : 1;
			unsigned int SQ14 : 5;
		} __attribute((__packed__));
	};
};
#define ADC1_SQR3 (*((volatile struct ADC1_SQR3_s*) 0x50000038))

struct ADC1_SQR4_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int SQ15 : 5;
			unsigned int : 1;
			unsigned int SQ16 : 5;
		} __attribute((__packed__));
	};
};
#define ADC1_SQR4 (*((volatile struct ADC1_SQR4_s*) 0x5000003c))

struct ADC1_DR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int regularDATA : 16;
		} __attribute((__packed__));
	};
};
#define ADC1_DR (*((volatile struct ADC1_DR_s*) 0x50000040))

struct ADC1_JSQR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int JL : 2;
			unsigned int JEXTSEL : 4;
			unsigned int JEXTEN : 2;
			unsigned int JSQ1 : 5;
			unsigned int : 1;
			unsigned int JSQ2 : 5;
			unsigned int : 1;
			unsigned int JSQ3 : 5;
			unsigned int : 1;
			unsigned int JSQ4 : 5;
		} __attribute((__packed__));
	};
};
#define ADC1_JSQR (*((volatile struct ADC1_JSQR_s*) 0x5000004c))

struct ADC1_OFR1_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int OFFSET1 : 12;
			unsigned int : 14;
			unsigned int OFFSET1_CH : 5;
			unsigned int OFFSET1_EN : 1;
		} __attribute((__packed__));
	};
};
#define ADC1_OFR1 (*((volatile struct ADC1_OFR1_s*) 0x50000060))

struct ADC1_OFR2_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int OFFSET2 : 12;
			unsigned int : 14;
			unsigned int OFFSET2_CH : 5;
			unsigned int OFFSET2_EN : 1;
		} __attribute((__packed__));
	};
};
#define ADC1_OFR2 (*((volatile struct ADC1_OFR2_s*) 0x50000064))

struct ADC1_OFR3_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int OFFSET3 : 12;
			unsigned int : 14;
			unsigned int OFFSET3_CH : 5;
			unsigned int OFFSET3_EN : 1;
		} __attribute((__packed__));
	};
};
#define ADC1_OFR3 (*((volatile struct ADC1_OFR3_s*) 0x50000068))

struct ADC1_OFR4_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int OFFSET4 : 12;
			unsigned int : 14;
			unsigned int OFFSET4_CH : 5;
			unsigned int OFFSET4_EN : 1;
		} __attribute((__packed__));
	};
};
#define ADC1_OFR4 (*((volatile struct ADC1_OFR4_s*) 0x5000006c))

struct ADC1_JDR1_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int JDATA1 : 16;
		} __attribute((__packed__));
	};
};
#define ADC1_JDR1 (*((volatile struct ADC1_JDR1_s*) 0x50000080))

struct ADC1_JDR2_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int JDATA2 : 16;
		} __attribute((__packed__));
	};
};
#define ADC1_JDR2 (*((volatile struct ADC1_JDR2_s*) 0x50000084))

struct ADC1_JDR3_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int JDATA3 : 16;
		} __attribute((__packed__));
	};
};
#define ADC1_JDR3 (*((volatile struct ADC1_JDR3_s*) 0x50000088))

struct ADC1_JDR4_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int JDATA4 : 16;
		} __attribute((__packed__));
	};
};
#define ADC1_JDR4 (*((volatile struct ADC1_JDR4_s*) 0x5000008c))

struct ADC1_AWD2CR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int : 1;
			unsigned int AWD2CH : 18;
		} __attribute((__packed__));
	};
};
#define ADC1_AWD2CR (*((volatile struct ADC1_AWD2CR_s*) 0x500000a0))

struct ADC1_AWD3CR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int : 1;
			unsigned int AWD3CH : 18;
		} __attribute((__packed__));
	};
};
#define ADC1_AWD3CR (*((volatile struct ADC1_AWD3CR_s*) 0x500000a4))

struct ADC1_DIFSEL_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int : 1;
			unsigned int DIFSEL_1_15 : 15;
			unsigned int DIFSEL_16_18 : 3;
		} __attribute((__packed__));
	};
};
#define ADC1_DIFSEL (*((volatile struct ADC1_DIFSEL_s*) 0x500000b0))

struct ADC1_CALFACT_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int CALFACT_S : 7;
			unsigned int : 9;
			unsigned int CALFACT_D : 7;
		} __attribute((__packed__));
	};
};
#define ADC1_CALFACT (*((volatile struct ADC1_CALFACT_s*) 0x500000b4))

// ADC2
struct ADC2_ISR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int ADRDY : 1;
			unsigned int EOSMP : 1;
			unsigned int EOC : 1;
			unsigned int EOS : 1;
			unsigned int OVR : 1;
			unsigned int JEOC : 1;
			unsigned int JEOS : 1;
			unsigned int AWD1 : 1;
			unsigned int AWD2 : 1;
			unsigned int AWD3 : 1;
			unsigned int JQOVF : 1;
		} __attribute((__packed__));
	};
};
#define ADC2_ISR (*((volatile struct ADC2_ISR_s*) 0x50000100))

struct ADC2_IER_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int ADRDYIE : 1;
			unsigned int EOSMPIE : 1;
			unsigned int EOCIE : 1;
			unsigned int EOSIE : 1;
			unsigned int OVRIE : 1;
			unsigned int JEOCIE : 1;
			unsigned int JEOSIE : 1;
			unsigned int AWD1IE : 1;
			unsigned int AWD2IE : 1;
			unsigned int AWD3IE : 1;
			unsigned int JQOVFIE : 1;
		} __attribute((__packed__));
	};
};
#define ADC2_IER (*((volatile struct ADC2_IER_s*) 0x50000104))

struct ADC2_CR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int ADEN : 1;
			unsigned int ADDIS : 1;
			unsigned int ADSTART : 1;
			unsigned int JADSTART : 1;
			unsigned int ADSTP : 1;
			unsigned int JADSTP : 1;
			unsigned int : 22;
			unsigned int ADVREGEN : 1;
			unsigned int DEEPPWD : 1;
			unsigned int ADCALDIF : 1;
			unsigned int ADCAL : 1;
		} __attribute((__packed__));
	};
};
#define ADC2_CR (*((volatile struct ADC2_CR_s*) 0x50000108))

struct ADC2_CFGR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int DMAEN : 1;
			unsigned int DMACFG : 1;
			unsigned int : 1;
			unsigned int RES : 2;
			unsigned int ALIGN : 1;
			unsigned int EXTSEL : 4;
			unsigned int EXTEN : 2;
			unsigned int OVRMOD : 1;
			unsigned int CONT : 1;
			unsigned int AUTDLY : 1;
			unsigned int AUTOFF : 1;
			unsigned int DISCEN : 1;
			unsigned int DISCNUM : 3;
			unsigned int JDISCEN : 1;
			unsigned int JQM : 1;
			unsigned int AWD1SGL : 1;
			unsigned int AWD1EN : 1;
			unsigned int JAWD1EN : 1;
			unsigned int JAUTO : 1;
			unsigned int AWDCH1CH : 5;
		} __attribute((__packed__));
	};
};
#define ADC2_CFGR (*((volatile struct ADC2_CFGR_s*) 0x5000010c))

struct ADC2_SMPR1_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int : 3;
			unsigned int SMP1 : 3;
			unsigned int SMP2 : 3;
			unsigned int SMP3 : 3;
			unsigned int SMP4 : 3;
			unsigned int SMP5 : 3;
			unsigned int SMP6 : 3;
			unsigned int SMP7 : 3;
			unsigned int SMP8 : 3;
			unsigned int SMP9 : 3;
		} __attribute((__packed__));
	};
};
#define ADC2_SMPR1 (*((volatile struct ADC2_SMPR1_s*) 0x50000114))

struct ADC2_SMPR2_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int SMP10 : 3;
			unsigned int SMP11 : 3;
			unsigned int SMP12 : 3;
			unsigned int SMP13 : 3;
			unsigned int SMP14 : 3;
			unsigned int SMP15 : 3;
			unsigned int SMP16 : 3;
			unsigned int SMP17 : 3;
			unsigned int SMP18 : 3;
		} __attribute((__packed__));
	};
};
#define ADC2_SMPR2 (*((volatile struct ADC2_SMPR2_s*) 0x50000118))

struct ADC2_TR1_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int LT1 : 12;
			unsigned int : 4;
			unsigned int HT1 : 12;
		} __attribute((__packed__));
	};
};
#define ADC2_TR1 (*((volatile struct ADC2_TR1_s*) 0x50000120))

struct ADC2_TR2_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int LT2 : 8;
			unsigned int : 8;
			unsigned int HT2 : 8;
		} __attribute((__packed__));
	};
};
#define ADC2_TR2 (*((volatile struct ADC2_TR2_s*) 0x50000124))

struct ADC2_TR3_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int LT3 : 8;
			unsigned int : 8;
			unsigned int HT3 : 8;
		} __attribute((__packed__));
	};
};
#define ADC2_TR3 (*((volatile struct ADC2_TR3_s*) 0x50000128))

struct ADC2_SQR1_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int L3 : 4;
			unsigned int : 2;
			unsigned int SQ1 : 5;
			unsigned int : 1;
			unsigned int SQ2 : 5;
			unsigned int : 1;
			unsigned int SQ3 : 5;
			unsigned int : 1;
			unsigned int SQ4 : 5;
		} __attribute((__packed__));
	};
};
#define ADC2_SQR1 (*((volatile struct ADC2_SQR1_s*) 0x50000130))

struct ADC2_SQR2_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int SQ5 : 5;
			unsigned int : 1;
			unsigned int SQ6 : 5;
			unsigned int : 1;
			unsigned int SQ7 : 5;
			unsigned int : 1;
			unsigned int SQ8 : 5;
			unsigned int : 1;
			unsigned int SQ9 : 5;
		} __attribute((__packed__));
	};
};
#define ADC2_SQR2 (*((volatile struct ADC2_SQR2_s*) 0x50000134))

struct ADC2_SQR3_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int SQ10 : 5;
			unsigned int : 1;
			unsigned int SQ11 : 5;
			unsigned int : 1;
			unsigned int SQ12 : 5;
			unsigned int : 1;
			unsigned int SQ13 : 5;
			unsigned int : 1;
			unsigned int SQ14 : 5;
		} __attribute((__packed__));
	};
};
#define ADC2_SQR3 (*((volatile struct ADC2_SQR3_s*) 0x50000138))

struct ADC2_SQR4_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int SQ15 : 5;
			unsigned int : 1;
			unsigned int SQ16 : 5;
		} __attribute((__packed__));
	};
};
#define ADC2_SQR4 (*((volatile struct ADC2_SQR4_s*) 0x5000013c))

struct ADC2_DR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int regularDATA : 16;
		} __attribute((__packed__));
	};
};
#define ADC2_DR (*((volatile struct ADC2_DR_s*) 0x50000140))

struct ADC2_JSQR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int JL : 2;
			unsigned int JEXTSEL : 4;
			unsigned int JEXTEN : 2;
			unsigned int JSQ1 : 5;
			unsigned int : 1;
			unsigned int JSQ2 : 5;
			unsigned int : 1;
			unsigned int JSQ3 : 5;
			unsigned int : 1;
			unsigned int JSQ4 : 5;
		} __attribute((__packed__));
	};
};
#define ADC2_JSQR (*((volatile struct ADC2_JSQR_s*) 0x5000014c))

struct ADC2_OFR1_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int OFFSET1 : 12;
			unsigned int : 14;
			unsigned int OFFSET1_CH : 5;
			unsigned int OFFSET1_EN : 1;
		} __attribute((__packed__));
	};
};
#define ADC2_OFR1 (*((volatile struct ADC2_OFR1_s*) 0x50000160))

struct ADC2_OFR2_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int OFFSET2 : 12;
			unsigned int : 14;
			unsigned int OFFSET2_CH : 5;
			unsigned int OFFSET2_EN : 1;
		} __attribute((__packed__));
	};
};
#define ADC2_OFR2 (*((volatile struct ADC2_OFR2_s*) 0x50000164))

struct ADC2_OFR3_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int OFFSET3 : 12;
			unsigned int : 14;
			unsigned int OFFSET3_CH : 5;
			unsigned int OFFSET3_EN : 1;
		} __attribute((__packed__));
	};
};
#define ADC2_OFR3 (*((volatile struct ADC2_OFR3_s*) 0x50000168))

struct ADC2_OFR4_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int OFFSET4 : 12;
			unsigned int : 14;
			unsigned int OFFSET4_CH : 5;
			unsigned int OFFSET4_EN : 1;
		} __attribute((__packed__));
	};
};
#define ADC2_OFR4 (*((volatile struct ADC2_OFR4_s*) 0x5000016c))

struct ADC2_JDR1_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int JDATA1 : 16;
		} __attribute((__packed__));
	};
};
#define ADC2_JDR1 (*((volatile struct ADC2_JDR1_s*) 0x50000180))

struct ADC2_JDR2_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int JDATA2 : 16;
		} __attribute((__packed__));
	};
};
#define ADC2_JDR2 (*((volatile struct ADC2_JDR2_s*) 0x50000184))

struct ADC2_JDR3_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int JDATA3 : 16;
		} __attribute((__packed__));
	};
};
#define ADC2_JDR3 (*((volatile struct ADC2_JDR3_s*) 0x50000188))

struct ADC2_JDR4_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int JDATA4 : 16;
		} __attribute((__packed__));
	};
};
#define ADC2_JDR4 (*((volatile struct ADC2_JDR4_s*) 0x5000018c))

struct ADC2_AWD2CR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int : 1;
			unsigned int AWD2CH : 18;
		} __attribute((__packed__));
	};
};
#define ADC2_AWD2CR (*((volatile struct ADC2_AWD2CR_s*) 0x500001a0))

struct ADC2_AWD3CR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int : 1;
			unsigned int AWD3CH : 18;
		} __attribute((__packed__));
	};
};
#define ADC2_AWD3CR (*((volatile struct ADC2_AWD3CR_s*) 0x500001a4))

struct ADC2_DIFSEL_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int : 1;
			unsigned int DIFSEL_1_15 : 15;
			unsigned int DIFSEL_16_18 : 3;
		} __attribute((__packed__));
	};
};
#define ADC2_DIFSEL (*((volatile struct ADC2_DIFSEL_s*) 0x500001b0))

struct ADC2_CALFACT_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int CALFACT_S : 7;
			unsigned int : 9;
			unsigned int CALFACT_D : 7;
		} __attribute((__packed__));
	};
};
#define ADC2_CALFACT (*((volatile struct ADC2_CALFACT_s*) 0x500001b4))

// ADC3
struct ADC3_ISR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int ADRDY : 1;
			unsigned int EOSMP : 1;
			unsigned int EOC : 1;
			unsigned int EOS : 1;
			unsigned int OVR : 1;
			unsigned int JEOC : 1;
			unsigned int JEOS : 1;
			unsigned int AWD1 : 1;
			unsigned int AWD2 : 1;
			unsigned int AWD3 : 1;
			unsigned int JQOVF : 1;
		} __attribute((__packed__));
	};
};
#define ADC3_ISR (*((volatile struct ADC3_ISR_s*) 0x50000400))

struct ADC3_IER_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int ADRDYIE : 1;
			unsigned int EOSMPIE : 1;
			unsigned int EOCIE : 1;
			unsigned int EOSIE : 1;
			unsigned int OVRIE : 1;
			unsigned int JEOCIE : 1;
			unsigned int JEOSIE : 1;
			unsigned int AWD1IE : 1;
			unsigned int AWD2IE : 1;
			unsigned int AWD3IE : 1;
			unsigned int JQOVFIE : 1;
		} __attribute((__packed__));
	};
};
#define ADC3_IER (*((volatile struct ADC3_IER_s*) 0x50000404))

struct ADC3_CR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int ADEN : 1;
			unsigned int ADDIS : 1;
			unsigned int ADSTART : 1;
			unsigned int JADSTART : 1;
			unsigned int ADSTP : 1;
			unsigned int JADSTP : 1;
			unsigned int : 22;
			unsigned int ADVREGEN : 1;
			unsigned int DEEPPWD : 1;
			unsigned int ADCALDIF : 1;
			unsigned int ADCAL : 1;
		} __attribute((__packed__));
	};
};
#define ADC3_CR (*((volatile struct ADC3_CR_s*) 0x50000408))

struct ADC3_CFGR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int DMAEN : 1;
			unsigned int DMACFG : 1;
			unsigned int : 1;
			unsigned int RES : 2;
			unsigned int ALIGN : 1;
			unsigned int EXTSEL : 4;
			unsigned int EXTEN : 2;
			unsigned int OVRMOD : 1;
			unsigned int CONT : 1;
			unsigned int AUTDLY : 1;
			unsigned int AUTOFF : 1;
			unsigned int DISCEN : 1;
			unsigned int DISCNUM : 3;
			unsigned int JDISCEN : 1;
			unsigned int JQM : 1;
			unsigned int AWD1SGL : 1;
			unsigned int AWD1EN : 1;
			unsigned int JAWD1EN : 1;
			unsigned int JAUTO : 1;
			unsigned int AWDCH1CH : 5;
		} __attribute((__packed__));
	};
};
#define ADC3_CFGR (*((volatile struct ADC3_CFGR_s*) 0x5000040c))

struct ADC3_SMPR1_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int : 3;
			unsigned int SMP1 : 3;
			unsigned int SMP2 : 3;
			unsigned int SMP3 : 3;
			unsigned int SMP4 : 3;
			unsigned int SMP5 : 3;
			unsigned int SMP6 : 3;
			unsigned int SMP7 : 3;
			unsigned int SMP8 : 3;
			unsigned int SMP9 : 3;
		} __attribute((__packed__));
	};
};
#define ADC3_SMPR1 (*((volatile struct ADC3_SMPR1_s*) 0x50000414))

struct ADC3_SMPR2_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int SMP10 : 3;
			unsigned int SMP11 : 3;
			unsigned int SMP12 : 3;
			unsigned int SMP13 : 3;
			unsigned int SMP14 : 3;
			unsigned int SMP15 : 3;
			unsigned int SMP16 : 3;
			unsigned int SMP17 : 3;
			unsigned int SMP18 : 3;
		} __attribute((__packed__));
	};
};
#define ADC3_SMPR2 (*((volatile struct ADC3_SMPR2_s*) 0x50000418))

struct ADC3_TR1_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int LT1 : 12;
			unsigned int : 4;
			unsigned int HT1 : 12;
		} __attribute((__packed__));
	};
};
#define ADC3_TR1 (*((volatile struct ADC3_TR1_s*) 0x50000420))

struct ADC3_TR2_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int LT2 : 8;
			unsigned int : 8;
			unsigned int HT2 : 8;
		} __attribute((__packed__));
	};
};
#define ADC3_TR2 (*((volatile struct ADC3_TR2_s*) 0x50000424))

struct ADC3_TR3_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int LT3 : 8;
			unsigned int : 8;
			unsigned int HT3 : 8;
		} __attribute((__packed__));
	};
};
#define ADC3_TR3 (*((volatile struct ADC3_TR3_s*) 0x50000428))

struct ADC3_SQR1_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int L3 : 4;
			unsigned int : 2;
			unsigned int SQ1 : 5;
			unsigned int : 1;
			unsigned int SQ2 : 5;
			unsigned int : 1;
			unsigned int SQ3 : 5;
			unsigned int : 1;
			unsigned int SQ4 : 5;
		} __attribute((__packed__));
	};
};
#define ADC3_SQR1 (*((volatile struct ADC3_SQR1_s*) 0x50000430))

struct ADC3_SQR2_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int SQ5 : 5;
			unsigned int : 1;
			unsigned int SQ6 : 5;
			unsigned int : 1;
			unsigned int SQ7 : 5;
			unsigned int : 1;
			unsigned int SQ8 : 5;
			unsigned int : 1;
			unsigned int SQ9 : 5;
		} __attribute((__packed__));
	};
};
#define ADC3_SQR2 (*((volatile struct ADC3_SQR2_s*) 0x50000434))

struct ADC3_SQR3_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int SQ10 : 5;
			unsigned int : 1;
			unsigned int SQ11 : 5;
			unsigned int : 1;
			unsigned int SQ12 : 5;
			unsigned int : 1;
			unsigned int SQ13 : 5;
			unsigned int : 1;
			unsigned int SQ14 : 5;
		} __attribute((__packed__));
	};
};
#define ADC3_SQR3 (*((volatile struct ADC3_SQR3_s*) 0x50000438))

struct ADC3_SQR4_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int SQ15 : 5;
			unsigned int : 1;
			unsigned int SQ16 : 5;
		} __attribute((__packed__));
	};
};
#define ADC3_SQR4 (*((volatile struct ADC3_SQR4_s*) 0x5000043c))

struct ADC3_DR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int regularDATA : 16;
		} __attribute((__packed__));
	};
};
#define ADC3_DR (*((volatile struct ADC3_DR_s*) 0x50000440))

struct ADC3_JSQR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int JL : 2;
			unsigned int JEXTSEL : 4;
			unsigned int JEXTEN : 2;
			unsigned int JSQ1 : 5;
			unsigned int : 1;
			unsigned int JSQ2 : 5;
			unsigned int : 1;
			unsigned int JSQ3 : 5;
			unsigned int : 1;
			unsigned int JSQ4 : 5;
		} __attribute((__packed__));
	};
};
#define ADC3_JSQR (*((volatile struct ADC3_JSQR_s*) 0x5000044c))

struct ADC3_OFR1_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int OFFSET1 : 12;
			unsigned int : 14;
			unsigned int OFFSET1_CH : 5;
			unsigned int OFFSET1_EN : 1;
		} __attribute((__packed__));
	};
};
#define ADC3_OFR1 (*((volatile struct ADC3_OFR1_s*) 0x50000460))

struct ADC3_OFR2_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int OFFSET2 : 12;
			unsigned int : 14;
			unsigned int OFFSET2_CH : 5;
			unsigned int OFFSET2_EN : 1;
		} __attribute((__packed__));
	};
};
#define ADC3_OFR2 (*((volatile struct ADC3_OFR2_s*) 0x50000464))

struct ADC3_OFR3_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int OFFSET3 : 12;
			unsigned int : 14;
			unsigned int OFFSET3_CH : 5;
			unsigned int OFFSET3_EN : 1;
		} __attribute((__packed__));
	};
};
#define ADC3_OFR3 (*((volatile struct ADC3_OFR3_s*) 0x50000468))

struct ADC3_OFR4_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int OFFSET4 : 12;
			unsigned int : 14;
			unsigned int OFFSET4_CH : 5;
			unsigned int OFFSET4_EN : 1;
		} __attribute((__packed__));
	};
};
#define ADC3_OFR4 (*((volatile struct ADC3_OFR4_s*) 0x5000046c))

struct ADC3_JDR1_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int JDATA1 : 16;
		} __attribute((__packed__));
	};
};
#define ADC3_JDR1 (*((volatile struct ADC3_JDR1_s*) 0x50000480))

struct ADC3_JDR2_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int JDATA2 : 16;
		} __attribute((__packed__));
	};
};
#define ADC3_JDR2 (*((volatile struct ADC3_JDR2_s*) 0x50000484))

struct ADC3_JDR3_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int JDATA3 : 16;
		} __attribute((__packed__));
	};
};
#define ADC3_JDR3 (*((volatile struct ADC3_JDR3_s*) 0x50000488))

struct ADC3_JDR4_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int JDATA4 : 16;
		} __attribute((__packed__));
	};
};
#define ADC3_JDR4 (*((volatile struct ADC3_JDR4_s*) 0x5000048c))

struct ADC3_AWD2CR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int : 1;
			unsigned int AWD2CH : 18;
		} __attribute((__packed__));
	};
};
#define ADC3_AWD2CR (*((volatile struct ADC3_AWD2CR_s*) 0x500004a0))

struct ADC3_AWD3CR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int : 1;
			unsigned int AWD3CH : 18;
		} __attribute((__packed__));
	};
};
#define ADC3_AWD3CR (*((volatile struct ADC3_AWD3CR_s*) 0x500004a4))

struct ADC3_DIFSEL_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int : 1;
			unsigned int DIFSEL_1_15 : 15;
			unsigned int DIFSEL_16_18 : 3;
		} __attribute((__packed__));
	};
};
#define ADC3_DIFSEL (*((volatile struct ADC3_DIFSEL_s*) 0x500004b0))

struct ADC3_CALFACT_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int CALFACT_S : 7;
			unsigned int : 9;
			unsigned int CALFACT_D : 7;
		} __attribute((__packed__));
	};
};
#define ADC3_CALFACT (*((volatile struct ADC3_CALFACT_s*) 0x500004b4))

// ADC4
struct ADC4_ISR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int ADRDY : 1;
			unsigned int EOSMP : 1;
			unsigned int EOC : 1;
			unsigned int EOS : 1;
			unsigned int OVR : 1;
			unsigned int JEOC : 1;
			unsigned int JEOS : 1;
			unsigned int AWD1 : 1;
			unsigned int AWD2 : 1;
			unsigned int AWD3 : 1;
			unsigned int JQOVF : 1;
		} __attribute((__packed__));
	};
};
#define ADC4_ISR (*((volatile struct ADC4_ISR_s*) 0x50000500))

struct ADC4_IER_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int ADRDYIE : 1;
			unsigned int EOSMPIE : 1;
			unsigned int EOCIE : 1;
			unsigned int EOSIE : 1;
			unsigned int OVRIE : 1;
			unsigned int JEOCIE : 1;
			unsigned int JEOSIE : 1;
			unsigned int AWD1IE : 1;
			unsigned int AWD2IE : 1;
			unsigned int AWD3IE : 1;
			unsigned int JQOVFIE : 1;
		} __attribute((__packed__));
	};
};
#define ADC4_IER (*((volatile struct ADC4_IER_s*) 0x50000504))

struct ADC4_CR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int ADEN : 1;
			unsigned int ADDIS : 1;
			unsigned int ADSTART : 1;
			unsigned int JADSTART : 1;
			unsigned int ADSTP : 1;
			unsigned int JADSTP : 1;
			unsigned int : 22;
			unsigned int ADVREGEN : 1;
			unsigned int DEEPPWD : 1;
			unsigned int ADCALDIF : 1;
			unsigned int ADCAL : 1;
		} __attribute((__packed__));
	};
};
#define ADC4_CR (*((volatile struct ADC4_CR_s*) 0x50000508))

struct ADC4_CFGR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int DMAEN : 1;
			unsigned int DMACFG : 1;
			unsigned int : 1;
			unsigned int RES : 2;
			unsigned int ALIGN : 1;
			unsigned int EXTSEL : 4;
			unsigned int EXTEN : 2;
			unsigned int OVRMOD : 1;
			unsigned int CONT : 1;
			unsigned int AUTDLY : 1;
			unsigned int AUTOFF : 1;
			unsigned int DISCEN : 1;
			unsigned int DISCNUM : 3;
			unsigned int JDISCEN : 1;
			unsigned int JQM : 1;
			unsigned int AWD1SGL : 1;
			unsigned int AWD1EN : 1;
			unsigned int JAWD1EN : 1;
			unsigned int JAUTO : 1;
			unsigned int AWDCH1CH : 5;
		} __attribute((__packed__));
	};
};
#define ADC4_CFGR (*((volatile struct ADC4_CFGR_s*) 0x5000050c))

struct ADC4_SMPR1_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int : 3;
			unsigned int SMP1 : 3;
			unsigned int SMP2 : 3;
			unsigned int SMP3 : 3;
			unsigned int SMP4 : 3;
			unsigned int SMP5 : 3;
			unsigned int SMP6 : 3;
			unsigned int SMP7 : 3;
			unsigned int SMP8 : 3;
			unsigned int SMP9 : 3;
		} __attribute((__packed__));
	};
};
#define ADC4_SMPR1 (*((volatile struct ADC4_SMPR1_s*) 0x50000514))

struct ADC4_SMPR2_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int SMP10 : 3;
			unsigned int SMP11 : 3;
			unsigned int SMP12 : 3;
			unsigned int SMP13 : 3;
			unsigned int SMP14 : 3;
			unsigned int SMP15 : 3;
			unsigned int SMP16 : 3;
			unsigned int SMP17 : 3;
			unsigned int SMP18 : 3;
		} __attribute((__packed__));
	};
};
#define ADC4_SMPR2 (*((volatile struct ADC4_SMPR2_s*) 0x50000518))

struct ADC4_TR1_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int LT1 : 12;
			unsigned int : 4;
			unsigned int HT1 : 12;
		} __attribute((__packed__));
	};
};
#define ADC4_TR1 (*((volatile struct ADC4_TR1_s*) 0x50000520))

struct ADC4_TR2_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int LT2 : 8;
			unsigned int : 8;
			unsigned int HT2 : 8;
		} __attribute((__packed__));
	};
};
#define ADC4_TR2 (*((volatile struct ADC4_TR2_s*) 0x50000524))

struct ADC4_TR3_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int LT3 : 8;
			unsigned int : 8;
			unsigned int HT3 : 8;
		} __attribute((__packed__));
	};
};
#define ADC4_TR3 (*((volatile struct ADC4_TR3_s*) 0x50000528))

struct ADC4_SQR1_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int L3 : 4;
			unsigned int : 2;
			unsigned int SQ1 : 5;
			unsigned int : 1;
			unsigned int SQ2 : 5;
			unsigned int : 1;
			unsigned int SQ3 : 5;
			unsigned int : 1;
			unsigned int SQ4 : 5;
		} __attribute((__packed__));
	};
};
#define ADC4_SQR1 (*((volatile struct ADC4_SQR1_s*) 0x50000530))

struct ADC4_SQR2_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int SQ5 : 5;
			unsigned int : 1;
			unsigned int SQ6 : 5;
			unsigned int : 1;
			unsigned int SQ7 : 5;
			unsigned int : 1;
			unsigned int SQ8 : 5;
			unsigned int : 1;
			unsigned int SQ9 : 5;
		} __attribute((__packed__));
	};
};
#define ADC4_SQR2 (*((volatile struct ADC4_SQR2_s*) 0x50000534))

struct ADC4_SQR3_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int SQ10 : 5;
			unsigned int : 1;
			unsigned int SQ11 : 5;
			unsigned int : 1;
			unsigned int SQ12 : 5;
			unsigned int : 1;
			unsigned int SQ13 : 5;
			unsigned int : 1;
			unsigned int SQ14 : 5;
		} __attribute((__packed__));
	};
};
#define ADC4_SQR3 (*((volatile struct ADC4_SQR3_s*) 0x50000538))

struct ADC4_SQR4_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int SQ15 : 5;
			unsigned int : 1;
			unsigned int SQ16 : 5;
		} __attribute((__packed__));
	};
};
#define ADC4_SQR4 (*((volatile struct ADC4_SQR4_s*) 0x5000053c))

struct ADC4_DR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int regularDATA : 16;
		} __attribute((__packed__));
	};
};
#define ADC4_DR (*((volatile struct ADC4_DR_s*) 0x50000540))

struct ADC4_JSQR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int JL : 2;
			unsigned int JEXTSEL : 4;
			unsigned int JEXTEN : 2;
			unsigned int JSQ1 : 5;
			unsigned int : 1;
			unsigned int JSQ2 : 5;
			unsigned int : 1;
			unsigned int JSQ3 : 5;
			unsigned int : 1;
			unsigned int JSQ4 : 5;
		} __attribute((__packed__));
	};
};
#define ADC4_JSQR (*((volatile struct ADC4_JSQR_s*) 0x5000054c))

struct ADC4_OFR1_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int OFFSET1 : 12;
			unsigned int : 14;
			unsigned int OFFSET1_CH : 5;
			unsigned int OFFSET1_EN : 1;
		} __attribute((__packed__));
	};
};
#define ADC4_OFR1 (*((volatile struct ADC4_OFR1_s*) 0x50000560))

struct ADC4_OFR2_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int OFFSET2 : 12;
			unsigned int : 14;
			unsigned int OFFSET2_CH : 5;
			unsigned int OFFSET2_EN : 1;
		} __attribute((__packed__));
	};
};
#define ADC4_OFR2 (*((volatile struct ADC4_OFR2_s*) 0x50000564))

struct ADC4_OFR3_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int OFFSET3 : 12;
			unsigned int : 14;
			unsigned int OFFSET3_CH : 5;
			unsigned int OFFSET3_EN : 1;
		} __attribute((__packed__));
	};
};
#define ADC4_OFR3 (*((volatile struct ADC4_OFR3_s*) 0x50000568))

struct ADC4_OFR4_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int OFFSET4 : 12;
			unsigned int : 14;
			unsigned int OFFSET4_CH : 5;
			unsigned int OFFSET4_EN : 1;
		} __attribute((__packed__));
	};
};
#define ADC4_OFR4 (*((volatile struct ADC4_OFR4_s*) 0x5000056c))

struct ADC4_JDR1_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int JDATA1 : 16;
		} __attribute((__packed__));
	};
};
#define ADC4_JDR1 (*((volatile struct ADC4_JDR1_s*) 0x50000580))

struct ADC4_JDR2_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int JDATA2 : 16;
		} __attribute((__packed__));
	};
};
#define ADC4_JDR2 (*((volatile struct ADC4_JDR2_s*) 0x50000584))

struct ADC4_JDR3_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int JDATA3 : 16;
		} __attribute((__packed__));
	};
};
#define ADC4_JDR3 (*((volatile struct ADC4_JDR3_s*) 0x50000588))

struct ADC4_JDR4_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int JDATA4 : 16;
		} __attribute((__packed__));
	};
};
#define ADC4_JDR4 (*((volatile struct ADC4_JDR4_s*) 0x5000058c))

struct ADC4_AWD2CR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int : 1;
			unsigned int AWD2CH : 18;
		} __attribute((__packed__));
	};
};
#define ADC4_AWD2CR (*((volatile struct ADC4_AWD2CR_s*) 0x500005a0))

struct ADC4_AWD3CR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int : 1;
			unsigned int AWD3CH : 18;
		} __attribute((__packed__));
	};
};
#define ADC4_AWD3CR (*((volatile struct ADC4_AWD3CR_s*) 0x500005a4))

struct ADC4_DIFSEL_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int : 1;
			unsigned int DIFSEL_1_15 : 15;
			unsigned int DIFSEL_16_18 : 3;
		} __attribute((__packed__));
	};
};
#define ADC4_DIFSEL (*((volatile struct ADC4_DIFSEL_s*) 0x500005b0))

struct ADC4_CALFACT_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int CALFACT_S : 7;
			unsigned int : 9;
			unsigned int CALFACT_D : 7;
		} __attribute((__packed__));
	};
};
#define ADC4_CALFACT (*((volatile struct ADC4_CALFACT_s*) 0x500005b4))

// ADC1_2
struct ADC1_2_CSR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int ADDRDY_MST : 1;
			unsigned int EOSMP_MST : 1;
			unsigned int EOC_MST : 1;
			unsigned int EOS_MST : 1;
			unsigned int OVR_MST : 1;
			unsigned int JEOC_MST : 1;
			unsigned int JEOS_MST : 1;
			unsigned int AWD1_MST : 1;
			unsigned int AWD2_MST : 1;
			unsigned int AWD3_MST : 1;
			unsigned int JQOVF_MST : 1;
			unsigned int : 5;
			unsigned int ADRDY_SLV : 1;
			unsigned int EOSMP_SLV : 1;
			unsigned int EOC_SLV : 1;
			unsigned int EOS_SLV : 1;
			unsigned int OVR_SLV : 1;
			unsigned int JEOC_SLV : 1;
			unsigned int JEOS_SLV : 1;
			unsigned int AWD1_SLV : 1;
			unsigned int AWD2_SLV : 1;
			unsigned int AWD3_SLV : 1;
			unsigned int JQOVF_SLV : 1;
		} __attribute((__packed__));
	};
};
#define ADC1_2_CSR (*((volatile struct ADC1_2_CSR_s*) 0x50000300))

struct ADC1_2_CCR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int MULT : 5;
			unsigned int : 3;
			unsigned int DELAY : 4;
			unsigned int : 1;
			unsigned int DMACFG : 1;
			unsigned int MDMA : 2;
			unsigned int CKMODE : 2;
			unsigned int : 4;
			unsigned int VREFEN : 1;
			unsigned int TSEN : 1;
			unsigned int VBATEN : 1;
		} __attribute((__packed__));
	};
};
#define ADC1_2_CCR (*((volatile struct ADC1_2_CCR_s*) 0x50000308))

struct ADC1_2_CDR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int RDATA_MST : 16;
			unsigned int RDATA_SLV : 16;
		} __attribute((__packed__));
	};
};
#define ADC1_2_CDR (*((volatile struct ADC1_2_CDR_s*) 0x5000030c))

// ADC3_4
struct ADC3_4_CSR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int ADDRDY_MST : 1;
			unsigned int EOSMP_MST : 1;
			unsigned int EOC_MST : 1;
			unsigned int EOS_MST : 1;
			unsigned int OVR_MST : 1;
			unsigned int JEOC_MST : 1;
			unsigned int JEOS_MST : 1;
			unsigned int AWD1_MST : 1;
			unsigned int AWD2_MST : 1;
			unsigned int AWD3_MST : 1;
			unsigned int JQOVF_MST : 1;
			unsigned int : 5;
			unsigned int ADRDY_SLV : 1;
			unsigned int EOSMP_SLV : 1;
			unsigned int EOC_SLV : 1;
			unsigned int EOS_SLV : 1;
			unsigned int OVR_SLV : 1;
			unsigned int JEOC_SLV : 1;
			unsigned int JEOS_SLV : 1;
			unsigned int AWD1_SLV : 1;
			unsigned int AWD2_SLV : 1;
			unsigned int AWD3_SLV : 1;
			unsigned int JQOVF_SLV : 1;
		} __attribute((__packed__));
	};
};
#define ADC3_4_CSR (*((volatile struct ADC3_4_CSR_s*) 0x50000700))

struct ADC3_4_CCR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int MULT : 5;
			unsigned int : 3;
			unsigned int DELAY : 4;
			unsigned int : 1;
			unsigned int DMACFG : 1;
			unsigned int MDMA : 2;
			unsigned int CKMODE : 2;
			unsigned int : 4;
			unsigned int VREFEN : 1;
			unsigned int TSEN : 1;
			unsigned int VBATEN : 1;
		} __attribute((__packed__));
	};
};
#define ADC3_4_CCR (*((volatile struct ADC3_4_CCR_s*) 0x50000708))

struct ADC3_4_CDR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int RDATA_MST : 16;
			unsigned int RDATA_SLV : 16;
		} __attribute((__packed__));
	};
};
#define ADC3_4_CDR (*((volatile struct ADC3_4_CDR_s*) 0x5000070c))

// SYSCFG_COMP_OPAMP
struct SYSCFG_COMP_OPAMP_SYSCFG_CFGR1_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int MEM_MODE : 2;
			unsigned int : 3;
			unsigned int USB_IT_RMP : 1;
			unsigned int TIM1_ITR_RMP : 1;
			unsigned int DAC_TRIG_RMP : 1;
			unsigned int ADC24_DMA_RMP : 1;
			unsigned int : 2;
			unsigned int TIM16_DMA_RMP : 1;
			unsigned int TIM17_DMA_RMP : 1;
			unsigned int TIM6_DAC1_DMA_RMP : 1;
			unsigned int TIM7_DAC2_DMA_RMP : 1;
			unsigned int : 1;
			unsigned int I2C_PB6_FM : 1;
			unsigned int I2C_PB7_FM : 1;
			unsigned int I2C_PB8_FM : 1;
			unsigned int I2C_PB9_FM : 1;
			unsigned int I2C1_FM : 1;
			unsigned int I2C2_FM : 1;
			unsigned int ENCODER_MODE : 2;
			unsigned int : 2;
			unsigned int FPU_IT : 6;
		} __attribute((__packed__));
	};
};
#define SYSCFG_COMP_OPAMP_SYSCFG_CFGR1 (*((volatile struct SYSCFG_COMP_OPAMP_SYSCFG_CFGR1_s*) 0x40010000))

struct SYSCFG_COMP_OPAMP_SYSCFG_EXTICR1_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int EXTI0 : 4;
			unsigned int EXTI1 : 4;
			unsigned int EXTI2 : 4;
			unsigned int EXTI3 : 4;
		} __attribute((__packed__));
	};
};
#define SYSCFG_COMP_OPAMP_SYSCFG_EXTICR1 (*((volatile struct SYSCFG_COMP_OPAMP_SYSCFG_EXTICR1_s*) 0x40010008))

struct SYSCFG_COMP_OPAMP_SYSCFG_EXTICR2_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int EXTI4 : 4;
			unsigned int EXTI5 : 4;
			unsigned int EXTI6 : 4;
			unsigned int EXTI7 : 4;
		} __attribute((__packed__));
	};
};
#define SYSCFG_COMP_OPAMP_SYSCFG_EXTICR2 (*((volatile struct SYSCFG_COMP_OPAMP_SYSCFG_EXTICR2_s*) 0x4001000c))

struct SYSCFG_COMP_OPAMP_SYSCFG_EXTICR3_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int EXTI8 : 4;
			unsigned int EXTI9 : 4;
			unsigned int EXTI10 : 4;
			unsigned int EXTI11 : 4;
		} __attribute((__packed__));
	};
};
#define SYSCFG_COMP_OPAMP_SYSCFG_EXTICR3 (*((volatile struct SYSCFG_COMP_OPAMP_SYSCFG_EXTICR3_s*) 0x40010010))

struct SYSCFG_COMP_OPAMP_SYSCFG_EXTICR4_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int EXTI12 : 4;
			unsigned int EXTI13 : 4;
			unsigned int EXTI14 : 4;
			unsigned int EXTI15 : 4;
		} __attribute((__packed__));
	};
};
#define SYSCFG_COMP_OPAMP_SYSCFG_EXTICR4 (*((volatile struct SYSCFG_COMP_OPAMP_SYSCFG_EXTICR4_s*) 0x40010014))

struct SYSCFG_COMP_OPAMP_SYSCFG_CFGR2_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int LOCUP_LOCK : 1;
			unsigned int SRAM_PARITY_LOCK : 1;
			unsigned int PVD_LOCK : 1;
			unsigned int : 1;
			unsigned int BYP_ADD_PAR : 1;
			unsigned int : 3;
			unsigned int SRAM_PEF : 1;
		} __attribute((__packed__));
	};
};
#define SYSCFG_COMP_OPAMP_SYSCFG_CFGR2 (*((volatile struct SYSCFG_COMP_OPAMP_SYSCFG_CFGR2_s*) 0x40010018))

struct SYSCFG_COMP_OPAMP_SYSCFG_RCR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int PAGE0_WP : 1;
			unsigned int PAGE1_WP : 1;
			unsigned int PAGE2_WP : 1;
			unsigned int PAGE3_WP : 1;
			unsigned int PAGE4_WP : 1;
			unsigned int PAGE5_WP : 1;
			unsigned int PAGE6_WP : 1;
			unsigned int PAGE7_WP : 1;
		} __attribute((__packed__));
	};
};
#define SYSCFG_COMP_OPAMP_SYSCFG_RCR (*((volatile struct SYSCFG_COMP_OPAMP_SYSCFG_RCR_s*) 0x40010004))

struct SYSCFG_COMP_OPAMP_COMP1_CSR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int COMP1EN : 1;
			unsigned int COMP1_INP_DAC : 1;
			unsigned int COMP1MODE : 2;
			unsigned int COMP1INSEL : 3;
			unsigned int : 3;
			unsigned int COMP1_OUT_SEL : 4;
			unsigned int : 1;
			unsigned int COMP1POL : 1;
			unsigned int COMP1HYST : 2;
			unsigned int COMP1_BLANKING : 3;
			unsigned int : 9;
			unsigned int COMP1OUT : 1;
			unsigned int COMP1LOCK : 1;
		} __attribute((__packed__));
	};
};
#define SYSCFG_COMP_OPAMP_COMP1_CSR (*((volatile struct SYSCFG_COMP_OPAMP_COMP1_CSR_s*) 0x4001001c))

struct SYSCFG_COMP_OPAMP_COMP2_CSR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int COMP2EN : 1;
			unsigned int : 1;
			unsigned int COMP2MODE : 2;
			unsigned int COMP2INSEL : 3;
			unsigned int COMP2INPSEL : 1;
			unsigned int : 1;
			unsigned int COMP2INMSEL : 1;
			unsigned int COMP2_OUT_SEL : 4;
			unsigned int : 1;
			unsigned int COMP2POL : 1;
			unsigned int COMP2HYST : 2;
			unsigned int COMP2_BLANKING : 3;
			unsigned int : 10;
			unsigned int COMP2LOCK : 1;
		} __attribute((__packed__));
	};
};
#define SYSCFG_COMP_OPAMP_COMP2_CSR (*((volatile struct SYSCFG_COMP_OPAMP_COMP2_CSR_s*) 0x40010020))

struct SYSCFG_COMP_OPAMP_COMP3_CSR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int COMP3EN : 1;
			unsigned int : 1;
			unsigned int COMP3MODE : 2;
			unsigned int COMP3INSEL : 3;
			unsigned int COMP3INPSEL : 1;
			unsigned int : 2;
			unsigned int COMP3_OUT_SEL : 4;
			unsigned int : 1;
			unsigned int COMP3POL : 1;
			unsigned int COMP3HYST : 2;
			unsigned int COMP3_BLANKING : 3;
			unsigned int : 9;
			unsigned int COMP3OUT : 1;
			unsigned int COMP3LOCK : 1;
		} __attribute((__packed__));
	};
};
#define SYSCFG_COMP_OPAMP_COMP3_CSR (*((volatile struct SYSCFG_COMP_OPAMP_COMP3_CSR_s*) 0x40010024))

struct SYSCFG_COMP_OPAMP_COMP4_CSR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int COMP4EN : 1;
			unsigned int : 1;
			unsigned int COMP4MODE : 2;
			unsigned int COMP4INSEL : 3;
			unsigned int COMP4INPSEL : 1;
			unsigned int : 1;
			unsigned int COM4WINMODE : 1;
			unsigned int COMP4_OUT_SEL : 4;
			unsigned int : 1;
			unsigned int COMP4POL : 1;
			unsigned int COMP4HYST : 2;
			unsigned int COMP4_BLANKING : 3;
			unsigned int : 9;
			unsigned int COMP4OUT : 1;
			unsigned int COMP4LOCK : 1;
		} __attribute((__packed__));
	};
};
#define SYSCFG_COMP_OPAMP_COMP4_CSR (*((volatile struct SYSCFG_COMP_OPAMP_COMP4_CSR_s*) 0x40010028))

struct SYSCFG_COMP_OPAMP_COMP5_CSR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int COMP5EN : 1;
			unsigned int : 1;
			unsigned int COMP5MODE : 2;
			unsigned int COMP5INSEL : 3;
			unsigned int COMP5INPSEL : 1;
			unsigned int : 2;
			unsigned int COMP5_OUT_SEL : 4;
			unsigned int : 1;
			unsigned int COMP5POL : 1;
			unsigned int COMP5HYST : 2;
			unsigned int COMP5_BLANKING : 3;
			unsigned int : 9;
			unsigned int COMP5OUT : 1;
			unsigned int COMP5LOCK : 1;
		} __attribute((__packed__));
	};
};
#define SYSCFG_COMP_OPAMP_COMP5_CSR (*((volatile struct SYSCFG_COMP_OPAMP_COMP5_CSR_s*) 0x4001002c))

struct SYSCFG_COMP_OPAMP_COMP6_CSR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int COMP6EN : 1;
			unsigned int : 1;
			unsigned int COMP6MODE : 2;
			unsigned int COMP6INSEL : 3;
			unsigned int COMP6INPSEL : 1;
			unsigned int : 1;
			unsigned int COM6WINMODE : 1;
			unsigned int COMP6_OUT_SEL : 4;
			unsigned int : 1;
			unsigned int COMP6POL : 1;
			unsigned int COMP6HYST : 2;
			unsigned int COMP6_BLANKING : 3;
			unsigned int : 9;
			unsigned int COMP6OUT : 1;
			unsigned int COMP6LOCK : 1;
		} __attribute((__packed__));
	};
};
#define SYSCFG_COMP_OPAMP_COMP6_CSR (*((volatile struct SYSCFG_COMP_OPAMP_COMP6_CSR_s*) 0x40010030))

struct SYSCFG_COMP_OPAMP_COMP7_CSR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int COMP7EN : 1;
			unsigned int : 1;
			unsigned int COMP7MODE : 2;
			unsigned int COMP7INSEL : 3;
			unsigned int COMP7INPSEL : 1;
			unsigned int : 2;
			unsigned int COMP7_OUT_SEL : 4;
			unsigned int : 1;
			unsigned int COMP7POL : 1;
			unsigned int COMP7HYST : 2;
			unsigned int COMP7_BLANKING : 3;
			unsigned int : 9;
			unsigned int COMP7OUT : 1;
			unsigned int COMP7LOCK : 1;
		} __attribute((__packed__));
	};
};
#define SYSCFG_COMP_OPAMP_COMP7_CSR (*((volatile struct SYSCFG_COMP_OPAMP_COMP7_CSR_s*) 0x40010034))

struct SYSCFG_COMP_OPAMP_OPAMP1_CSR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int OPAMP1_EN : 1;
			unsigned int FORCE_VP : 1;
			unsigned int VP_SEL : 2;
			unsigned int : 1;
			unsigned int VM_SEL : 2;
			unsigned int TCM_EN : 1;
			unsigned int VMS_SEL : 1;
			unsigned int VPS_SEL : 2;
			unsigned int CALON : 1;
			unsigned int CALSEL : 2;
			unsigned int PGA_GAIN : 4;
			unsigned int USER_TRIM : 1;
			unsigned int TRIMOFFSETP : 5;
			unsigned int TRIMOFFSETN : 5;
			unsigned int TSTREF : 1;
			unsigned int OUTCAL : 1;
			unsigned int LOCK : 1;
		} __attribute((__packed__));
	};
};
#define SYSCFG_COMP_OPAMP_OPAMP1_CSR (*((volatile struct SYSCFG_COMP_OPAMP_OPAMP1_CSR_s*) 0x40010038))

struct SYSCFG_COMP_OPAMP_OPAMP2_CSR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int OPAMP2EN : 1;
			unsigned int FORCE_VP : 1;
			unsigned int VP_SEL : 2;
			unsigned int : 1;
			unsigned int VM_SEL : 2;
			unsigned int TCM_EN : 1;
			unsigned int VMS_SEL : 1;
			unsigned int VPS_SEL : 2;
			unsigned int CALON : 1;
			unsigned int CAL_SEL : 2;
			unsigned int PGA_GAIN : 4;
			unsigned int USER_TRIM : 1;
			unsigned int TRIMOFFSETP : 5;
			unsigned int TRIMOFFSETN : 5;
			unsigned int TSTREF : 1;
			unsigned int OUTCAL : 1;
			unsigned int LOCK : 1;
		} __attribute((__packed__));
	};
};
#define SYSCFG_COMP_OPAMP_OPAMP2_CSR (*((volatile struct SYSCFG_COMP_OPAMP_OPAMP2_CSR_s*) 0x4001003c))

struct SYSCFG_COMP_OPAMP_OPAMP3_CSR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int OPAMP3EN : 1;
			unsigned int FORCE_VP : 1;
			unsigned int VP_SEL : 2;
			unsigned int : 1;
			unsigned int VM_SEL : 2;
			unsigned int TCM_EN : 1;
			unsigned int VMS_SEL : 1;
			unsigned int VPS_SEL : 2;
			unsigned int CALON : 1;
			unsigned int CALSEL : 2;
			unsigned int PGA_GAIN : 4;
			unsigned int USER_TRIM : 1;
			unsigned int TRIMOFFSETP : 5;
			unsigned int TRIMOFFSETN : 5;
			unsigned int TSTREF : 1;
			unsigned int OUTCAL : 1;
			unsigned int LOCK : 1;
		} __attribute((__packed__));
	};
};
#define SYSCFG_COMP_OPAMP_OPAMP3_CSR (*((volatile struct SYSCFG_COMP_OPAMP_OPAMP3_CSR_s*) 0x40010040))

struct SYSCFG_COMP_OPAMP_OPAMP4_CSR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int OPAMP4EN : 1;
			unsigned int FORCE_VP : 1;
			unsigned int VP_SEL : 2;
			unsigned int : 1;
			unsigned int VM_SEL : 2;
			unsigned int TCM_EN : 1;
			unsigned int VMS_SEL : 1;
			unsigned int VPS_SEL : 2;
			unsigned int CALON : 1;
			unsigned int CALSEL : 2;
			unsigned int PGA_GAIN : 4;
			unsigned int USER_TRIM : 1;
			unsigned int TRIMOFFSETP : 5;
			unsigned int TRIMOFFSETN : 5;
			unsigned int TSTREF : 1;
			unsigned int OUTCAL : 1;
			unsigned int LOCK : 1;
		} __attribute((__packed__));
	};
};
#define SYSCFG_COMP_OPAMP_OPAMP4_CSR (*((volatile struct SYSCFG_COMP_OPAMP_OPAMP4_CSR_s*) 0x40010044))

// FMC
struct FMC_BCR1_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int MBKEN : 1;
			unsigned int MUXEN : 1;
			unsigned int MTYP : 2;
			unsigned int MWID : 2;
			unsigned int FACCEN : 1;
			unsigned int : 1;
			unsigned int BURSTEN : 1;
			unsigned int WAITPOL : 1;
			unsigned int : 1;
			unsigned int WAITCFG : 1;
			unsigned int WREN : 1;
			unsigned int WAITEN : 1;
			unsigned int EXTMOD : 1;
			unsigned int ASYNCWAIT : 1;
			unsigned int : 3;
			unsigned int CBURSTRW : 1;
			unsigned int CCLKEN : 1;
		} __attribute((__packed__));
	};
};
#define FMC_BCR1 (*((volatile struct FMC_BCR1_s*) 0xa0000400))

struct FMC_BTR1_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int ADDSET : 4;
			unsigned int ADDHLD : 4;
			unsigned int DATAST : 8;
			unsigned int BUSTURN : 4;
			unsigned int CLKDIV : 4;
			unsigned int DATLAT : 4;
			unsigned int ACCMOD : 2;
		} __attribute((__packed__));
	};
};
#define FMC_BTR1 (*((volatile struct FMC_BTR1_s*) 0xa0000404))

struct FMC_BCR2_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int MBKEN : 1;
			unsigned int MUXEN : 1;
			unsigned int MTYP : 2;
			unsigned int MWID : 2;
			unsigned int FACCEN : 1;
			unsigned int : 1;
			unsigned int BURSTEN : 1;
			unsigned int WAITPOL : 1;
			unsigned int WRAPMOD : 1;
			unsigned int WAITCFG : 1;
			unsigned int WREN : 1;
			unsigned int WAITEN : 1;
			unsigned int EXTMOD : 1;
			unsigned int ASYNCWAIT : 1;
			unsigned int : 3;
			unsigned int CBURSTRW : 1;
		} __attribute((__packed__));
	};
};
#define FMC_BCR2 (*((volatile struct FMC_BCR2_s*) 0xa0000408))

struct FMC_BTR2_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int ADDSET : 4;
			unsigned int ADDHLD : 4;
			unsigned int DATAST : 8;
			unsigned int BUSTURN : 4;
			unsigned int CLKDIV : 4;
			unsigned int DATLAT : 4;
			unsigned int ACCMOD : 2;
		} __attribute((__packed__));
	};
};
#define FMC_BTR2 (*((volatile struct FMC_BTR2_s*) 0xa000040c))

struct FMC_BCR3_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int MBKEN : 1;
			unsigned int MUXEN : 1;
			unsigned int MTYP : 2;
			unsigned int MWID : 2;
			unsigned int FACCEN : 1;
			unsigned int : 1;
			unsigned int BURSTEN : 1;
			unsigned int WAITPOL : 1;
			unsigned int WRAPMOD : 1;
			unsigned int WAITCFG : 1;
			unsigned int WREN : 1;
			unsigned int WAITEN : 1;
			unsigned int EXTMOD : 1;
			unsigned int ASYNCWAIT : 1;
			unsigned int : 3;
			unsigned int CBURSTRW : 1;
		} __attribute((__packed__));
	};
};
#define FMC_BCR3 (*((volatile struct FMC_BCR3_s*) 0xa0000410))

struct FMC_BTR3_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int ADDSET : 4;
			unsigned int ADDHLD : 4;
			unsigned int DATAST : 8;
			unsigned int BUSTURN : 4;
			unsigned int CLKDIV : 4;
			unsigned int DATLAT : 4;
			unsigned int ACCMOD : 2;
		} __attribute((__packed__));
	};
};
#define FMC_BTR3 (*((volatile struct FMC_BTR3_s*) 0xa0000414))

struct FMC_BCR4_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int MBKEN : 1;
			unsigned int MUXEN : 1;
			unsigned int MTYP : 2;
			unsigned int MWID : 2;
			unsigned int FACCEN : 1;
			unsigned int : 1;
			unsigned int BURSTEN : 1;
			unsigned int WAITPOL : 1;
			unsigned int WRAPMOD : 1;
			unsigned int WAITCFG : 1;
			unsigned int WREN : 1;
			unsigned int WAITEN : 1;
			unsigned int EXTMOD : 1;
			unsigned int ASYNCWAIT : 1;
			unsigned int : 3;
			unsigned int CBURSTRW : 1;
		} __attribute((__packed__));
	};
};
#define FMC_BCR4 (*((volatile struct FMC_BCR4_s*) 0xa0000418))

struct FMC_BTR4_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int ADDSET : 4;
			unsigned int ADDHLD : 4;
			unsigned int DATAST : 8;
			unsigned int BUSTURN : 4;
			unsigned int CLKDIV : 4;
			unsigned int DATLAT : 4;
			unsigned int ACCMOD : 2;
		} __attribute((__packed__));
	};
};
#define FMC_BTR4 (*((volatile struct FMC_BTR4_s*) 0xa000041c))

struct FMC_PCR2_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int : 1;
			unsigned int PWAITEN : 1;
			unsigned int PBKEN : 1;
			unsigned int PTYP : 1;
			unsigned int PWID : 2;
			unsigned int ECCEN : 1;
			unsigned int : 2;
			unsigned int TCLR : 4;
			unsigned int TAR : 4;
			unsigned int ECCPS : 3;
		} __attribute((__packed__));
	};
};
#define FMC_PCR2 (*((volatile struct FMC_PCR2_s*) 0xa0000460))

struct FMC_SR2_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int IRS : 1;
			unsigned int ILS : 1;
			unsigned int IFS : 1;
			unsigned int IREN : 1;
			unsigned int ILEN : 1;
			unsigned int IFEN : 1;
			unsigned int FEMPT : 1;
		} __attribute((__packed__));
	};
};
#define FMC_SR2 (*((volatile struct FMC_SR2_s*) 0xa0000464))

struct FMC_PMEM2_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int MEMSETx : 8;
			unsigned int MEMWAITx : 8;
			unsigned int MEMHOLDx : 8;
			unsigned int MEMHIZx : 8;
		} __attribute((__packed__));
	};
};
#define FMC_PMEM2 (*((volatile struct FMC_PMEM2_s*) 0xa0000468))

struct FMC_PATT2_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int ATTSETx : 8;
			unsigned int ATTWAITx : 8;
			unsigned int ATTHOLDx : 8;
			unsigned int ATTHIZx : 8;
		} __attribute((__packed__));
	};
};
#define FMC_PATT2 (*((volatile struct FMC_PATT2_s*) 0xa000046c))

struct FMC_ECCR2_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int ECCx : 32;
		} __attribute((__packed__));
	};
};
#define FMC_ECCR2 (*((volatile struct FMC_ECCR2_s*) 0xa0000474))

struct FMC_PCR3_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int : 1;
			unsigned int PWAITEN : 1;
			unsigned int PBKEN : 1;
			unsigned int PTYP : 1;
			unsigned int PWID : 2;
			unsigned int ECCEN : 1;
			unsigned int : 2;
			unsigned int TCLR : 4;
			unsigned int TAR : 4;
			unsigned int ECCPS : 3;
		} __attribute((__packed__));
	};
};
#define FMC_PCR3 (*((volatile struct FMC_PCR3_s*) 0xa0000480))

struct FMC_SR3_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int IRS : 1;
			unsigned int ILS : 1;
			unsigned int IFS : 1;
			unsigned int IREN : 1;
			unsigned int ILEN : 1;
			unsigned int IFEN : 1;
			unsigned int FEMPT : 1;
		} __attribute((__packed__));
	};
};
#define FMC_SR3 (*((volatile struct FMC_SR3_s*) 0xa0000484))

struct FMC_PMEM3_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int MEMSETx : 8;
			unsigned int MEMWAITx : 8;
			unsigned int MEMHOLDx : 8;
			unsigned int MEMHIZx : 8;
		} __attribute((__packed__));
	};
};
#define FMC_PMEM3 (*((volatile struct FMC_PMEM3_s*) 0xa0000488))

struct FMC_PATT3_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int ATTSETx : 8;
			unsigned int ATTWAITx : 8;
			unsigned int ATTHOLDx : 8;
			unsigned int ATTHIZx : 8;
		} __attribute((__packed__));
	};
};
#define FMC_PATT3 (*((volatile struct FMC_PATT3_s*) 0xa000048c))

struct FMC_ECCR3_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int ECCx : 32;
		} __attribute((__packed__));
	};
};
#define FMC_ECCR3 (*((volatile struct FMC_ECCR3_s*) 0xa0000494))

struct FMC_PCR4_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int : 1;
			unsigned int PWAITEN : 1;
			unsigned int PBKEN : 1;
			unsigned int PTYP : 1;
			unsigned int PWID : 2;
			unsigned int ECCEN : 1;
			unsigned int : 2;
			unsigned int TCLR : 4;
			unsigned int TAR : 4;
			unsigned int ECCPS : 3;
		} __attribute((__packed__));
	};
};
#define FMC_PCR4 (*((volatile struct FMC_PCR4_s*) 0xa00004a0))

struct FMC_SR4_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int IRS : 1;
			unsigned int ILS : 1;
			unsigned int IFS : 1;
			unsigned int IREN : 1;
			unsigned int ILEN : 1;
			unsigned int IFEN : 1;
			unsigned int FEMPT : 1;
		} __attribute((__packed__));
	};
};
#define FMC_SR4 (*((volatile struct FMC_SR4_s*) 0xa00004a4))

struct FMC_PMEM4_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int MEMSETx : 8;
			unsigned int MEMWAITx : 8;
			unsigned int MEMHOLDx : 8;
			unsigned int MEMHIZx : 8;
		} __attribute((__packed__));
	};
};
#define FMC_PMEM4 (*((volatile struct FMC_PMEM4_s*) 0xa00004a8))

struct FMC_PATT4_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int ATTSETx : 8;
			unsigned int ATTWAITx : 8;
			unsigned int ATTHOLDx : 8;
			unsigned int ATTHIZx : 8;
		} __attribute((__packed__));
	};
};
#define FMC_PATT4 (*((volatile struct FMC_PATT4_s*) 0xa00004ac))

struct FMC_PIO4_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int IOSETx : 8;
			unsigned int IOWAITx : 8;
			unsigned int IOHOLDx : 8;
			unsigned int IOHIZx : 8;
		} __attribute((__packed__));
	};
};
#define FMC_PIO4 (*((volatile struct FMC_PIO4_s*) 0xa00004b0))

struct FMC_BWTR1_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int ADDSET : 4;
			unsigned int ADDHLD : 4;
			unsigned int DATAST : 8;
			unsigned int BUSTURN : 4;
			unsigned int CLKDIV : 4;
			unsigned int DATLAT : 4;
			unsigned int ACCMOD : 2;
		} __attribute((__packed__));
	};
};
#define FMC_BWTR1 (*((volatile struct FMC_BWTR1_s*) 0xa0000504))

struct FMC_BWTR2_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int ADDSET : 4;
			unsigned int ADDHLD : 4;
			unsigned int DATAST : 8;
			unsigned int BUSTURN : 4;
			unsigned int CLKDIV : 4;
			unsigned int DATLAT : 4;
			unsigned int ACCMOD : 2;
		} __attribute((__packed__));
	};
};
#define FMC_BWTR2 (*((volatile struct FMC_BWTR2_s*) 0xa000050c))

struct FMC_BWTR3_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int ADDSET : 4;
			unsigned int ADDHLD : 4;
			unsigned int DATAST : 8;
			unsigned int BUSTURN : 4;
			unsigned int CLKDIV : 4;
			unsigned int DATLAT : 4;
			unsigned int ACCMOD : 2;
		} __attribute((__packed__));
	};
};
#define FMC_BWTR3 (*((volatile struct FMC_BWTR3_s*) 0xa0000514))

struct FMC_BWTR4_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int ADDSET : 4;
			unsigned int ADDHLD : 4;
			unsigned int DATAST : 8;
			unsigned int BUSTURN : 4;
			unsigned int CLKDIV : 4;
			unsigned int DATLAT : 4;
			unsigned int ACCMOD : 2;
		} __attribute((__packed__));
	};
};
#define FMC_BWTR4 (*((volatile struct FMC_BWTR4_s*) 0xa000051c))

// NVIC
struct NVIC_ISER0_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int SETENA : 32;
		} __attribute((__packed__));
	};
};
#define NVIC_ISER0 (*((volatile struct NVIC_ISER0_s*) 0xe000e100))

struct NVIC_ISER1_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int SETENA : 32;
		} __attribute((__packed__));
	};
};
#define NVIC_ISER1 (*((volatile struct NVIC_ISER1_s*) 0xe000e104))

struct NVIC_ISER2_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int SETENA : 32;
		} __attribute((__packed__));
	};
};
#define NVIC_ISER2 (*((volatile struct NVIC_ISER2_s*) 0xe000e108))

struct NVIC_ICER0_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int CLRENA : 32;
		} __attribute((__packed__));
	};
};
#define NVIC_ICER0 (*((volatile struct NVIC_ICER0_s*) 0xe000e180))

struct NVIC_ICER1_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int CLRENA : 32;
		} __attribute((__packed__));
	};
};
#define NVIC_ICER1 (*((volatile struct NVIC_ICER1_s*) 0xe000e184))

struct NVIC_ICER2_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int CLRENA : 32;
		} __attribute((__packed__));
	};
};
#define NVIC_ICER2 (*((volatile struct NVIC_ICER2_s*) 0xe000e188))

struct NVIC_ISPR0_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int SETPEND : 32;
		} __attribute((__packed__));
	};
};
#define NVIC_ISPR0 (*((volatile struct NVIC_ISPR0_s*) 0xe000e200))

struct NVIC_ISPR1_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int SETPEND : 32;
		} __attribute((__packed__));
	};
};
#define NVIC_ISPR1 (*((volatile struct NVIC_ISPR1_s*) 0xe000e204))

struct NVIC_ISPR2_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int SETPEND : 32;
		} __attribute((__packed__));
	};
};
#define NVIC_ISPR2 (*((volatile struct NVIC_ISPR2_s*) 0xe000e208))

struct NVIC_ICPR0_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int CLRPEND : 32;
		} __attribute((__packed__));
	};
};
#define NVIC_ICPR0 (*((volatile struct NVIC_ICPR0_s*) 0xe000e280))

struct NVIC_ICPR1_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int CLRPEND : 32;
		} __attribute((__packed__));
	};
};
#define NVIC_ICPR1 (*((volatile struct NVIC_ICPR1_s*) 0xe000e284))

struct NVIC_ICPR2_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int CLRPEND : 32;
		} __attribute((__packed__));
	};
};
#define NVIC_ICPR2 (*((volatile struct NVIC_ICPR2_s*) 0xe000e288))

struct NVIC_IABR0_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int ACTIVE : 32;
		} __attribute((__packed__));
	};
};
#define NVIC_IABR0 (*((volatile struct NVIC_IABR0_s*) 0xe000e300))

struct NVIC_IABR1_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int ACTIVE : 32;
		} __attribute((__packed__));
	};
};
#define NVIC_IABR1 (*((volatile struct NVIC_IABR1_s*) 0xe000e304))

struct NVIC_IABR2_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int ACTIVE : 32;
		} __attribute((__packed__));
	};
};
#define NVIC_IABR2 (*((volatile struct NVIC_IABR2_s*) 0xe000e308))

struct NVIC_IPR0_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int IPR_N0 : 8;
			unsigned int IPR_N1 : 8;
			unsigned int IPR_N2 : 8;
			unsigned int IPR_N3 : 8;
		} __attribute((__packed__));
	};
};
#define NVIC_IPR0 (*((volatile struct NVIC_IPR0_s*) 0xe000e400))

struct NVIC_IPR1_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int IPR_N0 : 8;
			unsigned int IPR_N1 : 8;
			unsigned int IPR_N2 : 8;
			unsigned int IPR_N3 : 8;
		} __attribute((__packed__));
	};
};
#define NVIC_IPR1 (*((volatile struct NVIC_IPR1_s*) 0xe000e404))

struct NVIC_IPR2_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int IPR_N0 : 8;
			unsigned int IPR_N1 : 8;
			unsigned int IPR_N2 : 8;
			unsigned int IPR_N3 : 8;
		} __attribute((__packed__));
	};
};
#define NVIC_IPR2 (*((volatile struct NVIC_IPR2_s*) 0xe000e408))

struct NVIC_IPR3_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int IPR_N0 : 8;
			unsigned int IPR_N1 : 8;
			unsigned int IPR_N2 : 8;
			unsigned int IPR_N3 : 8;
		} __attribute((__packed__));
	};
};
#define NVIC_IPR3 (*((volatile struct NVIC_IPR3_s*) 0xe000e40c))

struct NVIC_IPR4_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int IPR_N0 : 8;
			unsigned int IPR_N1 : 8;
			unsigned int IPR_N2 : 8;
			unsigned int IPR_N3 : 8;
		} __attribute((__packed__));
	};
};
#define NVIC_IPR4 (*((volatile struct NVIC_IPR4_s*) 0xe000e410))

struct NVIC_IPR5_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int IPR_N0 : 8;
			unsigned int IPR_N1 : 8;
			unsigned int IPR_N2 : 8;
			unsigned int IPR_N3 : 8;
		} __attribute((__packed__));
	};
};
#define NVIC_IPR5 (*((volatile struct NVIC_IPR5_s*) 0xe000e414))

struct NVIC_IPR6_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int IPR_N0 : 8;
			unsigned int IPR_N1 : 8;
			unsigned int IPR_N2 : 8;
			unsigned int IPR_N3 : 8;
		} __attribute((__packed__));
	};
};
#define NVIC_IPR6 (*((volatile struct NVIC_IPR6_s*) 0xe000e418))

struct NVIC_IPR7_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int IPR_N0 : 8;
			unsigned int IPR_N1 : 8;
			unsigned int IPR_N2 : 8;
			unsigned int IPR_N3 : 8;
		} __attribute((__packed__));
	};
};
#define NVIC_IPR7 (*((volatile struct NVIC_IPR7_s*) 0xe000e41c))

struct NVIC_IPR8_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int IPR_N0 : 8;
			unsigned int IPR_N1 : 8;
			unsigned int IPR_N2 : 8;
			unsigned int IPR_N3 : 8;
		} __attribute((__packed__));
	};
};
#define NVIC_IPR8 (*((volatile struct NVIC_IPR8_s*) 0xe000e420))

struct NVIC_IPR9_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int IPR_N0 : 8;
			unsigned int IPR_N1 : 8;
			unsigned int IPR_N2 : 8;
			unsigned int IPR_N3 : 8;
		} __attribute((__packed__));
	};
};
#define NVIC_IPR9 (*((volatile struct NVIC_IPR9_s*) 0xe000e424))

struct NVIC_IPR10_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int IPR_N0 : 8;
			unsigned int IPR_N1 : 8;
			unsigned int IPR_N2 : 8;
			unsigned int IPR_N3 : 8;
		} __attribute((__packed__));
	};
};
#define NVIC_IPR10 (*((volatile struct NVIC_IPR10_s*) 0xe000e428))

struct NVIC_IPR11_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int IPR_N0 : 8;
			unsigned int IPR_N1 : 8;
			unsigned int IPR_N2 : 8;
			unsigned int IPR_N3 : 8;
		} __attribute((__packed__));
	};
};
#define NVIC_IPR11 (*((volatile struct NVIC_IPR11_s*) 0xe000e42c))

struct NVIC_IPR12_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int IPR_N0 : 8;
			unsigned int IPR_N1 : 8;
			unsigned int IPR_N2 : 8;
			unsigned int IPR_N3 : 8;
		} __attribute((__packed__));
	};
};
#define NVIC_IPR12 (*((volatile struct NVIC_IPR12_s*) 0xe000e430))

struct NVIC_IPR13_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int IPR_N0 : 8;
			unsigned int IPR_N1 : 8;
			unsigned int IPR_N2 : 8;
			unsigned int IPR_N3 : 8;
		} __attribute((__packed__));
	};
};
#define NVIC_IPR13 (*((volatile struct NVIC_IPR13_s*) 0xe000e434))

struct NVIC_IPR14_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int IPR_N0 : 8;
			unsigned int IPR_N1 : 8;
			unsigned int IPR_N2 : 8;
			unsigned int IPR_N3 : 8;
		} __attribute((__packed__));
	};
};
#define NVIC_IPR14 (*((volatile struct NVIC_IPR14_s*) 0xe000e438))

struct NVIC_IPR15_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int IPR_N0 : 8;
			unsigned int IPR_N1 : 8;
			unsigned int IPR_N2 : 8;
			unsigned int IPR_N3 : 8;
		} __attribute((__packed__));
	};
};
#define NVIC_IPR15 (*((volatile struct NVIC_IPR15_s*) 0xe000e43c))

struct NVIC_IPR16_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int IPR_N0 : 8;
			unsigned int IPR_N1 : 8;
			unsigned int IPR_N2 : 8;
			unsigned int IPR_N3 : 8;
		} __attribute((__packed__));
	};
};
#define NVIC_IPR16 (*((volatile struct NVIC_IPR16_s*) 0xe000e440))

struct NVIC_IPR17_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int IPR_N0 : 8;
			unsigned int IPR_N1 : 8;
			unsigned int IPR_N2 : 8;
			unsigned int IPR_N3 : 8;
		} __attribute((__packed__));
	};
};
#define NVIC_IPR17 (*((volatile struct NVIC_IPR17_s*) 0xe000e444))

struct NVIC_IPR18_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int IPR_N0 : 8;
			unsigned int IPR_N1 : 8;
			unsigned int IPR_N2 : 8;
			unsigned int IPR_N3 : 8;
		} __attribute((__packed__));
	};
};
#define NVIC_IPR18 (*((volatile struct NVIC_IPR18_s*) 0xe000e448))

struct NVIC_IPR19_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int IPR_N0 : 8;
			unsigned int IPR_N1 : 8;
			unsigned int IPR_N2 : 8;
			unsigned int IPR_N3 : 8;
		} __attribute((__packed__));
	};
};
#define NVIC_IPR19 (*((volatile struct NVIC_IPR19_s*) 0xe000e44c))

struct NVIC_IPR20_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int IPR_N0 : 8;
			unsigned int IPR_N1 : 8;
			unsigned int IPR_N2 : 8;
			unsigned int IPR_N3 : 8;
		} __attribute((__packed__));
	};
};
#define NVIC_IPR20 (*((volatile struct NVIC_IPR20_s*) 0xe000e450))

// FPU
struct FPU_FPCCR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int LSPACT : 1;
			unsigned int USER : 1;
			unsigned int : 1;
			unsigned int THREAD : 1;
			unsigned int HFRDY : 1;
			unsigned int MMRDY : 1;
			unsigned int BFRDY : 1;
			unsigned int : 1;
			unsigned int MONRDY : 1;
			unsigned int : 21;
			unsigned int LSPEN : 1;
			unsigned int ASPEN : 1;
		} __attribute((__packed__));
	};
};
#define FPU_FPCCR (*((volatile struct FPU_FPCCR_s*) 0xe000ef34))

struct FPU_FPCAR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int : 3;
			unsigned int ADDRESS : 29;
		} __attribute((__packed__));
	};
};
#define FPU_FPCAR (*((volatile struct FPU_FPCAR_s*) 0xe000ef38))

struct FPU_FPSCR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int IOC : 1;
			unsigned int DZC : 1;
			unsigned int OFC : 1;
			unsigned int UFC : 1;
			unsigned int IXC : 1;
			unsigned int : 2;
			unsigned int IDC : 1;
			unsigned int : 14;
			unsigned int RMode : 2;
			unsigned int FZ : 1;
			unsigned int DN : 1;
			unsigned int AHP : 1;
			unsigned int : 1;
			unsigned int V : 1;
			unsigned int C : 1;
			unsigned int Z : 1;
			unsigned int N : 1;
		} __attribute((__packed__));
	};
};
#define FPU_FPSCR (*((volatile struct FPU_FPSCR_s*) 0xe000ef3c))

// MPU
struct MPU_TYPER_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int SEPARATE : 1;
			unsigned int : 7;
			unsigned int DREGION : 8;
			unsigned int IREGION : 8;
		} __attribute((__packed__));
	};
};
#define MPU_TYPER (*((volatile struct MPU_TYPER_s*) 0xe000ed90))

struct MPU_CTRL_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int ENABLE : 1;
			unsigned int HFNMIENA : 1;
			unsigned int PRIVDEFENA : 1;
		} __attribute((__packed__));
	};
};
#define MPU_CTRL (*((volatile struct MPU_CTRL_s*) 0xe000ed94))

struct MPU_RNR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int REGION : 8;
		} __attribute((__packed__));
	};
};
#define MPU_RNR (*((volatile struct MPU_RNR_s*) 0xe000ed98))

struct MPU_RBAR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int REGION : 4;
			unsigned int VALID : 1;
			unsigned int ADDR : 27;
		} __attribute((__packed__));
	};
};
#define MPU_RBAR (*((volatile struct MPU_RBAR_s*) 0xe000ed9c))

struct MPU_RASR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int ENABLE : 1;
			unsigned int SIZE : 5;
			unsigned int : 2;
			unsigned int SRD : 8;
			unsigned int B : 1;
			unsigned int C : 1;
			unsigned int S : 1;
			unsigned int TEX : 3;
			unsigned int : 2;
			unsigned int AP : 3;
			unsigned int : 1;
			unsigned int XN : 1;
		} __attribute((__packed__));
	};
};
#define MPU_RASR (*((volatile struct MPU_RASR_s*) 0xe000eda0))

// STK
struct STK_CTRL_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int ENABLE : 1;
			unsigned int TICKINT : 1;
			unsigned int CLKSOURCE : 1;
			unsigned int : 13;
			unsigned int COUNTFLAG : 1;
		} __attribute((__packed__));
	};
};
#define STK_CTRL (*((volatile struct STK_CTRL_s*) 0xe000e010))

struct STK_LOAD_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int RELOAD : 24;
		} __attribute((__packed__));
	};
};
#define STK_LOAD (*((volatile struct STK_LOAD_s*) 0xe000e014))

struct STK_VAL_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int CURRENT : 24;
		} __attribute((__packed__));
	};
};
#define STK_VAL (*((volatile struct STK_VAL_s*) 0xe000e018))

struct STK_CALIB_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int TENMS : 24;
			unsigned int : 6;
			unsigned int SKEW : 1;
			unsigned int NOREF : 1;
		} __attribute((__packed__));
	};
};
#define STK_CALIB (*((volatile struct STK_CALIB_s*) 0xe000e01c))

// SCB
struct SCB_CPUID_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int Revision : 4;
			unsigned int PartNo : 12;
			unsigned int Constant : 4;
			unsigned int Variant : 4;
			unsigned int Implementer : 8;
		} __attribute((__packed__));
	};
};
#define SCB_CPUID (*((volatile struct SCB_CPUID_s*) 0xe000ed00))

struct SCB_ICSR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int VECTACTIVE : 9;
			unsigned int : 2;
			unsigned int RETTOBASE : 1;
			unsigned int VECTPENDING : 7;
			unsigned int : 3;
			unsigned int ISRPENDING : 1;
			unsigned int : 2;
			unsigned int PENDSTCLR : 1;
			unsigned int PENDSTSET : 1;
			unsigned int PENDSVCLR : 1;
			unsigned int PENDSVSET : 1;
			unsigned int : 2;
			unsigned int NMIPENDSET : 1;
		} __attribute((__packed__));
	};
};
#define SCB_ICSR (*((volatile struct SCB_ICSR_s*) 0xe000ed04))

struct SCB_VTOR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int : 9;
			unsigned int TBLOFF : 21;
		} __attribute((__packed__));
	};
};
#define SCB_VTOR (*((volatile struct SCB_VTOR_s*) 0xe000ed08))

struct SCB_AIRCR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int VECTRESET : 1;
			unsigned int VECTCLRACTIVE : 1;
			unsigned int SYSRESETREQ : 1;
			unsigned int : 5;
			unsigned int PRIGROUP : 3;
			unsigned int : 4;
			unsigned int ENDIANESS : 1;
			unsigned int VECTKEYSTAT : 16;
		} __attribute((__packed__));
	};
};
#define SCB_AIRCR (*((volatile struct SCB_AIRCR_s*) 0xe000ed0c))

struct SCB_SCR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int : 1;
			unsigned int SLEEPONEXIT : 1;
			unsigned int SLEEPDEEP : 1;
			unsigned int : 1;
			unsigned int SEVEONPEND : 1;
		} __attribute((__packed__));
	};
};
#define SCB_SCR (*((volatile struct SCB_SCR_s*) 0xe000ed10))

struct SCB_CCR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int NONBASETHRDENA : 1;
			unsigned int USERSETMPEND : 1;
			unsigned int : 1;
			unsigned int UNALIGN__TRP : 1;
			unsigned int DIV_0_TRP : 1;
			unsigned int : 3;
			unsigned int BFHFNMIGN : 1;
			unsigned int STKALIGN : 1;
		} __attribute((__packed__));
	};
};
#define SCB_CCR (*((volatile struct SCB_CCR_s*) 0xe000ed14))

struct SCB_SHPR1_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int PRI_4 : 8;
			unsigned int PRI_5 : 8;
			unsigned int PRI_6 : 8;
		} __attribute((__packed__));
	};
};
#define SCB_SHPR1 (*((volatile struct SCB_SHPR1_s*) 0xe000ed18))

struct SCB_SHPR2_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int : 24;
			unsigned int PRI_11 : 8;
		} __attribute((__packed__));
	};
};
#define SCB_SHPR2 (*((volatile struct SCB_SHPR2_s*) 0xe000ed1c))

struct SCB_SHPR3_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int : 16;
			unsigned int PRI_14 : 8;
			unsigned int PRI_15 : 8;
		} __attribute((__packed__));
	};
};
#define SCB_SHPR3 (*((volatile struct SCB_SHPR3_s*) 0xe000ed20))

struct SCB_SHCRS_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int MEMFAULTACT : 1;
			unsigned int BUSFAULTACT : 1;
			unsigned int : 1;
			unsigned int USGFAULTACT : 1;
			unsigned int : 3;
			unsigned int SVCALLACT : 1;
			unsigned int MONITORACT : 1;
			unsigned int : 1;
			unsigned int PENDSVACT : 1;
			unsigned int SYSTICKACT : 1;
			unsigned int USGFAULTPENDED : 1;
			unsigned int MEMFAULTPENDED : 1;
			unsigned int BUSFAULTPENDED : 1;
			unsigned int SVCALLPENDED : 1;
			unsigned int MEMFAULTENA : 1;
			unsigned int BUSFAULTENA : 1;
			unsigned int USGFAULTENA : 1;
		} __attribute((__packed__));
	};
};
#define SCB_SHCRS (*((volatile struct SCB_SHCRS_s*) 0xe000ed24))

struct SCB_CFSR_UFSR_BFSR_MMFSR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int : 1;
			unsigned int IACCVIOL : 1;
			unsigned int : 1;
			unsigned int MUNSTKERR : 1;
			unsigned int MSTKERR : 1;
			unsigned int MLSPERR : 1;
			unsigned int : 1;
			unsigned int MMARVALID : 1;
			unsigned int IBUSERR : 1;
			unsigned int PRECISERR : 1;
			unsigned int IMPRECISERR : 1;
			unsigned int UNSTKERR : 1;
			unsigned int STKERR : 1;
			unsigned int LSPERR : 1;
			unsigned int : 1;
			unsigned int BFARVALID : 1;
			unsigned int UNDEFINSTR : 1;
			unsigned int INVSTATE : 1;
			unsigned int INVPC : 1;
			unsigned int NOCP : 1;
			unsigned int : 4;
			unsigned int UNALIGNED : 1;
			unsigned int DIVBYZERO : 1;
		} __attribute((__packed__));
	};
};
#define SCB_CFSR_UFSR_BFSR_MMFSR (*((volatile struct SCB_CFSR_UFSR_BFSR_MMFSR_s*) 0xe000ed28))

struct SCB_HFSR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int : 1;
			unsigned int VECTTBL : 1;
			unsigned int : 28;
			unsigned int FORCED : 1;
			unsigned int DEBUG_VT : 1;
		} __attribute((__packed__));
	};
};
#define SCB_HFSR (*((volatile struct SCB_HFSR_s*) 0xe000ed2c))

struct SCB_MMFAR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int MMFAR : 32;
		} __attribute((__packed__));
	};
};
#define SCB_MMFAR (*((volatile struct SCB_MMFAR_s*) 0xe000ed34))

struct SCB_BFAR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int BFAR : 32;
		} __attribute((__packed__));
	};
};
#define SCB_BFAR (*((volatile struct SCB_BFAR_s*) 0xe000ed38))

struct SCB_AFSR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int IMPDEF : 32;
		} __attribute((__packed__));
	};
};
#define SCB_AFSR (*((volatile struct SCB_AFSR_s*) 0xe000ed3c))

// NVIC_STIR
struct NVIC_STIR_STIR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int INTID : 9;
		} __attribute((__packed__));
	};
};
#define NVIC_STIR_STIR (*((volatile struct NVIC_STIR_STIR_s*) 0xe000ef00))

// FPU_CPACR
struct FPU_CPACR_CPACR_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int : 20;
			unsigned int CP : 4;
		} __attribute((__packed__));
	};
};
#define FPU_CPACR_CPACR (*((volatile struct FPU_CPACR_CPACR_s*) 0xe000ed88))

// SCB_ACTRL
struct SCB_ACTRL_ACTRL_s 
{
	union
	{
		unsigned int reg;

		struct
		{
			unsigned int DISMCYCINT : 1;
			unsigned int DISDEFWBUF : 1;
			unsigned int DISFOLD : 1;
			unsigned int : 5;
			unsigned int DISFPCA : 1;
			unsigned int DISOOFP : 1;
		} __attribute((__packed__));
	};
};
#define SCB_ACTRL_ACTRL (*((volatile struct SCB_ACTRL_ACTRL_s*) 0xe000e008))

#endif //__STM32F303__
