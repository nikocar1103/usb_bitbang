#ifndef __STM32F303_H__
	#define __STM32F303_H__

#include <stm32f303reg.h>

void clock_init_60mhz();
void gpio_init();
void gpio_init_output();
void gpio_init_input();
void EXTI0_init();

void systick_init(int ms);
void led_init();
void led_on();
void led_off();
void led_toggle();

#endif
