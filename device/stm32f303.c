#include <stm32f303.h>

void clock_init_60mhz()
{
    Flash_ACR.reg = 0x32;

    // Disable PLL 
    RCC_CR.PLLON = 0;
    while(RCC_CR.PLLRDY)
        __asm__("nop");
    
    RCC_CFGR.PLLMUL = 0b1101;

    RCC_CFGR.PLLSRC = 0b0;

    // Enable PLL
    RCC_CR.PLLON = 1;
    while(!RCC_CR.PLLRDY)
        __asm__("nop");

    // Set PLL output as system clock source
    RCC_CFGR.SW = 0b10;
    while(RCC_CFGR.SWS != 0b10)
        __asm__("nop");
}

void gpio_init()
{
    // VBUS not connected
    // Enable I/O port A
    RCC_AHBENR.IOPAEN = 1;
    // PA0 output mode, used for D+
    //GPIOA_MODER.MODER0 = 1;
    // PA0 high speed mode
    //GPIOA_OSPEEDR.OSPEEDR0 = 0b11;
    // PA1 analog mode, used for D-
    //GPIOA_MODER.MODER1 = 1;
    // Set D- to pull up, to indicate low speed device
    //GPIOA_PUPDR.PUPDR1 = 0b1;
}

void gpio_init_output()
{
    // PA0 output mode, used for D+
    GPIOA_MODER.MODER0 = 1;
    // PA0 high speed mode
    //GPIOA_OSPEEDR.OSPEEDR0 = 0b11;
    // PA1 analog mode, used for D-
    GPIOA_MODER.MODER1 = 1;
    // Set D- to pull up, to indicate low speed device
    GPIOA_PUPDR.PUPDR1 = 0b1;
    //GPIOA_PUPDR.PUPDR0 = 2;
}

void gpio_init_input()
{
    // PA0 output mode, used for D+
    GPIOA_MODER.MODER0 = 0;
    // PA0 high speed mode
    //GPIOA_OSPEEDR.OSPEEDR0 = 0b11;
    // PA1 analog mode, used for D-
    GPIOA_MODER.MODER1 = 0;
    // Set D- to pull up, to indicate low speed device
    GPIOA_PUPDR.PUPDR1 = 0b1;
    GPIOA_PUPDR.PUPDR0 = 2;
}

void led_init()
{
    RCC_AHBENR.IOPBEN = 0x1;
    GPIOB_MODER.MODER3 = 0x1;
}

void led_on()
{
    GPIOB_ODR.ODR3 = 0x1;
}

void led_off()
{
    GPIOB_ODR.ODR3 = 0x0;
}

void led_toggle()
{
    GPIOB_ODR.ODR3 ^= 0x1;
}

/*
Hardware interrupt selection
To configure a line as interrupt source, use the following procedure:
• Configure the corresponding mask bit in the EXTI_IMR register.
• Configure the Trigger Selection bits of the Interrupt line (EXTI_RTSR and EXTI_FTSR)
• Configure the enable and mask bits that control the NVIC IRQ channel mapped to the
EXTI so that an interrupt coming from one of the EXTI line can be correctly
acknowledged.
Hardware event selection
To configure a line as event source, use the following procedure:
• Configure the corresponding mask bit in the EXTI_EMR register.
• Configure the Trigger Selection bits of the Event line (EXTI_RTSR and EXTI_FTSR)
*/
void EXTI0_init()
{
    // enable interrupt on line 0 
    EXTI_IMR1.MR0 = 0x1;

    SYSCFG_COMP_OPAMP_SYSCFG_EXTICR1.EXTI0 = 0x0;

    // enable rising trigger for line 0
    EXTI_RTSR1.TR0 = 0x1;

    // enable IRQ
    NVIC_ISER0.SETENA = 0x1 << 6;
 }

// SHOULD NOT BE USED
void systick_init(int ms)
{
    STK_CTRL.TICKINT = 1;
    STK_CTRL.ENABLE = 1;
}

__attribute__((interrupt))
void systick_handler()
{
    led_toggle();
}
