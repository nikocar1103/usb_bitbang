.syntax unified
.cpu cortex-m4
.thumb

.data
ACK: .byte 0x80, 0b11010010

.align
.type gpio_send_ack, %function
.global gpio_send_ack

@ This function assumes clock speed of exactly 60Mhz
gpio_send_ack:
    push {r0-r11}
    //add r8, sp, #0
    
    /* register assignments */
    @ r0 holds pointer to data buffer
    @ r1 holds size of data buffer
    @ r2 holds GPIOA data register address
    @ r3 holds data
    @ r4 holds current state // 1 = K, 2 = J
    @ r5 holds bit mask
    @ r6 holds data byte counter 
    @ r7 holds bit stuff counter 
.align 
    mov.w r2, #0x48000000
    ldr r3, [r2]
    movs r1, #1
    bfi r3, r1, #0, #2
    bfi r3, r1, #2, #2
    str r3, [r2]
.align
    adr r0, ACK
    mov r1, 2
    ldr r2, =0x48000014 
    mov r4, #2
    mov r5, #1
    mov r6, #0
    mov r7, #-1
.align
    bit_loop:
        // no data to send, go to eop
        cmp r6, r1
        beq send_eop

        ldrb r3, [r0, r6]

        and r3, r3, r5  // extract the masked bit

        // if bit is 1, we don't have to do anything
        // if bit is 0, we have to change state
        cmp r3, #0
        ittee eq
        // r3 == 0
        eoreq r4, #3
        moveq r7, #0
        // else
        addne r7, #1 
        eorne r2, #0

        // send the bit
        strb r4, [r2]

        // bit stuff
        cmp r7, #6
        ittee eq
        eoreq r4, #3    // flip the state
        moveq r7, #-1   // reset stuff counter

        eorne r4, #0    // waste time
        lslne r5, #1    // shift the mask

        nop
        nop
        nop
        nop
        nop
        nop
        
        cmp r5, #0b100000000
        // byte was sent
        ittee eq
        addeq r6, #1
        moveq r5, #1
        
        addne r6, #0    // waste time
        movne r5, r5    // waste time

        b bit_loop
        
send_eop:   
    mov r4, #0

    nop
    nop
    nop
    nop
    nop
    nop

    strb r4, [r2]
 
    mov r5, #13
    eop_wait_loop:
        subs r5, #1
        bne eop_wait_loop
    nop
    mov r4, #2
    str r4, [r2]

end:
    mov.w r2, #0x48000000
    ldr r3, [r2]
    bfc r3, #0, #2
    bfc r3, #2, #2
    str r3, [r2]
    nop
    //mov sp, r8
    pop {r0-r11}
    bx lr

