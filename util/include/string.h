#ifdef __TSTRING__H__
    #define __TSTRING_H__

void tstrncpy(unsigned char *, unsigned char *, unsigned int );
void tmemnset(unsigned char *p_dst, unsigned char value, unsigned int n);

#endif //__TSTRING_H__
