#include <string.h>

void tstrncpy(unsigned char *p_src, unsigned char *p_dest, unsigned int n)
{
    while(n--)
    {
        if(*p_src == '\0')
            return;
        *p_dest++ = *p_src++;
    }
}

void tmemnset(unsigned char *p_dst, unsigned char value, unsigned int n)
{
    while(n--)
    {
        *p_dst ++ = value;
    }
}
