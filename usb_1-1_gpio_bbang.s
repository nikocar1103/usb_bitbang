.syntax unified
.cpu cortex-m4
.thumb


// data
.data
.align

// USER DATA
.global usb_packet_s


DATAX_PREV:   .byte 0 // previous data PID (DATA0 / DATA1) 
// we need to save state of the entire transaction.
// meaning control packet we received, and optionally, 
// data packet that followed
.align
TRANSACTION_CONTROL:    .byte 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
.align
TRANSACTION_DATA:       .byte 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
.align

DEVICE_DESCRIPTOR_1: .byte 12, 0x80, 0x00, 0x12, 0x01, 0x00, 0x02, 0x00, 0x00, 0x00, 0x08, 0x57, 0xe7
DEVICE_DESCRIPTOR_2: .byte 12, 0x80, 0x00, 0x61, 0x04, 0x03, 0x4d, 0x00, 0x01, 0x00, 0x02, 0xc1, 0xec
DEVICE_DESCRIPTOR_3: .byte 6, 0x80, 0x00, 0x00, 0x01, 0x3f, 0x8f
DEVICE_DESCRIPTOR_OFFSET: .byte 0
.align

CONFIGURATION_DESCRIPTOR_1: .byte 12, 0x80, 0x00, 0x09, 0x02, 0x22, 0x0, 0x1, 0x1, 0x0, 0x00 , 0x0a, 0xe0
CONFIGURATION_DESCRIPTOR_2: .byte 5, 0x80, 0x00, 0x32, 0xc1, 0x6a
CONFIGURATION_DESCRIPTOR_OFFSET: .byte 0

.align
FULL_DESCRIPTOR:  .byte 12, 0x80, 0x00, 0x09, 0x02, 0x22, 0x0, 0x1, 0x1, 0x0, 0x00, 0x0a, 0xe0, 12, 0x80, 0x00, 0x32, 0x09, 0x04, 0x00, 0x00, 0x01, 0x03, 0x01, 0x35, 0x4d,  12, 0x80, 0x00, 0x02, 0x00, 0x09, 0x21, 0x00, 0x01, 0x00, 0x01, 0x12, 0xb3,  12, 0x80, 0x00, 0x22, 0x34, 0x00, 0x07, 0x05, 0x81, 0x03, 0x04, 0xad, 0xe1,  6, 0x80, 0x00, 0x00, 0x0a, 0x7e, 0x48
FULL_DESCRIPTOR_OFFSET: .byte 0

INTERFACE_DESCRIPTOR:   .byte 12, 0x80, 0x00, 0x05, 0x01, 0x09, 0x02, 0xa1, 0x01, 0x09, 0x01, 0xa3, 0xfe, 12, 0x80, 0x00, 0xa1, 0x00, 0x05, 0x09, 0x19, 0x01, 0x29, 0x03, 0xa1, 0x19, 12, 0x80, 0x00, 0x15, 0x00, 0x25, 0x01, 0x95, 0x03, 0x75, 0x01, 0x7e, 0xae, 12, 0x80, 0x00, 0x81, 0x02, 0x95, 0x01, 0x75, 0x05, 0x81, 0x01, 0xde, 0xc0, 12, 0x80, 0x00, 0x05, 0x01, 0x09, 0x30, 0x09, 0x31, 0x09, 0x38, 0x7a, 0x47, 12, 0x80, 0x00, 0x15, 0x81, 0x25, 0x7f, 0x75, 0x08, 0x95, 0x03, 0x49, 0xaf, 8, 0x80, 0x00, 0x81, 0x06, 0xc0, 0xc0, 0x67, 0xb6
INTERFACE_DESCRIPTOR_OFFSET: .byte 0

.align

NAK_HEADER: .byte 0x80, 0b01011010
ACK_HEADER: .byte 0x80, 0b11010010
NULL_DATA:  .byte 0x80, 0x00, 0x00, 0x00


PACKET_BUFFER: .space 12

.align 4
// functions

.type EXTI0_handler, %function
.global EXTI0_handler
.type gpio_send_packet, %function
.global gpio_send_packet

@ This function assumes clock speed of exactly 60Mhz
EXTI0_handler:
     
    push {r0-r10, lr}

    // need to waste some time here
    //mov r1, #0
    //mov r1, #1
    mov r1, #2
    mov r1, #3
    mov r1, #4

EXTI0_hanlder_start:
    /* register assignments */
    @ r0 holds output buffer address
    @ r1 holds temporary read data
    @ r2 holds GPIOA input data register address
    @ r3 holds data
    @ r4 holds current state
    @ r5 and r6 hold line data
    @ r7 holds bit stuff counter
    @ r8 holds write bit mask
    @ r9 holds write byte index

    ldr r0, =PACKET_BUFFER
    mov r1, #0
    ldr r2, =0x48000010

    // D+ is low during idle, starting state is therefore 0
    // we only need to look at 1 line for data
    // but look at both lines for SE0 (EOP)
    mov r4, #1
    mov r7, #0
    // we start reading at bit 1
    mov r8, #2
    mov r9, #0

    EXTI0_main_loop:
        ldrb r3, [r2]
        // extract the bits
        and r5, r3, #1      // D+
        and r6, r3, #2      // D-
        // if the lines are equal, we assume SE0
        cmp r5, r6
        beq EXTI0_handler_end
        cmp r5, r4
        ittee eq
        // if state is same, bit is 1
        addeq r1, r8        // add state to current data
        addeq r7, #1        // increment bit stuff counter

        addne r1, #0        // waste time
        movne r7, #0        // reset bit stuff counter
        
        // check bit stuff counter
        /*
        cmp r7, #6
        ittee eq
        addeq r7, #0 
        moveq r4, r4        // update the state

        lslne r8, #1
        movne r4, r5
        */
        lsl r8, #1
        mov r4, r5

        // we're ignoring bit stuffs. That is not good.
        nop
        nop
        nop

        
        cmp r8, #0b100000000
        ittee eq
        strbeq r1, [r0], #1
        addeq r9, #1

        addne r9, #0
        strbne r1, [r0]
        
        cmp r8, #0b100000000
        ittee eq
        moveq r8, #1
        moveq r1, #0

        movne r8, r8
        movne r1, r1
        
        b EXTI0_main_loop

    EXTI0_handler_end:

    ldr r0, =PACKET_BUFFER
    ldrb r1, [r0, #1]
    // if PID == SETUP, save it and go back to main
    cmp r1, #0x2d
    beq pid_setup
    // if PID == DATA0, check if previous PID was SETUP
    // if so, send ACK packet
    cmp r1, #0b11000011 // 0xc3
    beq pid_data
    cmp r1, #0b01001011
    beq pid_data
    // if PID == IN, we have to send data
    cmp r1, #0b01101001 // 0x69
    beq pid_in
    // if PID == OUT, just save it and exit 
    cmp r1, #0b11100001
    beq pid_out
    //ldr r2, =TRANSACTION_CONTROL
    //str r1, [r2]
    

    EXTI0_exit:
        // clear EXTI0 pending bit
        ldr r1, =0x40010414
        movs r3, #1
        str r3, [r1]
        pop {r0-r10, lr}
        bx lr
       
    pid_setup:
        ldr r2, =TRANSACTION_CONTROL
        // save control PID
        // we don't have to save the rest of the packet
        // like addr and endp since we assume the packet
        // as always directed to us and we have only 1
        // endpoint
        str r1, [r2] 
        b EXTI0_exit

    pid_data:
        ldr r2, =TRANSACTION_DATA
        // copy data into previous data buffer
        // r0 holds packet buffer
        // there are at most 12 bytes to copy 
        
        ldr r0, =PACKET_BUFFER
        ldr.w r1, [r0], #4
        str.w r1, [r2], #4
        ldr.w r1, [r0], #4
        str.w r1, [r2], #4
        ldr.w r1, [r0]
        str.w r1, [r2]
        // we also need to save the current data PID
        ldr r0, =DATAX_PREV
        sub r2, #7
        ldr r3, [r2]
        strb r3, [r0]

        /*
        ldr r2, =TRANSACTION_CONTROL
        ldr r3, [r2]
        ldr r0, =ACK_HEADER
        mov r1, #2
        cmp r3, #0x2d
        it eq
        bleq gpio_send_packet
        */
        // acknowledge the reading of the packet
        ldr r0, =ACK_HEADER
        mov r1, #2
        bl gpio_send_packet
        b EXTI0_exit

    pid_in:
        // we have to read the TRANSACTION_DATA packet
        // and figure out what descriptor the host is requesting
        // we also have to figure out what exact packet of
        // the descriptor we are currently on.
        // for now, we are ignoring ack's and pretend 
        // everything is sent correctly.
        
        // if previous transaction has finished and we get
        // an IN request, the host is requesting our data!
        // Jolly good!
        ldr r0, =TRANSACTION_CONTROL
        ldrb r1, [r0]
        cmp r1, #0
        beq send_user_data

        ldr r0, =TRANSACTION_DATA
        add r0, #2 
        ldrb r1, [r0]    // 1st byte of data
        /*
        D7 Data Phase Transfer Direction
        0 = Host to Device
        1 = Device to Host
        D6..5 Type
        0 = Standard
        1 = Class
        2 = Vendor
        3 = Reserved
        D4..0 Recipient
        0 = Device
        1 = Interface
        2 = Endpoint
        3 = Other
        4..31 = Reserved
        */
        cmp r1, #0b10000000  // if this bit 1 is, descriptor is requested
        beq pid_in_request_descriptor

        cmp r1, #0b10000001
        beq pid_in_interface
        
        // otherwise, we need to look at the next byte
        ldrb r1, [r0, #1]
        cmp r1, #0x5   // SET_ADDRESS
        beq pid_in_set_address

        cmp r1, #0x9   // SET_CONFIGURATION
        beq pid_in_set_configuration

        cmp r1, #0x0a
        beq pid_in_get_interface

        pid_in_set_address:
            // send null data
            ldr r0, =NULL_DATA
            mov r1, #4
            ldr r3, =DATAX_PREV
            ldr r2, [r3]
            cmp r2, #0b11000011
            ite eq
                moveq r2, #0b01001011 
                movne r2, #0b11000011 
            strb r2, [r3]
            strb r2, [r0, #1]
            bl gpio_send_packet
            b EXTI0_exit

        pid_in_request_descriptor:
        
            // we need to figure out what kind of descriptor
            // the host is requesting
            // 1 = device descriptor
            // 2 = configuration descriptor
            // 3 = string descriptor, optional
            ldrb r1, [r0, #3]

            cmp r1, #0x01
            beq pid_in_device_descriptor
            cmp r1, #0x02
            beq pid_in_configuration_descriptor
            cmp r1, #0x03
            // we do not support string descriptor right now, 
            // so we can send empty data packet
            //beq pid_in_set_address
            beq pid_in_string_descriptor 

            //tst r1, #1  // requesting device descriptor
            //beq pid_in_device_descriptor

        pid_in_device_descriptor:

            ldr r0, =DEVICE_DESCRIPTOR_1
            
            ldr r2, =DEVICE_DESCRIPTOR_OFFSET
            ldrb r3, [r2]
            
            add r0, r3
            add r3, #13
            strb r3, [r2]
            
            ldrb r1, [r0] // lenght of packet
            add r0, #1
            // now we have to figure out number of datax PID
            ldr r3, =DATAX_PREV
            ldr r2, [r3]
            cmp r2, #0b11000011
            ite eq
                moveq r2, #0b01001011 
                movne r2, #0b11000011 
            strb r2, [r3]
            strb r2, [r0, #1]
            
            // finally, we check the descriptor offset.
            // if we sent the entire descriptor, we can reset it.
            ldr r2, =DEVICE_DESCRIPTOR_OFFSET
            ldrb r3, [r2]
            cmp r3, #39
            itt eq
                moveq r3, #0
                strbeq r3, [r2]

            bl gpio_send_packet
            b EXTI0_exit

        pid_in_configuration_descriptor:
            // there are 2 scenarios for this.
            // first, the host demands only the configuration descriptor,
            // with expected size of data 9.
            // Secondly, the host will request 22 bytes of data
            // starting with configuration descriptor, down to endp
            // descriptor.
            // since r0 holds pointer to data, we can simply read the
            // size that the host expects
            ldrb r1, [r0, #6]
            cmp r1, #0x22   // full descriptor family expected

            beq pid_in_full_descriptor

            ldr r0, =CONFIGURATION_DESCRIPTOR_1
            ldr r2, =CONFIGURATION_DESCRIPTOR_OFFSET
            ldrb r3, [r2]
            
            add r0, r3
            add r3, #13
            strb r3, [r2]
            
            ldrb r1, [r0] // lenght of packet
            add r0, #1
            // now we have to figure out number of datax PID
            ldr r3, =DATAX_PREV
            ldr r2, [r3]
            cmp r2, #0b11000011
            ite eq
                moveq r2, #0b01001011 
                movne r2, #0b11000011 
            strb r2, [r3]
            strb r2, [r0, #1]
            
            // finally, we check the descriptor offset.
            // if we sent the entire descriptor, we can reset it.
            ldr r2, =DEVICE_DESCRIPTOR_OFFSET
            ldrb r3, [r2]
            cmp r3, #39
            itt eq
                moveq r3, #0
                strbeq r3, [r2]

            bl gpio_send_packet
            b EXTI0_exit

        pid_in_full_descriptor:
            ldr r0, =FULL_DESCRIPTOR
            ldr r2, =FULL_DESCRIPTOR_OFFSET
            ldrb r3, [r2]
            add r0, r3
            add r3, #13
            strb r3, [r2]
            
            ldrb r1, [r0] // length of packet
            add r0, #1
            // now we have to figure out number of datax PID
            ldr r3, =DATAX_PREV
            ldr r2, [r3]
            cmp r2, #0b11000011
            ite eq
                moveq r2, #0b01001011 
                movne r2, #0b11000011 
            strb r2, [r3]
            strb r2, [r0, #1]
            
            // finally, we check the descriptor offset.
            // if we sent the entire descriptor, we can reset it.
            ldr r2, =FULL_DESCRIPTOR_OFFSET
            ldrb r3, [r2]
            cmp r3, #60
            itt eq
                moveq r3, #0
                strbeq r3, [r2]

            bl gpio_send_packet
            b EXTI0_exit


        pid_in_string_descriptor:
            ldr r0, =NULL_DATA
            mov r1, #4
            ldr r3, =DATAX_PREV
            ldr r2, [r3]
            cmp r2, #0b11000011
            ite eq
                moveq r2, #0b01001011 
                movne r2, #0b11000011 
            strb r2, [r3]
            strb r2, [r0, #1]

            bl gpio_send_packet
            b EXTI0_exit

        pid_in_set_configuration:
            ldr r0, =NULL_DATA
            mov r1, #4
            ldr r3, =DATAX_PREV
            ldr r2, [r3]
            cmp r2, #0b11000011
            ite eq
                moveq r2, #0b01001011 
                movne r2, #0b11000011 
            strb r2, [r3]
            strb r2, [r0, #1]

            bl gpio_send_packet
            b EXTI0_exit

        pid_in_get_interface:
            ldr r0, =NULL_DATA
            mov r1, #4
            ldr r3, =DATAX_PREV
            ldr r2, [r3]
            cmp r2, #0b11000011
            ite eq
                moveq r2, #0b01001011 
                movne r2, #0b11000011 
            strb r2, [r3]
            strb r2, [r0, #1]

            bl gpio_send_packet
            b EXTI0_exit

        pid_in_interface:
            ldrb r1, [r0, #1]
            cmp r1, #0x06   // interface descriptor
            beq pid_in_interface_descriptor
            b EXTI0_exit

        pid_in_interface_descriptor:
            ldr r0, =INTERFACE_DESCRIPTOR
            ldr r2, =INTERFACE_DESCRIPTOR_OFFSET
            ldrb r3, [r2]
            add r0, r3
            add r3, #13
            strb r3, [r2]
            
            ldrb r1, [r0] // length of packet
            add r0, #1
            // now we have to figure out number of datax PID
            ldr r3, =DATAX_PREV
            ldr r2, [r3]
            cmp r2, #0b11000011
            ite eq
                moveq r2, #0b01001011 
                movne r2, #0b11000011 
            strb r2, [r3]
            strb r2, [r0, #1]
            
            // finally, we check the descriptor offset.
            // if we sent the entire descriptor, we can reset it.
            ldr r2, =INTERFACE_DESCRIPTOR_OFFSET
            ldrb r3, [r2]
            cmp r3, #84
            itt eq
                moveq r3, #0
                strbeq r3, [r2]

            bl gpio_send_packet
            b EXTI0_exit


        pid_out:
            // set saved PID to 0.
            // we can do this because the transaction is over,
            // and any subsequent transaction will set a new PID.
            // if PID is 0 at IN stage, we know we can send bulk data.
            ldr r0, =TRANSACTION_CONTROL
            mov r1, #0
            strb r1, [r0]
            b EXTI0_exit

        send_user_data:
            ldr r0, =usb_packet_s
            // check if data is ready
            ldrb r1, [r0]
            cmp r1, #0
            // if not, send nak
            beq send_nak 

            // data is ready
            // figure out # of DATAX
            ldrb r1, [r0, #1] // data_len

            ldr r3, =DATAX_PREV
            ldr r2, [r3]
            cmp r2, #0b11000011
            ite eq
                moveq r2, #0b01001011 
                movne r2, #0b11000011 
            strb r2, [r3]
            strb r2, [r0, #3]

            add r0, #2  // move pointer to data buffer
            bl gpio_send_packet
            b EXTI0_exit

            send_nak:
                ldr r0, =NAK_HEADER
                mov r1, #2
                bl gpio_send_packet
                b EXTI0_exit



@ This function assumes clock speed of exactly 60Mhz
gpio_send_packet:
    push {r0-r11}
    add r8, sp, #0
    
    /* register assignments */
    @ r0 holds pointer to data buffer
    @ r1 holds size of data buffer
    @ r2 holds GPIOA data register address
    @ r3 holds data
    @ r4 holds current state // 1 = K, 2 = J
    @ r5 holds bit mask
    @ r6 holds data byte counter 
    @ r7 holds bit stuff counter 
.align 
    mov.w r2, #0x48000000
    ldr r3, [r2]
    movs r4, #1
    bfi r3, r4, #0, #2
    bfi r3, r4, #2, #2
    str r3, [r2]
.align
 
    ldr r2, =0x48000014 
    mov r4, #2
    mov r5, #1
    mov r6, #0
    mov r7, #-1
.align

    gpio_send_packet_main_loop:
        // no data to send, go to eop
        cmp r6, r1
        beq gpio_send_packet_send_eop
        
        ldrb r3, [r0, r6]
        and r3, r3, r5  // extract the masked bit

        // if bit is 1, we don't have to do anything
        // if bit is 0, we have to change state
        cmp r3, #0
        ittee eq
        // r3 == 0
        eoreq r4, #3
        moveq r7, #0
        // else
        addne r7, #1 
        eorne r2, #0
        
        // send the bit
        strb r4, [r2]

        // bit stuff
        cmp r7, #6
        ittee eq
        eoreq r4, #3    // flip the state
        moveq r7, #-1   // reset stuff counter

        eorne r4, #0    // waste time
        lslne r5, #1    // shift the mask
        nop
        nop
        nop
        nop
        nop
        cmp r5, #0b100000000
        // byte was sent
        ittee eq
        addeq r6, #1
        moveq r5, #1
        
        addne r6, #0    // waste time
        movne r5, r5    // waste time

        b gpio_send_packet_main_loop
        
gpio_send_packet_send_eop:   
    mov r4, #0

    nop
    nop
    nop
    nop
    nop
    nop

    strb r4, [r2]
 
    mov r5, #13
    gpio_send_packet_eop_wait_loop:
        subs r5, #1
        bne gpio_send_packet_eop_wait_loop
    nop
    mov r4, #2
    str r4, [r2]

end:
    mov.w r2, #0x48000000
    ldr r3, [r2]
    bfc r3, #0, #2
    bfc r3, #2, #2
    str r3, [r2]

    nop
    mov sp, r8
    pop {r0-r11}
    bx lr


