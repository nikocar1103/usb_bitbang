# Edit here
NAME = bitusb
SOURCEDIRS = . driver device util
INCLUDEDIRS = . include device/include util/include
EXECUTABLE = $(NAME).elf
BUILDDIR = build
FLASHADDR = 0x8000000

# Compiler
CC = arm-none-eabi-gcc 
ASM = arm-none-eabi-as
OBJCP = arm-none-eabi-objcopy
OPT = -O0
MAPFILE = $(NAME).map
IGNWARN = -Wno-main
LFLAGS = -g -mcpu=cortex-m4 -mthumb -Wall --specs=nosys.specs -Xlinker -Map=$(MAPFILE) -nostartfiles -nostdlib -nodefaultlibs -fno-builtin -fno-common -lgcc
CFLAGS = -c -g $(OPT) $(IGNWARN) -mcpu=cortex-m4 -mthumb -Wall -Wno-pointer-sign -nostdlib -nodefaultlibs -fno-builtin -fno-common
ASMFLAGS = -mcpu=cortex-m4 -mthumb
###########

C_FILES := $(foreach DIR, $(SOURCEDIRS), $(wildcard $(DIR)/*.c))
ASM_FILES := $(foreach DIR, $(SOURCEDIRS), $(wildcard $(DIR)/*.s))
LD_FILE := $(foreach DIR, $(SOURCEDIRS), $(wildcard $(DIR)/*.ld))
#INCLUDES := $(foreach DIR, $(INCLUDEDIRS), $(wildcard $(DIR)/*.h)) 
INCDIRS := $(foreach DIR, $(INCLUDEDIRS), $(addprefix -I, $(DIR)))

OBJECTS := $(C_FILES:.c=.o)
OBJECTS += $(ASM_FILES:.s=.o)

all: $(BUILDDIR)/$(EXECUTABLE)
	@echo "Project compiled" 

$(BUILDDIR)/$(EXECUTABLE): $(OBJECTS)
	$(CC) $(LFLAGS) -T$(LD_FILE) $^ -o $@

%.o: %.c
	@mkdir -p $(BUILDDIR)
	$(CC) $(CFLAGS) $(INCDIRS) $< -o $@

%.o: %.s
	$(ASM) $(ASMFLAGS) $< -o $@

bin: all
	$(OBJCP) -O binary $(BUILDDIR)/$(EXECUTABLE) $(BUILDDIR)/$(NAME).bin

flash: bin 
	st-flash write $(BUILDDIR)/$(NAME).bin $(FLASHADDR)

ocd:
	openocd -f /usr/local/share/openocd/scripts/interface/stlink.cfg -f /usr/local/share/openocd/scripts/board/st_nucleo_f3.cfg

gdb:
	gdb-multiarch -ex "target remote :3333" build/$(EXECUTABLE)

erase:
	st-flash erase

clean:
	rm -f $(foreach DIR, $(SOURCEDIRS), $(addprefix $(DIR)/, *.o)) *.map $(BUILDDIR)/*.bin $(BUILDDIR)/*.elf

purge: clean
	rm -f build/$(EXECUTABLE)

help:
	@echo "make clean\t\tdelete object files"\
		  "\nmake purge\t\tdelete all build files"\
		  "\nmake bin\t\tobjdump executable to bin"\
		  "\nmake flash\t\tflash the stm device"\
		  "\nmake ocd\t\tstart openocd"\
		  "\nmake gdb\t\tstart gdb"\
		  "\nmake erase\t\tearse the chip"\
		  "\nmake help\t\tdisplay this page"

.PHONY: all clean purge bin flash help ocd gdb erase

