#include <stm32f303.h>
#include <bitusb.h>
#include <crc16.h>


void usb_packet_pop(usb_packet *pack, unsigned char *data, unsigned int n)
{
    pack->data_buff[0] = 0x80;
    pack->data_buff[1] = 0x00; // we don't know PID yet

    unsigned char *p = pack->data_buff + 2;
    unsigned int i = n;

    // Copy data into buffer
    while(i--)
    {
        *p++ = *data++; 
    }

    pack->data_len = n + 2;

    if(n > 0)
    {
        pack->data_len += 2;
        
        // Calculate the crc
        unsigned short crc = crc16(0xffff, pack->data_buff + 2, n);
        pack->data_buff[n + 2] = (unsigned char) crc & 0xff;
        pack->data_buff[n + 3] = (unsigned char) (crc >> 8) & 0xff;
    }
    pack->data_ready = 1;
}

