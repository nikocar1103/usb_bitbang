#ifndef __BITUSB_H__
    #define __BITUSB_H__

typedef struct 
{
    // data buffer
    // first 2 bytes are preamble and PID
    // the last 2 bytes are reserved for crc
    unsigned char data_ready;
    unsigned char data_len;
    unsigned char data_buff[8 + 4];
} usb_packet;

/*
typedef struct
{
    char bLength;
    char bDescriptorType;
    short bcdUSB;
    char bDeviceClass;
    char bDeviceSubClass;
    char bDeviceProtocol;
    char bMaxPacketSize0;
    short idVendor;
    short idProduct;
    short bcdDevice;
    char iManufacturer;
    char iProduct;
    char iSerialNumber;
    char bNumConfigurations;
} usb_device_descriptor;

typedef struct
{
    char bLength;
    char bDescriptorType;
    short wTotalLength;
    char bNumberInterfaces;
    char bConfigurationValue;
    char iConfiguration;
    char bmAttributes;
    char bMaxPower;
} usb_configuration_descriptor;

typedef struct
{
    char bLength,
    char bDescriptorType,
    char bInterfaceNumber,
    char bAlternateSetting,
    char bNumEndpoints,
    char bInterfaceClass,
    char bInterfaceSubclass,
    char bInterfaceProtocol,
    char iInterface;
} usb_interface_descriptor;

typedef struct
{
    char bLength;
    char bDescriptorType;
    char bEndpointAddress;
    char bmAttributes;
    short wMaxPacketSize;
    char bInterval;
} usb_endpoint_descriptor;
*/

void usb_packet_pop(usb_packet *pack, unsigned char *data, unsigned int n);

#endif
