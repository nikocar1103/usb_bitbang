#include <stm32f303.h>
#include <bitusb.h>
#include <util.h>
#include <crc16.h>


char DATA_BUFFER_L[8] = {0x00, 0x1, 0x00, 0x00};
char DATA_BUFFER_R[8] = {0x00, 0xff, 0x00, 0x00};

usb_packet usb_packet_s;

__attribute__((noreturn))
void main()
{

    clock_init_60mhz();
    //led_init();    

    gpio_init();
    gpio_init_input(); 
    EXTI0_init();

    while(1)
    {    
        usb_packet_pop(&usb_packet_s, DATA_BUFFER_L, 4);
        delay(5000000);
        usb_packet_pop(&usb_packet_s, DATA_BUFFER_R, 4);
        delay(5000000);
    }
   
    for(;;); 
}
